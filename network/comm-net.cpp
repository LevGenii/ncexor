/*
 * CommNet.cpp
 *
 *  Created on: Dec 11, 2015
 *      Author: tsokalo
 */

#include <functional>
#include "comm-net.h"
#include "utils/nc-packet.h"

namespace ncr {

CommNet::CommNet(uint16_t numNodes) :
	m_gen(m_rd()) {
	m_simulator = simulator_ptr(new Simulator());

	m_nodes.resize(numNodes);
	std::vector<std::shared_ptr<CommNode> >::iterator it = m_nodes.begin();
	for (auto& i : m_nodes)
	{
		i = std::shared_ptr < CommNode > (new CommNode (std::distance (m_nodes.begin (), it++), m_simulator));
	};;

	m_src = -1;
	m_dst = -1;
}
CommNet::~CommNet() {

}
/*
 *  between the src and dst we create two edges; output edges have shared parts with input edges
 *  double e1 is the packet loss ratio on the edge (dst, src)
 *  double e2 is the packet loss ratio on the edge (src, dst)
 */
void CommNet::ConnectNodes(UanAddress src, UanAddress dst, double e1, double e2) {
	assert(m_nodes.size () > (uint16_t)src && m_nodes.size () > (uint16_t)dst && src != dst && m_nodes.at (src) && m_nodes.at (dst));

	std::shared_ptr<Edge> fromSrc = m_nodes.at(src)->CreateOutputEdge(dst, m_nodes.at(dst)->CreateInputEdge(src, e1));
	std::shared_ptr<Edge> fromDst = m_nodes.at(dst)->CreateOutputEdge(src, m_nodes.at(src)->CreateInputEdge(dst, e2));
	fromSrc->reverse_edge_ = fromDst;
	fromDst->reverse_edge_ = fromSrc;
}
void CommNet::ConnectNodesDirected(UanAddress src, UanAddress dst, double e) {
	assert(m_nodes.size () > (uint16_t)src && m_nodes.size () > (uint16_t)dst && src != dst && m_nodes.at (src) && m_nodes.at (dst));

	m_nodes.at(src)->CreateOutputEdge(dst, m_nodes.at(dst)->CreateInputEdge(src, e));
}
void CommNet::PrintNet() {
	for (auto i : m_nodes)
	{
		SIM_LOG(COMM_NET_LOG, ">> Node " << i->GetId ());
		i->PrintEdges ();
	};;
}
void CommNet::Run(int64_t cycles) {
	if (m_logger) {
		for (auto& i : m_nodes)
		{
			i->SetLogCallback(std::bind(&Logger::AddLog, m_logger, std::placeholders::_1, std::placeholders::_2));
		};;
	}

	while (cycles-- > 0) {
		SIM_LOG_FUNC(COMM_NET_LOG);
		DoBroadcast(SelectSender());
		m_simulator->Execute();
		if (m_logger) m_logger->IncTime();
	}
}
/*
 * only one source is possible
 */
void CommNet::SetSource(UanAddress i) {
	assert(m_nodes.size() > (uint16_t)i);
	assert(m_src == -1);
	m_src = i;
}
/*
 * only one destination is possible
 */
void CommNet::SetDestination(UanAddress i) {
	assert(m_nodes.size() > (uint16_t)i);
	assert(m_dst == -1);
	m_dst = i;

	for (uint16_t j = 0; j < m_nodes.size(); j++) {
		if (j == m_src) m_nodes.at(j)->Configure(SOURCE_NODE_TYPE, m_dst);
		else if (j == m_dst) m_nodes.at(j)->Configure(DESTINATION_NODE_TYPE, m_dst);
		else m_nodes.at(j)->Configure(RELAY_NODE_TYPE, m_dst);
	}
}
void CommNet::DoBroadcast(node_ptr sender) {

	NcPacket symb = sender->DoBroadcast();
}
void CommNet::EnableLog(std::string path) {
	m_logger = logger_ptr(new Logger(path));
	m_simulator->SetIncTimeCallback(std::bind(&Logger::IncTime, m_logger));
	m_simulator->SetMessTypeCallback(std::bind(&Logger::SetMessType, m_logger, std::placeholders::_1));
}
CommNet::node_ptr CommNet::SelectSender() {
	std::vector<uint16_t> v;
	for (std::vector<node_ptr>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++) {
		if ((*it)->GetId() == m_dst) continue;
		if ((*it)->DoIwannaSend()) v.push_back(std::distance(m_nodes.begin(), it));
	}
	assert(!v.empty());
	std::uniform_int_distribution<> dis(0, v.size() - 1);
	return m_nodes.at(v.at(dis(m_gen)));
}
}//ncr

