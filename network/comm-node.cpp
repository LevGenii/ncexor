/*
 * CommNode.cpp
 *
 *  Created on: Dec 11, 2015
 *      Author: tsokalo
 */

#include "comm-node.h"
#include "utils/utils.h"
#include "utils/nc-packet.h"
#include <chrono>

using namespace std::placeholders;

namespace ncr {

CommNode::CommNode(UanAddress id, simulator_ptr simulator) :
	m_id(id), m_distribution(0.0, 1.0) {

	typedef std::chrono::high_resolution_clock myclock;
	myclock::time_point beginning = myclock::now();
	myclock::duration d = myclock::now() - beginning;
	uint8_t seed_v = d.count() + (seed_corrector++);
	m_generator.seed(seed_v);

	m_simulator = simulator;
}
CommNode::~CommNode() {

}
void CommNode::SetLogCallback(add_log_func addLog) {
	m_brr->SetLogCallback(addLog);
}
void CommNode::Configure(NodeType type, UanAddress dst) {
	m_nodeType = type;
	m_numGen = 2;
	m_genSize = 100;
	m_symbolSize = 10;
	m_apiRate = 1000000;

	m_brr = routing_rules_ptr(new NcRoutingRules(m_id, m_nodeType, dst, m_numGen, m_apiRate));

	if (m_nodeType == SOURCE_NODE_TYPE) {
		m_encQueue = encoder_queue_ptr(new encoder_queue(m_numGen, m_genSize, m_symbolSize));
		m_encQueue->set_notify_callback(std::bind(&CommNode::NotifyGen, this, std::placeholders::_1));
		auto send_func = std::bind(&encoder_queue::enque, m_encQueue, std::placeholders::_1);
		m_trafGen = traf_gen_ptr(new TrafficGenerator(send_func, m_symbolSize, m_apiRate));
		m_brr->SetGetRankCallback(std::bind(&encoder_queue::rank, m_encQueue, std::placeholders::_1));

		m_trafGen->Start(m_genSize * m_numGen);

		SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " type " << m_nodeType);
	}
	else {
		m_decQueue = decoder_queue_ptr(new decoder_queue(m_numGen, m_genSize, m_symbolSize));
		m_brr->SetGetRankCallback(std::bind(&decoder_queue::rank, m_decQueue, std::placeholders::_1));

		SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " type " << m_nodeType);
	}
}

Edge_ptr CommNode::CreateInputEdge(int16_t src_id, double e) {
	assert(src_id != m_id);
	Edge_ptr edge = Edge_ptr(new Edge(src_id, std::bind(&CommNode::Receive, this, std::placeholders::_1, std::placeholders::_2)));
	edge->SetNotifyLoss(std::bind(&CommNode::NotifyLoss, this, std::placeholders::_1, std::placeholders::_2));
	edge->SetLossProcess(std::shared_ptr<BernoulliLossProcess>(new BernoulliLossProcess(e)));

	m_ins.push_back(edge);
	m_in_ids[edge->v_] = m_ins.size() - 1;
	return edge;
}
Edge_ptr CommNode::CreateOutputEdge(int16_t dst_id, Edge_ptr input) {
	assert(dst_id != m_id);

	Edge_ptr edge = Edge_ptr(new Edge(std::bind(&Edge::Receive, input, std::placeholders::_1), dst_id));
	edge->SetLossProcess(input->GetLossProcess());

	m_outs.push_back(edge);
	m_out_ids[edge->v_] = m_outs.size() - 1;
	return edge;
}

Edge_ptr CommNode::GetEdge(int16_t src_id, int16_t dst_id) {
	return (src_id == m_id) ? m_outs.at(m_out_ids[dst_id]) : m_ins.at(m_in_ids[src_id]);
}

void CommNode::PrintEdges() {
	if (COMM_NODE_LOG) {
		std::cout << "Ins: ";
		for (auto i : m_ins)
		std::cout << "(" << i->v_ << " -> " << m_id << " : " << i->GetLossProcess ()->GetMean () << " , "
		<< i->GetLossProcess ()->IsLost () << ") ";
		std::cout << std::endl;
		std::cout << "Out: ";
		for (auto i : m_outs)
		std::cout << "(" << i->v_ << " <- " << m_id << " : " << i->GetLossProcess ()->GetMean () << " , "
		<< i->GetLossProcess ()->IsLost () << ") ";
		std::cout << std::endl;
	}
}

UanAddress CommNode::GetId() {
	return m_id;
}

//
// let all nodes connected to output edges here the transmission
//
NcPacket CommNode::DoBroadcast() {

	SIM_LOG_FUNC(COMM_NODE_LOG);

	assert(m_nodeType != DESTINATION_NODE_TYPE);

	TxPlan plan = m_brr->GetTxPlan();
	assert(!plan.empty());

	auto accumulate = [](TxPlan plan)->uint32_t
	{
		uint32_t sum = 0;
		for(auto item : plan)
		{
			sum += item.second.num_all;
		}
		return sum;
	};;

	if (m_nodeType == SOURCE_NODE_TYPE) {
		if (accumulate(plan) <= m_genSize * (m_numGen - 1)) {
			m_trafGen->Start(m_genSize);
		}
	}

	auto item_it = plan.begin_orig_order();;

	assert(item_it != plan.end());

	GenId genId = item_it->first;
	TxPlanItem planItem = item_it->second;

	assert(planItem.num_all != 0);

	SIM_LOG(COMM_NODE_LOG, "Node " << m_id << ": for generation " << genId << " number of packets in plan: " << planItem.num_all);

	m_brr->UpdateSent(genId, 1);

	HeaderInfo header = m_brr->GetHeaderInfo(genId);
	header.genId = genId;

	NcPacket pkt;
	if (m_nodeType == SOURCE_NODE_TYPE) {
		pkt.SetData(m_encQueue->get_coded(genId));
	}
	else {
		pkt.SetData(m_decQueue->get_coded(genId));
	}
	pkt.SetHeader(header);

	for (auto &i : m_outs) m_simulator->Schedule(std::bind(&Edge::Transmit, i, std::placeholders::_1), pkt, (i->v_ == m_outs.at(0)->v_), DATA_MSG_TYPE);;
	return pkt;
}

//
// it will be automatically called with input edges when the transmission is triggered by any output edges
// of other nodes, when the input edge of the current node coincides with the output edge of the other node
//
void CommNode::Receive(Edge* input, NcPacket pkt) {

	SIM_LOG_FUNC(COMM_NODE_LOG);

	SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " receives from " << input->v_);

	m_brr->RcvHeaderInfo(pkt.GetHeader());

	if (!pkt.IsFeedbackSymbol()) {

		if (m_nodeType == SOURCE_NODE_TYPE) return;

		GenId genId = pkt.GetHeader().genId;
		SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " receives packet from generation " << genId);

		auto rank = m_decQueue->rank(genId);
		m_decQueue->enque(pkt.GetData(), genId);

		if (rank < m_decQueue->rank(genId)) {
			m_brr->UpdateRcvd(genId, input->v_, 1);
		}
		else {
			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << ", receiving linear dependent packet");
			m_brr->UpdateRcvd(genId, input->v_, 1, true);
		}

		if (m_brr->MaySendFeedback()) {
			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " sends the feedback");
			NcPacket feedback;
			feedback.SetHeader(m_brr->GetHeaderInfo());
			feedback.SetFeedback(m_brr->GetFeedbackInfo());
			assert(!m_outs.empty());
			for (auto i : m_outs) m_simulator->Schedule(std::bind(&Edge::Transmit, i, std::placeholders::_1), feedback, (i->v_ == m_outs.at(0)->v_), FEEDBACK_MSG_TYPE);;
		}
		else {
			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " refuses to send the feedback");
		}

		if (m_brr->MaySendNetDiscovery()) {
			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " sends the network discovery message with maximum TTL");
			NcPacket feedback;
			feedback.SetHeader(m_brr->GetHeaderInfo());
			feedback.SetFeedback(m_brr->GetNetDiscoveryInfo());
			assert(!m_outs.empty());
			for (auto i : m_outs) m_simulator->Schedule(std::bind(&Edge::Transmit, i, std::placeholders::_1), feedback, (i->v_ == m_outs.at(0)->v_), NETDISC_MSG_TYPE);;
		}
		else {
			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " refuses to send the network discovery message");
		}
	}
	else {
		SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " receive feedback symbol");
		m_brr->RcvFeedbackInfo(pkt.GetFeedback());

		if (pkt.GetFeedback().netDiscovery) {

			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " receive the network discovery message with TTL " << pkt.GetFeedback().ttl);

			if (pkt.GetFeedback().ttl != 0) {

				if (m_brr->MaySendNetDiscovery(pkt.GetFeedback().ttl)) {
					SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " sends the network discovery message with TTL " << pkt.GetFeedback().ttl - 1);
					NcPacket feedback;
					feedback.SetHeader(m_brr->GetHeaderInfo());
					feedback.SetFeedback(m_brr->GetNetDiscoveryInfo(pkt.GetFeedback().ttl - 1));
					for (auto i : m_outs) m_simulator->Schedule(std::bind(&Edge::Transmit, i, std::placeholders::_1), feedback, (i->v_ == m_outs.at(0)->v_), NETDISC_MSG_TYPE);;
				}
				else {
					SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " refuses to send the network discovery message");
				}
			}
			else {
				SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " TTL has expired");
			}
		}
		else {
			SIM_LOG(COMM_NODE_LOG, "Node " << m_id << ". Network discovery flag is not set");
		}
	}
}
bool CommNode::DoIwannaSend() {

	SIM_LOG_FUNC(COMM_NODE_LOG);

	TxPlan txPlan = m_brr->GetTxPlan();
	auto accumulate = [](TxPlan plan)->uint32_t
	{
		uint32_t sum = 0;
		for(auto item : plan)
		{
			sum += item.second.num_all;
		}
		return sum;
	};;

	return (accumulate(txPlan) > 0);
}

void CommNode::NotifyGen(GenId genId) {

	SIM_LOG_FUNC(COMM_NODE_LOG);

	m_brr->UpdateRcvd(genId, m_id, 1);
}
void CommNode::NotifyLoss(Edge * input, NcPacket pkt) {

	SIM_LOG_FUNC(COMM_NODE_LOG);

	SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " symbol loss is notified");

	if (!pkt.IsFeedbackSymbol()) {

		SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " loosing information symbol");
		m_brr->UpdateLoss(pkt.GetHeader().genId, input->v_, 1);
	}
	else {
		SIM_LOG(COMM_NODE_LOG, "Node " << m_id << " loosing feedback symbol - dropping information");
	}
}
}//ncr
