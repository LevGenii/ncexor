/*
 * header.h
 *
 *  Created on: Dec 11, 2015
 *      Author: tsokalo
 */

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <fstream>
#include <stdint.h>
#include <map>
#include <unordered_map>

#include "utils/filter-arithmetics.h"
#include "utils/priority.h"

namespace ncr {

/*
 * unit [bps]
 */
#define SMALLEST_API_DATA_RATE		1000
#define PRIORITY_HYSTERESIS_WIDTH 	(SMALLEST_API_DATA_RATE / 10)
#define PHW							PRIORITY_HYSTERESIS_WIDTH

typedef Priority<PHW> priority_t;

extern uint16_t seed_corrector;

/*************************************************************************************************/
/********************************************** ENUMS ********************************************/
/*************************************************************************************************/
enum CurrentSender {
	RELAY_TRANS, SOURCE_TRANS
};

enum Policy {
	WFF_POLICY, WFH_POLICY, ZH_POLICY, PLNCOOL_POLICY, TRY_K_POLICY, TRY_S_POLICY, ZH_REAL_POLICY, TRY_S_RNO_POLICY
};

enum WeightCoefs {
	NO_WEIGHT_COEFS, LIN_LAST_MOST_WEIGHT_COEFS, LIN_FIRST_MOST_WEIGHT_COEFS
};
enum NodeType {
	SOURCE_NODE_TYPE, RELAY_NODE_TYPE, DESTINATION_NODE_TYPE
};

enum NcPolicyType {
	ANTiCS_NC_POLICY_TYPE, SENDALWAYS_NC_POLICY_TYPE, PLAYNCOOL_NC_POLICY_TYPE, ANTiCS_E_NC_POLICY_TYPE
};

enum UniqueDofEstimation {
	NODE_ID_INFORMATION, SENDER_UNIQUE_DOF_ESTIMATION, RECEIVER_SUM_UNIQUE_DOF_ESTIMATION, RECEIVER_VEC_UNIQUE_DOF_ESTIMATION
};
enum GenerationState {
	FORWARDING_GENERATION_STATE, RETRANSMITTING_GENERATION_STATE
};
enum ProgMode {
	RUN_MODE, EVAL_MODE
};
enum MessType
{
	DATA_MSG_TYPE,
	FEEDBACK_MSG_TYPE,
	NETDISC_MSG_TYPE
};
/*************************************************************************************************/
/******************************************** STRUCTURES *****************************************/
/*************************************************************************************************/

struct MarkovState {
	uint16_t relayDofRel;
	uint16_t relayDof;
	uint16_t destDof;
	CurrentSender trans;
};
struct SentPkts {
	uint32_t relay;
	uint32_t source;
	uint32_t source_before_relay;
};

struct Losses {
	double e1;
	double e2;
	double e3;
};
struct TransProb {
	double Prdr;
	double Prds;
	double Psrr;
	double Psrs;
	double Psds;
	double Psdr;
	double Psnr;
	double Psns;
	double Prns;
	double Prnr;
};

struct NcStat {
	uint32_t lost;
	uint32_t l_rlnc;
	uint32_t l_copy;
};

struct Statistic {
	// <iteration>
	std::vector<uint32_t> uninterrupted;
	std::vector<uint32_t> copies;
	double aveUninterrupted;
	double destDofPerTrans;
	double copiesPerTrans;
	// <node id> <iteration>
	std::vector<std::vector<uint32_t> > uniqueRcvd;
	std::vector<std::vector<uint32_t> > totalSent;
	// <node id>
	std::vector<double> uniqueRcvdAve;
	std::vector<double> totalSentAve;
	std::vector<double> uniqueRcvdCalc;
	std::vector<double> totalSentCalc;
};
/*************************************************************************************************/
/********************************************* NEW STUFF *****************************************/
/*************************************************************************************************/
typedef std::vector<uint8_t> NcSymbol;
typedef NcSymbol OrigSymbol;
typedef int16_t UanAddress;
typedef int16_t EdgeId;
//typedef Ssn<uint16_t, 1000> GenId;
typedef uint16_t GenId;
/*
 * unit [bits per second]
 */
typedef double Datarate;

/*
 * The logging item for node v and time point t
 * All variables below are the estimation of the current node
 */

struct LogItem {

	LogItem() {
		cr = 1;
		d = 0;
		cs = 0;
		ns = 0;
		nr = 0;
	}
	LogItem(std::string init) {
		std::stringstream ss(init);

		int32_t v;
		ss >> v;
		ss >> v;
		ss >> v;
		std::string str;
		ss >> str;
		d = std::stod(str);
		str.clear();
		ss >> str;
		if (str == "DESTINATION_PRIORITY") p = DESTINATION_PRIORITY;
		else p = std::stod(str);
		ss >> cr;
		ss >> cs;
		ss >> ns;
		ss >> nr;
		uint16_t s_fp = 0, s_eps = 0;
		ss >> s_fp;
		ss >> s_eps;

		UanAddress iv = 0;
		double dv = 0;
		for (uint16_t i = 0; i < s_fp; i++) {
			ss >> iv;
			ss >> dv;
			fp[iv] = dv;
		}
		for (uint16_t i = 0; i < s_eps; i++) {
			ss >> iv;
			ss >> dv;
			eps[iv] = dv;
		}
	}

	LogItem&
	operator=(const LogItem& other) // copy assignment
	{
		if (this != &other) { // self-assignment check expected
			this->eps.clear();
			this->eps = other.eps;
			this->cr = other.cr;
			this->fp.clear();
			this->fp = other.fp;
			this->p = other.p;
			this->cs = other.cs;
			this->ns = other.ns;
			this->nr = other.nr;
		}
		return *this;
	}

	friend std::ostream&
	operator<<(std::ostream& os, const LogItem& l) {
		os << l.d << "\t" << l.p << "\t" << l.cr << "\t" << l.cs << "\t" << l.ns << "\t" << l.nr << "\t" << l.fp.size() << "\t" << l.eps.size();

		for (std::map<int16_t, double>::const_iterator it = l.fp.begin(); it != l.fp.end(); it++)
			os << "\t" << it->first << "\t" << it->second;
		for (std::map<int16_t, double>::const_iterator it = l.eps.begin(); it != l.eps.end(); it++)
			os << "\t" << it->first << "\t" << it->second;
		return os;
	}

	/*
	 * estimation of the expectation value of packet loss ratio for each out-coming edge e in O(v)
	 * int16_t - id of the v^+(e)
	 */
	std::map<int16_t, double> eps;
	/*
	 * coding rate = 1 / (1 + redundancy)
	 */
	double cr;
	/*
	 * filtering probability for each in-coming edge e in I(v)
	 * int16_t - id of the v^-(e)
	 */
	std::map<int16_t, double> fp;
	/*
	 * priority of v
	 */
	priority_t p;
	/*
	 * sending data rate
	 */
	Datarate d;

	/*
	 * size of the coalition
	 */
	uint16_t cs;
	/*
	 * number of sent packets per the transmission opportunity (!not the total amount)
	 */
	uint16_t ns;
	/*
	 * number linear independent packets received by the destination
	 */
	uint16_t nr;
};
/*
 * The history of logging items for node v
 * int64_t - time point t
 */
struct LogPair {
	LogPair() {
		t = 0;
	}
	LogPair(int64_t t, MessType m, LogItem log) {
		this->t = t;
		this->log = log;
		this->m = m;
	}
	int64_t t;
	MessType m;
	LogItem log;
};
typedef std::vector<LogPair> LogHistory;
/*
 * The history of logging items for all nodes v in V
 * int16_t - id of v
 */
typedef std::map<UanAddress, LogHistory> LogBank;

typedef double Dof;

typedef std::unordered_map<UanAddress, double> pf_t;
struct HeaderInfo {
	HeaderInfo& operator=(const HeaderInfo& other) // copy assignment
	{
		if (this != &other) { // self-assignment check expected
			this->addr = other.addr;
			this->p = other.p;
			this->genId = other.genId;
			this->pf = other.pf;
		}
		return *this;
	}
	/*
	 * sender address
	 */
	UanAddress addr;
	priority_t p;
	GenId genId;
	pf_t pf;
};
typedef std::vector<std::vector<uint8_t> > CodingMatrix;
struct DecoderDetails {
	// either
	uint16_t genSize;
	uint16_t rank;
	std::map<UanAddress, SeenMap> seenMaps;
	std::map<UanAddress, DecodedMap> decodedMaps;
	// or
	CodingMatrix codingCoefs;
};

struct RetransRequestInfo: public std::map<GenId, DecoderDetails> {

};

typedef int16_t ttl_t;

struct FeedbackInfo {

	FeedbackInfo() {
		netDiscovery = false;
		ttl = 0;
	}
	FeedbackInfo(const FeedbackInfo &other) {
		this->addr = other.addr;
		this->p = other.p;
		this->rcvMap = other.rcvMap;
		this->rrInfo = other.rrInfo;
		this->netDiscovery = other.netDiscovery;
		this->ttl = other.ttl;
	}

	FeedbackInfo& operator=(const FeedbackInfo& other) // copy assignment
	{
		if (this != &other) { // self-assignment check expected
			this->addr = other.addr;
			this->p = other.p;
			this->rcvMap = other.rcvMap;
			this->rrInfo = other.rrInfo;
			this->netDiscovery = other.netDiscovery;
			this->ttl = other.ttl;
		}
		return *this;
	}
	/*
	 * sender address
	 */
	UanAddress addr;
	priority_t p;
	/*
	 * Loss maps of input edges
	 */
	std::map<UanAddress, RcvMap> rcvMap;

	RetransRequestInfo rrInfo;

	/*
	 * network discovery flag
	 */
	bool netDiscovery;
	/*
	 * time to live
	 */
	ttl_t ttl;
};

struct NetDiscoveryInfo: public FeedbackInfo {
	NetDiscoveryInfo() {
		netDiscovery = true;
	}

	NetDiscoveryInfo(FeedbackInfo& other, ttl_t ttl)
	{
		this->addr = other.addr;
		this->p = other.p;
		this->rcvMap = other.rcvMap;
		this->rrInfo = other.rrInfo;
		this->netDiscovery = true;
		this->ttl = ttl;
	}
};

struct TxPlanItem {
	TxPlanItem() {
		num_all = 0;
	}
	TxPlanItem& operator=(const TxPlanItem& other) // copy assignment
	{
		if (this != &other) { // self-assignment check expected
			this->num_all = other.num_all;
			this->placement.clear();
			this->placement = other.placement;
		}
		return *this;
	}
	uint32_t num_all;
	std::vector<uint32_t> placement;
};

/*
 * rank of the coding matrix containing all the received symbols
 */
typedef std::function<uint32_t(GenId)> get_rank_func;
/*
 * rank of the coding matrix containing the received symbols only from the nodes with higher priority
 */
typedef std::function<uint32_t(GenId)> get_rank_high_func;
typedef std::function<void(LogItem item, UanAddress node_id)> add_log_func;

typedef std::map<uint16_t, double> TdmAccessPlan;

}//ncr
#endif /* HEADER_H_ */
