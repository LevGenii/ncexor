/*
 * nc-routing-rules.h
 *
 *  Created on: 02.10.2016
 *      Author: tsokalo
 */

#ifndef NCROUTINGRULES_H_
#define NCROUTINGRULES_H_

#include <map>
#include <deque>
#include <unordered_map>
#include <random>
#include <memory>

#include "utils/conn-id.h"
#include "utils/filter-arithmetics.h"
#include "header.h"
#include "utils/node-map.h"
#include "utils/special-map.h"

namespace ncr {

class NcRoutingRules {

	typedef std::function<CodingMatrix(GenId)> get_coding_matrix_func;
	typedef node_map_base_t::iterator node_map_it;

public:
	NcRoutingRules(UanAddress ownAddress, NodeType type, UanAddress destAddress, uint16_t numGen, Datarate apiRate);
	virtual ~NcRoutingRules();

	void
	SetLogCallback(add_log_func addLog);
	void
	SetGetRankCallback(get_rank_func c);
	void
	SetGetCodingMatrixCallback(get_coding_matrix_func c);

	/*
	 * INPUTS
	 */
	void RcvHeaderInfo(HeaderInfo l);
	void RcvFeedbackInfo(FeedbackInfo l);
	void UpdateSent(GenId genId, uint32_t num);
	void UpdateRcvd(GenId genId, UanAddress id, uint32_t num, bool linDep = false);
	void UpdateLoss(GenId genId, UanAddress id, uint32_t num);

	/*
	 * OUTPUTS
	 */
	TxPlan GetTxPlan();
	FeedbackInfo GetFeedbackInfo();
	HeaderInfo GetHeaderInfo(GenId genId = -1);
	NetDiscoveryInfo GetNetDiscoveryInfo(ttl_t ttl = -2);
	bool MaySendFeedback();
	bool MaySendNetDiscovery(ttl_t ttl = -1);

private:

	void UpdateCoalition();
	/*
	 * called both on reception and sending data to update the forwarding plan
	 */
	void DoFilter();
	void DoUpdateFilter();
	void DoCalcRedundancy();
	void DoUpdateTxPlan();
	void DoUpdatePriority(node_map_t outputs);
	void DoUpdateCoalition();
	void DoForgetGeneration();

	node_map_it LookUpInputs(UanAddress id);
	node_map_it LookUpOutputs(UanAddress id);

	void
	UpdateLogItem();

	HeaderInfo m_h;
	FeedbackInfo m_f;

	/********************************************************************************************************
	 * all received packets from the nodes with lower priority and increasing the coding matrix rank;
	 * they are the subject to be filtered
	 */
	RcvNum m_rcvNum;
	/*
	 * forwarding plan specifies the number of degrees of freedom to be forwarded to the sinks of output edges;
	 * it does not include redundancy
	 */
	ForwardPlan m_forwardPlan;
	/*
	 * transmission plan specifies the number of recoded packets to be sent to the sinks of output edges;
	 * it does include redundancy; roughly speaking the transmission plan is the forwarding plan plus redundancy
	 */
	TxPlan m_txPlan;
	/*
	 * number of actually sent recoded packets
	 */
	SentNum m_sentNum;
	/*
	 * if at least one retransmission request comes for a particular generation its state is marked as "RETRANSMITTING"
	 */
	GenStateMap m_genState;
	/********************************************************************************************************/

	/*
	 * filtering probabilities for the current node and data coming from source of input edges;
	 * to be used for filtering
	 */
	pf_t m_inF;
	/*
	 * filtering probabilities for sinks of the out-coming edges; to be sent with feedback
	 */
	pf_t m_outF;

	/*
	 * priorities of output nodes
	 */
	node_map_t m_outputs;
	/*
	 * coalition is the subset of outputs
	 */
	node_map_t m_coalition;
	/*
	 * own priority
	 */
	priority_t m_p;
	/*
	 * the node with the priority less or equal the threshold priority will not be added to a coalition
	 */
	const priority_t m_thresholdP;
	/*
	 * sending data rate
	 */
	Datarate m_d;
	/*
	 * coding rate
	 */
	double m_cr;

	/*
	 * receiving maps for in-coming edges are updated by the node itself
	 */
	std::map<UanAddress, RcvMap> m_inRcvMap;
	/*
	 * receiving maps for out-coming edges are updated with the feedback information
	 */
	std::map<UanAddress, RcvMap> m_outRcvMap;

	/*
	 * destination address
	 */
	UanAddress m_dst;

	/*
	 * own address
	 */
	UanAddress m_id;

	/*
	 * different RVs
	 */
	std::random_device m_rd;
	std::mt19937 m_gen;
	std::uniform_real_distribution<> m_dis;

	/*
	 * threshold difference of priority for staying in a coalition
	 */
	double m_b;
	/*
	 * probability to send the feedback after reception
	 */
	double m_feedbackP;
	/*
	 * probability to send the network discovery message after the reception
	 */
	double m_netDiscP;
	/*
	 * true if the last received packet was sent by the node with higher or equal priority
	 * otherwise - false
	 */
	double m_rcvdAndUsed;
	/*
	 * number of generations in memory before being erased
	 */
	uint16_t m_numGen;
	/*
	 * number of generations in the receiving buffer before the destination starts sending retransmission requests
	 */
	uint16_t m_numGenRetrans;
	/*
	 * time to live for network discovery messages
	 */
	ttl_t m_ttl;

	NodeType m_nodeType;

	LogItem m_logItem;
	add_log_func m_addLog;
	get_rank_func m_getRank;
	get_coding_matrix_func m_getCodingMatrix;
};

}//ncr
#endif /* NCROUTINGRULES_H_ */
