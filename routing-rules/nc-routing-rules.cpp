/*
 * nc-routing-rules.cc
 *
 *  Created on: 03.10.2016
 *      Author: tsokalo
 */
#include "nc-routing-rules.h"
#include "utils/comparison.h"
#include <assert.h>

namespace ncr {

NcRoutingRules::NcRoutingRules(UanAddress ownAddress, NodeType type, UanAddress destAddress, uint16_t numGen, Datarate apiRate) :
	m_thresholdP(SMALLEST_API_DATA_RATE >> 1), m_gen(m_rd()), m_dis(0, 1) {

	m_id = ownAddress;
	m_dst = destAddress;
	m_nodeType = type;
	m_numGen = numGen;
	m_d = apiRate;

	m_h.addr = m_id;
	m_f.addr = m_id;
	m_inF[m_id] = 0;
	m_cr = 1;
	m_p = m_thresholdP;
	if (m_id == m_dst) m_p = DESTINATION_PRIORITY;
	m_h.p = m_p;
	m_f.p = m_p;
	m_logItem.p = m_p;
	m_logItem.d = m_d;
	m_logItem.cr = m_cr;

	m_feedbackP = 0.01;
	m_netDiscP = 0.1;
	m_ttl = 2;
	m_rcvdAndUsed = false;
	m_b = 0.001;
	m_numGenRetrans = 2;
	m_numGenRetrans = (m_numGen > m_numGenRetrans) ? m_numGenRetrans : (m_numGen - 1);
}
NcRoutingRules::~NcRoutingRules() {

}
void NcRoutingRules::SetLogCallback(add_log_func addLog) {
	m_addLog = addLog;
}
void NcRoutingRules::SetGetRankCallback(get_rank_func c) {
	m_getRank = c;
}
void NcRoutingRules::SetGetCodingMatrixCallback(get_coding_matrix_func c) {
	m_getCodingMatrix = c;
}
//////////////////////////////////////////////////////////////////////
void NcRoutingRules::RcvHeaderInfo(HeaderInfo l) {

	SIM_LOG_FUNC(BRR_LOG);

	SIM_LOG_NP(BRR_LOG, m_id, m_p, "address " << l.addr << ", p " << l.p);

	m_outputs.add(l.addr, l.p);

	if (l.p >= m_p) {

		SIM_LOG_NP(BRR_LOG, m_id, m_p, "add output edge: " << m_outputs);
		UpdateCoalition();
		m_rcvdAndUsed = false;
	}
	else {
		if (l.pf.find(m_id) != l.pf.end()) {
			m_inF[l.addr] = l.pf.at(m_id);
			m_logItem.fp[l.addr] = m_inF[l.addr];
			if (m_addLog) m_addLog(m_logItem, m_id);
		}
		m_rcvdAndUsed = true;
	}
}
void NcRoutingRules::RcvFeedbackInfo(FeedbackInfo l) {

	SIM_LOG_FUNC(BRR_LOG);

	m_outputs.add(l.addr, l.p);
	SIM_LOG_NP(BRR_LOG, m_id, m_p, "outputs: " << m_outputs);
	if (l.rcvMap.find(m_id) != l.rcvMap.end()) {
		m_outRcvMap[l.addr] = l.rcvMap.at(m_id);
		m_logItem.eps[l.addr] = m_outRcvMap[l.addr].val_unrel();
		if (m_addLog) m_addLog(m_logItem, m_id);
	}
	else {
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "feedback has no receive map for me");
	}

	//		if (!l.rrInfo.empty()) {
	//			for (RcvNum::iterator it = l.rrInfo.begin(); it != l.rrInfo.end(); it++) {
	//				m_genState[it->first] = RETRANSMITTING_GENERATION_STATE;
	//			}
	//		}

	UpdateCoalition();
	DoCalcRedundancy();

}

void NcRoutingRules::UpdateSent(GenId genId, uint32_t num) {

	SIM_LOG_FUNC(BRR_LOG);

	m_sentNum[genId] += num;

	m_logItem.ns = num;
	if (m_addLog) m_addLog(m_logItem, m_id);
	m_logItem.ns = 0;

	DoFilter();

	DoUpdateTxPlan();
}
void NcRoutingRules::UpdateRcvd(GenId genId, UanAddress id, uint32_t num, bool linDep) {

	SIM_LOG_FUNC(BRR_LOG);

	if (!linDep) {
		if (m_genState.find(genId) == m_genState.end()) m_genState[genId] = FORWARDING_GENERATION_STATE;

		SIM_LOG_NP(BRR_LOG, m_id, m_p, "from " << id << ", GID " << genId << ", number " << num);

		m_rcvNum[genId][id] += num;
		m_inRcvMap[id].add(num, 0);

		m_logItem.nr = num;
		if (m_addLog) m_addLog(m_logItem, m_id);
		m_logItem.nr = 0;

		DoForgetGeneration();

		DoFilter();

		DoUpdateTxPlan();
	}
	else {
		m_inRcvMap[id].add(num, 0);
	}
}
void NcRoutingRules::UpdateLoss(GenId genId, UanAddress id, uint32_t num) {

	SIM_LOG_FUNC(BRR_LOG);

	m_inRcvMap[id].add(num, 1);
}
//////////////////////////////////////////////////////////////////////
TxPlan NcRoutingRules::GetTxPlan() {

	SIM_LOG_FUNC(BRR_LOG);

	return m_txPlan;
}
FeedbackInfo NcRoutingRules::GetFeedbackInfo() {

	SIM_LOG_FUNC(BRR_LOG);

	m_f.rcvMap.clear();
	m_f.rcvMap = m_inRcvMap;
	for (std::map<UanAddress, RcvMap>::iterator it = m_f.rcvMap.begin(); it != m_f.rcvMap.end(); it++) {
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "Prepare feedback with receive map for " << it->first);
	}

	//	m_f.rrInfo.clear();
	//	for (RcvNum::iterator it = m_rcvNum.begin(); it != m_rcvNum.end(); it++) {
	//		m_f.rrInfo[it->first] = m_getCodingMatrix(it->first);
	//	}
	return m_f;
}
HeaderInfo NcRoutingRules::GetHeaderInfo(GenId genId) {

	SIM_LOG_FUNC(BRR_LOG);

	DoUpdateFilter();

	m_h.genId = genId;

	return m_h;
}
NetDiscoveryInfo NcRoutingRules::GetNetDiscoveryInfo(ttl_t ttl) {

	assert(ttl != -1);
	return (ttl < 0) ? NetDiscoveryInfo(m_f, m_ttl) : NetDiscoveryInfo(m_f, ttl);
}
bool NcRoutingRules::MaySendFeedback() {

	SIM_LOG_FUNC(BRR_LOG);

	//
	// source never sends the feedback
	//
	if (m_nodeType == SOURCE_NODE_TYPE) return false;
	//
	// the coalition of the destination is always empty; the nodes without coalition cannot help the sender
	// So, they do not have to inform the sender about their presence sending the feedback
	//
	if (m_coalition.empty() && m_id != m_dst) return false;

	return (m_dis(m_gen) < m_feedbackP);
}

bool NcRoutingRules::MaySendNetDiscovery(ttl_t ttl) {

	SIM_LOG_FUNC(BRR_LOG);

	//
	// source never sends the network discovery messages
	//
	if (m_nodeType == SOURCE_NODE_TYPE) return false;
	//
	// if TTL is equal or above zero then the current node retransmits the network discovery message
	// upon the reception of the network discovery message
	//
	if (ttl >= 0) return true;
	//
	// if the current node has at least anybody in its coalition, it does not need explore the network
	// by sending the network discovery message; instead it has an opportunity to send the data packets
	// and can receive the feedback for them, which gives to the current node the same information as the
	// response on the network discovery message
	//
	if (!m_coalition.empty()) return false;
	//
	// the destination does not need to explore the network itself; it should only respond on the
	// network discovery messages; "ttl < 0" means that the current function is called when
	// not a network discovery message was received
	//
	if (m_id == m_dst && ttl < 0) return false;

	return (m_dis(m_gen) < m_netDiscP);
}

void NcRoutingRules::UpdateCoalition() {

	SIM_LOG_FUNC(BRR_LOG);

	DoUpdateCoalition();

	m_logItem.cs = m_coalition.size();
	if (m_addLog) m_addLog(m_logItem, m_id);
}
//////////////////////////////////////////////////////////////////////
void NcRoutingRules::DoFilter() {

	SIM_LOG_FUNC(BRR_LOG);

	//
	// for all generations
	//
	for (RcvNum::iterator it = m_rcvNum.begin(); it != m_rcvNum.end(); it++) {

		auto genId = it->first;;
		//
		// for all nodes
		//
		uint32_t k_t = 0, s_t = m_sentNum[genId];
		for (std::unordered_map<UanAddress, uint32_t>::iterator itv = it->second.begin(); itv != it->second.end(); itv++) {

			//
			// if no filter exists then the received data is obtained from the node with greater or equal priority
			// and it does not have to be forwarded
			//
			if (m_inF.find(itv->first) == m_inF.end()) continue;

			if (m_id != itv->first) {

				assert(m_outputs.find(itv->first) != m_outputs.end());
				if (m_outputs.at(itv->first) >= m_p) continue;
			}
			uint32_t k = floor(itv->second * (1 - m_inF.at(itv->first)));
			double p = itv->second * (1 - m_inF.at(itv->first)) - k;
			if (neq(p, 0)) if (m_dis(m_gen) < p) k++;
			k_t += k;

			SIM_LOG_NP(BRR_LOG, m_id, m_p, "sender " << itv->first << ", GID " << genId << ", filtering probability " << m_inF.at(itv->first)
					<< ", received " << itv->second << ", to forward " << k);
		}

		assert(m_getRank);
		uint32_t r = m_getRank(genId);
		uint32_t max_f = (k_t > r) ? r : k_t;
		m_forwardPlan[genId] = (s_t >= max_f) ? 0 : (max_f - s_t);

		SIM_LOG_NP(BRR_LOG, m_id, m_p, "GID " << genId << ", total to forward " << k_t << ", total already sent " << s_t << ", rank " << r
				<< ", remaining to forward " << m_forwardPlan[genId]);
	}
}
void NcRoutingRules::DoUpdateFilter() {

	SIM_LOG_FUNC(BRR_LOG);

	FilterArithmetics arith;
	m_outF.clear();
	for (node_map_it it = m_coalition.begin(); it != m_coalition.end(); it++) {
		double e = 1;
		//
		// e is always zero for the sink node of the first edge in the coalition
		//
		if (arith.check_sync()) e = arith.do_and().val_unrel();
		m_outF[it->first] = 1 - e;
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "Address " << it->first << ", filter probability " << m_outF[it->first]);
		//
		// if no receiving map is present consider the link without losses
		// which means that the remaining nodes have to drop all received data
		//
		if (m_outRcvMap.find(it->first) == m_outRcvMap.end()) break;
		arith.add(m_outRcvMap.at(it->first));
	}
	m_h.pf = m_outF;

}
void NcRoutingRules::DoCalcRedundancy() {

	SIM_LOG_FUNC(BRR_LOG);

	FilterArithmetics arith;
	for (node_map_it it = m_coalition.begin(); it != m_coalition.end(); it++) {
		arith.add(m_outRcvMap[it->first]);
	}

	m_cr = (arith.check_sync()) ? 1 / (1 - arith.do_and().val_unrel()) : 1;

	SIM_LOG_NP(BRR_LOG, m_id, m_p, "coding rate " << m_cr);

	m_logItem.cr = m_cr;
	if (m_addLog) m_addLog(m_logItem, m_id);

}
void NcRoutingRules::DoUpdateTxPlan() {

	SIM_LOG_FUNC(BRR_LOG);

	m_txPlan.clear();

	for (std::unordered_map<GenId, uint32_t>::iterator it = m_forwardPlan.begin(); it != m_forwardPlan.end(); it++) {
		//
		// find number of redundant packets
		//
		auto genId = it->first;;
		m_txPlan[genId].num_all = floor(m_forwardPlan.at(genId) * m_cr);
		double p = m_forwardPlan.at(genId) * m_cr - m_txPlan[genId].num_all;
		if (m_dis(m_gen) < p) m_txPlan[genId].num_all++;
		if (m_txPlan[genId].num_all == 0) {
			//
			// here we do not forget the generation but just exclude it from the current TX plan
			// do to missing new data to be sent
			//
			SIM_LOG(BRR_LOG, "GID " << genId << ": nothing to send");
			m_txPlan.erase(genId);
			continue;
		}

		//		//
		//		// define the position of each redundant packet
		//		//
		//		m_txPlan[genId].placement.resize(m_txPlan[genId].num_all - m_forwardPlan.at(genId), 0);
		//		std::uniform_int_distribution<> dis(0, m_txPlan[genId].placement.size() - 1);
		//		uint16_t i = 0;
		//		while (i++ != m_txPlan[genId].num_all) {
		//			auto index = dis(m_gen);;
		//			assert(index < m_txPlan[genId].placement.size());
		//			m_txPlan[genId].placement.at(index)++;
		//		}
		//		//
		//		// check check
		//		//
		//		auto v = m_txPlan[genId].placement;;
		//		assert(m_txPlan[genId].num_all == std::accumulate(v.begin(), v.end(), 0));
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "GID " << genId << ", TX number " << m_txPlan[genId].num_all);
	}
}
void NcRoutingRules::DoUpdatePriority(node_map_t outputs) {

	SIM_LOG_FUNC(BRR_LOG);
	SIM_LOG_NP(BRR_LOG, m_id, m_p, "Update priority with coalition " << outputs);

	if (m_dst == m_id) {
		m_p = DESTINATION_PRIORITY;
		return;
	}

	if (outputs.empty()) {
		m_p = m_thresholdP;
		return;
	}

	auto a = [&]()->double
	{
		FilterArithmetics arith;
		double prod_e = 1;
		for (node_map_it it = outputs.begin(); it != outputs.end(); it++)
		{
			if(m_outRcvMap[it->first].is_ready())
			{
				arith.add(m_outRcvMap.at(it->first));
				SIM_LOG_NP(BRR_LOG, m_id, m_p, "it->first " << it->first << ", m_outRcvMap.at(it->first) " << m_outRcvMap.at(it->first).val_unrel());
			}
			else
			{
				prod_e *= m_outRcvMap.at(it->first).val_unrel();
				SIM_LOG_NP(BRR_LOG, m_id, m_p, "it->first " << it->first << ", prod_e " << prod_e);
			}
		}
		double v = (arith.check_sync()) ? arith.do_and().val_unrel() : 1;

		SIM_LOG_NP(BRR_LOG, m_id, m_p, "a coefficient: prod_e = " << prod_e << ", v = "
				<< v << ", m_d = " << m_d);
		return m_d * (1 - prod_e * v);
	};;

	auto b = [&](UanAddress u)->double
	{
		if(u == m_dst)return 0;

		FilterArithmetics arith;
		double prod_e = 1;
		for (node_map_it it = outputs.begin(); it != outputs.end(); it++)
		{
			SIM_LOG_NP(BRR_LOG, m_id, m_p, "Try for node " << it->first << ", requested " << u);
			if(it->first == u)break;
			assert(it->second >= outputs.at(u));
			SIM_LOG_NP(BRR_LOG, m_id, m_p, "Use for node " << it->first << ", requested " << u);
			if(m_outRcvMap[it->first].is_ready())
			{
				arith.add(m_outRcvMap.at(it->first));
			}
			else
			{
				prod_e *= m_outRcvMap.at(it->first).val_unrel();
			}
		}
		double v = (arith.check_sync()) ? arith.do_and().val_unrel() : 1;

		double loss_on_e = (m_outRcvMap.find(u) == m_outRcvMap.end()) ? 0 : m_outRcvMap.at(u).val_unrel();
		double p = m_d * (1 - loss_on_e) * prod_e * v;
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "output node " << u << ", b coefficient: prod_e = " << prod_e << ", loss on e = "
				<< loss_on_e << ", v = " << v << ", m_d = " << m_d << ", p = " << p);
		//		assert(geq(p,0.0));
		return p;
	};;

	double denominator = 1;
	for (node_map_it it = outputs.begin(); it != outputs.end(); it++) {

		assert(m_p < it->second);
		assert(it->second > m_thresholdP);

		double bb = b(it->first);
		denominator += bb / it->second;
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "sink node " << it->first << ", priority " << it->second << ", b coefficient " << bb);
	}

	double aa = a();
	double p = m_p.val();
	m_p = aa / denominator;
	SIM_LOG_NP(BRR_LOG, m_id, m_p, "a coefficient: " << aa << ", old priority: " << p);

	m_h.p = m_p;
	m_f.p = m_p;
	m_logItem.p = m_p;
	if (m_addLog) m_addLog(m_logItem, m_id);
}

void NcRoutingRules::DoUpdateCoalition() {

	SIM_LOG_FUNC(BRR_LOG);

	m_coalition.clear();
	if (m_dst == m_id) {
		return;
	}
	double old_p = 0;

	//
	// verify the order
	//
	SIM_LOG_NP(BRR_LOG, m_id, m_p, "using output edges " << m_outputs);
	node_map_it it = m_outputs.begin();
	it++;
	for (node_map_it itt = m_outputs.begin(); it != m_outputs.end(); itt++) {
		SIM_LOG_NP(BRR_LOG, m_id, m_p, "check " << itt->second << " => " << it->second);
		assert(itt->second >= it->second);
		it++;
	}

	//
	// do the job
	//
	for (node_map_it it = m_outputs.begin(); it != m_outputs.end(); it++) {

		//		SIM_LOG_NP(BRR_LOG, m_id, m_p, "(it->second > m_p) " << (it->second > m_p));
		//		SIM_LOG_NP(BRR_LOG, m_id, m_p, "(it->second > m_thresholdP) " << (it->second > m_thresholdP));
		if ((it->second > m_p) && (it->second > m_thresholdP)) {
			SIM_LOG_NP(BRR_LOG, m_id, m_p, "adding edge to coalition. Sink node ID " << it->first
					<< ", priority " << it->second << ", threshold " << m_thresholdP);

			m_coalition.add(*it);

			DoUpdatePriority(m_coalition);
			if (m_p.val() - old_p < m_b * m_p.val()) {
				m_coalition.remove(*it);
				break;
			}
			old_p = m_p.val();
		}
		else {
			SIM_LOG_NP(BRR_LOG, m_id, m_p, "NOT adding edge to coalition. Sink node ID " << it->first
					<< ", priority " << it->second << ", threshold " << m_thresholdP);
		}
	}

	SIM_LOG_NP(BRR_LOG, m_id, m_p, "Coalition " << m_coalition);
}
void NcRoutingRules::DoForgetGeneration() {

	SIM_LOG_FUNC(BRR_LOG);

	assert(m_rcvNum.size() <= m_numGen + 1);

	if (m_rcvNum.size() == m_numGen + 1) {
		auto it = m_rcvNum.begin_orig_order();
		if (it == m_rcvNum.end()) return;
		GenId genId = it->first;
		SIM_LOG(BRR_LOG, "Erasing generation " << genId);
		if (m_forwardPlan.find(genId) != m_forwardPlan.end()) m_forwardPlan.erase(genId);
		if (m_txPlan.find(genId) != m_txPlan.end()) m_txPlan.erase(genId);
		if (m_rcvNum.find(genId) != m_rcvNum.end()) m_rcvNum.erase(genId);
		if (m_sentNum.find(genId) != m_sentNum.end()) m_sentNum.erase(genId);
	}
}

}//ncr
