// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing
/*
 * coder.h
 *
 *  Created on: 29.10.2016
 *      Author: tsokalo
 */

#ifndef CODER_H_
#define CODER_H_

#include <algorithm>
#include <ctime>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <deque>
#include <unordered_map>

#include <storage/split.hpp>

#include <kodo_rlnc/on_the_fly_codes.hpp>

#include <kodo_core/set_trace_callback.hpp>
#include <kodo_core/has_set_trace_callback.hpp>

#include "header.h"
#include "ssn.h"

namespace ncr {

typedef kodo_rlnc::on_the_fly_encoder<fifi::binary8> encoder;
typedef kodo_rlnc::on_the_fly_decoder<fifi::binary8> decoder;
typedef std::shared_ptr<encoder> encoder_ptr;
typedef std::shared_ptr<decoder> decoder_ptr;

template<class T>
class coder_queue {

	typedef std::unordered_map<GenId, T> CoderMap;

public:
	coder_queue(uint32_t symbolSize) {
		m_symbolSize = symbolSize;
	}
	T at(GenId genId) {
		return m_coders.at(genId);
	}
	bool find(GenId genId) {
		return (m_coders.find(genId) != m_coders.end());
	}
	void pop_front() {
		if (m_coders.empty()) return;
		m_coders.erase(*m_ids.begin());
		m_ids.pop_front();
	}
	T last() {
		return m_coders.at(m_ids.at(m_ids.size() - 1));
	}
	NcSymbol get_coded(GenId genId) {
		assert(find(genId));
		NcSymbol symb(m_coders.at(genId)->payload_size(), 0);
		m_coders.at(genId)->write_payload(symb.data());
		return symb;
	}
	uint32_t rank(GenId genId) {
		return (find(genId) ? m_coders.at(genId)->rank() : 0);
	}

protected:

	CoderMap m_coders;
	std::deque<GenId> m_ids;
	uint32_t m_symbolSize;
	Ssn<uint16_t, 1000> m_lastGenId;
};

class encoder_queue: public coder_queue<encoder_ptr> {

	typedef std::function<void(GenId)> notify_enque_func;

public:
	encoder_queue(uint16_t numGen, uint32_t genSize, uint32_t symbolSize) :
		coder_queue<encoder_ptr> (symbolSize), m_encFactory(genSize, symbolSize) {

		m_numGen = numGen;
		m_genSize = genSize;
	}

	void enque(NcSymbol symb) {

		SIM_LOG(CODER_LOG, "Enqueue symbol with size " << symb.size());

		assert(symb.size() == m_symbolSize);
		this->push_back(symb);
	}

	void set_notify_callback(notify_enque_func c) {
		m_notifyEnque = c;
	}

private:

	void push_back(NcSymbol symb) {

		auto add_coder = [this]()
		{
			m_lastGenId++;
			auto enc = m_encFactory.build();
			m_coders[m_lastGenId.val()] = enc;
			if (m_coders.size() > m_numGen) pop_front();
			m_ids.push_back(m_lastGenId.val());

			SIM_LOG(CODER_LOG, "Add coder for generator with ID: " << m_lastGenId.val());
		};;

		if (m_coders.empty()) add_coder();
		encoder_ptr c = this->last();
		auto rank = c->rank();
		if (rank == m_genSize) {
			add_coder();
			c = this->last();
			rank = c->rank();
		}
		c->set_const_symbol(rank, storage::storage(symb));
		if (m_notifyEnque) m_notifyEnque(m_lastGenId.val());

		SIM_LOG(CODER_LOG, "New rank: " << rank + 1);
	}

	encoder::factory m_encFactory;
	uint16_t m_numGen;
	uint32_t m_genSize;
	notify_enque_func m_notifyEnque;
};

class decoder_queue: public coder_queue<decoder_ptr> {

public:
	decoder_queue(uint16_t numGen, uint32_t genSize, uint32_t symbolSize) :
		coder_queue<decoder_ptr> (symbolSize), m_decFactory(genSize, symbolSize) {

		m_numGen = numGen;
		m_genSize = genSize;
	}

	void enque(NcSymbol symb, GenId genId) {

		SIM_LOG(CODER_LOG, "Enqueue symbol with size " << symb.size() << ", generation ID: " << genId);

		this->add(symb, genId);
	}

	//
	// get only those uncoded symbols, which were not copied yet
	//
	std::vector<OrigSymbol> get_uncoded() {
		std::vector<OrigSymbol> s;
		auto it = m_coders.begin();
		GenId genId = it->first;
		while (it != m_coders.end()) {
			for (uint32_t i = 0; i < m_genSize; i++) {
				if (m_coders.at(genId)->is_symbol_uncoded(i)) {
					if (m_forwardMap[genId].find(i) == m_forwardMap[genId].end()) {
						s.push_back(std::vector<uint8_t>(m_symbolSize, 0));
						it->second->copy_from_symbol(i, storage::storage(s.at(s.size() - 1)));
						m_forwardMap[genId][i] = true;
						SIM_LOG(CODER_LOG, "New uncoded. Generation ID: " << genId << ", index: " << i);
					}
					else {
						SIM_LOG(CODER_LOG, "Previously uncoded. Generation ID: " << genId << ", index: " << i);
					}
				}
			}
			it++;
		}
		if (m_forwardMap.size() > m_numGen) {
			auto itf = m_forwardMap.begin();
			GenId genId = itf->first;
			while (itf != m_forwardMap.end()) {
				if (m_coders.find(genId) == m_coders.end()) {
					m_forwardMap.erase(genId);
					SIM_LOG(CODER_LOG, "Forgetting generation with ID: " << genId);
				}
				else {
					itf++;
				}
			}
		}
		return s;
	}
private:

	void add(NcSymbol symb, GenId genId) {

		if (this->find(genId)) {
			auto dec = this->at(genId);
			dec->read_payload(symb.data());
		}
		else {
			auto dec = m_decFactory.build();
			dec->read_payload(symb.data());
			m_coders[genId] = dec;
			if (m_coders.size() > m_numGen) pop_front();
		}
	}

	decoder::factory m_decFactory;
	uint16_t m_numGen;
	uint32_t m_genSize;
	std::map<GenId, std::map<uint32_t, bool> > m_forwardMap;

};

}//ncr

#endif /* CODER_H_ */
