/*
 * utils.cpp
 *
 *  Created on: 27.10.2016
 *      Author: tsokalo
 */

#include <string>

#include "utils.h"

namespace ncr {

void PrintSymbol(NcSymbol sym) {
	for(auto i : sym)std::cout << i << " ";
	std::cout << std::endl;
}

void GetDirListing(FileList& result, const std::string& dirpath) {
	DIR* dir = opendir(dirpath.c_str());
	if (dir) {
		struct dirent* entry;
		while ((entry = readdir(dir))) {
			struct stat entryinfo;
			std::string entryname = entry->d_name;
			std::string entrypath = dirpath + "/" + entryname;
			if (!stat(entrypath.c_str(), &entryinfo)) result.push_back(entrypath);
		}
		closedir(dir);
	}
}

int16_t CreateDirectory(std::string path) {
	//mode_t mode = 0x0666;
	Stat st;
	int32_t status = 0;

	if (stat(path.c_str(), &st) != 0) {
		/* Directory does not exist. EEXIST for race condition */
		if (mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0 && errno != EEXIST) status = -1;//, mode
	}
	else if (!S_ISDIR(st.st_mode)) {
		errno = ENOTDIR;
		status = -1;
	}

	return status;
}

bool RemoveDirectory(std::string folderPath) {
	std::cout << "Deleting directory: " << folderPath << std::endl;
	FileList dirtree;
	GetDirListing(dirtree, folderPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		std::string str(dirtree[i]);
		std::string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR (st_buf.st_mode)) {
			RemoveDirectory(fullPath);
		}
		else {
			std::remove(fullPath.c_str());
		}
		rmdir(fullPath.c_str());
	}
	return true;
}
FileList FindFiles(std::string searchPath, std::string filePartName) {
	//  std::cout << "Searching in directory: " << searchPath << " for file with " << filePartName << " in its name" << std::endl;
	FileList matchFullPath;
	FileList dirtree;
	GetDirListing(dirtree, searchPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		std::string str(dirtree[i]);
		std::string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR (st_buf.st_mode)) {
			continue;
		}
		else {
			std::size_t found = str.find(filePartName);
			if (found != std::string::npos) {
				if (str.find('~') == std::string::npos) {
					matchFullPath.push_back(fullPath);
					//                  std::cout << "Found file: " << fullPath << std::endl;
				}
			}
		}
	}
	return matchFullPath;
}
std::string GetLogFileName() {
	return "log.txt";
}
LogBank ReadLogBank(std::string path) {

	LogBank lb;

	std::ifstream f(path, std::ios_base::in);
	std::string line;

	//
	// ignore the first line - with header
	//
	getline(f, line, '\n');

	while (getline(f, line, '\n')) {
		std::stringstream ss(line);
		UanAddress addr;
		uint64_t t;
		uint16_t m;
		ss >> t;
		ss >> m;
		ss >> addr;
		lb[addr].push_back(LogPair(t, MessType(m), LogItem(line)));
		LogItem item(line);
	}
	f.close();
	return lb;
}

void PlotPriorities(std::vector<UanAddress> nids, LogBank lb, std::string path) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = gnuplot_dir + "data.txt";
	std::string figure_file = res_dir + "priorities.svg";

	//
	// make data file
	//
	typedef std::map<UanAddress, std::map<uint64_t, priority_t> > priorities_t;
	priorities_t priorities;
	for (LogBank::iterator t = lb.begin(); t != lb.end(); t++) {
		for (LogHistory::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			priorities[t->first][tt->t] = tt->log.p;
		}
	}
	std::ofstream fd(data_file, std::ios_base::out);
	for (priorities_t::iterator t = priorities.begin(); t != priorities.end(); t++) {
		for (std::map<uint64_t, priority_t>::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			if (tt->second == DESTINATION_PRIORITY) continue;
			fd << tt->first << "\t" << t->first << "\t" << tt->second << std::endl;
		}
	}
	fd.close();

	//
	// make plot command
	//
	std::stringstream plot_command;

	plot_command << "set output '" << figure_file << "'" << std::endl;
	plot_command << "plot ";
	for (uint16_t i = 0; i < nids.size(); i++) {
		plot_command << "\"" << data_file << "\"" << " using 1:(($2==" << nids.at(i) << ") ? $3/1000000 : 1/0)";
		plot_command << " with linespoints ls 1 lw 1 linecolor " << nids.at(i) + 1 << " pt 7 ps 0.3 title \"node=" << nids.at(i) << "\"";
		if (i != nids.size() - 1) plot_command << ",\\" << std::endl;
	};;

	auto str_to_const_char = [](std::string str)
	{
		return str.c_str();
	};;
	//
	// make plot
	//
	std::string gnuplot_filename = gnuplot_dir + "plot_priorities.p";
	std::string gnuplot_filename_temp = gnuplot_dir + "plot_priorities_temp.p";
	system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
	std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
	f << plot_command.str();
	f.close();
	system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
}
void PlotInputFilters(std::vector<UanAddress> nids, LogBank lb, std::string path) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = gnuplot_dir + "data.txt";

	//
	// make data file
	//
	typedef std::map<UanAddress, std::map<UanAddress, bool> > node_pairs_t;
	node_pairs_t node_pairs;
	std::ofstream fd(data_file, std::ios_base::out);
	for (LogBank::iterator t = lb.begin(); t != lb.end(); t++) {
		for (LogHistory::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			for (std::map<int16_t, double>::iterator ttt = tt->log.fp.begin(); ttt != tt->log.fp.end(); ttt++) {
				node_pairs[t->first][ttt->first] = true;
				fd << tt->t << "\t" << t->first << "\t" << ttt->first << "\t" << ttt->second << std::endl;
			}
		}
	}
	fd.close();

	for (node_pairs_t::iterator it = node_pairs.begin(); it != node_pairs.end(); it++) {
		//
		// make plot command
		//
		std::stringstream plot_command;
		std::stringstream figure_file;
		figure_file << res_dir << "input_filters_" << it->first << ".svg";
		plot_command << "set output '" << figure_file.str() << "'" << std::endl;
		plot_command << "plot ";
		for (std::map<UanAddress, bool>::iterator itt = it->second.begin(); itt != it->second.end();) {

			plot_command << "\"" << data_file << "\"" << " using 1:(($2==" << it->first << "&&$3==" << itt->first << ") ? $4 : 1/0)";
			plot_command << " with linespoints ls 1 lw 1 linecolor " << itt->first + 1 << " pt 7 ps 0.3 title \"edge=<" << itt->first << "," << it->first
					<< ">\"";
			itt++;
			if (itt != it->second.end()) plot_command << ",\\" << std::endl;
		};;

		auto str_to_const_char = [](std::string str)
		{
			return str.c_str();
		};;
		//
		// make plot
		//
		std::string gnuplot_filename = gnuplot_dir + "plot_input_filters.p";
		std::string gnuplot_filename_temp = gnuplot_dir + "plot_input_filters_temp.p";
		system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
		std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
		f << plot_command.str();
		f.close();
		system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
	}
}
void PlotLossRatios(std::vector<UanAddress> nids, LogBank lb, std::string path) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = gnuplot_dir + "data.txt";

	//
	// make data file
	//
	typedef std::map<UanAddress, std::map<UanAddress, bool> > node_pairs_t;
	node_pairs_t node_pairs;
	std::map<UanAddress, std::map<UanAddress, double> > v;
	std::ofstream fd(data_file, std::ios_base::out);
	for (LogBank::iterator t = lb.begin(); t != lb.end(); t++) {
		for (LogHistory::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			for (std::map<int16_t, double>::iterator ttt = tt->log.eps.begin(); ttt != tt->log.eps.end(); ttt++) {
				if (v[t->first][ttt->first] == ttt->second) continue;
				v[t->first][ttt->first] = ttt->second;
				if (ttt->second != 0) node_pairs[t->first][ttt->first] = true;
				fd << tt->t << "\t" << t->first << "\t" << ttt->first << "\t" << ttt->second << std::endl;
			}
		}
	}
	fd.close();

	for (node_pairs_t::iterator it = node_pairs.begin(); it != node_pairs.end(); it++) {
		//
		// make plot command
		//
		std::stringstream plot_command;
		std::stringstream figure_file;
		figure_file << res_dir << "loss_ratios_" << it->first << ".svg";
		plot_command << "set output '" << figure_file.str() << "'" << std::endl;
		plot_command << "plot ";
		for (std::map<UanAddress, bool>::iterator itt = it->second.begin(); itt != it->second.end();) {

			plot_command << "\"" << data_file << "\"" << " using 1:(($2==" << it->first << "&&$3==" << itt->first << ") ? $4 : 1/0)";
			plot_command << " with linespoints ls 1 lw 1 linecolor " << itt->first + 1 << " pt 7 ps 0.3 title \"edge=<" << it->first << "," << itt->first
					<< ">\"";
			itt++;
			if (itt != it->second.end()) plot_command << ",\\" << std::endl;
		};;

		auto str_to_const_char = [](std::string str)
		{
			return str.c_str();
		};;
		//
		// make plot
		//
		std::string gnuplot_filename = gnuplot_dir + "plot_loss_ratios.p";
		std::string gnuplot_filename_temp = gnuplot_dir + "plot_loss_ratios_temp.p";
		system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
		std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
		f << plot_command.str();
		f.close();
		system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
	}
}
void PlotCoalitions(std::vector<UanAddress> nids, LogBank lb, std::string path) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = res_dir + GetLogFileName();
	std::string figure_file = res_dir + "coalitions.svg";
	//
	// make plot command
	//
	std::stringstream plot_command;

	plot_command << "set output '" << figure_file << "'" << std::endl;
	plot_command << "plot ";
	for (uint16_t i = 0; i < nids.size(); i++) {
		plot_command << "\"" << data_file << "\"" << " using 1:(($3==" << nids.at(i) << ") ? $7 : 1/0)";
		plot_command << " with linespoints ls 1 lw 1 linecolor " << nids.at(i) + 1 << " pt 7 ps 0.3 title \"node=" << nids.at(i) << "\"";
		if (i != nids.size() - 1) plot_command << ",\\" << std::endl;
	};;

	auto str_to_const_char = [](std::string str)
	{
		return str.c_str();
	};;
	//
	// make plot
	//
	std::string gnuplot_filename = gnuplot_dir + "plot_coalitions.p";
	std::string gnuplot_filename_temp = gnuplot_dir + "plot_coalitions_temp.p";
	system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
	std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
	f << plot_command.str();
	f.close();
	system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
}
void PlotCodingRates(std::vector<UanAddress> nids, LogBank lb, std::string path) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = res_dir + GetLogFileName();
	std::string figure_file = res_dir + "coding_rate.svg";
	//
	// make plot command
	//
	std::stringstream plot_command;

	plot_command << "set output '" << figure_file << "'" << std::endl;
	plot_command << "plot ";
	for (uint16_t i = 0; i < nids.size(); i++) {
		plot_command << "\"" << data_file << "\"" << " using 1:(($3==" << nids.at(i) << ") ? $6 : 1/0)";
		plot_command << " with linespoints ls 1 lw 1 linecolor " << nids.at(i) + 1 << " pt 7 ps 0.3 title \"node=" << nids.at(i) << "\"";
		if (i != nids.size() - 1) plot_command << ",\\" << std::endl;
	};;

	auto str_to_const_char = [](std::string str)
	{
		return str.c_str();
	};;
	//
	// make plot
	//
	std::string gnuplot_filename = gnuplot_dir + "plot_coding_rate.p";
	std::string gnuplot_filename_temp = gnuplot_dir + "plot_coding_rate_temp.p";
	system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
	std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
	f << plot_command.str();
	f.close();
	system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
}
void PlotSendingHistory(std::vector<UanAddress> nids, LogBank lb, std::string path) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = res_dir + GetLogFileName();
	std::string figure_file = res_dir + "send_history.svg";
	//
	// make plot command
	//
	std::stringstream plot_command;

	plot_command << "set output '" << figure_file << "'" << std::endl;
	plot_command << "plot ";
	for (uint16_t i = 0; i < nids.size(); i++) {
		plot_command << "\"" << data_file << "\"" << " using 1:(($2==" << nids.at(i) << ") ? $7 : 1/0)";
		plot_command << " with boxes ls 1 lw 1 linecolor " << nids.at(i) + 1 << " title \"node=" << nids.at(i) << "\"";
		if (i != nids.size() - 1) plot_command << ",\\" << std::endl;
	};;

	auto str_to_const_char = [](std::string str)
	{
		return str.c_str();
	};;
	//
	// make plot
	//
	std::string gnuplot_filename = gnuplot_dir + "plot_send_history.p";
	std::string gnuplot_filename_temp = gnuplot_dir + "plot_send_history_temp.p";
	system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
	std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
	f << plot_command.str();
	f.close();
	system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
}
void PlotSendingStatistics(std::vector<UanAddress> nids, LogBank lb, std::string path, TdmAccessPlan godPlan, TdmAccessPlan optPlan) {

	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = gnuplot_dir + "data.txt";
	std::string figure_file = res_dir + "send_statistics.svg";

	//
	// make data file
	//
	std::map<UanAddress, uint64_t> sum_send;
	uint64_t totalSum = 0;
	for (LogBank::iterator t = lb.begin(); t != lb.end(); t++) {
		for (LogHistory::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			if (tt->log.p == DESTINATION_PRIORITY) continue;
			sum_send[t->first] += tt->log.ns;
			totalSum += tt->log.ns;
		}
	}

	TdmAccessPlan actualPlan;
	for (std::map<UanAddress, uint64_t>::iterator t = sum_send.begin(); t != sum_send.end(); t++) {
		actualPlan[t->first] = (long double) t->second / (long double) totalSum;
	}

	auto add_zeros = [](TdmAccessPlan &p1, TdmAccessPlan &p2)
	{
		for (TdmAccessPlan::iterator t = p1.begin(); t != p1.end(); t++) {
			p2[t->first];
		}
		for (TdmAccessPlan::iterator t = p2.begin(); t != p2.end(); t++) {
			p1[t->first];
		}
	};;

	add_zeros(actualPlan, godPlan);
	add_zeros(actualPlan, optPlan);
	add_zeros(godPlan, optPlan);

	assert(actualPlan.size() == godPlan.size() && actualPlan.size() == optPlan.size());
	std::ofstream fd(data_file, std::ios_base::out);
	auto god_it = godPlan.begin();
	auto opt_it = optPlan.begin();

	fd << "Node" << "\t" << "Simulation" << "\t" << "Calculation" << "\t" << "Optimal" << std::endl;
	for (TdmAccessPlan::iterator t = actualPlan.begin(); t != actualPlan.end(); t++) {
		assert(god_it->first == t->first && opt_it->first == t->first);
		fd << t->first << "\t" << t->second << "\t" << god_it->second << "\t" << opt_it->second << std::endl;
		god_it++;
		opt_it++;
	}
	fd.close();

	//
	// make plot command
	//
	std::stringstream plot_command;

	plot_command << "set output '" << figure_file << "'" << std::endl;

	plot_command << "plot ";
	plot_command << "\"" << data_file << "\"";
	plot_command << " using 2 ti col, '' using 3 ti col, '' using 4:xticlabels(1) ti col";


	auto str_to_const_char = [](std::string str)
	{
		return str.c_str();
	};;
	//
	// make plot
	//
	std::string gnuplot_filename = gnuplot_dir + "plot_send_statistics.p";
	std::string gnuplot_filename_temp = gnuplot_dir + "plot_send_statistics_temp.p";
	system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
	std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
	f << plot_command.str();
	f.close();
	system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
}

void PlotResourceWaste(LogBank lb, std::string path, double sigma) {
	std::string gnuplot_dir = path + "gnuplot/";
	std::string res_dir = path + "Results/";
	std::string data_file = gnuplot_dir + "data.txt";
	std::string figure_file = res_dir + "resource_waste.svg";

	//
	// calculate total amount of linear independent packets received by the destination
	// calculate total amount of feedback messages sent by all nodes
	// calculate total amount of network discovery messages sent by all nodes
	// calculate total amount of sent messages by all nodes
	//
	uint32_t nr = 0, fb = 0, nd = 0, ns = 0;
	int64_t prev_t = 0;
	std::map<int64_t, int16_t> fbs, nds;

	uint32_t warmup_period = 5000;

	for (LogBank::iterator t = lb.begin(); t != lb.end(); t++) {
		for (LogHistory::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			if (tt->t < warmup_period) continue;
			if (tt->m == FEEDBACK_MSG_TYPE) fbs[tt->t] = 1;
			if (tt->m == NETDISC_MSG_TYPE) nds[tt->t] = 1;
		}
	}
	fb = fbs.size();
	nd = nds.size();

	for (LogBank::iterator t = lb.begin(); t != lb.end(); t++) {
		for (LogHistory::iterator tt = t->second.begin(); tt != t->second.end(); tt++) {
			if (tt->t < warmup_period) continue;
			if (tt->log.p == DESTINATION_PRIORITY) nr += tt->log.nr;
			ns += tt->log.ns;
		}
	}
	ns = ns + fb + nd;

	std::cout << "ns " << ns << " fb " << fb << " nd " << nd << " nr " << nr << " sigma " << sigma << std::endl;
	std::ofstream fd(data_file, std::ios_base::out);
	fd << "\"Network discovery\"\t" << (double) nd / (double) ns * 100 << std::endl;
	fd << "\"\\nFeedback\"\t" << (double) fb / (double) ns * 100 << std::endl;
	fd << "\"Excessive redundancy\"\t" << (double) (ns - fb - nd - (double) nr / sigma) / (double) ns * 100 << std::endl;
	fd << "\"\\nMain data\"\t" << (double) nr / sigma / (double) ns * 100 << std::endl;
	fd.close();

	//
	// make plot command
	//
	std::stringstream plot_command;

	plot_command << "set output '" << figure_file << "'" << std::endl;
	plot_command << "plot ";
	plot_command << "\"" << data_file << "\"" << " using 2:xticlabels(1)";
	plot_command << " with boxes ls 1 lw 1 linecolor 3 notitle";

	auto str_to_const_char = [](std::string str)
	{
		return str.c_str();
	};;
	//
	// make plot
	//
	std::string gnuplot_filename = gnuplot_dir + "plot_resource_waste.p";
	std::string gnuplot_filename_temp = gnuplot_dir + "plot_resource_waste_temp.p";
	system(str_to_const_char("cp " + gnuplot_filename + " " + gnuplot_filename_temp));
	std::ofstream f(gnuplot_filename_temp, std::ios_base::out | std::ios_base::app);
	f << plot_command.str();
	f.close();
	system(str_to_const_char("gnuplot " + gnuplot_filename_temp));
}

}
