/*
 * special-map.h
 *
 *  Created on: 01.11.2016
 *      Author: tsokalo
 */

#ifndef SPECIALMAP_H_
#define SPECIALMAP_H_

#include <unordered_map>
#include <vector>

#include "header.h"

namespace ncr {

template<class Key, class T>
struct special_map: public std::unordered_map<Key, T> {

	typedef typename std::unordered_map<Key, T>::iterator map_it;

	T& operator[](Key k) {
		auto it = std::find(m_keys.begin(), m_keys.end(), k);
		if (it == m_keys.end()) m_keys.push_back(k);
		return this->std::unordered_map<Key, T>::operator[](k);
	}

	map_it begin_orig_order() {

		if (m_keys.empty()) return this->end();

		auto it = this->find(m_keys.at(0));;
		assert(it != this->end());
		return it;
	}

	map_it last_orig_order() {

		if (m_keys.empty()) return this->end();

		return this->find(m_keys.at(m_keys.size() - 1));
	}
	void erase(Key k) {

		auto it = std::find(m_keys.begin(), m_keys.end(), k);
		m_keys.erase(it, it + 1);
		this->std::unordered_map<Key, T>::erase(k);
	}

private:

	std::vector<Key> m_keys;
};

typedef special_map<GenId, std::unordered_map<UanAddress, uint32_t> > RcvNum;
typedef special_map<GenId, uint32_t> SentNum;
typedef special_map<GenId, uint32_t> CodedSeqId;
typedef special_map<GenId, TxPlanItem> TxPlan;
typedef special_map<GenId, uint32_t> ForwardPlan;
typedef special_map<GenId, GenerationState> GenStateMap;

}//ncr

#endif /* SPECIALMAP_H_ */
