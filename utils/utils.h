/*
 * utils.h
 *
 *  Created on: 27.10.2016
 *      Author: tsokalo
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <array>
#include <memory>
#include <limits>
#include <vector>
#include <stdexcept>
#include <map>
#include <utility>
#include <algorithm>
#include <functional>
#include <cmath>

#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>

#include "header.h"

namespace ncr {
void
PrintSymbol(NcSymbol sym);

typedef std::vector<std::string> FileList;
typedef struct stat Stat;

void GetDirListing(FileList& result, const std::string& dirpath);
int16_t CreateDirectory(std::string path);
bool RemoveDirectory(std::string folderPath);
FileList FindFiles(std::string searchPath, std::string filePartName);

std::string GetLogFileName();
LogBank ReadLogBank(std::string path);

void PlotPriorities(std::vector<UanAddress> nids, LogBank lb, std::string path);
void PlotInputFilters(std::vector<UanAddress> nids, LogBank lb, std::string path);
void PlotLossRatios(std::vector<UanAddress> nids, LogBank lb, std::string path);
void PlotCoalitions(std::vector<UanAddress> nids, LogBank lb, std::string path);
void PlotCodingRates(std::vector<UanAddress> nids, LogBank lb, std::string path);
void PlotSendingHistory(std::vector<UanAddress> nids, LogBank lb, std::string path);
void PlotSendingStatistics(std::vector<UanAddress> nids, LogBank lb, std::string path, TdmAccessPlan godPlan, TdmAccessPlan optPlan);
void PlotResourceWaste(LogBank lb, std::string path, double sigma);

}

#endif /* UTILS_H_ */
