var searchData=
[
  ['need_5fbasis_5fsort',['NEED_BASIS_SORT',['../AbcSimplex_8hpp.html#a6d83a9ced2f3fde211edac98ff327f6c',1,'AbcSimplex.hpp']]],
  ['new_5fchunk_5fsize',['NEW_CHUNK_SIZE',['../CoinAbcHelperFunctions_8hpp.html#abb9a170db5facfb2186c8502c15f194f',1,'CoinAbcHelperFunctions.hpp']]],
  ['new_5fchunk_5fsize_5fincrement',['NEW_CHUNK_SIZE_INCREMENT',['../CoinAbcHelperFunctions_8hpp.html#a39141c13219eaf47f702325dffce424b',1,'CoinAbcHelperFunctions.hpp']]],
  ['new_5fchunk_5fsize_5foffset',['NEW_CHUNK_SIZE_OFFSET',['../CoinAbcHelperFunctions_8hpp.html#a30b1a8ae79fae3fa63164a741ee869a9',1,'CoinAbcHelperFunctions.hpp']]],
  ['no_5flink',['NO_LINK',['../CoinUtils_2src_2CoinPresolveMatrix_8hpp.html#a302dd036236eaa23b3cfd611d008dc59',1,'NO_LINK():&#160;CoinPresolveMatrix.hpp'],['../include_2coin_2CoinPresolveMatrix_8hpp.html#a302dd036236eaa23b3cfd611d008dc59',1,'NO_LINK():&#160;CoinPresolveMatrix.hpp']]],
  ['no_5fshift',['NO_SHIFT',['../CoinOslC_8h.html#a2bc3c35d7d12c88b67bee7e224cd8d21',1,'CoinOslC.h']]],
  ['not_5fzero',['NOT_ZERO',['../CoinOslC_8h.html#a3bf395e08253956c78c55ea732ead823',1,'CoinOslC.h']]],
  ['number_5fcolumn_5fblocks',['NUMBER_COLUMN_BLOCKS',['../AbcMatrix_8hpp.html#a382437edc306b6de9a9b161210dd5901',1,'AbcMatrix.hpp']]],
  ['number_5frow_5fblocks',['NUMBER_ROW_BLOCKS',['../AbcMatrix_8hpp.html#a65e03da4ed64a6ffae7e11a621131058',1,'AbcMatrix.hpp']]],
  ['number_5fthreads',['NUMBER_THREADS',['../AbcSimplex_8hpp.html#a04a9471eb94edc6ab1e73b3124cea6bd',1,'AbcSimplex.hpp']]]
];
