var searchData=
[
  ['slightlyinfeasible',['slightlyInfeasible',['../classClpEventHandler.html#abaaf9e2c8085520d0e083d80321f08c2a3f214cb7f87525f8b4465965645feb92',1,'ClpEventHandler']]],
  ['solokey',['soloKey',['../classClpDynamicMatrix.html#a3d4b9c6d4c6c836258060462e1642ee2a8250951bfc5432688b8ca4c39063ee3a',1,'ClpDynamicMatrix']]],
  ['solution',['solution',['../classClpEventHandler.html#abaaf9e2c8085520d0e083d80321f08c2ac12ae89ca3537e251af047846513398f',1,'ClpEventHandler']]],
  ['startofcrossover',['startOfCrossover',['../classClpEventHandler.html#abaaf9e2c8085520d0e083d80321f08c2a01c516944c4d4f649006f4274b96a8a1',1,'ClpEventHandler']]],
  ['startofiterationindual',['startOfIterationInDual',['../classClpEventHandler.html#abaaf9e2c8085520d0e083d80321f08c2a40e33e87ddf512b1ffd599a78fd187d8',1,'ClpEventHandler']]],
  ['startofstatusofproblemindual',['startOfStatusOfProblemInDual',['../classClpEventHandler.html#abaaf9e2c8085520d0e083d80321f08c2a4683f6d03135afc0906b3e6b5ce35476',1,'ClpEventHandler']]],
  ['startofstatusofprobleminprimal',['startOfStatusOfProblemInPrimal',['../classClpEventHandler.html#abaaf9e2c8085520d0e083d80321f08c2a890efd8a547c2c32313e445d4ce43b46',1,'ClpEventHandler']]],
  ['superbasic',['superBasic',['../classAbcSimplex.html#adb2cad49c75fa459d27d9d198384ce38adb1b97a83cc1e421e0a151a09327f645',1,'AbcSimplex::superBasic()'],['../classClpSimplex.html#a21143b50762085902b3852b103b3704fa4146cba25f133f010f399c59dc39fa53',1,'ClpSimplex::superBasic()'],['../classCoinPrePostsolveMatrix.html#aa103148594116d4889514cc8a3464280ae697abe659cc786bf4042565f13d8214',1,'CoinPrePostsolveMatrix::superBasic()']]]
];
