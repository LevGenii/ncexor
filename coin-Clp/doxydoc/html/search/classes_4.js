var searchData=
[
  ['do_5ftighten_5faction',['do_tighten_action',['../classdo__tighten__action.html',1,'']]],
  ['doubleton_5faction',['doubleton_action',['../classdoubleton__action.html',1,'']]],
  ['drop_5fempty_5fcols_5faction',['drop_empty_cols_action',['../classdrop__empty__cols__action.html',1,'']]],
  ['drop_5fempty_5frows_5faction',['drop_empty_rows_action',['../classdrop__empty__rows__action.html',1,'']]],
  ['drop_5fzero_5fcoefficients_5faction',['drop_zero_coefficients_action',['../classdrop__zero__coefficients__action.html',1,'']]],
  ['dropped_5fzero',['dropped_zero',['../structdropped__zero.html',1,'']]],
  ['dualcolumnresult',['dualColumnResult',['../structdualColumnResult.html',1,'']]],
  ['dupcol_5faction',['dupcol_action',['../classdupcol__action.html',1,'']]],
  ['duprow3_5faction',['duprow3_action',['../classduprow3__action.html',1,'']]],
  ['duprow_5faction',['duprow_action',['../classduprow__action.html',1,'']]]
];
