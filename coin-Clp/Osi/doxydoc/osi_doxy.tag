<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>config.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>config_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_COINUTILS</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a3d2273b012898f0612a5de41b8d20ebb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_NETLIB</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a6e1a694c8ecdefddf586c7ce148b63f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_SAMPLE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a8ea44e87e1d5c8c821de2055bb250662</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_OSI_CHECKLEVEL</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ae56d95e210219b3dc3bbb72d4501fda9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_OSI_VERBOSITY</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ab8e0758c05668b9b7689795beb5be88e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GLPK_HAS_INTOPT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a97a1c0ab8150066fc69144d537faa09f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_CFLOAT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a34d6c379f30f7070f7ae75abb128e5a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_CMATH</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a5ebdfdabf36598943dc9faa5699a1ba5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_DLFCN_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a0ee1617ff2f6885ef384a3dd46f9b9d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_INTTYPES_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ab90a030ff2790ebdc176660a6dd2a478</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_MEMORY_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ae93a78f9d076138897af441c9f86f285</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STDINT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ab6cd6d1c63c1e26ea2d4537b77148354</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STDLIB_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a9e0e434ec1a6ddbd97db12b5a32905e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STRINGS_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a405d10d46190bcb0320524c54eafc850</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STRING_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ad4c234dd1625255dc626a15886306e7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_STAT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ace156430ba007d19b4348a950d0c692b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_TYPES_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a69dc70bea5d1f8bd2be9740e974fa666</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_UNISTD_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a219b06937831d0da94d801ab13987639</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_SVN_REV</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a367f07867f12eb2c37dbedf5414af328</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a4a67ffee84bb2785a365b3d1dfe6a1d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_MAJOR</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>af7ff1f57c7766284c7ef8e5aee4cc772</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_MINOR</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a7d86531fd1a8655df2e2f2fc3766810d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_RELEASE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a05f844e18f7d7dc8bb0c6db543c8732d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>aca8570fb706c81df371b7f9bc454ae03</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_BUGREPORT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1d1d2d7f8d2f95b376954d649ab03233</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_NAME</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1c0439e4355794c09b64274849eb0279</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_STRING</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ac73e6f903c16eca7710f92e36e1c6fbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_TARNAME</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>af415af6bfede0e8d5453708afe68651c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>aa326a05d5e30f9e9a4bb0b4469d5d0c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STDC_HEADERS</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a550e5c272cc3cf3814651721167dcd23</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1c6d5de492ac61ad29aec7aa9a436bbf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>config_default.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>config__default_8h</filename>
    <includes id="config__osi__default_8h" name="config_osi_default.h" local="yes" imported="no">config_osi_default.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>COIN_OSI_CHECKLEVEL</name>
      <anchorfile>config__default_8h.html</anchorfile>
      <anchor>ae56d95e210219b3dc3bbb72d4501fda9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_OSI_VERBOSITY</name>
      <anchorfile>config__default_8h.html</anchorfile>
      <anchor>ab8e0758c05668b9b7689795beb5be88e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_COINUTILS</name>
      <anchorfile>config__default_8h.html</anchorfile>
      <anchor>a3d2273b012898f0612a5de41b8d20ebb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>config_osi.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>config__osi_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION</name>
      <anchorfile>config__osi_8h.html</anchorfile>
      <anchor>a4a67ffee84bb2785a365b3d1dfe6a1d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_MAJOR</name>
      <anchorfile>config__osi_8h.html</anchorfile>
      <anchor>af7ff1f57c7766284c7ef8e5aee4cc772</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_MINOR</name>
      <anchorfile>config__osi_8h.html</anchorfile>
      <anchor>a7d86531fd1a8655df2e2f2fc3766810d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_RELEASE</name>
      <anchorfile>config__osi_8h.html</anchorfile>
      <anchor>a05f844e18f7d7dc8bb0c6db543c8732d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>config_osi_default.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>config__osi__default_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION</name>
      <anchorfile>config__osi__default_8h.html</anchorfile>
      <anchor>a4a67ffee84bb2785a365b3d1dfe6a1d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_MAJOR</name>
      <anchorfile>config__osi__default_8h.html</anchorfile>
      <anchor>af7ff1f57c7766284c7ef8e5aee4cc772</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_MINOR</name>
      <anchorfile>config__osi__default_8h.html</anchorfile>
      <anchor>a7d86531fd1a8655df2e2f2fc3766810d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSI_VERSION_RELEASE</name>
      <anchorfile>config__osi__default_8h.html</anchorfile>
      <anchor>a05f844e18f7d7dc8bb0c6db543c8732d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiAuxInfo.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiAuxInfo_8hpp</filename>
    <class kind="class">OsiAuxInfo</class>
    <class kind="class">OsiBabSolver</class>
  </compound>
  <compound kind="file">
    <name>OsiBranchingObject.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiBranchingObject_8hpp</filename>
    <class kind="class">OsiObject</class>
    <class kind="class">OsiObject2</class>
    <class kind="class">OsiBranchingObject</class>
    <class kind="class">OsiBranchingInformation</class>
    <class kind="class">OsiTwoWayBranchingObject</class>
    <class kind="class">OsiSimpleInteger</class>
    <class kind="class">OsiIntegerBranchingObject</class>
    <class kind="class">OsiSOS</class>
    <class kind="class">OsiSOSBranchingObject</class>
    <class kind="class">OsiLotsize</class>
    <class kind="class">OsiLotsizeBranchingObject</class>
  </compound>
  <compound kind="file">
    <name>OsiChooseVariable.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiChooseVariable_8hpp</filename>
    <includes id="OsiBranchingObject_8hpp" name="OsiBranchingObject.hpp" local="yes" imported="no">OsiBranchingObject.hpp</includes>
    <class kind="class">OsiChooseVariable</class>
    <class kind="class">OsiPseudoCosts</class>
    <class kind="class">OsiChooseStrong</class>
    <class kind="class">OsiHotInfo</class>
  </compound>
  <compound kind="file">
    <name>OsiColCut.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiColCut_8hpp</filename>
    <includes id="OsiCollections_8hpp" name="OsiCollections.hpp" local="yes" imported="no">OsiCollections.hpp</includes>
    <includes id="OsiCut_8hpp" name="OsiCut.hpp" local="yes" imported="no">OsiCut.hpp</includes>
    <class kind="class">OsiColCut</class>
  </compound>
  <compound kind="file">
    <name>OsiCollections.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiCollections_8hpp</filename>
    <member kind="typedef">
      <type>std::vector&lt; int &gt;</type>
      <name>OsiVectorInt</name>
      <anchorfile>OsiCollections_8hpp.html</anchorfile>
      <anchor>a2c63f603bad899c58fefcee0253fd75a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; double &gt;</type>
      <name>OsiVectorDouble</name>
      <anchorfile>OsiCollections_8hpp.html</anchorfile>
      <anchor>ad4d4f116239932a94181ac8b786617d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; OsiColCut * &gt;</type>
      <name>OsiVectorColCutPtr</name>
      <anchorfile>OsiCollections_8hpp.html</anchorfile>
      <anchor>a55289824a9470c394d311e8dbafc3083</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; OsiRowCut * &gt;</type>
      <name>OsiVectorRowCutPtr</name>
      <anchorfile>OsiCollections_8hpp.html</anchorfile>
      <anchor>a1d90a61c53e315bc7128c7b281d376aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; OsiCut * &gt;</type>
      <name>OsiVectorCutPtr</name>
      <anchorfile>OsiCollections_8hpp.html</anchorfile>
      <anchor>a3cfe0f5592995b22e941da5b1f4227db</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiConfig.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiConfig_8h</filename>
    <includes id="config__osi__default_8h" name="config_osi_default.h" local="yes" imported="no">config_osi_default.h</includes>
  </compound>
  <compound kind="file">
    <name>OsiCut.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiCut_8hpp</filename>
    <includes id="OsiCollections_8hpp" name="OsiCollections.hpp" local="yes" imported="no">OsiCollections.hpp</includes>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiCut</class>
    <member kind="define">
      <type>#define</type>
      <name>COIN_DEFAULT_VALUE_FOR_DUPLICATE</name>
      <anchorfile>OsiCut_8hpp.html</anchorfile>
      <anchor>ab20eaf1813ff887cfe62a45b3b56a538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiCuts.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiCuts_8hpp</filename>
    <includes id="OsiCollections_8hpp" name="OsiCollections.hpp" local="yes" imported="no">OsiCollections.hpp</includes>
    <includes id="OsiRowCut_8hpp" name="OsiRowCut.hpp" local="yes" imported="no">OsiRowCut.hpp</includes>
    <includes id="OsiColCut_8hpp" name="OsiColCut.hpp" local="yes" imported="no">OsiColCut.hpp</includes>
    <class kind="class">OsiCuts</class>
    <class kind="class">OsiCuts::iterator</class>
    <class kind="class">OsiCuts::const_iterator</class>
    <class kind="class">OsiCuts::OsiCutCompare</class>
  </compound>
  <compound kind="file">
    <name>OsiPresolve.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiPresolve_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiPresolve</class>
  </compound>
  <compound kind="file">
    <name>OsiRowCut.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiRowCut_8hpp</filename>
    <includes id="OsiCollections_8hpp" name="OsiCollections.hpp" local="yes" imported="no">OsiCollections.hpp</includes>
    <includes id="OsiCut_8hpp" name="OsiCut.hpp" local="yes" imported="no">OsiCut.hpp</includes>
    <class kind="class">OsiRowCut</class>
    <class kind="class">OsiRowCut2</class>
    <member kind="define">
      <type>#define</type>
      <name>OsiRowCut_inline</name>
      <anchorfile>OsiRowCut_8hpp.html</anchorfile>
      <anchor>a63beeb76c5548bf6a45b0952feb02e47</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiRowCutDebugger.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiRowCutDebugger_8hpp</filename>
    <includes id="OsiCuts_8hpp" name="OsiCuts.hpp" local="yes" imported="no">OsiCuts.hpp</includes>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiRowCutDebugger</class>
  </compound>
  <compound kind="file">
    <name>OsiSolverBranch.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiSolverBranch_8hpp</filename>
    <class kind="class">OsiSolverBranch</class>
    <class kind="class">OsiSolverResult</class>
  </compound>
  <compound kind="file">
    <name>OsiSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiSolverInterface_8hpp</filename>
    <includes id="OsiCollections_8hpp" name="OsiCollections.hpp" local="yes" imported="no">OsiCollections.hpp</includes>
    <includes id="OsiSolverParameters_8hpp" name="OsiSolverParameters.hpp" local="yes" imported="no">OsiSolverParameters.hpp</includes>
    <class kind="class">OsiSolverInterface</class>
    <class kind="class">OsiSolverInterface::ApplyCutsReturnCode</class>
  </compound>
  <compound kind="file">
    <name>OsiSolverParameters.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>OsiSolverParameters_8hpp</filename>
    <member kind="enumeration">
      <type></type>
      <name>OsiIntParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>acf0c820dfa5579b09eae30a7335eb20f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiMaxNumIteration</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>acf0c820dfa5579b09eae30a7335eb20fafcfa83afbda5cf2ba7fe76952dd65bde</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiMaxNumIterationHotStart</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>acf0c820dfa5579b09eae30a7335eb20facf54cf5685e356d7eb10153c8e1fd63a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiNameDiscipline</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>acf0c820dfa5579b09eae30a7335eb20fa11c6725745f06661b84d8607fb35e38a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiLastIntParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>acf0c820dfa5579b09eae30a7335eb20fa2cfa3ed3d63d83c5e2108471dc79c01d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>OsiDblParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDualObjectiveLimit</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01ae1de448b2c2701069459febef8d2f3ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiPrimalObjectiveLimit</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01a85a894664922cb17f727137e99452fa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDualTolerance</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01a1eb064ea165dbcf3c67b2c9272a8cae7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiPrimalTolerance</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01acdbff6206ca2d963c08fd6a5c27e2490</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiObjOffset</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01ad6aa527bee2540acfc1a3de5a3fc5e90</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiLastDblParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a4c1b891397cf6640df90cde63c14cd01ae51a79dd341b1918972969dd3efd2be9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>OsiStrParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a5060b3ac748c55ace32d168add29fce6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiProbName</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a5060b3ac748c55ace32d168add29fce6a6a0b0a72924ce58da8261d32e2b261f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiSolverName</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a5060b3ac748c55ace32d168add29fce6aaff3f14a311c3468b493eece3b5f300c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiLastStrParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>a5060b3ac748c55ace32d168add29fce6a4914dcd1c338961fde9e2ccf9338e5ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>OsiHintParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoPresolveInInitial</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa247f4c1746839ffdf16c150f7f9a2f2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoDualInInitial</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa257c735faa049093bcea77e3cc227ce7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoPresolveInResolve</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa83baa1e59b2ef386cb357d40a0dc1e00</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoDualInResolve</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa991fc618b39934e5a08e51611f2ca7ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoScale</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa84cd796aff09d455ac89bcd74aad1f87</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoCrash</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa4be862614cacfd8d5ffcef1b84fed1e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoReducePrint</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa7518bbe7ed577d433f376b2e99f1764d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiDoInBranchAndCut</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa2b203491edcfb0588a3a6ff52a8d1bed</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiLastHintParam</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>ab1e3caa8d88907e59daccad09ef3377aa9a8b31a2671e743495a57976853ea170</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>OsiHintStrength</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>aadb1a3aa136dad0733ed2fb4c585397b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiHintIgnore</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>aadb1a3aa136dad0733ed2fb4c585397bad73c828a2806237f240a7a85e1656384</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiHintTry</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>aadb1a3aa136dad0733ed2fb4c585397bab7e9c80fedf072eec23b457ac5ea9f4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiHintDo</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>aadb1a3aa136dad0733ed2fb4c585397ba47424160fb06a37426924b7549da227e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>OsiForceDo</name>
      <anchorfile>OsiSolverParameters_8hpp.html</anchorfile>
      <anchor>aadb1a3aa136dad0733ed2fb4c585397ba73c1f38aad6aa61ee0bc18efc09abb1e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiUnitTests.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCommonTest/</path>
    <filename>OsiUnitTests_8hpp</filename>
    <class kind="class">OsiUnitTest::TestOutcome</class>
    <class kind="class">OsiUnitTest::TestOutcomes</class>
    <namespace>OsiUnitTest</namespace>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_QUOTEME_</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>afd719ced354b705b61e8a6b03f79fd6d</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_QUOTEME</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a368357ba9526b31b6a3de53a92b3307d</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_ADD_OUTCOME</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a7b794d6d84739786d6846b64c30a0fb9</anchor>
      <arglist>(component, testname, testcondition, severity, expected)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_ASSERT_SEVERITY_EXPECTED</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a2327d9e7b52835d7ef3b4f01396be90b</anchor>
      <arglist>(condition, failurecode, component, testname, severity, expected)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_ASSERT_ERROR</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>af8bb82e53f016b1b6d04e28797ce26a1</anchor>
      <arglist>(condition, failurecode, component, testname)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_ASSERT_WARNING</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a25646a5ef71d72fc1052ccfe991ae9ca</anchor>
      <arglist>(condition, failurecode, component, testname)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_CATCH_SEVERITY_EXPECTED</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a75948d6e47f86d51cfc90e48dc5f4290</anchor>
      <arglist>(trycode, catchcode, component, testname, severity, expected)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_CATCH_ERROR</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>aaf08e614e76109af7d2d8585cfe4bde6</anchor>
      <arglist>(trycode, catchcode, component, testname)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>OSIUNITTEST_CATCH_WARNING</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a6e044b744b970aee046c0588ebe6147d</anchor>
      <arglist>(trycode, catchcode, component, testname)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiSolverInterfaceMpsUnitTest</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a92de2d84b150be2ae1ca3078efe926a7</anchor>
      <arglist>(const std::vector&lt; OsiSolverInterface * &gt; &amp;vecEmptySiP, const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiSolverInterfaceCommonUnitTest</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a5740577958c910d3d0628f18e5256e2d</anchor>
      <arglist>(const OsiSolverInterface *emptySi, const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiColCutUnitTest</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>aed6f54762016a69a0b4fe42d0d2e639c</anchor>
      <arglist>(const OsiSolverInterface *baseSiP, const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiRowCutUnitTest</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a97aab68b88609d86d8791f32897a733a</anchor>
      <arglist>(const OsiSolverInterface *baseSiP, const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiRowCutDebuggerUnitTest</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>acc149ef7d160e813359a1f3536e240bb</anchor>
      <arglist>(const OsiSolverInterface *siP, const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiCutsUnitTest</name>
      <anchorfile>OsiUnitTests_8hpp.html</anchorfile>
      <anchor>a40f4dd50933870a94de23f49c17605ec</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a73c5c8f2b4aff0e91f16df862b887964</anchor>
      <arglist>(const std::string &amp;solverName, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a5b27dabe2c8d4076cdf9c01a6ed696c6</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a26a27b97f6e571baf300c70eaf5b77e3</anchor>
      <arglist>(const std::string &amp;solverName, const std::string &amp;testname, const std::string &amp;testcond)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a71144ed72c864ef0e2c88500cbc29e97</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const std::string &amp;testname, const std::string &amp;testcond)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>testingMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a0c822bb3a0ad104812155660c5503ecf</anchor>
      <arglist>(const char *const msg)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>equivalentVectors</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a3b915f44e262c8b214adb1818124b1da</anchor>
      <arglist>(const OsiSolverInterface *si1, const OsiSolverInterface *si2, double tol, const double *v1, const double *v2, int size)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>compareProblems</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a81d6ceb7ab13802d56255f266802c68d</anchor>
      <arglist>(OsiSolverInterface *osi1, OsiSolverInterface *osi2)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>ad5615e81938c2fd1e8969adadebabf5d</anchor>
      <arglist>(const CoinPackedVectorBase &amp;pv, int n, const double *fv)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>processParameters</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a153694396f3ac26853bc2547f858ed63</anchor>
      <arglist>(int argc, const char **argv, std::map&lt; std::string, std::string &gt; &amp;parms, const std::map&lt; std::string, int &gt; &amp;ignorekeywords=std::map&lt; std::string, int &gt;())</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>OsiUnitTestAssertSeverityExpected</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a4952d26b07bd333e08d099cb038827ae</anchor>
      <arglist>(bool condition, const char *condition_str, const char *filename, int line, const Component &amp;component, const std::string &amp;testname, TestOutcome::SeverityLevel severity, bool expected)</arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>verbosity</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a974eaac2e2b9d0c0be94ecee2999ce9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>haltonerror</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>ae045fc425c77cb73b07f256fd4e68e26</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>TestOutcomes</type>
      <name>outcomes</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>ad5c84c7610bcbb8a477ffc0faf262fca</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiCpxSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCpx/</path>
    <filename>OsiCpxSolverInterface_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <includes id="OsiColCut_8hpp" name="OsiColCut.hpp" local="yes" imported="no">OsiColCut.hpp</includes>
    <includes id="OsiRowCut_8hpp" name="OsiRowCut.hpp" local="yes" imported="no">OsiRowCut.hpp</includes>
    <class kind="class">OsiCpxSolverInterface</class>
    <member kind="typedef">
      <type>struct cpxlp *</type>
      <name>CPXLPptr</name>
      <anchorfile>OsiCpxSolverInterface_8hpp.html</anchorfile>
      <anchor>ac7e9dbfd8faa8324c07e65a6567f29ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct cpxenv *</type>
      <name>CPXENVptr</name>
      <anchorfile>OsiCpxSolverInterface_8hpp.html</anchorfile>
      <anchor>a7ef4207f4aa8fc83b2c1ade08084c353</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiCpxSolverInterfaceUnitTest</name>
      <anchorfile>OsiCpxSolverInterface_8hpp.html</anchorfile>
      <anchor>ac7f9bd9ce9661ba69a99adb2cb059bd7</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiGlpkSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGlpk/</path>
    <filename>OsiGlpkSolverInterface_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="struct">glp_prob</class>
    <class kind="class">OsiGlpkSolverInterface</class>
    <member kind="define">
      <type>#define</type>
      <name>LPX</name>
      <anchorfile>OsiGlpkSolverInterface_8hpp.html</anchorfile>
      <anchor>a9d999f9c5a61e93381f5fd68a943f9f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GLP_PROB_DEFINED</name>
      <anchorfile>OsiGlpkSolverInterface_8hpp.html</anchorfile>
      <anchor>a6d43c56c07ff0514ac9c493c62549a23</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiGlpkSolverInterfaceUnitTest</name>
      <anchorfile>OsiGlpkSolverInterface_8hpp.html</anchorfile>
      <anchor>ab5fe829b1c6c86035102dd01bc21cd2f</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiGrbSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGrb/</path>
    <filename>OsiGrbSolverInterface_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiGrbSolverInterface</class>
    <member kind="typedef">
      <type>struct _GRBmodel</type>
      <name>GRBmodel</name>
      <anchorfile>OsiGrbSolverInterface_8hpp.html</anchorfile>
      <anchor>a865e461c79c88bc5c0d5f56087ae4876</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct _GRBenv</type>
      <name>GRBenv</name>
      <anchorfile>OsiGrbSolverInterface_8hpp.html</anchorfile>
      <anchor>a2963ba0c46b86ac4680fb3bb4ca8b2f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiGrbSolverInterfaceUnitTest</name>
      <anchorfile>OsiGrbSolverInterface_8hpp.html</anchorfile>
      <anchor>ab4ab8f979eecd8300822acde2fbd59eb</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiMskSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiMsk/</path>
    <filename>OsiMskSolverInterface_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiMskSolverInterface</class>
    <member kind="typedef">
      <type>void *</type>
      <name>MSKtask_t</name>
      <anchorfile>OsiMskSolverInterface_8hpp.html</anchorfile>
      <anchor>a6c5ccb56cfa60d39b2c629bfe3ce92a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void *</type>
      <name>MSKenv_t</name>
      <anchorfile>OsiMskSolverInterface_8hpp.html</anchorfile>
      <anchor>a5b8230327c053380174b336669cb538a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiMskSolverInterfaceUnitTest</name>
      <anchorfile>OsiMskSolverInterface_8hpp.html</anchorfile>
      <anchor>abd3f7d4ea20bfdf2c0070a9b30241449</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiSpxSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiSpx/</path>
    <filename>OsiSpxSolverInterface_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiSpxSolverInterface</class>
    <namespace>soplex</namespace>
    <member kind="function">
      <type>void</type>
      <name>OsiSpxSolverInterfaceUnitTest</name>
      <anchorfile>OsiSpxSolverInterface_8hpp.html</anchorfile>
      <anchor>a271b1841c85292d3968c61a26ee18e2e</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>OsiXprSolverInterface.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiXpr/</path>
    <filename>OsiXprSolverInterface_8hpp</filename>
    <includes id="OsiSolverInterface_8hpp" name="OsiSolverInterface.hpp" local="yes" imported="no">OsiSolverInterface.hpp</includes>
    <class kind="class">OsiXprSolverInterface</class>
    <member kind="typedef">
      <type>struct xo_prob_struct *</type>
      <name>XPRSprob</name>
      <anchorfile>OsiXprSolverInterface_8hpp.html</anchorfile>
      <anchor>a52d017fc4654de89d9a4ebd32c571719</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>OsiXprSolverInterfaceUnitTest</name>
      <anchorfile>OsiXprSolverInterface_8hpp.html</anchorfile>
      <anchor>aba4a830e25e527cb078ac041d5528e53</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>glp_prob</name>
    <filename>structglp__prob.html</filename>
    <member kind="variable">
      <type>double</type>
      <name>_opaque_prob</name>
      <anchorfile>structglp__prob.html</anchorfile>
      <anchor>aac3906c10056d1a010e4fa961c004494</anchor>
      <arglist>[100]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiAuxInfo</name>
    <filename>classOsiAuxInfo.html</filename>
    <member kind="function">
      <type></type>
      <name>OsiAuxInfo</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>a8bce9243a53a356b8f7a769e6dcc5ed6</anchor>
      <arglist>(void *appData=NULL)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiAuxInfo</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>a4f687d0d0c6cf6e874ea894e1d587dcf</anchor>
      <arglist>(const OsiAuxInfo &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiAuxInfo</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>a5cb97f74b1dd67db5bff4b2f9ad20047</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiAuxInfo *</type>
      <name>clone</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>a4ee5dc262002a37d5c83a454e309e2bd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiAuxInfo &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>a6d7034b64b1c5b5e943730372ca93c76</anchor>
      <arglist>(const OsiAuxInfo &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>getApplicationData</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>adaa4a8d36c79cdf53691e0f9d5ceb4bf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>void *</type>
      <name>appData_</name>
      <anchorfile>classOsiAuxInfo.html</anchorfile>
      <anchor>aeeddcca21e53eb0eb622076f0c655837</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiBabSolver</name>
    <filename>classOsiBabSolver.html</filename>
    <base>OsiAuxInfo</base>
    <member kind="function">
      <type></type>
      <name>OsiBabSolver</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>aaf24c5ed9a77a772853793e524bebb42</anchor>
      <arglist>(int solverType=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiBabSolver</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a6bed424632598093a82142296f48c521</anchor>
      <arglist>(const OsiBabSolver &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiBabSolver</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a14682af41cdd7217ea900345e22a276f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiAuxInfo *</type>
      <name>clone</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a6c95376b51c8b55cd73a992a8a3549c3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiBabSolver &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a063355110f1983e34de316b3daa0a2c9</anchor>
      <arglist>(const OsiBabSolver &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSolver</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a581b163c6743c179b26e9c8b49651444</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSolver</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ac5a77ffa572d52720c30f6a88ea62705</anchor>
      <arglist>(const OsiSolverInterface &amp;solver)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>solution</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a47f8993024b33b7bf66db9512135e400</anchor>
      <arglist>(double &amp;objectiveValue, double *newSolution, int numberColumns)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSolution</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a7a2c73883c4045a5712a966cfcb03206</anchor>
      <arglist>(const double *solution, int numberColumns, double objectiveValue)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>hasSolution</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a155ee8d31123d47357af6d0ddf05874e</anchor>
      <arglist>(double &amp;solutionValue, double *solution)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSolverType</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a8ace72db18ba0bb661ee6a539e1dee01</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>solverType</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>afc2b67434a609a9c53ad4bf5574fbb05</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>solutionAddsCuts</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a502e5ec897d4934345a38bf272084218</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>alwaysTryCutsAtRootNode</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ad3217423b2df3ed92a3781cf1cf96f96</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>solverAccurate</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a30a70ca8149f45e2d5134030dc9a4cc2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>reducedCostsAccurate</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ad27daed55efe1dd04343e5e7b6dbd91d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>mipBound</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a87d7680158137ef8fdab5a51d12f0285</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>mipFeasible</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a1ebef9e7b8e4f7480b3c30a1313e2095</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMipBound</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ae1c626d05c5b26acf91de42ad123b266</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>bestObjectiveValue</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ac4c91f4e59b72ddb05444a9747fea184</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>tryCuts</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>afa71b032dc9eb60c14584159d3a444d2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>warmStart</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a5c4bed842e4cc715367f320e106b7a16</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>extraCharacteristics</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>acce1594deec57adb5a70857bb023ec52</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setExtraCharacteristics</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a7a83c82a052ddf55293b35999705acd5</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>beforeLower</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a65c13dcd27d0acd5137ea5e65fe1fb10</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBeforeLower</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>abc2c7275e7fb109cc6157e3528a1291b</anchor>
      <arglist>(const double *array)</arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>beforeUpper</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a8ed00749e4ec2cd1553aa4ae58a6c474</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBeforeUpper</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a56424b00efc96414dc13dda8a9f283cd</anchor>
      <arglist>(const double *array)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>bestObjectiveValue_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a8ec7dd0a0eda30d62f965d6e79af6acf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>mipBound_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>af2d5def6a4b826c98c8af63ef62986c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>const OsiSolverInterface *</type>
      <name>solver_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a866557bcf4fb43ceafc38f813364b4ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>bestSolution_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a70a902f2950077e17785e86b64e143f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>const double *</type>
      <name>beforeLower_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a714f4394fe21ff2f2b20e740efc5a67a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>const double *</type>
      <name>beforeUpper_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>aa86b5a67198774008e39e0ffaa47fe2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>solverType_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ad94f198c46b375e70ef51073eb16df9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>sizeSolution_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>ab3a23a5fc7e7fcff05be6d14798663c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>extraCharacteristics_</name>
      <anchorfile>classOsiBabSolver.html</anchorfile>
      <anchor>a7703d63e80f63edcc2f6aa8c5f9d1f61</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiBranchingInformation</name>
    <filename>classOsiBranchingInformation.html</filename>
    <member kind="function">
      <type></type>
      <name>OsiBranchingInformation</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>aa54f5ec7e78355d4f7e08f3e1c283e4f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiBranchingInformation</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a8aee5b13cf330a28a0f51a8d05c33fe5</anchor>
      <arglist>(const OsiSolverInterface *solver, bool normalSolver, bool copySolution=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiBranchingInformation</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a0cb5209ffb4d41d2f0d3a37215ec276b</anchor>
      <arglist>(const OsiBranchingInformation &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiBranchingInformation &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a5ebe8c5b9eddbd5e8dc10e51d479aecd</anchor>
      <arglist>(const OsiBranchingInformation &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingInformation *</type>
      <name>clone</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>ad15c8fd424094b7d582ff655c5b6fdce</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiBranchingInformation</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>abc4aa4adb77e67a35f7d7723438b062d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>stateOfSearch_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a2c8c608b127770bd0ab2e7b1fc89d56c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>objectiveValue_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a14b3c0c1d94313662abe468886e4f1ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>cutoff_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>ae7fb813240b0d3264f17bc4157cb070c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>direction_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>aae44e8ce0e0ec4fa7e8fb510f1a0e095</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>integerTolerance_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>af95a0012d7264d056654a6703a10c18a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>primalTolerance_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a2e87ec824c22452f95a9a489e97b2d1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>timeRemaining_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>ac4cbeda8b9ed71979d1389950f3ab0f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>defaultDual_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a174ab8b859dd28c85d7c569fa04acdb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const OsiSolverInterface *</type>
      <name>solver_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a6bd478a428be82cdb87846f801abd05c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>ab2f540afcaf27a1631c5b4d6f3994100</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>lower_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>af87ac2b45f92f02d225cfff4caf50109</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>solution_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a34567d1261b379e1ae0d20eb10fb55c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>upper_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>acb07adba05e4b07e71b4abe408d69008</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>hotstartSolution_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a810a4ee91286d9af721a1d0bbce92e97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>pi_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a377f71f541944464043ccb7f26a38e93</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>rowActivity_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a76a814b78a673c4351473ade50fbaaa6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>objective_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a6c688e1887bc07d28e72fe3884a3d062</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>rowLower_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a246b38ec33fce93da41bcf168b49c2cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>rowUpper_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>aeeb9645a7a9ea48e07aa965d9d641b1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>elementByColumn_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a5687418611e7dbe72a2f3babb764bbd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const CoinBigIndex *</type>
      <name>columnStart_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a518aabc17782e5743c95ff092df18c21</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const int *</type>
      <name>columnLength_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>ab77eeeb7e02388eaadb72adbb54a2da6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const int *</type>
      <name>row_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a51577770c4becfffc1c96ca8619fa424</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>usefulRegion_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a80589ac23b0ef549ff03d25a9b53de4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>indexRegion_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a9d5ae1f19267341749b7c9fa8271e899</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberSolutions_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>ae624e88585720ab5142fcba0dde0b7f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberBranchingSolutions_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a08253fd2b7e88f963c57f61bbc907f9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>depth_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a634034d4f5f4b9598029a978a140c9e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>owningSolution_</name>
      <anchorfile>classOsiBranchingInformation.html</anchorfile>
      <anchor>a153e9da6cd61d938aee504c7597d0bcf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiBranchingObject</name>
    <filename>classOsiBranchingObject.html</filename>
    <member kind="function">
      <type></type>
      <name>OsiBranchingObject</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a22407bf0c8339fbbbcfb92084ffb7232</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiBranchingObject</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a76061ddb42acd6abb55ffa084dd72452</anchor>
      <arglist>(OsiSolverInterface *solver, double value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiBranchingObject</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a4b9a55b5202570cf65bf5dc6ee040190</anchor>
      <arglist>(const OsiBranchingObject &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiBranchingObject &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>af61f2af699375dacb7dba4b87af45a1b</anchor>
      <arglist>(const OsiBranchingObject &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual OsiBranchingObject *</type>
      <name>clone</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a222a2805e8d82654a591f14d8d1f7ee2</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiBranchingObject</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a47d76747407a48b9a4d3709430a952a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberBranches</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a09318f08fd084a8330cb00192d206c6b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberBranchesLeft</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a4f6040aa9288e37857f8308b3499640a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>incrementNumberBranchesLeft</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>ad27851fb1316a383eacca12382ee2e72</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberBranchesLeft</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a2eacdf7f2931a8ec1d12e1470f557c69</anchor>
      <arglist>(int)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>decrementNumberBranchesLeft</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a5487bfcd9a7b5a018f8c2237730a4067</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>branch</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a22e003ff4212acb34da7994c910c0770</anchor>
      <arglist>(OsiSolverInterface *solver)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>branch</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a26f4258981159a7d133623620f7f94b6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>boundBranch</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>ac65975b8f1d2f7036fb5dfb81e91e708</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>branchIndex</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a1e856cef9b9498d886e20dd024f3733e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBranchingIndex</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>abb16dc00e482ef9517bf9992af860b74</anchor>
      <arglist>(int branchIndex)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>value</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a8f06597fdea6a83633ea2fa296bde493</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const OsiObject *</type>
      <name>originalObject</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>ab0a5ecf1f6ca366124f199e79507ad02</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalObject</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a1593a6c76d329960773de823b5f651d7</anchor>
      <arglist>(const OsiObject *object)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>checkIsCutoff</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>af7916caec73d0e9d0ccd6e9d3b2418aa</anchor>
      <arglist>(double)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>columnNumber</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>af1ef440b71de341db96570b50b11a53d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a7788ed1063fd09de7dac4b4ca0850204</anchor>
      <arglist>(const OsiSolverInterface *=NULL) const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>value_</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>aa79f780b613239e95150031c2c43128a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>const OsiObject *</type>
      <name>originalObject_</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a650e73a19488f805c7b4687c599664b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberBranches_</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>a9c64ebed4344b51f2659ef66892b6556</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>short</type>
      <name>branchIndex_</name>
      <anchorfile>classOsiBranchingObject.html</anchorfile>
      <anchor>ab84bd176b6a8f23b4b61b06fb7c52d3a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiChooseStrong</name>
    <filename>classOsiChooseStrong.html</filename>
    <base>OsiChooseVariable</base>
    <member kind="function">
      <type></type>
      <name>OsiChooseStrong</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a572a06e17624e2b70b1db31b824fca31</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiChooseStrong</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a6de380748d66f0ae330e705d8fbd16aa</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiChooseStrong</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a36627aee610156fa6cd0a1808dcaaa03</anchor>
      <arglist>(const OsiChooseStrong &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiChooseStrong &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>ad59364002b4a82bd926c2843d5fe960b</anchor>
      <arglist>(const OsiChooseStrong &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiChooseVariable *</type>
      <name>clone</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a4f96f2268912dd22143f6530c3aadd57</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiChooseStrong</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a8aa83a8d92ed5e4b9d78782c0ed75227</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>setupList</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a98b6ff42d3fd1bb56ec9c3cf568724c6</anchor>
      <arglist>(OsiBranchingInformation *info, bool initialize)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>chooseVariable</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a8752b4335d86799cf522eb55d1370dc9</anchor>
      <arglist>(OsiSolverInterface *solver, OsiBranchingInformation *info, bool fixVariables)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>shadowPriceMode</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a74057317cb50ca6811d4cb4be5029741</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShadowPriceMode</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a0ed2cee31189d3464984c418e68db339</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>const OsiPseudoCosts &amp;</type>
      <name>pseudoCosts</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a6178fd4cb76cc44ed6929a70101f75eb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiPseudoCosts &amp;</type>
      <name>pseudoCosts</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a630adead068cf86f8fd9db0973f4ebf0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberBeforeTrusted</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a2139596691b116105d2b9763a4e4d184</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberBeforeTrusted</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>ac86c842cb5341491feffb741c55bf4cb</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberObjects</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a3ed777a6f5933dbedda157986b06e711</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiChooseVariable</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a78b2cfe9348b2bbf1d0423c274c81067</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiChooseVariable</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aa4c8a7b4b67775e75f941631d411a9d1</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiChooseVariable</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a328d503d3036833622cd0ca1db1d06db</anchor>
      <arglist>(const OsiChooseVariable &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiChooseVariable &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a4be86e4d137d0cfae37709a0eb1264d0</anchor>
      <arglist>(const OsiChooseVariable &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiChooseVariable</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a870a216e51d08dc205e841102b276378</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>feasibleSolution</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a95d9f415c0695efa7462d59f9c9fff63</anchor>
      <arglist>(const OsiBranchingInformation *info, const double *solution, int numberObjects, const OsiObject **objects)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>saveSolution</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a5f4c1a3ce1e12fb16304c81f9e5a4b36</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearGoodSolution</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>acee7e5c2845d4f96a008dc577d9a6d9f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>updateInformation</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a2848426ba2ce5274523c77087ef091ec</anchor>
      <arglist>(const OsiBranchingInformation *info, int branch, OsiHotInfo *hotInfo)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>updateInformation</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a74ebaa849860eca50dbc51b71276be48</anchor>
      <arglist>(int whichObject, int branch, double changeInObjective, double changeInValue, int status)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>goodObjectiveValue</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aa7f400c7fe0722a6f22b2cea1b21bc01</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>upChange</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a7a0a247562e3825b50cd9c50ca7f491c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>downChange</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aa03beadcfe00bbb6da27a3255e5bbcea</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>goodSolution</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aac720b30a670b33612d009838d676cb0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>bestObjectIndex</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a059c6e3de22aabc7904bdd022f0925d1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBestObjectIndex</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a37c6f3c4c7c9d69d346206ccd08b8138</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>bestWhichWay</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a65a2247b8c800cf90c67bb844732c478</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBestWhichWay</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ab90037b9d5c5f906af17d452eff60b4d</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>firstForcedObjectIndex</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a9133ce50200f9c34f301262ef5f6bd4a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFirstForcedObjectIndex</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a311adf73867df0a197a602f4c1ae514e</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>firstForcedWhichWay</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a4399f030f44307a3554f6d39f9130098</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFirstForcedWhichWay</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aedb86a79082b991a030355672c812fb9</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberUnsatisfied</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ab4ee245c6792fd511bf1fe8cbd19455e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberStrong</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a97efd9b15a9f282c815cace3d6bfd21f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberStrong</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a24b2c72007502aafd2dfb7baca95b4fa</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberOnList</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a28f43b5e47a3bd5dbefa58ba98c3d196</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberStrongDone</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a282b28b68fa2b17749df36416d603fb0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberStrongIterations</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a65d683de9f39ddafd5b63f64cea49240</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberStrongFixed</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a37a29e5d1d64d96a71d8d562d60f387f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>candidates</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a40db58a66422a214ffe3066cf92abb20</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>trustStrongForBound</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>adad7bb9a4686cd04bf878ae8e39b3bd5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTrustStrongForBound</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ab11b7301d57f9300a30416e1999cbe84</anchor>
      <arglist>(bool yesNo)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>trustStrongForSolution</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a0908530e7c99650450ae75822d8983e8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTrustStrongForSolution</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a1017f8cc9d4a50bc3e3ddd66644fa530</anchor>
      <arglist>(bool yesNo)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSolver</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ac6c6b9d418ed471c53fa83b92ab33972</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>status</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ab68ca4e3c157f779c9cc6300bf497e41</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStatus</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a1b497d54335243115fdbfdab1e7d2d6e</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>doStrongBranching</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>ae91ddf7defda6b795d5086db90d268bf</anchor>
      <arglist>(OsiSolverInterface *solver, OsiBranchingInformation *info, int numberToDo, int returnCriterion)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>resetResults</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>abe15b5f1285e51c99e0a6cb56d488e16</anchor>
      <arglist>(int num)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>shadowPriceMode_</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>aa6b5b5b2b649effa78287b082995f8cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>OsiPseudoCosts</type>
      <name>pseudoCosts_</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>acc2c9a9f2527cac7ef0399e13d9f733a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>OsiHotInfo *</type>
      <name>results_</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>acee7a26b9aaed748e4a14ca40501c4d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numResults_</name>
      <anchorfile>classOsiChooseStrong.html</anchorfile>
      <anchor>a490a0f1f337500c84794b7a1af1cc772</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>goodObjectiveValue_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a849edcb23e95dcea1132d650de8e1623</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>upChange_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>acd273802a09d0a3420ba49cd7a3d77b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>downChange_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ade011e18984b51d4323752832a2de773</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>goodSolution_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a2a03f27c38c80796d74167602f411165</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>list_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a7990917f38926fc795c00bd0e6cd721a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>useful_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a999ccbe861b9f435c9e8af408ad2762c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>const OsiSolverInterface *</type>
      <name>solver_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>acbbf312e7e9d95a87fa5f275da46c982</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>status_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a66cb86ec5613d90526266ba965968232</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>bestObjectIndex_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ac50340eb00467a8edb375e2126170bce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>bestWhichWay_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>af406bec9f597ef52cc53dac1e9f20130</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>firstForcedObjectIndex_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a9e70a9e456badd8016bda21e3f7d6b51</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>firstForcedWhichWay_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a9579dcd7190c7d47df23e9dc24c52bd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberUnsatisfied_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aabfa888e577044e098d1a11d0dff709b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberStrong_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ac7004f02578da08888fc4cb9e5e0448b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberOnList_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>acfa802c8d0a09fc7a0f77fbf349d1c5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberStrongDone_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>ae033acf7e694b85dbcb45b5d531ce4a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberStrongIterations_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a6dd57aa3a0699175d2250eb82320666b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberStrongFixed_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a73797ccd5ec85be5fe1a5232db180680</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>trustStrongForBound_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>afdb53f86aa2645bf43aeca363e142c69</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>trustStrongForSolution_</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>aa1d092bf97d1c99c98d7254defd964d5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiChooseVariable</name>
    <filename>classOsiChooseVariable.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiChooseVariable *</type>
      <name>clone</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a3e1f1bfaee589ae56419fc88ec548a15</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>setupList</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>a9d2523dee109dc61f04061754a1e98a3</anchor>
      <arglist>(OsiBranchingInformation *info, bool initialize)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>chooseVariable</name>
      <anchorfile>classOsiChooseVariable.html</anchorfile>
      <anchor>afa5c4c39e963558e2daa1412763f3f4d</anchor>
      <arglist>(OsiSolverInterface *solver, OsiBranchingInformation *info, bool fixVariables)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiColCut</name>
    <filename>classOsiColCut.html</filename>
    <base>OsiCut</base>
    <member kind="function">
      <type>void</type>
      <name>setLbs</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>aab3f066661ed9225842b20658675e85e</anchor>
      <arglist>(int nElements, const int *colIndices, const double *lbElements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLbs</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a8daf1df9c8539440127c587b96b910ba</anchor>
      <arglist>(const CoinPackedVector &amp;lbs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setUbs</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a57e596746d9244045fc3633cf708fed0</anchor>
      <arglist>(int nElements, const int *colIndices, const double *ubElements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setUbs</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>ad18d72f8870084d8c573eebd041c5930</anchor>
      <arglist>(const CoinPackedVector &amp;ubs)</arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedVector &amp;</type>
      <name>lbs</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a3829d43533e1779249f9dc7a3cb7bb0c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedVector &amp;</type>
      <name>ubs</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a3660b58ea3549d7072c1a19cc596bf61</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>af2b6b3d43db7189c8c5499bd8abdb9b0</anchor>
      <arglist>(const OsiColCut &amp;rhs) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a08bdc6a7f92574f3e7d19899872f40d3</anchor>
      <arglist>(const OsiColCut &amp;rhs) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>consistent</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a3853a051c0d58bf16255e2b38b0e0254</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>consistent</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a46e108fffe23871048d030f1bc362aeb</anchor>
      <arglist>(const OsiSolverInterface &amp;im) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>infeasible</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a02134895e6259ed4c1166bb70faf8e50</anchor>
      <arglist>(const OsiSolverInterface &amp;im) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>violated</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>ac263975539833acb99574a05e7797575</anchor>
      <arglist>(const double *solution) const </arglist>
    </member>
    <member kind="function">
      <type>OsiColCut &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a068bfc2311c62ff00610ed6f7c1328a3</anchor>
      <arglist>(const OsiColCut &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiColCut</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a3f6c018da33448cf2058fd5a44a83430</anchor>
      <arglist>(const OsiColCut &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiColCut</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a462ad43a3eb329bd238120252e2adb81</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiColCut *</type>
      <name>clone</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a6560488bef4ddc601b1a7d82d97c009d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiColCut</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a751ecec7a4d44184870d60e473654ea1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>af01e108c68a331d4a59954f542afded6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setEffectiveness</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a6805d6719ea0aa475c6698a7ae4bc7b8</anchor>
      <arglist>(double e)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>effectiveness</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a83382df88c95f307cd7929b7f1d88806</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setGloballyValid</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>aab34993094d2007c484df6fd27521311</anchor>
      <arglist>(bool trueFalse)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setGloballyValid</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a6b09a10bba4be34487de8d167f28bf07</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNotGloballyValid</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a59822f86b59b6d932972f48944e1787e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>globallyValid</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>ab65858b57a2b912402fc7202e2c971fe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setGloballyValidAsInteger</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>abc1f3c32ca67f5710ddda63eff193950</anchor>
      <arglist>(int trueFalse)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>globallyValidAsInteger</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a7e975acda830dbbbd5e7f381998196e2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>aa88e4c9c92407a2a499ca1c9b3766737</anchor>
      <arglist>(const OsiCut &amp;rhs) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a8e77fc344e8f01d22f8e5721c79c5ce3</anchor>
      <arglist>(const OsiCut &amp;rhs) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator&lt;</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a07c3bdb6522085230cd2a4b6a0e6fc94</anchor>
      <arglist>(const OsiCut &amp;rhs) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator&gt;</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>af67554ed722e7c55c5ab7838cc05f5b4</anchor>
      <arglist>(const OsiCut &amp;rhs) const </arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedVector</type>
      <name>lbs_</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a15c8baaac2466a41f3e80051d929db14</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedVector</type>
      <name>ubs_</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>a64dd7488a109bc9727589a4642a6c735</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiColCutUnitTest</name>
      <anchorfile>classOsiColCut.html</anchorfile>
      <anchor>aed6f54762016a69a0b4fe42d0d2e639c</anchor>
      <arglist>(const OsiSolverInterface *baseSiP, const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>OsiCut</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>ab10c6e0fdcb225533ae34c5b0a71e05c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>OsiCut</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a023a947fa97381815bff21aff5daa32c</anchor>
      <arglist>(const OsiCut &amp;)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>OsiCut &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>ab9077ae7b97d0914f60e3043153dfcd8</anchor>
      <arglist>(const OsiCut &amp;rhs)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiCut</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a460b2a66aa8e8feaf62953c2b4bd44b5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiCpxSolverInterface</name>
    <filename>classOsiCpxSolverInterface.html</filename>
    <base virtualness="virtual">OsiSolverInterface</base>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ac9920bdce9bf39e9fd666349b9a7c769</anchor>
      <arglist>(double s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae1f13fccd96f36394c44e61a8ab13b21</anchor>
      <arglist>(const double *colsol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a671d0b810da51563770c7d94685d265f</anchor>
      <arglist>(const double *rowprice)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getCtype</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a99259cf3211c25b4b71927eddfdb6b93</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af952289282c26727a0f19fde1b3ebfe6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae4d57d4484407ab4c5d47868e54a33e9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2c090a522465b7d57b0d8f817555e271</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ab07a4012fb457a1cc31d7e0d4aaf09ff</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a65427042b79c2b8df27e0d6bb504ca0f</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setStrParam</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a9c4341e8b9370af10ca162cbc0426900</anchor>
      <arglist>(OsiStrParam key, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ac4351607a56f0ffc06818a35a6a43efe</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ad7f97361579f3f386f36fb54c9663b14</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7effda4f9b313b84f1066d25b3900509</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMipStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2438bbeac54b08e6e5bd171566443042</anchor>
      <arglist>(bool value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getMipStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae32309614ef2551d9ebd8b9f3134fcaa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a6c1bf08618fde90d30528eb5e4c54a7b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adc484fb79475f2eb1b2f454f406b9e05</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae8328545a7c1d7ff41495b716a116c75</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a40a4f76ee66513af3d7ad5af5abebce9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isPrimalObjectiveLimitReached</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af29c19247cf55d8eb66cee1ab1fa5669</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a589a67468b9eab7e1ce59993f79295ba</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a1f41c37350e8d18b2d3b860007ab0ba6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a744907e7fe7e4148320b94ee97ff3f1b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ad7cc5d099cb97e1e841e79399c6bcf06</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a8dcdc24851ff4448438e9669b6ba6029</anchor>
      <arglist>(const CoinWarmStart *warmstart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aeacf60cefe20499e4f151f085b508dc7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>acd2ba4c970541160124314c4b3f8341e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a288071e7d96b5f623f3dbc8167c28aae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>acf83c70f864e56c602c38a85e6ffe7c2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>acaf1b6109fbcb065161deb2cdd0b5f59</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a48a6ba2656fe7009313317c76e480b79</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2898e51234ba477a712d9cdd611823fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a97674345ceb43286809dc09c1c439ef5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adb5e9bbee3b069c182a442802c711077</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a95d823dad9ad6ce657550ffa10c4291a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7973ebaa68ba81fd963ed48190e90269</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a3d2f86c62bb3c92213b8151267326681</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a91375853931903c6fb82d46e09a9858f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae5a6f73d2dff4f950ad5c1e5b0896153</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a508cd2f447f53df703e56f50a7d8977a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a46fbc6f7b5750acff5881c8f8a9fd4f8</anchor>
      <arglist>(int colNumber) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ac70ee94175e03d8cd2e9d9c11fd100c5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aa13b6ec91473b680823ad005639a78b4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>afe0f96c29471aa36bbe87fa6c3ba89a5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aed3e75d45eaa0846a649a7f0fa8548b9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a16f0391e49b9c776ba84f17f78a52ef3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a39409f009fa528794199ab39e1308a99</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a01280e192fe38fdc7e8c1f204568d3a7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a95b35b4137d26d0613fd947f78748632</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aa55303f5b83701d6e2b5ec7f3d78e831</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a9e64a45dd02cf8d135ca931fca7feb9f</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a6d602573074b2bc715b647c5745682a2</anchor>
      <arglist>(int maxNumRays) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a9b87fe27b8ed98a65aec5ee75cffea93</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeffSet</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a217f81ee579eb784915e54320fff0007</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *coeffList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ab848b5a274c25e3571daaa26c446c17b</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adc2a2a200ed25115cd86901236bd38da</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a517f9cdc39da27df8c5946c6cd8686b8</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSetBounds</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a516a0cd5eba5c92844b32f553906094d</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a10beca1c2ac15b9dfe5ce83c44b6ee19</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae78d0c7b47b559f949c7f135f91b7d45</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a09ffc9feba0f064dfe9008ccd1247d2c</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aa65b001f2287278ae0941baa084f3ac4</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetBounds</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a0c5bc39e0b7bdd03c6b59bd00a80be29</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetTypes</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>acbf229c63fd79960f25522cd1e22fbfc</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const char *senseList, const double *rhsList, const double *rangeList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7edd27063f80117ab054374129e18031</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>afb5fa1474fdba3ba1fcf8d210a0ccb17</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a663a08b6acfe559d241ea03a2217a287</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a488eebf7dee879bdcd05046e447a7dfb</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a1b1341cef44d0df3254271f69bd7e717</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>abda1920d223789bc2c029de2655815a7</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a5c8345d3edf34fe6289c303261518f3e</anchor>
      <arglist>(const int num, const int *colIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a62da3b3180b970021e0492b908c625fe</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a86cf0e015ebe2a90f94ff2b1e243c0ce</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7cd1f4b868274f327cbb56b8b9322927</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a43ad066ca2e243234c97384a98c4f11c</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a39563d9cb63232fcfd3db38339636a07</anchor>
      <arglist>(const int num, const int *rowIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2de4d829463e84c9e5b2bd3bd3a84c30</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a06b38117a1f502488e343ae83ad375df</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a8440992e560b43fb28facd58ab24a028</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2aa35840958333a0508e8ef8100598ed</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a4c41c95c46652ca2e4f6659487265285</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2684a064697c21f2a33b4ae61b27320f</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a395a0f8d85374b23d3d81bbea38fadaa</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a76d74a5d93908e662c49c36ad4906cbb</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>passInMessageHandler</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a54b95df53b859c309a201a27faf13890</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiCpxSolverInterface</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a1006a9f1e014c9980554fd73037c1bc9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ac388eed7673420b37ac24b7d6ff58ffb</anchor>
      <arglist>(bool copyData=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiCpxSolverInterface</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a51855fd1c1e9310b13c184d76674ded9</anchor>
      <arglist>(const OsiCpxSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiCpxSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a060f1d094cc60bb1f775d114353b020a</anchor>
      <arglist>(const OsiCpxSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiCpxSolverInterface</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7ba433b56152d4f9829d43c5a7d67904</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>reset</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a46712812aacec42c61f119f1cfdd9931</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>canDoSimplexInterface</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a2cd8f59de48ee6cd117f28572b66503e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enableSimplexInterface</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aa1d936b605186ac43d51acc58983fa82</anchor>
      <arglist>(int doingPrimal)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>disableSimplexInterface</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a0d47ba60f9d112105ed2cb6b1614ee4d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enableFactorization</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a8adc670c9bbfc046904ad65aca0dcbf6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>disableFactorization</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a3a4a1518334db557ff24bec926db82e5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>basisIsAvailable</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a496c5ffbaf4e2f0946450e932a42523b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBasisStatus</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>abbc0c75789d3e9cc28076d23686f6e71</anchor>
      <arglist>(int *cstat, int *rstat) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvARow</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a15f7bc7da7232722ebac14fdcbc86162</anchor>
      <arglist>(int row, double *z, double *slack=NULL) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvRow</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ab8764cf6ac5f7e5d44b0e205a4c595be</anchor>
      <arglist>(int row, double *z) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvACol</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a0487e65e378651d3eff9d012fc30f664</anchor>
      <arglist>(int col, double *vec) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvCol</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>afd35e8c1257ae4ddbee8425156d5dc27</anchor>
      <arglist>(int col, double *vec) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBasics</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a578feb5dea1befdfee8e0c9d9351d361</anchor>
      <arglist>(int *index) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>switchToLP</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a96e6d6e45dcae0a820a8eca34477ec2c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>switchToMIP</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a79a5ff7f8b60491056a5652851acd8b8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setHintParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3bd13c47a7c0ecd2c7aa68f09126a9cb</anchor>
      <arglist>(OsiHintParam key, bool yesNo=true, OsiHintStrength strength=OsiHintTry, void *=NULL)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>getHintParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a990d199e2677d35ba7d5d7064ff805c8</anchor>
      <arglist>(OsiHintParam key, bool &amp;yesNo, OsiHintStrength &amp;strength, void *&amp;otherInformation) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>getHintParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2a9be8120d683214254ae7c3505d9fbb</anchor>
      <arglist>(OsiHintParam key, bool &amp;yesNo, OsiHintStrength &amp;strength) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>getHintParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a277b9fa31e97bb15d3aad286a0fcf366</anchor>
      <arglist>(OsiHintParam key, bool &amp;yesNo) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyParameters</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>abe89f57e960884fee498218a15a52e39</anchor>
      <arglist>(OsiSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getIntegerTolerance</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a929f1824674691f7d0386dbbf1dd853b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getPointerToWarmStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a243a73c2c4741788382cc5675c574214</anchor>
      <arglist>(bool &amp;mustDelete)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumIntegers</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0070b94557adb4e2af0b46e9a512ee67</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isBinary</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a568df0452193c8ada8a991e810ab0038</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isInteger</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>abd44896d6d3b1aac453d2f6d55c02edd</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIntegerNonBinary</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a6aaafde5bcdbf7f341dbd70adc761969</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isFreeBinary</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a87fdfc647fa26710837b32102626dc5e</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnType</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aff120e7f058230fd2434f76b019e1218</anchor>
      <arglist>(bool refresh=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getColType</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a131176ccb91020293b83b47bab14cfe1</anchor>
      <arglist>(bool refresh=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinPackedMatrix *</type>
      <name>getMutableMatrixByRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad1c55f7e2ba0af17434dab431061b0dc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinPackedMatrix *</type>
      <name>getMutableMatrixByCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a730918c2acb9013c714e2928b1cbc3ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getStrictColSolution</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad11c86fc262294ea4540cf3ed68619e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiVectorInt</type>
      <name>getFractionalIndices</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a938f029a96f7ad86243e387773d32a64</anchor>
      <arglist>(const double etol=1.e-05) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjective</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a9cd81727718486fbb7d7729e389a599a</anchor>
      <arglist>(const double *array)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af400534680816396b981b7d7a8455005</anchor>
      <arglist>(const double *array)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a72843ad6192a5e5db5891233d6a508ad</anchor>
      <arglist>(const double *array)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>reducedCostFix</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa808d84586b80a1903ff38ad1c34b792</anchor>
      <arglist>(double gap, bool justInteger=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>dfltRowColName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ae117f25dda4abaecbb51cb1f4d8fb5d3</anchor>
      <arglist>(char rc, int ndx, unsigned digits=7) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getObjName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>acbe883a26ca545a6f3282a2bf8a09406</anchor>
      <arglist>(unsigned maxLen=static_cast&lt; unsigned &gt;(std::string::npos)) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a13239c4587919519e1cedef06b540873</anchor>
      <arglist>(std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getRowName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a7e2a3cca44b6f9ac8d049d84d69c1f3f</anchor>
      <arglist>(int rowIndex, unsigned maxLen=static_cast&lt; unsigned &gt;(std::string::npos)) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const OsiNameVec &amp;</type>
      <name>getRowNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a65de24b5d811ed89cba44b9f9dab58d2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a6e6efff02408e8dbdce0aec25f418350</anchor>
      <arglist>(int ndx, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa40560b926caed539c3bbee85b91e472</anchor>
      <arglist>(OsiNameVec &amp;srcNames, int srcStart, int len, int tgtStart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRowNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad2f74438ef7a5a603e45b6729a7c2eb7</anchor>
      <arglist>(int tgtStart, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getColName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8919b1644a144753c3fa83eb9bffac7c</anchor>
      <arglist>(int colIndex, unsigned maxLen=static_cast&lt; unsigned &gt;(std::string::npos)) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const OsiNameVec &amp;</type>
      <name>getColNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a71283d3076c9bea77f1f73301e98ff30</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColName</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa2ec42a9c64a7b47b534fd1c8448e8cb</anchor>
      <arglist>(int ndx, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>abd2af8264c4ec0b7fff92a48849faf1f</anchor>
      <arglist>(OsiNameVec &amp;srcNames, int srcStart, int len, int tgtStart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteColNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a1af2ecfd5ee40b7a002a7e3657c4be58</anchor>
      <arglist>(int tgtStart, int len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowColNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a62d64bde76f0e608c2d6efba7e93f56f</anchor>
      <arglist>(const CoinMpsIO &amp;mps)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowColNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad4f1228bdb21b33633032ab645e5ff02</anchor>
      <arglist>(CoinModel &amp;mod)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowColNames</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a17f15c2dda007348051f52e74d187aa8</anchor>
      <arglist>(CoinLpIO &amp;mod)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a1e45ed1697119c6a1bfe51c9da8544ba</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0de24e290845312bf17b0c2f0d7c029b</anchor>
      <arglist>(int numberElements, const int *rows, const double *elements, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad9dc21e813287b50b4bdc6a516fc73e4</anchor>
      <arglist>(int numberElements, const int *rows, const double *elements, const double collb, const double colub, const double obj, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a318099cce687e5d7101a39cac340c44f</anchor>
      <arglist>(const int numcols, const int *columnStarts, const int *rows, const double *elements, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addCols</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aca60f8a78babc2a9f6584d1475d336f9</anchor>
      <arglist>(const CoinBuild &amp;buildObject)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addCols</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5e9e794e31ab92ac4e476924bdb2fac5</anchor>
      <arglist>(CoinModel &amp;modelObject)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8007140b8d07605789f9524f024f9ee7</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad89b9a64d02dd59903ae60f31fbcdb54</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>adecc389126e33041b00107595b2179b0</anchor>
      <arglist>(int numberElements, const int *columns, const double *element, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a7a9e28b93c14ec3ab3d291d1ab55207d</anchor>
      <arglist>(const int numrows, const int *rowStarts, const int *columns, const double *element, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>abfaad7fc8ce5c386758c6d8c5e333efc</anchor>
      <arglist>(const CoinBuild &amp;buildObject)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a6805bb6b6b4418aa27dc59c0f65327e7</anchor>
      <arglist>(CoinModel &amp;modelObject)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>replaceMatrixOptional</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a43a7d96a233b2410a86b82a8e714b0d2</anchor>
      <arglist>(const CoinPackedMatrix &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>replaceMatrix</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>add3e73b0c1edc3e279d68a06be1180c4</anchor>
      <arglist>(const CoinPackedMatrix &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>saveBaseModel</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab3517030cb21592b970e8d7fb987ef65</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>restoreBaseModel</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a6139921e03b3e45a5a94e71970f70b96</anchor>
      <arglist>(int numberRows)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual ApplyCutsReturnCode</type>
      <name>applyCuts</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a81c095e029d1a00f1d24a7131d4a8cc2</anchor>
      <arglist>(const OsiCuts &amp;cs, double effectivenessLb=0.0)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCuts</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a60a79e6dde98fad5bb2bb20b5444705e</anchor>
      <arglist>(int numberCuts, const OsiRowCut *cuts)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCuts</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af7d909fc2fafc9fef6045e86696614a9</anchor>
      <arglist>(int numberCuts, const OsiRowCut **cuts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteBranchingInfo</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aec5556a81f2606adf4b3ec6575bc8bfc</anchor>
      <arglist>(int numberDeleted, const int *which)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>loadFromCoinModel</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>afb2bcd68ebf296cf40003896a267c31a</anchor>
      <arglist>(CoinModel &amp;modelObject, bool keepSolution=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aec8a22eab5e244f372f3dfa83e1cd8a1</anchor>
      <arglist>(const char *filename, const char *extension, int &amp;numberSets, CoinSet **&amp;sets)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readGMPL</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8ab1c82480b4743bdb32a6a17ee9f400</anchor>
      <arglist>(const char *filename, const char *dataname=NULL)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeMpsNative</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ae9046fe7759aa7208b1532ebca9cd5a4</anchor>
      <arglist>(const char *filename, const char **rowNames, const char **columnNames, int formatType=0, int numberAcross=2, double objSense=0.0, int numberSOS=0, const CoinSet *setInfo=NULL) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeLp</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a060a2bf323cdb45c483fab35758cd214</anchor>
      <arglist>(const char *filename, const char *extension=&quot;lp&quot;, double epsilon=1e-5, int numberAcross=10, int decimals=5, double objSense=0.0, bool useRowNames=true) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeLp</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af86d1a2d12711ecf0d5064a958233e4f</anchor>
      <arglist>(FILE *fp, double epsilon=1e-5, int numberAcross=10, int decimals=5, double objSense=0.0, bool useRowNames=true) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeLpNative</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0a992fb0e472e11b6571f585b8aafb0b</anchor>
      <arglist>(const char *filename, char const *const *const rowNames, char const *const *const columnNames, const double epsilon=1.0e-5, const int numberAcross=10, const int decimals=5, const double objSense=0.0, const bool useRowNames=true) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeLpNative</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aeae556294243ed2b3de0d4a624b3a7af</anchor>
      <arglist>(FILE *fp, char const *const *const rowNames, char const *const *const columnNames, const double epsilon=1.0e-5, const int numberAcross=10, const int decimals=5, const double objSense=0.0, const bool useRowNames=true) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readLp</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aaa666c503d4b485c1346ade3b285f555</anchor>
      <arglist>(const char *filename, const double epsilon=1e-5)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readLp</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab07e1f6bb61a0f23503013c6b48285ec</anchor>
      <arglist>(FILE *fp, const double epsilon=1e-5)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>differentModel</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad36731c738439e7072f1afc3225eb7b2</anchor>
      <arglist>(OsiSolverInterface &amp;other, bool ignoreNames=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setApplicationData</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3ca390951a73f878d5b2b203ab85d940</anchor>
      <arglist>(void *appData)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAuxiliaryInfo</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3988c6a9496861a673c4e557820cc018</anchor>
      <arglist>(OsiAuxInfo *auxiliaryInfo)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>getApplicationData</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af2d4e3c4bc709c94ee0a628f4a1fb219</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiAuxInfo *</type>
      <name>getAuxiliaryInfo</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab6b13ae3ed41025a962f34a0f2d53ef5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>newLanguage</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a65b365a6f2350cf21c5e08650737c8db</anchor>
      <arglist>(CoinMessages::Language language)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLanguage</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a911610acbc75f4fad8447b7922f7bde4</anchor>
      <arglist>(CoinMessages::Language language)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler *</type>
      <name>messageHandler</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a534f993e842007f5cb2fbc380cf381a1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinMessages</type>
      <name>messages</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0c67fbf4fc2eac97787f2d85ec9dda50</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinMessages *</type>
      <name>messagesPointer</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ada849d14278c890e89c47d406f064f2a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>defaultHandler</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a591630b0933a1e420955d8f397aa1c99</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>findIntegers</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2884068394fd54cbe434a1da4c1e83d4</anchor>
      <arglist>(bool justCount)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>findIntegersAndSOS</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8a9630129cc557f81bf7d2c5e5e282b3</anchor>
      <arglist>(bool justCount)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberObjects</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>afc67912239d00aec8ec778b8d8aa89f3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberObjects</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a132363742f82f5518858dc4c7dfdc717</anchor>
      <arglist>(int number)</arglist>
    </member>
    <member kind="function">
      <type>OsiObject **</type>
      <name>objects</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a4d13f65371d4594041e7ba521bad4993</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const OsiObject *</type>
      <name>object</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a9920f20e95708bcdfdabb319697a9dfe</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>OsiObject *</type>
      <name>modifiableObject</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a4d49559ef7172752dc9bfbfe919086fa</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteObjects</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab2ef672c18541dde3845066a271d9594</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addObjects</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8afc1ab3b5fca0aa630c479b40b21dc1</anchor>
      <arglist>(int numberObjects, OsiObject **objects)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>forceFeasible</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa49a2362de82f3c2f3eedc6e82407398</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>activateRowCutDebugger</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ae7242db8c244161c1777132fec421bce</anchor>
      <arglist>(const char *modelName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>activateRowCutDebugger</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa66544c0e698a9237e42c506f1357e68</anchor>
      <arglist>(const double *solution, bool enforceOptimality=true)</arglist>
    </member>
    <member kind="function">
      <type>const OsiRowCutDebugger *</type>
      <name>getRowCutDebugger</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad353f882122966ae6e74f325024d82cf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCutDebugger *</type>
      <name>getRowCutDebuggerAlways</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3b95b1fa95a69aac429fc4ded10f98c9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>optimalBasisIsAvailable</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5e5e94e81a1713c438c70dc182725705</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>setBasisStatus</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2b98d8caac4d9f3ef34e32f92af285d9</anchor>
      <arglist>(const int *cstat, const int *rstat)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getReducedGradient</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af64aa3109e1f7ecbb0707486d92fa5d5</anchor>
      <arglist>(double *columnReducedCosts, double *duals, const double *c) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enableSimplexInterface</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af23647710568896bf5148d17e4037387</anchor>
      <arglist>(bool doingPrimal)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>pivot</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a99d7bc9cb648cbdd798898b066f04388</anchor>
      <arglist>(int colIn, int colOut, int outStatus)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>primalPivotResult</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ae852ee9f3f1c46c5471ea6ea3bf267c7</anchor>
      <arglist>(int colIn, int sign, int &amp;colOut, int &amp;outStatus, double &amp;t, CoinPackedVector *dx)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>dualPivotResult</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2420ab683725e39911aa69d51f4bcfe0</anchor>
      <arglist>(int &amp;colIn, int &amp;sign, int colOut, int outStatus, double &amp;t, CoinPackedVector *dx)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverInterface</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a4d99653b178c2e7b673a3b56fad65ad5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverInterface</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2360e7664739224035fd9e3695a947db</anchor>
      <arglist>(const OsiSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a711bfeba5775cabcf34031cc5257eee7</anchor>
      <arglist>(const OsiSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiSolverInterface</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a492c86c789fce5155883479a746f4bb0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>CPXLPptr</type>
      <name>getMutableLpPtr</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a97b557d4e5ed569f9ff435f605f1a3a2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>CPXENVptr</type>
      <name>getMutableEnvironmentPtr</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a821967b5886ae21d1c341b231e78fe25</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a83902fa2387ad5ffae9a2c6069113124</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af06285ead17af9555d3f77e9925540d4</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>convertBoundToSense</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a01d347dc7e3e6f5c278b4aeaa88a96a0</anchor>
      <arglist>(const double lower, const double upper, char &amp;sense, double &amp;right, double &amp;range) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>convertSenseToBound</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a47c1ab062d5a8d7a447c1c54792fafc2</anchor>
      <arglist>(const char sense, const double right, const double range, double &amp;lower, double &amp;upper) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>T</type>
      <name>forceIntoRange</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5d4da89b1f20265cff582a9f1dee7e8d</anchor>
      <arglist>(const T value, const T lower, const T upper) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>setInitialData</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8cb2124f71bc3dac7bbe31a4c1b0ba85</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>resizeColType</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af63a05b9a516c14e9e2650f87aa797d0</anchor>
      <arglist>(int minsize)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeColType</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ab8c1ae7d9e1b838b364a2a35c430628d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>abf896043a19087a6d4e1d9b20d61aaaf</anchor>
      <arglist>(const OsiCpxSolverInterface &amp;source)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfConstructor</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adbb18d45d0163095e5de81b447fbd405</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a4fd8b42fec3838bed18a633058cf4f3c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedColRim</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adce2362132bfec303333c79407334155</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedRowRim</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a61be632ffbd68dd3498b422935bb51c6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedResults</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a3123c488fce008e5f901cba99b782dad</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedMatrix</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a722d4d0c575492ccd72a7f4787351700</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedData</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a02062071c13b6334f8684347e370ed3e</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeAllMemory</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae48d9424c82570ec1b281595a477c585</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CPXENVptr</type>
      <name>env_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a233e9b5f8cb71c554057f2202ec99cb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CPXLPptr</type>
      <name>lp_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7ea38902771a488989fae173a3841407</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>hotStartCStat_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ad839811f3bbcb57ba966a7658a650788</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartCStatSize_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af978699dc167c8a6278f2f69f26fe8c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>hotStartRStat_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af59162d3425549309c268f8ba9a14bff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartRStatSize_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a71f7d25f802532c3f83228d459ab268f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartMaxIteration_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>abb831114de05eefbce0400ffd37838bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>obj_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aaae1e85ed74b46f3708abb397e551392</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a3d87eae48be6bf6c442ebddf5af3afab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a739d16efd038883e2ffb6368cbc343fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a1eaa2f04d7fc27b10977755e0226744c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ae5199a47567c1ee9a5c20bddde22584b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a52f516d69388335894adb23aeb0bf49d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adfdd15bf206c6af269e6321a78cdb864</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a80d21782f15ebd32fae311af82d9e70d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colsol_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a4b1e2cbbc5f08f27bf7835de6caa644a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowsol_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a13e589293d4765d101a1e41619dbb02c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>redcost_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a7fb1f4f2d72c15834e3fd2917c059f85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowact_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>adc3142903cdfd5c70323d565c16e3eff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a814f7daf6f02c70db51f0c535bedba5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByCol_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>af71c196260353d5e32149b36e295d65e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>coltype_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ab1a1dfd35542c7db644063b16215ed59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>coltypesize_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>aa4895b0a53949a68744ec7391268a306</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>probtypemip_</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a8f891a2d2068ec746ea6aa6cd15125ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>domipstart</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a8c246c1b6c5cfc45df21cd76e8c15b51</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>disableadvbasis</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a29125c5acd606c8ae630e182f4ee4957</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiCpxSolverInterfaceUnitTest</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ac7f9bd9ce9661ba69a99adb2cb059bd7</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>keepCachedFlag</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_NONE</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39da8b8157aca2012c89c8906ea786823743</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_COLUMN</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39daa97c8f4c4192f872a3e91f429b343cfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ROW</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39da69d289155ae7ce2ab05ff0fd2d4011ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_MATRIX</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39dadf9cde5b897c7395208ce16161c22f71</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_RESULTS</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39da0aa57798b2ba1e92cbe01423bd49a85f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_PROBLEM</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39dae2d4d0a796d94ba343e323c469ed75d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ALL</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39da4d2e8e3cd6d4f1a40b43928f2fa76e5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_COLUMN</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39da9daba9c2ba73bf9d046b1bfb326d7d79</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_ROW</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39dae6fb50db6137b84c7b2ea1e4026bbad7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_MATRIX</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39dac9e889818b699586ac64de2689e23c3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_RESULTS</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ada6d8473b12d4bf252cfdbef40b3a39da0a735af825a94de25411e1c0d5722ab8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>CPXLPptr</type>
      <name>getLpPtr</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>ab7f9431f389e12eec2158c128a8a4987</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function">
      <type>CPXENVptr</type>
      <name>getEnvironmentPtr</name>
      <anchorfile>classOsiCpxSolverInterface.html</anchorfile>
      <anchor>a5928fa8f19aca6ae36a9d84ba2d94945</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; std::string &gt;</type>
      <name>OsiNameVec</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0ba737b281781ebffb2d11b9e726de70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>OsiRowCutDebugger *</type>
      <name>rowCutDebugger_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a4eedc103078fb1bf09d3ccdeedc56880</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessageHandler *</type>
      <name>handler_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a16174ed1a5c2f36755a45a61d1cd2450</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>defaultHandler_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>afbfd0d0dddca7f0fc5d0bb7c2b9324fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessages</type>
      <name>messages_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a9d905c8f1aa4b1474dc2c34911ffaa7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberIntegers_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0ed14660d29b11f3506884c02b83b9e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberObjects_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0fd27726e07523d98496650617c129c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>OsiObject **</type>
      <name>object_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac000119de5c4509e90fde183026b9cfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>columnType_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab8e442b8cf18b716251abc586b05f375</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiCut</name>
    <filename>classOsiCut.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>ac837dc34c095228e0413635878b70e37</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>consistent</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a4094f67cc5522ff3a1f7515340251028</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>consistent</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>af0435a01d108b71cd3da041c720dc9da</anchor>
      <arglist>(const OsiSolverInterface &amp;si) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>infeasible</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a0d3020c7f139b3df1888dd3dbde25cca</anchor>
      <arglist>(const OsiSolverInterface &amp;si) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>violated</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a22888c96a72fb468bd267a26eb0271ed</anchor>
      <arglist>(const double *solution) const =0</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>effectiveness_</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a7b305233cb34b18931cfc154061dac9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>globallyValid_</name>
      <anchorfile>classOsiCut.html</anchorfile>
      <anchor>a079612579919bbe4df2e7c02ac48c275</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiCuts</name>
    <filename>classOsiCuts.html</filename>
    <class kind="class">OsiCuts::const_iterator</class>
    <class kind="class">OsiCuts::iterator</class>
    <class kind="class">OsiCuts::OsiCutCompare</class>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a545507d9cb9df4d06b44ebe8eac4dce6</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insertIfNotDuplicate</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>ad9aa905dbf0cffa047d4481f0c873344</anchor>
      <arglist>(OsiRowCut &amp;rc, CoinAbsFltEq treatAsSame=CoinAbsFltEq(1.0e-12))</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insertIfNotDuplicate</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>abdaca5154c13d8da2adef429ebd9a321</anchor>
      <arglist>(OsiRowCut &amp;rc, CoinRelFltEq treatAsSame)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a263907eea09aed5dd377703de04d51c6</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a143f5ca931e6fe24c0a711225d0a16f3</anchor>
      <arglist>(OsiRowCut *&amp;rcPtr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>acd77a961ea4b9cf3b9f3d3325f5ef675</anchor>
      <arglist>(OsiColCut *&amp;ccPtr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>aa4c41791e370a22596a1d8148b70cdde</anchor>
      <arglist>(const OsiCuts &amp;cs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sizeRowCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a728acdd7b83c7ecb663a86624a24d1f5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sizeColCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>adba6789a31cb98702713fadd93185d2f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sizeCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a2ee5e015304ec848345d8d69143f1148</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a9cf407dbb46d6bb27c457affe9834bd4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut *</type>
      <name>rowCutPtr</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a969ae16630784bc60a96deb88baf98ff</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>const OsiRowCut *</type>
      <name>rowCutPtr</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a9097fbe8e3c880ef99172b73aa376adb</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>OsiColCut *</type>
      <name>colCutPtr</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>ae82e81c6c8c03ba8870d9c4c3214e24f</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>const OsiColCut *</type>
      <name>colCutPtr</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a97a7d4237c872afb244ec20f366c8d0f</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut &amp;</type>
      <name>rowCut</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>ad8e19fededdd0c68ee46e970776c554f</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>const OsiRowCut &amp;</type>
      <name>rowCut</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>ae5f2146f1d10562d8b81af9998f88875</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>OsiColCut &amp;</type>
      <name>colCut</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a6846018217180bb9bbb877dac71bc56a</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>const OsiColCut &amp;</type>
      <name>colCut</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a2cdeaab4970b03aa4dd76c704f5781ac</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const OsiCut *</type>
      <name>mostEffectiveCutPtr</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a53e394599967ad6b3e1cbd000f566187</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiCut *</type>
      <name>mostEffectiveCutPtr</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a000f9fda58d205187222e3d8a574c267</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseRowCut</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>ad8ae3305fe7b676155e238637602c75a</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseColCut</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a62965c2e4aa47a394aa0308e6a96e635</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut *</type>
      <name>rowCutPtrAndZap</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>ab9f8df0d405ab9ac2df16210043a88d7</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dumpCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>acd90aa4d66432be0c8fd819b954139a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>eraseAndDumpCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a2a13278820e6c24d81911917a21507ab</anchor>
      <arglist>(const std::vector&lt; int &gt; to_erase)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sort</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a6cb478ba2c0daf08ac39162f85d1fa41</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>iterator</type>
      <name>begin</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a8519044a5afc89ff47d1ee9dba07deca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const_iterator</type>
      <name>begin</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a904d3d551a492ca0a1ef3faec51b21ca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>iterator</type>
      <name>end</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a663f7a9b8c7d1d731506ff6ee53fa9b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const_iterator</type>
      <name>end</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>afb146135d65e31bf035469e72b5b9157</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a95b7ec99ade32552fd8bb2da91c8caaa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>af3a4cd993b77f47d29de6b58e4bf8ada</anchor>
      <arglist>(const OsiCuts &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiCuts &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a2c591e4faaa92431c2c6c4f192e1d655</anchor>
      <arglist>(const OsiCuts &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiCuts</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>abcdc9f5196bfabdb6196269987f467a4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a305099ce471868a36f55b12e8cc0b824</anchor>
      <arglist>(const OsiCuts &amp;source)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a7b70226e8f7d60c7ddd9f6696e15ff6b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiVectorRowCutPtr</type>
      <name>rowCutPtrs_</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a49a9165dffceef8118b3e24142f868e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiVectorColCutPtr</type>
      <name>colCutPtrs_</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a6956d75e791f4242780484d9bd90374f</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiCutsUnitTest</name>
      <anchorfile>classOsiCuts.html</anchorfile>
      <anchor>a40f4dd50933870a94de23f49c17605ec</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiCuts::const_iterator</name>
    <filename>classOsiCuts_1_1const__iterator.html</filename>
    <member kind="typedef">
      <type>std::forward_iterator_tag</type>
      <name>iterator_category</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>abc10a3bd9b90cc5edb2d62cba85965e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>OsiCut *</type>
      <name>value_type</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>ae388f181e93362d916bb325e7dc47a37</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>size_t</type>
      <name>difference_type</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>ae9c0fc92eeb84a1b8a7ec58126c7df06</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>OsiCut **</type>
      <name>pointer</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>aa428e9b661a7a4c9dd58208c13627fed</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>OsiCut *&amp;</type>
      <name>reference</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a2c8fbad6d8ce0a2ace7f1d4a78b3ede4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>const_iterator</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>aaf2da12a3c4c400f5dae81ef1dffedb8</anchor>
      <arglist>(const OsiCuts &amp;cuts)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>const_iterator</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a6180c7c64e09213c07493cb146c7aeb4</anchor>
      <arglist>(const const_iterator &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>const_iterator &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a89d1185b75304725357ef40ab26526f4</anchor>
      <arglist>(const const_iterator &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~const_iterator</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a439fc8d81d79a4df2dcbd9c1c5113e18</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const OsiCut *</type>
      <name>operator*</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a0c84afc580c84464d6a1207fd0c6827b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const_iterator</type>
      <name>operator++</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>aae4ede2747779fdb10100ca7f575c40c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const_iterator</type>
      <name>operator++</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a2f27c8f6c98bd893995b6b340c35374b</anchor>
      <arglist>(int)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a402199982c9fbd12c7942c3814013afc</anchor>
      <arglist>(const const_iterator &amp;it) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>ac4843faf95dbfc2773eeaadbe74a9cc5</anchor>
      <arglist>(const const_iterator &amp;it) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a9c9f158b94c059b7601d9e91b1f422c4</anchor>
      <arglist>(const const_iterator &amp;it) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>const_iterator</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a0aa29fb1d06d3b7f5926e706d667dddd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>const_iterator</type>
      <name>begin</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a598c17d9ab199cfd340dcdcb38791d38</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>const_iterator</type>
      <name>end</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a21298cb7b1010a5ff07926d506b1510c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>const OsiCuts *</type>
      <name>cutsPtr_</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a85bb345333c43bf2c509dff3ef879895</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>rowCutIndex_</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a3f4a69eadccfa17416b86c06c87f92b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>colCutIndex_</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>ac47518041896502f79f9abc50373face</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>const OsiCut *</type>
      <name>cutP_</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a3d5ad9c3ccf00c7301a03d418a95f797</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>OsiCuts</name>
      <anchorfile>classOsiCuts_1_1const__iterator.html</anchorfile>
      <anchor>a8568644f42d57f0f9d2db4ee9865ed87</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiCuts::iterator</name>
    <filename>classOsiCuts_1_1iterator.html</filename>
    <member kind="function">
      <type></type>
      <name>iterator</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a9dc8def70fe76f41e1908eef7ce738ca</anchor>
      <arglist>(OsiCuts &amp;cuts)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>iterator</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>ad1aa19ab5451a0e7d6a1ab4975c8cff8</anchor>
      <arglist>(const iterator &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>iterator &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a59cfc995e84703ffee1e6a9d712fc5ed</anchor>
      <arglist>(const iterator &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~iterator</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a514ec1d4d288b3f00b1f5576ef07e3c5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>OsiCut *</type>
      <name>operator*</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>af7d294f5b1c0929e9ac37580b742913c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>iterator</type>
      <name>operator++</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a8ab782d0a621b816a5d5fdb68fa47be4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>iterator</type>
      <name>operator++</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a56b65f20af83d4df1e11592b549858d6</anchor>
      <arglist>(int)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>ae12d1f51ecc314207bf69ecab06a8590</anchor>
      <arglist>(const iterator &amp;it) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a8af09f5e520272c2a74874d33f625053</anchor>
      <arglist>(const iterator &amp;it) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a73e3698430214e73cb389c9f9ca24743</anchor>
      <arglist>(const iterator &amp;it) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type></type>
      <name>iterator</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a6afab7ea6e6109daac2ffda682122b14</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>iterator</type>
      <name>begin</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a6febe4a4cca119f03c5ceb5df491afeb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>iterator</type>
      <name>end</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a9f1828e17bd89edd326e1903e8c7be46</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiCuts &amp;</type>
      <name>cuts_</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a0476bcd7b9ec279d281c7e6ebbec916c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>rowCutIndex_</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a017c846674fec8b93f05ec089eaaca6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>colCutIndex_</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>ab72bce250775b1a7f2c8c7fe788f8344</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiCut *</type>
      <name>cutP_</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>ad6e37086f1fb33d2187323f7291f4879</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>OsiCuts</name>
      <anchorfile>classOsiCuts_1_1iterator.html</anchorfile>
      <anchor>a8568644f42d57f0f9d2db4ee9865ed87</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiCuts::OsiCutCompare</name>
    <filename>classOsiCuts_1_1OsiCutCompare.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classOsiCuts_1_1OsiCutCompare.html</anchorfile>
      <anchor>abaf92e5138d5838b748f55e613c3e922</anchor>
      <arglist>(const OsiCut *c1P, const OsiCut *c2P)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiGlpkSolverInterface</name>
    <filename>classOsiGlpkSolverInterface.html</filename>
    <base virtualness="virtual">OsiSolverInterface</base>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ad176ab5ca80d22e71567e1c7ae4c39c6</anchor>
      <arglist>(double s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a3f30e33debaf90e5872c9e13a27f3ec6</anchor>
      <arglist>(const double *colsol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a81d77cd7e1004f44d933b3a14d633a60</anchor>
      <arglist>(const double *rowprice)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aa7748c8b78e5232905c825ad003a7546</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a4ddac745905098caf45f6417919b55ad</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a75cff71819d3829f7dc5e50b485fde7a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ae3f41ea78d595074cf16194ff8b16b9f</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a7c7325d60beb443713b03040f1f135e1</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setStrParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a1caed0aed4291751406764d77c8c2a80</anchor>
      <arglist>(OsiStrParam key, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setHintParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a93acc44e753ba0f1dde53896c80b22c4</anchor>
      <arglist>(OsiHintParam key, bool sense=true, OsiHintStrength strength=OsiHintTry, void *info=0)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a8004e1a5e859de10302c4b2fb74ea835</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a39d3ff0abbc042bffdb0184d6064807c</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a94f693f2dbf492c039de2dc5de6116cd</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ae21b0e1a377ce9b7c97a61bc425feab4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>afa6829c83b01d29956382cf742ad7b39</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a1e4bbf0b1f28776518021dc21d093ccb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>adafaefbcc9a6546955384312a30a3122</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isPrimalObjectiveLimitReached</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a8c507224aaca9235fea2d06fa0feafc8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a0c4ebf95750210d93b60e89a313e6a85</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a86e19c7c2ddc387b38c3d0f8a6dabd2c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isTimeLimitReached</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a80a7ea95de2cac2853eb295886767259</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isFeasible</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a550caa4cffb2a7fe80a06081f7af266f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a98ef868a1280951568be507681b1a075</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a77a9582221b006b9416a217e3ffb53e8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a1098f71a0887aaf1fafcb7c0673fa9f6</anchor>
      <arglist>(const CoinWarmStart *warmstart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4cc934101aa4c98925d9056fea1a97e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a30fb339dd7a6927b3b500536c315391b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a9e3af0ffc09a0c2da1ef0011e8454914</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a4ab2d7d8a8b49ab8d7c17acc7dda2d47</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a901d4bad7e1322ffdb9cc21742f2fd52</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a49342d2f880cbc5323496516f96f8f88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af7e24da9f8badeedb45ada46efe9fba1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aaaeb50152354401dd80e40fb5524d04a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a01c81d423649d409b5112445b4ade129</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a37b1d489ba89a38ff280db2dfa4240a3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a64ad16d836bee4d05a625e25fa13980e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a4ea66726bf978ffc971c333139bdf1d8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6b10966949d40d90bae6d1c97b088cad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a005b2546cae9fcdd7396475b7226e289</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a5bc47bf809185c8d35f4a1fdbc2ed6f1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a17350687b312948624edd5f32d9bd1a1</anchor>
      <arglist>(int colNumber) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a0f4af18670f3a45a2ad8adc7baaa3a41</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>abc1449fecabd4486cdb9944b70d82420</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a9891393e26d5710c5d5fd9af3b4ef9ac</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a64dc3d4d5ffddbfa130fa76b5f9e4717</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a957c9957fc4aa3041901faaf1edc42ff</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac9836c43ac9cc16f54c46db100cc7726</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>acc29881754ced20d85e5af67d80c3dd5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a8c0e5f70f3d33190e865d0085c87faec</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac6a7b397328a2bd3dfa2067eecc1e68f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a8edf4957f25f59a9f692b07882378526</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6221c1a019a1631e2ee0bd547471c591</anchor>
      <arglist>(int maxNumRays) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a0da4249497dc65b44a434dbeb547fdc2</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aec87503522efbde6e02d3dbb2570f9ec</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a932a7f62a25297faf3462e64b02c8aa7</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a9c2139373bedd52db43f1df81311ddb4</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSetBounds</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ada2b2f52a49bd3bba55f2c2c916163d6</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a911f1fd7f5460e6558cd6180f3829ac1</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af4a176ce9e8442542482a3ae81348be3</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a21b400a099656ec253a99ba6133c4bad</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a2f7546b62756cf6959d7080ddb27b4d3</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetBounds</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ad8ed10603b1c21dba93ab99583d8cc01</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetTypes</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a12d57e4ba276f442605bb23939ec92a2</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const char *senseList, const double *rhsList, const double *rangeList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ae7733057dcbc1efb1cbc424000385b00</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a88557e56e375a37919f219d9c883b1fc</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a42080b73bb9bcf537ab8b194f45d8cba</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a244985e05171e2b77ff324aa6abc704d</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a859d36415ed411a331fce6b1cdb61339</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a18b1d13d21310e3afca387253c4b0d08</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a60f4f2ae5a95ba02426a1357ca7156d3</anchor>
      <arglist>(const int num, const int *colIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ad96430a86b24d53a03e90a8f53a611e8</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a86a5cba60e38ffe0279df56479a8d0e8</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6c797db617b0e5e986c6959d50a05b8c</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac60cee1cc82f29ba99fcae33d74e28df</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af9cb5ce89706279dc2331cf47989e6df</anchor>
      <arglist>(const int num, const int *rowIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6bed5a0dbb09397863cc6d8bfa7e3b49</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>afbdc048d3b98a5f6881cac3a1509ea08</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a460468f8c4524d65492056b8bce98b8b</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a915c52feb1cffc08156e0596ae461909</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aaf6fd32c7d9c9a9becd114bb2f1ccde9</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a102401d11538cee03b67ea932aca3dcc</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aa86036ff6af74f95bea48dcb2d15d8ab</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af4fa5daf9405366709be89797ca14ea6</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjName</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a8e0761c7cf998025fb6e68509ebeb00a</anchor>
      <arglist>(std::string name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowName</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a9fb62b6b974dd2092f8c3c34aac81c5f</anchor>
      <arglist>(int ndx, std::string name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColName</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6be1ea8b9d29e6affc984ae3ce64ee27</anchor>
      <arglist>(int ndx, std::string name)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiGlpkSolverInterface</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ab1215c16de8c402136a82e06ace1f63d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a48b2c7684c875b622d94a2dd82cf07f1</anchor>
      <arglist>(bool copyData=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiGlpkSolverInterface</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6a8efd237d92bb7677f97caef7d85509</anchor>
      <arglist>(const OsiGlpkSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiGlpkSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a2a52fc9bdb40ddbe02da90da33f6c74e</anchor>
      <arglist>(const OsiGlpkSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiGlpkSolverInterface</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a411f2dcb91246f6d58a71ce5684f7dc7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>reset</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a527436eee6fd8605e0404d8cac8ccee6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeffSet</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa814128a6965a02f1766dc68d513c351</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *coeffList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>passInMessageHandler</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a109f1496bc4eb01111a9f3b9d3ebfa73</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>canDoSimplexInterface</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a04734f02af4e164032596cd61643afd5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enableFactorization</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>afb2fb22a2bc674b6f0700431659b474b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>disableFactorization</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3adf732c22d8b1b48c372afdc3d83dde</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>basisIsAvailable</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad992422eecf8464133157a1acdb7cbce</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBasisStatus</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a15eab6ec4779c799fec409b25645ed4e</anchor>
      <arglist>(int *cstat, int *rstat) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvARow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a9cb17bc1732ce70ac864dae4918f21a2</anchor>
      <arglist>(int row, double *z, double *slack=NULL) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>accba45f39b7671964b9fb429c1c70f8a</anchor>
      <arglist>(int row, double *z) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvACol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab408f9b496cf438bcb5b74d0e4f4ce61</anchor>
      <arglist>(int col, double *vec) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBInvCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af303267016d926b8cb895abd8d426694</anchor>
      <arglist>(int col, double *vec) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBasics</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a387a708cbd59ad4d57601b8b9c0847e3</anchor>
      <arglist>(int *index) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>disableSimplexInterface</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a80d389bc889a65fbf8c235e939d0c86a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>incrementInstanceCounter</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a221b988fffb4bc641566189d1e06b6d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>decrementInstanceCounter</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a079d0aba01da7fe2f0826e143ed373c2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static unsigned int</type>
      <name>getNumInstances</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a42ca626728a8616807e3d5b556f68041</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a0d1364b2c907fa9e0b4a130aaf12dac1</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a0905343911459ac7b370b4ba5f576abb</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>LPX *</type>
      <name>getMutableModelPtr</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a5a67508d4428bedeacd55ce524fc99ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>acb3df5b54e3903ba3a5def370a548938</anchor>
      <arglist>(const OsiGlpkSolverInterface &amp;source)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfConstructor</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aadfa05295f723144072706556777bfce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ab5a0144ff50068307c86a09f2aacf09a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedColRim</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af07dd9fb89130351aefee84d9dd4d246</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedRowRim</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a7b5cef3731886486d1da3eb9cf02ada4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedResults</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a07063e120a728f03ecd74435c6d8495f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedMatrix</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>afd9f0da88db05e4384cd301c8d8cb085</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedData</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a3207728cde2c8d605030fcc73bc354c8</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeAllMemory</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ab58a75297ff2884407043aa4d1098dec</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>printBounds</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a1d24bb049a5a3902b89159ce43762caf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>fillColBounds</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af3109d5e453fe8668c712569c9daee9f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>iter_used_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af1b40a8bc07594345046addfbb10abfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>obj_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a63eb4f4614a83421a25466e51f8eb87b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>abfb2f7bcc217ee41158c145e22f2d1f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a22cccdd39aafd4a0042f21f807913ed7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>ctype_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ad872b057c3ccc6649266a1aa9dd5e697</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aba8f9f6fa0ca3e0c8ec7e4a2838434a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>adb6cdcc658f7a08db5524dd9e431b207</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a44d6894e2c566347eaf32be2bac1d0c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ab0ca22f5df9e6970e90d1693736ebc42</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a4535de00fa02911d5a491532475d148f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colsol_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>adf3a664aff1d584351f7577f066a3861</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowsol_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a045e7b15c758379fc88efe9ad16a8728</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>redcost_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aa1ce4458a96aff33f9e0c3c4ad06e6bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowact_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af1ed6764726974132eb30c62b653a912</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a98332f9f7cef65d84d00094927afb728</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByCol_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a55dce8cf5ff5e701eec8e07b646fd731</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiGlpkSolverInterfaceUnitTest</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ab5fe829b1c6c86035102dd01bc21cd2f</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>keepCachedFlag</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_NONE</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2a4a3913b6364f96b6fefd6ade8aa8192f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_COLUMN</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2aef3c3fde11a10f3a2a64957ceb4b9ffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ROW</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2a9835ea7cc20fd22dd3b19175a704981b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_MATRIX</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2acc279a46dbc2954f9f66e6195a201e85</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_RESULTS</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2aa9b1ab11a26eb80511bcd19f0a3e8148</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_PROBLEM</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2a9436ab0eb3fe2992493141414eb6d7ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ALL</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2ab3c2fd3ef2dd410fa0e099be1707e4d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_COLUMN</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2a955cb25eca53a31ee76117fbaa778fef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_ROW</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2acb0c19917eefd4973d4fe6ed2eff6a3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_MATRIX</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2adbf6f17f4940a76b31fe74a8f32fb4e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_RESULTS</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ac4211d7b5449fdd74da65a3286b0e9b2aa693dd37f7a0f89f026d90be0d1bcb17</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>LPX *</type>
      <name>getModelPtr</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ae9c6e2989db69176bb1b0872de9596e5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>LPX *</type>
      <name>lp_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a6f7766ae48b0811aa34a7a4228f59c09</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>bbWasLast_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>acbe85396167bbe5e1348e2d482e3ea79</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>maxIteration_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a7c125db84361166a95dae33ae97c05cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartMaxIteration_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a2384998ac87ec05cda8e1304eb007cad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>nameDisc_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a87f6559104af5a3130ad9471b6247531</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>dualObjectiveLimit_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>acc776596de4656e90437dea9bf70b9a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>primalObjectiveLimit_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aeb11fc06395660b3330d480442d5a795</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>dualTolerance_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ae7fa86679c7535137b0cc5d0617de748</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>primalTolerance_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a89fa9150e8a31ac8a7df3aff5fe8a29d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>objOffset_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a64343fb1300666a7a24a38e3b458be08</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::string</type>
      <name>probName_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a56092391bf470e4dee31c86cfaaabc62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>void *</type>
      <name>info_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aa4e0884de41da3aac5cc2e74e83209fb</anchor>
      <arglist>[OsiLastHintParam]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartCStatSize_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aab0dfd28b34f7f212326166dbd5f4b28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>hotStartCStat_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a7d9371573d9f052eba12ad16354a2767</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>hotStartCVal_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>aa23b35f447b3b3d39053111de40f1925</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>hotStartCDualVal_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ae5913ea6e8af285b619d97a55ea4a56c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartRStatSize_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>ab143a4512816b8aaef37a0c9d0b83653</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>hotStartRStat_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a3f3707d5ed3cba84d0da04d584209c90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>hotStartRVal_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a3de2396fc26c6eaefb13536a25adfc52</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>hotStartRDualVal_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a093fb88e47e1f345f2bbe5578123eb9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isIterationLimitReached_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>abbcee659d07e09addd19deb2b8a0d104</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isTimeLimitReached_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a09e92f95a1e70c4c3395e0116e27b43e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isAbandoned_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a56d3b9d088d566d4c1dea89a196e8f75</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isObjLowerLimitReached_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a1f26f12cde736b86924b46dcb698c75f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isObjUpperLimitReached_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>af5273f73b4746a7e230b365390b91307</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isPrimInfeasible_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a92b13063987c1a29667dde6726bd4b15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isDualInfeasible_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a4346a18eaa82a67c0fc8750e5e254a95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>isFeasible_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>acead1f2906990031f2aad466a2786eee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static unsigned int</type>
      <name>numInstances_</name>
      <anchorfile>classOsiGlpkSolverInterface.html</anchorfile>
      <anchor>a47a4b54031715af31ac2d0a65b6ed3d6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiGrbSolverInterface</name>
    <filename>classOsiGrbSolverInterface.html</filename>
    <base virtualness="virtual">OsiSolverInterface</base>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a065242e755df231cec25e5ae0ca9cb64</anchor>
      <arglist>(double s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>af5b45ba614e06381c7dcfca138e99fba</anchor>
      <arglist>(const double *colsol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac85715e9c8ab3651b955fea1ddc86c93</anchor>
      <arglist>(const double *rowprice)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getCtype</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9fec29098468bde32c6891e42fbdca1e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface::ApplyCutsReturnCode</type>
      <name>applyCuts</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a52002e14b33957a62366869ca84949c8</anchor>
      <arglist>(const OsiCuts &amp;cs, double effectivenessLb=0.0)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>acc04629f80f5924fe1bf732fe35d164b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a814b977df6495a0bdf2190d41c6bbb33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9aaadbf754c08175c894139d16249f96</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab47065b3ce3b5dec1961841f65b68094</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0e106ac5615f6c997512f49efd0a808c</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setStrParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a2b5034b67e304c1fb6355738f936a6ab</anchor>
      <arglist>(OsiStrParam key, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setHintParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a48ac47b8e0186ab038acdb3282cc1244</anchor>
      <arglist>(OsiHintParam key, bool yesNo=true, OsiHintStrength strength=OsiHintTry, void *=NULL)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aeba6eb62c93c7f266565887e2d0eebe8</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a46f05473df91deb5f0177e9027d3aebc</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>adcf4e9422ca5b6bf305387a92f26bf63</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getHintParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>af733a82869de8de1a6cae097e1294ac6</anchor>
      <arglist>(OsiHintParam key, bool &amp;yesNo, OsiHintStrength &amp;strength, void *&amp;otherInformation) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getHintParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a12fcb29709f575c5dccc80d2c1f96948</anchor>
      <arglist>(OsiHintParam key, bool &amp;yesNo, OsiHintStrength &amp;strength) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getHintParam</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>af2a90bc77abebb379c91f99f8638e63c</anchor>
      <arglist>(OsiHintParam key, bool &amp;yesNo) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMipStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a7cc684569a9473a965bddf03ccd2b791</anchor>
      <arglist>(bool value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getMipStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a037c811a7b797c3f0730b2a85341f8a8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a6983b2b312fd808e044051568a72d150</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0fd3b41222872ddb149e49b5a895d57c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac4751edaf3363456a6560b9e97e35565</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a39e0386170751ff4580f5971f49e5ed7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isPrimalObjectiveLimitReached</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae7491aab52171aea3e9b7eed449dfb7a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a810aa106b6493f11c1165279928b390b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ad0bf432a00daed05c40c9f1446c3094a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9a82904bf15f7db24c4fe4a2f60ef4ee</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab8e8f06e7e53abb51b0c6a0084cb92f0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a46b32ff87b8025f62c438c14e1676e01</anchor>
      <arglist>(const CoinWarmStart *warmstart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a1135a339371df02b1c664c012fdb5266</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a732a240366e80f64e49348283ff05682</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a1cf1bb24099231975340d7efc6889323</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a536187204f099a2c23d0e28116f29d34</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a37cd08321524ee5188da2ebefa550f5e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9f26e81ff8a6c63843f5be9b003fd9d6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afef564f65ee79ed63d1c1e6d043cf172</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afe55db513aa9ca3dbc3552e702e00f9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a4fe5d90c15ed3da2881cb78b8092c4d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a6aa0f3f50329dddf78cfe62e2c541573</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a696c8f31ae62abb2770ccfbec481b100</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab6fe13f78e2621228992e3cbf9ecc2bf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0542fec5c6ab609cf33a4846de52af8f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a180177044f13f0aa607461dd13c7f4f4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9dd829ccbe8e700f2a58a51750c3be21</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a73a0e07f767580fe6a0e6e733b2476a5</anchor>
      <arglist>(int colNumber) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aa97ec812046641cff07c9092c2450984</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ababfe2ecaf4706c85ac793c638564b44</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a97b35e33de16c98ce9926bdc70e40fb4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a3eb8060e4e99138153c0265d9429445a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a4195f45aeda60ab438047d7dfa0f0573</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a770ac62a8f67a9e1c498e77afca396db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a6098d4fd197fb5175c38a5a381b262af</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac0e1d5b4322b8e4aab1ff84312dd821a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a245ef10be7fe0189984a6a91836944df</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae4d5ff4c6ffcffef334ed133d0167569</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a44ad34fcb7ecd6db85856f4953743148</anchor>
      <arglist>(int maxNumRays) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a1f24aef716b0a3a94f5c2286f8f44726</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeffSet</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>abcd817e8607784dff2064b456124fca2</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *coeffList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aed56eb5285faba17971a84d402c8ebb3</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac1a60eca04505deeeb0582fb3cb2ab0b</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ad9c42a4d15b8591dadc8db133355f8f8</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSetBounds</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0aa397d548d5270d34915594b5738ac8</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>add3844f2734fc0b62bc066aa501b14ab</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a2b414ee79256588ba731698c34acee80</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9654860aa97ce98b0bfadae5df88f32b</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae97bb9250a40cf6956132d4e9ea3dac9</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetBounds</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afe7c70b7774bb9b85335fe96d76b5b88</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetTypes</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac10bf7784de9152342390002cf4bf1b8</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const char *senseList, const double *rhsList, const double *rangeList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a1e44d67c1f765a0f31e70f017ddcf9a6</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9e773266f57be33b7c248878011a5208</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9580aaaaa3c324721c5f55bc15371c8e</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a512f277388d7077c8b32fd1fde64007e</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowName</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a945852d38689c6c578af546091bc301a</anchor>
      <arglist>(int ndx, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColName</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a415e74158ed4a2a9617b04431f97252c</anchor>
      <arglist>(int ndx, std::string name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a53c0fdeb07b8170be1b3bac56f6e687f</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aad9001df5db2784c96bc355277e4aba4</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab458d320d8775d55ac7c6d67da6521a0</anchor>
      <arglist>(const int num, const int *colIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>af8fbd3c7ba0ac79017bd67a64f38ebbf</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a368c8983e57453f81cb1c6dbf9011e3e</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a5f964a95e2cfdd9274388f0f25abbcb4</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aad28e72d1debd92a68434766f39dee2e</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a2c404632aad569a2372155b53dd05017</anchor>
      <arglist>(const int num, const int *rowIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac48d6315f992f5c7a21177e5217f68e5</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a01532d4528cb9d72fbc248eda1788d87</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab84331a3db7915034165d8cd03c13d9b</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a5c1529640655d9743d2b5113b9253f3b</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae125cb7419e38e31bb8f41a9ad3fec60</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a54eec4eff8eba253059193b342a21e98</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a3a02518a0d24f3aa10e5a5857a185a5d</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a280b07fa6445efa071cfd0e0b7a48284</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiGrbSolverInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a041267e01a0b769371e7cd290348ddbf</anchor>
      <arglist>(bool use_local_env=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiGrbSolverInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a97698f47388019fc2dd5c1f3b2215b8f</anchor>
      <arglist>(GRBenv *localgrbenv)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aa28f6b3d95dfa5a5b5201ed05df254bc</anchor>
      <arglist>(bool copyData=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiGrbSolverInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aad0826ffcffa2ece104b45e1d69b8403</anchor>
      <arglist>(const OsiGrbSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiGrbSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a8aa9344cbc0f62c947c011fed5cc5b04</anchor>
      <arglist>(const OsiGrbSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiGrbSolverInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a60a304d546d419bafb785f986869eaae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>reset</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a10a5ca8d3ce2d2a2dc92d00e7549e759</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>canDoSimplexInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac72ee8ae010c4521f3cc0a623e89297e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enableSimplexInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a481e462313e95c29e79df8fc23a1d784</anchor>
      <arglist>(int doingPrimal)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>disableSimplexInterface</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aa6f6148fd49a5c599c549da4f65f5f73</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enableFactorization</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a341d409ae6c507ab8af1fd4829f0d898</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>disableFactorization</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a837164034d48a4858c683b86d99293b8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>basisIsAvailable</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a267c77817b34b6853b9d0bd40012fc22</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBasisStatus</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0e4e1e0f7367b376e3924f10733db119</anchor>
      <arglist>(int *cstat, int *rstat) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>switchToLP</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a2ba321ef68ef0aa19d1848f07375934b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>switchToMIP</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a23417ae1188b4bc4785fd47b4b576a92</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>incrementInstanceCounter</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a484948a8c1151409262cb100dc77e524</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>decrementInstanceCounter</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ac686b8aa1a9fc8f68fec7ee684f7770d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setEnvironment</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a07addeac400ebabf20948ded9de55ede</anchor>
      <arglist>(GRBenv *globalenv)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static unsigned int</type>
      <name>getNumInstances</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a5acc035cd2f3caeb7c17134aa87e315c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9c1c853940ad99f5ba53bb747d81dcf7</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a826c059281b3e3b73f049ae831c793eb</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>resizeColSpace</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae5869442926026d13bfd504150c397ea</anchor>
      <arglist>(int minsize)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeColSpace</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab0ba14f9045403e2451d7cac1be9242b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>resizeAuxColSpace</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a413c2ab1d3311023b114c94d69d91823</anchor>
      <arglist>(int minsize)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>resizeAuxColIndSpace</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>af1a4c2e8675c4354dba8a36c3fc1bf53</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>GRBmodel *</type>
      <name>getMutableLpPtr</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae28147caa8c57bf46edc17ea9346a790</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afa58528b7b86bcd7b7cbe7c88a81063c</anchor>
      <arglist>(const OsiGrbSolverInterface &amp;source)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfConstructor</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a5f55752b65c3355122f2ae2a1eab3b07</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>abc90bff3e88b04335cb99d4f9c1a7223</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedColRim</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab9f40088a6fc45055fa9928c25d77b6f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedRowRim</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a40c53910108172faaa049d916370441e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedResults</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0aad0f86182b586e05b34937dc4d0b03</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedMatrix</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a8a144f44c0012eff59fe0a1ffebadd9c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedData</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a2985b79a9af842c9067e063044e761fc</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeAllMemory</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a34479484e21795baab0084f0b1ff5111</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>convertToRangedRow</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a54b59dbbaf35e4b0a240f3c1ee6b5b55</anchor>
      <arglist>(int rowidx, double rhs, double range)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>convertToNormalRow</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a53c2490ce1141cee1765f95a14c9aff6</anchor>
      <arglist>(int rowidx, char sense, double rhs)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>GRBenv *</type>
      <name>localenv_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a5044257831f6305c5bc49cc49c6d39d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>GRBmodel *</type>
      <name>lp_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab333896e9ceadb6d2bfb005f26a2766e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>hotStartCStat_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>af24640901a58e3c1130f67591663c15b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartCStatSize_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a7004628ac5628e281338383dda8f7b0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>hotStartRStat_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab829dafb59416e8deaae694aac1a2280</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartRStatSize_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a6459d92ef0868952b549af9448166f66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartMaxIteration_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a7d6dd3d8f280d31da606b6f19338b6ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>nameDisc_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ade8e6cc37bab1f3bb7784bc97e1e4249</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>obj_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a5416a90302da941afe43bc68c7062111</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a19e3766c119f8b76f1da242bb7023bec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a95894fb8249e9b7b260a865e15626a6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a3ab10324c3b89a864793b54f51df2924</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aceede0d4d27c89349aaf63b399e14a23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a55a36bd337c09f012b4b4ba5ee90e052</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a7800d8e2661d08969bab4a36db249717</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aef4004cac1c2dde9772c87cc6a04daf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colsol_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a41631193c335fb040de26a8b722f97a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowsol_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ae3b68502ff8000c180e5c7d27c36509e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>redcost_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afa773719b725b57ad1955a568d0e80e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowact_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a0f9d07e52d9333117e6f5e244efb8754</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a492da5be5b49c6aa29b7eea1d90035d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByCol_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a3acaec06f625fe3b0da923376a35313c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>probtypemip_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a34789746a0d43b579ece39d7f3d33b70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>domipstart</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a49b1f1b5beae2962492c8c51ff15dd96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>colspace_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a25a671dcbb12a2bde61e0e98446242bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>coltype_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a700e8bdf83a55f31b067e2e113177bb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>nauxcols</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a9bab70b2f79999b7decb14aff145058a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>auxcolspace</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a3d1fe27d125cacf6e644fba9def7d75c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>colmap_O2G</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a33a49db2f0bc992612cbf2533b5d5ae4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>colmap_G2O</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aa031be015685b532576be93ad9f23b23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>auxcolindspace</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a69b1179a87fba00a451913f7913bec65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>auxcolind</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a71a30bba1a2859fd10386b8508adfc05</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static GRBenv *</type>
      <name>globalenv_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>aaf6d67dfac7ece0dbc3cd9508527f865</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static bool</type>
      <name>globalenv_is_ours</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>accc9308352201fd297ff2d3679ef774d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static unsigned int</type>
      <name>numInstances_</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a49d996f4b4fb775600c526218de56bd3</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiGrbSolverInterfaceUnitTest</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ab4ab8f979eecd8300822acde2fbd59eb</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>keepCachedFlag</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_NONE</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424a57d14c55edb9126ae8195c7118208bfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_COLUMN</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424a5f0f933d1dda70ff7db2b756582f88cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ROW</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424aac58d47502824fef888b4e271231d276</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_MATRIX</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424ab7432fd086fe55b6518789caded7ad35</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_RESULTS</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424af60d8ad1d9b5d9694b8847845bad9fa1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_PROBLEM</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424aa52c36e780d6fea961d901b219f9acbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ALL</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424ad44038b6fd01be02d6e962d8e6fe3eb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_COLUMN</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424af61ab31a97d322bccb239e26f6245bc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_ROW</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424a80405d98b1304b23cadc3b107b527307</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_MATRIX</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424a938e072352000a33f7c81333153c284a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_RESULTS</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>afb109280d93381dfb8523551691d0424ae4d6177aa76872d6750f807957e1c817</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>GRBmodel *</type>
      <name>getLpPtr</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>a3b2a2d4127a37c812125a42c6ad33f32</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function">
      <type>GRBenv *</type>
      <name>getEnvironmentPtr</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>acb582548eedbaa9f780fdf19dd1d0528</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isDemoLicense</name>
      <anchorfile>classOsiGrbSolverInterface.html</anchorfile>
      <anchor>ad8575c9dce20783e94b31c8c31c23c45</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiHotInfo</name>
    <filename>classOsiHotInfo.html</filename>
    <member kind="function">
      <type></type>
      <name>OsiHotInfo</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a3bd834bb30925ed2a2f0de6c9b56f149</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiHotInfo</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>af95c3161d9a17a56e9e48853c51fc04c</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info, const OsiObject *const *objects, int whichObject)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiHotInfo</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a91938a4b49c38168491401a6eb0053b9</anchor>
      <arglist>(const OsiHotInfo &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiHotInfo &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a4230a5819393c568bdd83acba8015396</anchor>
      <arglist>(const OsiHotInfo &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiHotInfo *</type>
      <name>clone</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>aaec9e7d771cf774d29ba62a088eb6583</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiHotInfo</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a24d37a1f4ca8741a7940fb4b70949f54</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>updateInformation</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>af9a0d2831c50929891164904fa95240f</anchor>
      <arglist>(const OsiSolverInterface *solver, const OsiBranchingInformation *info, OsiChooseVariable *choose)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>originalObjectiveValue</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a50f593481d124d1466c15d6ba6dc158e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>upChange</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a20ccb72139793abf8ac784a673ad54d3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>downChange</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a049060ea6e0a2dcd0bd5d615a71197e3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setUpChange</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a00e2fbd3ac547e3f78400be646db63b0</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDownChange</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a2cac7d89d6c0f43198d99c0ecdff18a6</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>change</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>ad6bcfb4cbbef8fb2eb07148c8af5ee09</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>upIterationCount</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a1e1407916cc22dfbaff5f6b3c206fd89</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>downIterationCount</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>acd388ff1e0dc826199c2f4cb2a427ccc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>iterationCount</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>ab0b2b704db4310b849a59c0064ed45a8</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>upStatus</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>aa68141300433969343dc18a6f6d60f0a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>downStatus</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a3460aef7389b8af2221cac2a22a4b249</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setUpStatus</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>acabb36b78f4eced946cc06beb713ea8f</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDownStatus</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>aae56aea2f4a0ee879347bc9c5b61a2f5</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>status</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a34c58e796da7bf6652f3e3d1cb2c99ef</anchor>
      <arglist>(int k) const </arglist>
    </member>
    <member kind="function">
      <type>OsiBranchingObject *</type>
      <name>branchingObject</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a30aaa58880f7dd10ad73dc78abf7d9dc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>whichObject</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a9812df835bc2952210e9dba8a7ce368b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>originalObjectiveValue_</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a2f0fac4f990e05a92a03c403c117e839</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>changes_</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a50d8e58d691e8871a58b496608d666d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>iterationCounts_</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>af8db0ddca4dc5e9670679a91eb6711a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>statuses_</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>ab21dcceefb219a9daee4103932a4bc5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>OsiBranchingObject *</type>
      <name>branchingObject_</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>aa9024fe6af077139428187583ff2c1d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>whichObject_</name>
      <anchorfile>classOsiHotInfo.html</anchorfile>
      <anchor>a9a3650812a56dfa7fe7a3009863b04fa</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiIntegerBranchingObject</name>
    <filename>classOsiIntegerBranchingObject.html</filename>
    <base>OsiTwoWayBranchingObject</base>
    <member kind="function">
      <type></type>
      <name>OsiIntegerBranchingObject</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>a771d8e0c810d816ed7c31bed9a365385</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiIntegerBranchingObject</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>a2df43b5463f65fb694665c7a43fd0fdd</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiSimpleInteger *originalObject, int way, double value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiIntegerBranchingObject</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>a6bd18553c8abf6e82f77104eabb98e09</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiSimpleInteger *originalObject, int way, double value, double downUpperBound, double upLowerBound)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiIntegerBranchingObject</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>a8d5487f6fe8ef256c5080dd95e2bf9d0</anchor>
      <arglist>(const OsiIntegerBranchingObject &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiIntegerBranchingObject &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>ad5033bc28bfbcc2586b9612ff98a2961</anchor>
      <arglist>(const OsiIntegerBranchingObject &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>clone</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>a5a894d3ef510da7aadb306328a6eca08</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiIntegerBranchingObject</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>a375bc191b3798cb2e420d1bae32ba266</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>branch</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>af516f68de7cf50880b06ea8ef23b4d84</anchor>
      <arglist>(OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>ad9b57643dc315e9d756da575d11ca355</anchor>
      <arglist>(const OsiSolverInterface *solver=NULL)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiTwoWayBranchingObject</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>a0dcd658b2865a25b6236dfb2931978f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiTwoWayBranchingObject</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>ad3be15dbf594dd99bca7d4c278ded49b</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiObject *originalObject, int way, double value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiTwoWayBranchingObject</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>a10b66aa30e19d0821258e894424a97b0</anchor>
      <arglist>(const OsiTwoWayBranchingObject &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiTwoWayBranchingObject &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>ae1cb65eb986aac19d91039660181276f</anchor>
      <arglist>(const OsiTwoWayBranchingObject &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiTwoWayBranchingObject</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>a570c4e7969943d91a544536fcca014b4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>firstBranch</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>acd3630356d55bca819b4ff6bbbebc7d1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>way</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>aae64d581bd8701da0ac3d921a1a573f3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>down_</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>af8f92b943459908ef0966d5736682642</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>up_</name>
      <anchorfile>classOsiIntegerBranchingObject.html</anchorfile>
      <anchor>ad62b57ef54df48ec5536286ac58ac00b</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>firstBranch_</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>a3694b45d5094309310b4903cca5b3690</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiLotsize</name>
    <filename>classOsiLotsize.html</filename>
    <base>OsiObject2</base>
    <member kind="function">
      <type></type>
      <name>OsiLotsize</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ad853c8f7db4d1b423fdc2f22277de4b9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiLotsize</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a79a0afe72cf808a8321416a0bc10e485</anchor>
      <arglist>(const OsiSolverInterface *solver, int iColumn, int numberPoints, const double *points, bool range=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiLotsize</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a9f20ae4a75671914d564ef8dbfe432a5</anchor>
      <arglist>(const OsiLotsize &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiObject *</type>
      <name>clone</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ac427b369743389021a8e46b42de73736</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiLotsize &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ac2344673da7cecf4fd249c6baec20c31</anchor>
      <arglist>(const OsiLotsize &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiLotsize</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>aabb2dd32c49198cec715714bfe3df382</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>infeasibility</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ac191677fd08038bf9b556f3c4dadcadc</anchor>
      <arglist>(const OsiBranchingInformation *info, int &amp;whichWay) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>feasibleRegion</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>afedc91b9db9fc17a1f6476b590989435</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>createBranch</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a100d69564056d67115fcf931a8d88250</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info, int way) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnNumber</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a8b70aaa9efc3e39c0290fb5396252c16</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>columnNumber</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ab2965273c41cb9c03c2c71273ac4265a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetBounds</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a65b3f228ad71d5dd9be93a2a1ff16d6b</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>findRange</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a6e429c7347a2220072459066b861d1b6</anchor>
      <arglist>(double value, double integerTolerance) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>floorCeiling</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ae18d44507ee51c13ce936bc52897df28</anchor>
      <arglist>(double &amp;floorLotsize, double &amp;ceilingLotsize, double value, double tolerance) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>originalLowerBound</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a91521f6b33c4a9e4df8f9f0218b8bb6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>originalUpperBound</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ac94a9924fcc84f4b8f3fc4be969d2299</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rangeType</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a842067dfe47a1ea529cbf66aadc4cfb3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRanges</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a52764cc85b15c81a66a5abf5bb1205a3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>bound</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>af28568cb7d057035ab475ddf5715f83f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetSequenceEtc</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ae6d317670077a4e7d9c0c5ec70590a02</anchor>
      <arglist>(int numberColumns, const int *originalColumns)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>upEstimate</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ad84e2f97d7ce7769933eae639407d318</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>downEstimate</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a955ab9d5e300fb870174002b005d794d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canHandleShadowPrices</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>abd58699248ba14c49f58f1e677b1293c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canDoHeuristics</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a9341ccd7e4cd4e2c703ad9ac7d0adf93</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiObject2</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>ad0648e242bc9c772908878d846ae71c6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiObject2</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>af1d64f581c50200399a554a9179e532e</anchor>
      <arglist>(const OsiObject2 &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiObject2 &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>a4294682f8b90a61ef98fb611e3b473fb</anchor>
      <arglist>(const OsiObject2 &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiObject2</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>a71d7c0ffbe2c01240289fb2642935492</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreferredWay</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>af455f61e06fdde7aac3dd35e8f97a49c</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>preferredWay</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>a555876b528a634bdb315ea9f702725e5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiObject</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>aca171be0a6aab40f19368e84dfe34261</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiObject</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a6e6be1bc84d19436a2fa9b49e9a5693e</anchor>
      <arglist>(const OsiObject &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiObject &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>aa12d9842de840ee3fff62b368ab1ae23</anchor>
      <arglist>(const OsiObject &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiObject</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>aa2b3bf7dc4ed7d2f4a4597e47a33a6c0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>infeasibility</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>ae76b07675616caa03df965c51688f4ad</anchor>
      <arglist>(const OsiSolverInterface *solver, int &amp;whichWay) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>checkInfeasibility</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a8cfb3404a82acb885becafc6e7e815fa</anchor>
      <arglist>(const OsiBranchingInformation *info) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>feasibleRegion</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a035415506365614fd9ce294cf7f9cc66</anchor>
      <arglist>(OsiSolverInterface *solver) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canMoveToNearest</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a0d68a13f0572e4f3f14bf4f955284a81</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>priority</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a653e57e113d87af816ba31e1144132b8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPriority</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a799c2765bd8d27e012f7534d18176afe</anchor>
      <arglist>(int priority)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>boundBranch</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>adc37217c1483e70aa84ab3d47a8592b7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberWays</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a04ee7d15fe47c89658f8a9bdeafed2d0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberWays</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>ab56ed02b89ea7a6499928dd17aa69b65</anchor>
      <arglist>(int numberWays)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setWhichWay</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a8abb435f05c9a6901417980494fc7872</anchor>
      <arglist>(int way)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>whichWay</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>ab3904412b4bbbc5774291c4ebeaad4be</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>infeasibility</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a25133e8a5e5503958cef887d5c587472</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>updateBefore</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a3c6fbb8d6adfc16975c019de30519a0d</anchor>
      <arglist>(const OsiObject *)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>updateAfter</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a1bb3eb3050ff20d49b8ac94087a73ab0</anchor>
      <arglist>(const OsiObject *, const OsiObject *)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>columnNumber_</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a6905d61ac9af8d0809a1542a3a84b42d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>rangeType_</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a939b203b62013653bd0c783a86deb8f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>numberRanges_</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>abc2209046df627b910b98d1ddc8860a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>largestGap_</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>a6d7e1798c17a4507131ab9aab7d41fba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>bound_</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ab5edf1e72f09ba5d81ac087088c40292</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>range_</name>
      <anchorfile>classOsiLotsize.html</anchorfile>
      <anchor>ae1c6d92a4ae3c73c501ece31b5a41020</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>preferredWay_</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>a675e065a7ae557e2f1c9c42943ecb40b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>otherInfeasibility_</name>
      <anchorfile>classOsiObject2.html</anchorfile>
      <anchor>a605a653b1519237845d1a89e190d631a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>infeasibility_</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a0d9e1f611bdd7ba84c00ec8ba0fee6f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>short</type>
      <name>whichWay_</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a31eda701aabaeb0526b159c2afeaec94</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>short</type>
      <name>numberWays_</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a249536f2a0ad12d3587ee20bbc082b78</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>priority_</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a4fd5511aa87d7790ef508d434f03f5c5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiLotsizeBranchingObject</name>
    <filename>classOsiLotsizeBranchingObject.html</filename>
    <base>OsiTwoWayBranchingObject</base>
    <member kind="function">
      <type></type>
      <name>OsiLotsizeBranchingObject</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>a62280eb8e71ab2da01877874f05618d2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiLotsizeBranchingObject</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>a34c58bb7bc0dff7f69cbfc904318e7fc</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiLotsize *originalObject, int way, double value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiLotsizeBranchingObject</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>a9575d9d83d6d0bf5fe3ba7123a848847</anchor>
      <arglist>(const OsiLotsizeBranchingObject &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiLotsizeBranchingObject &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>a2f504e863bfc8467a0bc06b4cb8ae250</anchor>
      <arglist>(const OsiLotsizeBranchingObject &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>clone</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>aa82ced28485835aa54c0aa2d6fe6b8ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiLotsizeBranchingObject</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>a3614e36b0482e69ca71b0e54d4de2da5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>branch</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>ad198bf7312b915526c62c8cc31938047</anchor>
      <arglist>(OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>aa71b8714e2e0b61125c87f6aca09d6e2</anchor>
      <arglist>(const OsiSolverInterface *solver=NULL)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>down_</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>a912271d698cefdce7b84d1d29df5ae00</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>up_</name>
      <anchorfile>classOsiLotsizeBranchingObject.html</anchorfile>
      <anchor>aab7487c11da34c515cb21d3bb2a33bfb</anchor>
      <arglist>[2]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiMskSolverInterface</name>
    <filename>classOsiMskSolverInterface.html</filename>
    <base virtualness="virtual">OsiSolverInterface</base>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a42a7faef727d3100af596d3c6f64ee07</anchor>
      <arglist>(double s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a7b40fe3dff5facfcc7d81a52c75da709</anchor>
      <arglist>(const double *colsol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac73d28b54ac74a08631567298d070e06</anchor>
      <arglist>(const double *rowprice)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getCtype</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a34770ae31b10470a583027250adc8521</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a80e90ba91acde0e5bb33a77e423eef6e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>adb8b5c02a820c9585d5f7cacb8855b4f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a61608062c2b0bdf898dacdbd78036e3c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac882707dcd0ce2ae9fec6905fdc6397f</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a3fca29f84aa7eff0bd7253092e064307</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setStrParam</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aeef0ea59cc60ab058f5893f3fefa9e7e</anchor>
      <arglist>(OsiStrParam key, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a4bd145ffeaaf86a611c4e5ff928139e4</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab9b8f5e31c606fbb475365c07fe6d594</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a41ed693f9eecbd43db08378c0ba24154</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afa414dd8ebd8dde713fc887037002425</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a6bdaf51ce4839f899955760e5f7a0b6f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a19c7e3098ed55727cfda84de344eea43</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a147a8778a10c2e8613668d208a7c6003</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isPrimalObjectiveLimitReached</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a26cdc556434b723430bf6b42393ade9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab8adbc16d85588de30ab654f96aa6576</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a952abc8b613e0cea11bfa7daff8185bf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isLicenseError</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ae31a81f5e59dd183e342983179e33b9a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getRescode</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a082e33d760038ae1433d4ae9bd07732b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a39cc4eedf33f3e8ed2243e30f0ff4dc8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a813045f92a9b75272eccec7f280da17e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aeebf979e40a5b1639abcb4f3648a912a</anchor>
      <arglist>(const CoinWarmStart *warmstart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a3ce95d5db17de057d639509818e29238</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ad7caea8686edb4e8496b59ae35597f55</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a48a4289c48ab061a1afd85966637d273</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a4b47d46a5bb650560f950805ae90413f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab38c20586637ff966362508be7e3e2a3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>addba8063abc70f58b5500936c58b10eb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a76a522214739d9547c7b1dbc84a952cb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a57edd8596f2f342a58a7d140cddde1df</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a625cbbf382824a13de86a215b0c24a88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ae58430a976b4186dd4611d93b7968c78</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a93d05fa8e882c66814f681f086398f16</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aad6444a41dd05ee3bb60814ba392c787</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>af9143d50a79c71d1e86c26a22359c386</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a6330723e417aa804078a22a7dcf34572</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a278e69daca1e447e0bee8e180fdeb9d8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a98cae2798cc2fcf0a2b48ee1a657b28e</anchor>
      <arglist>(int colNumber) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab4a386e695de14b23d913b6645ca8b1d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afc882fb2e223ad761d34623dfba98b25</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>af17929edc5a96347d16c7c5919c8444d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab4f42d3b37015ee8abae7e143b216019</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a3e02e6d3410e2a4d6b4867d581288d16</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aff89e62b4e66177ffc5963bc425892fb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afb8a467b45987b20de88404ea33109d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a3f3ff624d74de769261c071c625fd1bb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a274b861650f7bc99bcd16d15ea8497d4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aba4b07d68cb52ead3a81bf7871197b53</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aef8cd065eb696d39dc27d1047126a502</anchor>
      <arglist>(int maxNumRays) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>abea9626ad49e269b5a32d426f4c744e4</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeffSet</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ae17f45c1bea850e67eb9020411564e57</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *coeffList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab53078809d6e2e04fac73bfa2f8d7edf</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aa03c86f774c33ca1e6d095bef5b138ad</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac65d4230b58c0b2b7035d694e28a44e9</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSetBounds</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a72a727f8ca500a05553a46be187e91db</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aea9f744e110dd95da20dd98d443945fe</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a9751e0bad8452e3e699e5fedb0969569</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a4c80bd3734b2bb977c40b7605cad5c69</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac2d20697c077b63027881197eb4a7acc</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetBounds</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab4794b44c50661fb615edf61451b79a5</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetTypes</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afca378dd790921fe205fe4dda7b855e9</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const char *senseList, const double *rhsList, const double *rangeList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a5b9e342366d1f95328f4c8ea0653bfb5</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a30907bdb73ae11f05734cc3802806ead</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a80841e959b368ee4c6c818dafb8a7907</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac85a3cb3ed3d9fbbc78cbd1e4b726bcb</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a7031e8c5e5bbc862d050ac376ca8fab2</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a075e1159308b05369850eefa9cfec787</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a5000683cd67f52b003ccc0d6d1ff6097</anchor>
      <arglist>(const int num, const int *colIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aded81c09a3dd0f49a51f7b5c398efd54</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a65bbf81ae61564261dbb2c361fdd033b</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ae344f3b311c9c006283d4450fa293b81</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aa8405a316ccd9e31647cb60e968d1fa4</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ae528d62942ce199a3b0c9b4f2bf87d0a</anchor>
      <arglist>(const int num, const int *rowIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aa7ae2f6bf072be657a9a9961d7297bf4</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>af9b62ff2ecf20bd95244c586c8866273</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ace19a38b0bba614d10f6f806e3281fff</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>abbf4fba9d795af3dbe86bf761d95bd8e</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac7e92087ac0f3bdf2bd879cbc127c0a3</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a393b16b84c97efbb68bd57db2eb1a1c2</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a7fbb862f88a5a42657b6681650fd51ae</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ad255b03a795bd81c5e85edc471e9c9df</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>passInMessageHandler</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a81e52786273396f0deb3c9f4d5a186d7</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiMskSolverInterface</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a4d80eb618b13703e10178c16b79ff4cd</anchor>
      <arglist>(MSKenv_t mskenv=NULL)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a43447fa7d4b493f1ec323a400382afdf</anchor>
      <arglist>(bool copyData=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiMskSolverInterface</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aae57d021dcaae420105b2212549e15f1</anchor>
      <arglist>(const OsiMskSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiMskSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aadb84be2dd46c7ef355133d19490b5bf</anchor>
      <arglist>(const OsiMskSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiMskSolverInterface</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>abf2801b984dbd276f02bc461c74ba6e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>reset</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac66bb8371cf6587326125772e2994b37</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>incrementInstanceCounter</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a0748191d10b2b71efb5df1b7e84d3a8b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>decrementInstanceCounter</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab5a0e08ceba4c79fc5dc8f92cb084bef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static unsigned int</type>
      <name>getNumInstances</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a9e413d4701a9c522831c9ae392efc66d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>MSKtask_t</type>
      <name>task_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a9768de6b10584ae8054bc3c7016d17db</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hotStartCStat_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a788fe2539193ac6b1a69bb55a46ee292</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>hotStartCStatSize_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab2cfd7c2e98c57584a689db4ee318d4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hotStartRStat_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>af89d5bf544b2e661a3143be28b8fb6fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>hotStartRStatSize_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a0739cc0d42d498fa6b3f26cc39c2490e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>hotStartMaxIteration_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a430919f8d29588fbb7c138e2cde43421</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>obj_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a2be4621cba70faec860dcea30b4f8bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a05540fccc0a67c9d46db568961fa5042</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a3f93e7829c3f4c3a71d01879c10e1c27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ab35b95602e3a5072bbfa2fce4f0cd96a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a6bdbb50d777724139a197eaa5c276872</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a5daeca145a080ebba3f31d4e9474d506</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a490c98a77216ac82964bbaaf7876db1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>acf0f35426739722577f2d8ed24b53873</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>colsol_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a604c1a04aa49fe074e3c21b73d43779b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowsol_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a7f02479deacfea07bf87443ea57e9e02</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>redcost_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac48375c35069c1f6acae73f07783ae0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowact_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>acbbd61ffcc82362109fb2f867b577623</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a76ec70b89af5e665c484fbd06c5bf511</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinPackedMatrix *</type>
      <name>matrixByCol_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a4658ad67b782ddb157d879c6789a9572</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>coltype_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a1e3e9811f41224495f228f27e667fe9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>coltypesize_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a5a95c47a2b9acf0128ce6b23f1af8d5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>probtypemip_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a27e70032d433db728ba0a0697f2e440b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a87487bf401ef350cb55182d2e77ac7e6</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a59114486e2f86a41968c9ac55e41a8af</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>switchToLP</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>abbba641cdd86c246035341d296c6f340</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>switchToMIP</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>af8167fa9ce9197c6c6bfd6ccd9720a36</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>resizeColType</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a93cbadf874905ad836ec4be2cf8a56cb</anchor>
      <arglist>(int minsize)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeColType</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aae3dd7a3d4a910f901a3f52f9a2532aa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>bool</type>
      <name>definedSolution</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a8b428a2b17297ac454f6b18e487f8fe1</anchor>
      <arglist>(int solution) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>int</type>
      <name>solverUsed</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a1cdc16e01f94c2102a305a167edb0b17</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static unsigned int</type>
      <name>numInstances_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a47a6632593a6c6184ba7b3e8a10a7c54</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static MSKenv_t</type>
      <name>env_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>abc85eaeee3efdc1c8f05254f618d2abc</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiMskSolverInterfaceUnitTest</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>abd3f7d4ea20bfdf2c0070a9b30241449</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>keepCachedFlag</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_NONE</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaacfaf6b86589d1fed1ce2d441dcd3222b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_COLUMN</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaa24701c4ff77d97a51946317331cc4821</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ROW</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaa6e6fd215a4c51b736bb1d339498663b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_MATRIX</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaa5bb165a05911920b77fc4250bda1f629</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_RESULTS</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaafea4efa6bced79c8386e67d0716eca78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_PROBLEM</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaaafce27c53a9d56cd50e65249efc573e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ALL</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaa1f8e3dc9f47d607ee992dbc0a0eebd8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_COLUMN</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaa2883bee92f3a7e4d2506671f01017348</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_ROW</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaabbcc8f018b39158e1fdd67f3ccbb0658</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_MATRIX</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaab17576b4c135b571807b7abfbd9c2848</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_RESULTS</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>afdae1673e0df4dadcf030e8e40e2d1eaadbfdd9fe890c11d9c54465abe7015a61</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>MSKtask_t</type>
      <name>getLpPtr</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a9ddebceac8c18cefda78e025364b8fd8</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function">
      <type>MSKenv_t</type>
      <name>getEnvironmentPtr</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a6aaca98147b8b245304535b467e9de25</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>Mskerr</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>acc7c79931a368601f599b63dfc4dd83b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>MSKsolverused_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a37cc674b62e141e92ad36341a51cfee9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>ObjOffset_</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a117d756be7b379bf1840c0534408856e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>InitialSolver</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a0b22b05d2b84522a1fd181fad1667127</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>MSKtask_t</type>
      <name>getMutableLpPtr</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>aedc2b8f1c7449a5b1faf5c5bc8e62572</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a7e1d0622c423f23100722a26ce9e89df</anchor>
      <arglist>(const OsiMskSolverInterface &amp;source)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfConstructor</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a49493966c8b2e77f32ef9e1ba83180a6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>ac7646f5d2b71391fb62ac8e01b05ce88</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeCachedColRim</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a00b750a5cd4df88123431b70ca102fd5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeCachedRowRim</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a0f0feb3e12807d271ae165ca1e34a0f7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeCachedResults</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>acdde65b64c80f7136fc56eb8b7c89089</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeCachedMatrix</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a678050028ad4435303896a4118f8f108</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeCachedData</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a179f95e0d2d17f97dceb11dade34244e</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeAllMemory</name>
      <anchorfile>classOsiMskSolverInterface.html</anchorfile>
      <anchor>a2c6a7f96f021d68d48a5b838243cd327</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiObject</name>
    <filename>classOsiObject.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual OsiObject *</type>
      <name>clone</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a566c95811b7da11d057a2d0067c537fa</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>infeasibility</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a45f0f56c36bd5feda2e5abfa41502287</anchor>
      <arglist>(const OsiBranchingInformation *info, int &amp;whichWay) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>feasibleRegion</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>ae593405f9cdf36f5961bbcb5b660f4d8</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info) const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>createBranch</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>aa0d00a5121f5859f68f71214bdf426a1</anchor>
      <arglist>(OsiSolverInterface *, const OsiBranchingInformation *, int) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canDoHeuristics</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>aa211be9ebdd09a082e7c2496d3e48498</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>columnNumber</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a009857f6764afbe01063a6536b9ce366</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canHandleShadowPrices</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a579883a8cf5e23b9d951297140fde5a3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>preferredWay</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>aa30c56143d71b8a4aef8b1a20e23fcae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>upEstimate</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a9da150c404d9f15bff48dcacffd42f54</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>downEstimate</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a0ef0ba334ce3fdc8de7721ab494504b6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetBounds</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a5bee408761a291b806b50cb6af1a6609</anchor>
      <arglist>(const OsiSolverInterface *)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetSequenceEtc</name>
      <anchorfile>classOsiObject.html</anchorfile>
      <anchor>a38622f171cc678224e700a7fb5de336d</anchor>
      <arglist>(int, const int *)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiObject2</name>
    <filename>classOsiObject2.html</filename>
    <base>OsiObject</base>
  </compound>
  <compound kind="class">
    <name>OsiPresolve</name>
    <filename>classOsiPresolve.html</filename>
    <member kind="function">
      <type></type>
      <name>OsiPresolve</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a2a5957cb17f230c60034550e68a361a7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiPresolve</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a63fe854b23c976e9d717f968ebf07154</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>presolvedModel</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ad3200cf73532fdb3d07c890c581b0d41</anchor>
      <arglist>(OsiSolverInterface &amp;origModel, double feasibilityTolerance=0.0, bool keepIntegers=true, int numberPasses=5, const char *prohibited=NULL, bool doStatus=true, const char *rowProhibited=NULL)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>postsolve</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a4aea55e17c41a802fe637d0131d535ab</anchor>
      <arglist>(bool updateStatus=true)</arglist>
    </member>
    <member kind="function">
      <type>OsiSolverInterface *</type>
      <name>model</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a549191f21a3eb811f3e3f0b614e7c94c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiSolverInterface *</type>
      <name>originalModel</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>aa7745a377225a8f6ce0db547386ea83e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalModel</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>afc854d8f6f2e0359ef9d8585b9cf25b7</anchor>
      <arglist>(OsiSolverInterface *model)</arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>originalColumns</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a87b4b53f8a4e417acf6bd59298b11167</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>originalRows</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a4cad3f19bbc940442b51768424162019</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ab0604d485d1b39942ef2c2f6ce35dfdb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ae2d9ef6456da29b4ee2af7aaa32bef76</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNonLinearValue</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a24777c21a7b45410bca6e3dfb4adec44</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>nonLinearValue</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a35c019d2a0e8376d52ce2858041dd274</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPresolveActions</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a855e3eecee6d267970861198edee0fd7</anchor>
      <arglist>(int action)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a85115710d997cb956dfc9351e83eeb3b</anchor>
      <arglist>(CoinPresolveMatrix *prob)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>postsolve</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ac43b93c9394059aa99a687206af2d62a</anchor>
      <arglist>(CoinPostsolveMatrix &amp;prob)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfDestroy</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a2a6cf6a57886cf41de48d05c0f3eca29</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiSolverInterface *</type>
      <name>originalModel_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a5bdb6bd43b2e03e1963d9cd5f32344e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiSolverInterface *</type>
      <name>presolvedModel_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a89da762669ee033987a38f8ee3882a8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>nonLinearValue_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>aaa3e2dc9138d4ac4f45c2a8fe22eb372</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>originalColumn_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a19521b8b8b789bb14bb1304ba41e51da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>originalRow_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a8b91b8cb838f89321aa03492a21cc1f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>const CoinPresolveAction *</type>
      <name>paction_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ae48d19d501cec86cca8cf175b3e0f3df</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>ncols_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a8c1dcef842cbe03bdecbf22fdd5dde5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>nrows_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ab007cc2fa01451ba782c0fd35ed16596</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinBigIndex</type>
      <name>nelems_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a7a1178d47fcf8b2e6689b3f7a8706784</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>presolveActions_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>a770dadbe688e4ccf5296cf6a388d4488</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>numberPasses_</name>
      <anchorfile>classOsiPresolve.html</anchorfile>
      <anchor>ace773cc004dc2bcfe9242d5e9552b9c6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiPseudoCosts</name>
    <filename>classOsiPseudoCosts.html</filename>
    <member kind="function">
      <type></type>
      <name>OsiPseudoCosts</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>adf7f2a962e7f85ff3e6080dd5f328314</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiPseudoCosts</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a58751612a1a180f889ff00ad89779f87</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiPseudoCosts</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a08b77ccec56920a1ea03189f97c43b3c</anchor>
      <arglist>(const OsiPseudoCosts &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>OsiPseudoCosts &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>ab9d821352e82c9d00c9d5d1d80a01b48</anchor>
      <arglist>(const OsiPseudoCosts &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberBeforeTrusted</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a28d411e68c6e7bef5eb55d0cf8f341ce</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberBeforeTrusted</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a8ac544a42370a4b16aecaa06739c9ac3</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a6b40b1d97fd73c35b7684814b6415fb1</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberObjects</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a0ecaf757b1cd4a43d09dfa8de5982e56</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>updateInformation</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a35f823bf61bf97707a00f64f98ebe22e</anchor>
      <arglist>(const OsiBranchingInformation *info, int branch, OsiHotInfo *hotInfo)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>updateInformation</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a057ba056985e1b09fcc14492e301ee46</anchor>
      <arglist>(int whichObject, int branch, double changeInObjective, double changeInValue, int status)</arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>upTotalChange</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a53425aeb93b60c74870ab7a02e28557a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>upTotalChange</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a023b3a477c5bddf604d8385c64a8e1ff</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>downTotalChange</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a967c5fe236bc3e96766b9f2a68401760</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>downTotalChange</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>af32939bba21ea52aa0191d8f9b34c365</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>upNumber</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a229102b17d92bff134c88d439beb9d82</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>upNumber</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a94a10f23291cab60a18d15a4bd8c9455</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>downNumber</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>af8b4d99525eb035ea6a2ce89b65830fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>downNumber</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a4585a3fcd7735ee2d2128b6309561bae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>upTotalChange_</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a3c21f31763f2efd7a63de1fc05a7859c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>downTotalChange_</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>ac3e59df062d59eef89b2cfd1d563ef0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>upNumber_</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a197fe71e10ea1501d115f345be8a2a2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>downNumber_</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>ad92183bf8a65633e8d654bcd2e521371</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberObjects_</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>ae3770163874cd98a3f8e217a62188989</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberBeforeTrusted_</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a04991eea7906e42971c63dafb96455d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfDelete</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>ac8be25907147e3018bc28cb9c443f517</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiPseudoCosts.html</anchorfile>
      <anchor>a6dd1ff0eab71464ca9d5a4f3eab209aa</anchor>
      <arglist>(const OsiPseudoCosts &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiRowCut</name>
    <filename>classOsiRowCut.html</filename>
    <base>OsiCut</base>
    <member kind="function">
      <type>void</type>
      <name>sortIncrIndex</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a0d3c3e9888539041f7b3331b108711cd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline double</type>
      <name>lb</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a375c79029628363eac18021c029e7441</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline void</type>
      <name>setLb</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a35880547d33131a994a49c9f3025dd95</anchor>
      <arglist>(double lb)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline double</type>
      <name>ub</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a9fd82473bb5b80536ca0aed06e978649</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline void</type>
      <name>setUb</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a333e733c642faf2f5ae64c412b167330</anchor>
      <arglist>(double ub)</arglist>
    </member>
    <member kind="function">
      <type>char</type>
      <name>sense</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a4581085c77178d6b68c245e701e9eb80</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>rhs</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a9db3f45c80316f16ea2bc6ddd5c71abe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>range</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a036d75d72563f889103ddb61170d5b39</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline void</type>
      <name>setRow</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a9130cd2b0e582de214870380f6fbd9b2</anchor>
      <arglist>(int size, const int *colIndices, const double *elements, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline void</type>
      <name>setRow</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a6ff79ea7bf8af5e27cdc26b37f654d05</anchor>
      <arglist>(const CoinPackedVector &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline const CoinPackedVector &amp;</type>
      <name>row</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>ac4f9995dd083561ec411dbbdebfe0d67</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline CoinPackedVector &amp;</type>
      <name>mutableRow</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a8319b47f497fa22b431042b7f4e9d83e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline bool</type>
      <name>operator==</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a4d888255cf93d8275de8046c036252f8</anchor>
      <arglist>(const OsiRowCut &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline bool</type>
      <name>operator!=</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a448996ff04ca1b86ca02d7c2a0ea7471</anchor>
      <arglist>(const OsiRowCut &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline bool</type>
      <name>consistent</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a34f6983755661d2d7587b8551d8012fe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline bool</type>
      <name>consistent</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a228d8348fef1c813e881d292a7dc28bd</anchor>
      <arglist>(const OsiSolverInterface &amp;im) const </arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut_inline bool</type>
      <name>infeasible</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>aef5e08ac33b0d2e51270555ab5731727</anchor>
      <arglist>(const OsiSolverInterface &amp;im) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>violated</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>ab93a3289bea5f5d7b5ac32018cd800b6</anchor>
      <arglist>(const double *solution) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator+=</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>ab7e4003a85c92e7a8d0d8464aeb107f3</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator-=</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a92fde354959ac294c845cf261f9b13a6</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator*=</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a3cc3d8c2f9c4ec0f2cca70c6105d037c</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator/=</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a84bff71e11fb743a7547dacd1e93392e</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>ab2128c78d5f0d8b61db54b20f0e5b487</anchor>
      <arglist>(const OsiRowCut &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCut</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a4693645e8b0577a094b9873fa41c370b</anchor>
      <arglist>(const OsiRowCut &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiRowCut *</type>
      <name>clone</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>ac2213e970dcf1510386a6536edf8574d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCut</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a8ca967e7097a435d213187f8c65e243f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCut</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>adaaf54aedd807d93ebe0fae2deaac195</anchor>
      <arglist>(double cutlb, double cutub, int capacity, int size, int *&amp;colIndices, double *&amp;elements)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiRowCut</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a5ed9e00e5094a5ef8059a3dc434c8f57</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a1462c5f6a6b7201aabab1550f77dabad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedVector</type>
      <name>row_</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a613387942c0aed55020774b47f796de3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>lb_</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>ab45fd0d04a13296983d62fda2195d6cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>ub_</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>afcd25a641a08f73cc4e5232e3b781624</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiRowCutUnitTest</name>
      <anchorfile>classOsiRowCut.html</anchorfile>
      <anchor>a97aab68b88609d86d8791f32897a733a</anchor>
      <arglist>(const OsiSolverInterface *baseSiP, const std::string &amp;mpsDir)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiRowCut2</name>
    <filename>classOsiRowCut2.html</filename>
    <base>OsiRowCut</base>
    <member kind="function">
      <type>int</type>
      <name>whichRow</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>aca1fa0855a3db7202acbd005c6a7c7fb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setWhichRow</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>a5b942cb67a23090f8986817ada28ec28</anchor>
      <arglist>(int row)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCut2 &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>a4c0113bc27340e6e3a7191a5fb9d7c9f</anchor>
      <arglist>(const OsiRowCut2 &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCut2</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>a56ec920cc95cff2b20f71cf8b462253a</anchor>
      <arglist>(const OsiRowCut2 &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiRowCut *</type>
      <name>clone</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>ac1075835b30cf686c9d17955d15ee61b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCut2</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>ab9efd33557f32146838ff93b4f3b043b</anchor>
      <arglist>(int row=-1)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiRowCut2</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>ae47c4d78c0bd7611253a640c1c3a5a17</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>whichRow_</name>
      <anchorfile>classOsiRowCut2.html</anchorfile>
      <anchor>a904495424ee7c4b44e8f8ab5561e2232</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiRowCutDebugger</name>
    <filename>classOsiRowCutDebugger.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>validateCuts</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>abb5ccf22d285b8c8e389c60310c783c7</anchor>
      <arglist>(const OsiCuts &amp;cs, int first, int last) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invalidCut</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a303458c73cadf5d0555edfc9565d0fff</anchor>
      <arglist>(const OsiRowCut &amp;rowcut) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>onOptimalPath</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a986092978379109bf756465cd3be95bc</anchor>
      <arglist>(const OsiSolverInterface &amp;si) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>activate</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a8e5540797f47fe18fd38565a6483c800</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const char *model)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>activate</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>af0d02ae3684bc40a73d06bb8acf763de</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const double *solution, bool keepContinuous=false)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>active</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>ae23795ef466c8cbe3089ceeedf1286b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>optimalSolution</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a8322c290f6d1e8c7af543aead629a425</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColumns</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>ab4a3386ee441c8a357f4829cdded35f0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>optimalValue</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a94fadc19f4b1fe59c00dc818bfd57b6e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>redoSolution</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>ae64db190e0a08d06483fcff57787bca9</anchor>
      <arglist>(int numberColumns, const int *originalColumns)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>printOptimalSolution</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a88c5cb90e3b9bb41e67ae83f4feff644</anchor>
      <arglist>(const OsiSolverInterface &amp;si) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCutDebugger</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>afb313b0e0c1bb4a094fca95bf8487e7e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCutDebugger</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a5fdf4775dbc131c309d93ba772545ddc</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const char *model)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCutDebugger</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a653eb50f66bc931c0a763635c38bde30</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const double *solution, bool enforceOptimality=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiRowCutDebugger</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>aff740a12fa51c1a3d9c7fd1ffae990e6</anchor>
      <arglist>(const OsiRowCutDebugger &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiRowCutDebugger &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>ae2de329960baa47454ac4823ea38cace</anchor>
      <arglist>(const OsiRowCutDebugger &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiRowCutDebugger</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>aea5ef5956b143ed66e4c837da8637eae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>knownValue_</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a1ac011e4936880c80ea9f0037c7206a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a14c3cc4febecfdfc111761f5e4cd3749</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool *</type>
      <name>integerVariable_</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>a7357d038b3726dcb64e6235a57a33657</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>knownSolution_</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>afb30a53358cdd28c4be437124c7de01b</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiRowCutDebuggerUnitTest</name>
      <anchorfile>classOsiRowCutDebugger.html</anchorfile>
      <anchor>acc149ef7d160e813359a1f3536e240bb</anchor>
      <arglist>(const OsiSolverInterface *siP, const std::string &amp;mpsDir)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSimpleInteger</name>
    <filename>classOsiSimpleInteger.html</filename>
    <base>OsiObject2</base>
    <member kind="function">
      <type></type>
      <name>OsiSimpleInteger</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>af49262c65c639e1c0baf8677123cff9a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSimpleInteger</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>abf7459330cd23a96397a6fad7331e0d2</anchor>
      <arglist>(const OsiSolverInterface *solver, int iColumn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSimpleInteger</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a2bdcbeb8ce8252454184376fc686d85a</anchor>
      <arglist>(int iColumn, double lower, double upper)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSimpleInteger</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a6cb0bbc4d99586ea5d81af492add4727</anchor>
      <arglist>(const OsiSimpleInteger &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiObject *</type>
      <name>clone</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a52c8e6e789a5b7855ed35c212910c786</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiSimpleInteger &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>aede1cfffd59416034ba77678128451ef</anchor>
      <arglist>(const OsiSimpleInteger &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiSimpleInteger</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a5a584529e22dcf9d1aa47c47b263c9d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>infeasibility</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a0ebfa443ef7284d2cb4c61a88d85a614</anchor>
      <arglist>(const OsiBranchingInformation *info, int &amp;whichWay) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>feasibleRegion</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a27667a55e8b94426ded1dc8a8eb952e1</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>createBranch</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>aafd9a91b3563000a3fd0dafc56645217</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info, int way) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnNumber</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>aa6f0031a523465183c6b01d5ee493ab5</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>columnNumber</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a5bbb57fc0a52f98642a639b69864b938</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>originalLowerBound</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a3b0cdf445775497feb0087f5fb93e011</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalLowerBound</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a7e6a10a1b88fe1aaea674306d1c0f7c6</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>originalUpperBound</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>af41c8516e4d77e6db368fd91fcc7326f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalUpperBound</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a4cef7fbdc16389ba45527de8750eb8e8</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetBounds</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a5a53dd591feee551cbf1c02b619b111d</anchor>
      <arglist>(const OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetSequenceEtc</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a0afbdb7f473be7cd1516071e6a3fae1e</anchor>
      <arglist>(int numberColumns, const int *originalColumns)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>upEstimate</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a352482db976226832b432ec38f7e9629</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>downEstimate</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a691d9498ca416d5348110e96200f004e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canHandleShadowPrices</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a9fa2e169da9a61ba104ac135da9c5bd3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>originalLower_</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>ab4c367e0306b41723f9b28f8b0c49088</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>originalUpper_</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a83f51b3b0eaf03c6793d35d917377d56</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>columnNumber_</name>
      <anchorfile>classOsiSimpleInteger.html</anchorfile>
      <anchor>a2bece2bf51ac6a09029632e789a8b84a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSolverBranch</name>
    <filename>classOsiSolverBranch.html</filename>
    <member kind="function">
      <type>void</type>
      <name>addBranch</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a8d891f9871e8dbea6deb4334e1b677e6</anchor>
      <arglist>(int iColumn, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addBranch</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>af0382592d88a8ee303bfb1d4bb1d7161</anchor>
      <arglist>(int way, int numberTighterLower, const int *whichLower, const double *newLower, int numberTighterUpper, const int *whichUpper, const double *newUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addBranch</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a33d6fa7120223af6ac2946f362de8899</anchor>
      <arglist>(int way, int numberColumns, const double *oldLower, const double *newLower, const double *oldUpper, const double *newUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>applyBounds</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a604c9f1e7c3a39e2fd8df8e3f071e37d</anchor>
      <arglist>(OsiSolverInterface &amp;solver, int way) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>feasibleOneWay</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a5ce18300c4ac6fe2d00fd6241ac6592e</anchor>
      <arglist>(const OsiSolverInterface &amp;solver) const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>starts</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a8cf9974135b4520a81a89688891c6f16</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>which</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>ad9960e3ebf2b99af5c289e0bc89d7117</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>bounds</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a97caa5978eddff7d72e95eff9dfb359f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverBranch</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a30ee9086670c64bb337db80d6d7130bf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverBranch</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a1ad5629f6bb3ebfe7270103f49bb22e0</anchor>
      <arglist>(const OsiSolverBranch &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>OsiSolverBranch &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>aa2b6bc58f99273d14afe3df9f13d89be</anchor>
      <arglist>(const OsiSolverBranch &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~OsiSolverBranch</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>aa1fe98a8832783f16565cfc6cd5221e9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>start_</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a28b77a2613fbd30d4f48799af641350e</anchor>
      <arglist>[5]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>indices_</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a3ecd276b8b6db31aa19b05dc07be5df1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>bound_</name>
      <anchorfile>classOsiSolverBranch.html</anchorfile>
      <anchor>a95cde0d5a8128607539847f3d9eea3cc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSolverInterface</name>
    <filename>classOsiSolverInterface.html</filename>
    <class kind="class">OsiSolverInterface::ApplyCutsReturnCode</class>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a9b4537cca4d4114e056048f7a7036a0b</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0c611674ae8a4420fdf9e8d6d4250c68</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa0ece26fcf59a10c4cbbffac57454424</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a98b52c4481e9e30464605639d6320971</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac16228dd6419b19109e5db4405870de1</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setStrParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac37f2fe0781f7e69fc853c44d330d402</anchor>
      <arglist>(OsiStrParam key, const std::string &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2719d1448fe2f2cd4107a75b866df4a9</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a552d9332b8e48751d7a3db4908a402d5</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5a838b360afe5b442ff6662bc1882ecf</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac215b1d1dff3899a20cace3ffa0609fb</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a47c2a59634bea59050399e62c59bc930</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3027c87922774b29d594b4847851c04e</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8d4e9e74af2967692ac839dd6c7aa023</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isPrimalObjectiveLimitReached</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2932ebb3224e672320b7d984ac75f87f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>adb21ed8e26752f55cb46dee2be3be0d1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a615208310949041411ea7e103a58d42c</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a23f0411beed15645934c11744ffd5514</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af942c65a484de38db29e672bbd1e546f</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af7dba660a92526fd4de6507bae3c6d2a</anchor>
      <arglist>(const CoinWarmStart *warmstart)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8df9ee1731850ae9eabeed29bee6216f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>adfcc8091906950b332f24382b5122f22</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a41626d6d1ff9ae3aa0db98eee974fd47</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>adc1fb78e4e2a2368dad28307b809ae9e</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a94470e552b923148a436c9dec8b7d971</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab92819cbeafc47e35004f5ccb0b25db5</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0023289b37ed08da33a69ffba8faf319</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a7335a62c33b98b15a21fdbf5add6b0bc</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a944bfc5160e9339af2ad9aac4ad0cefe</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a7711848f7c3cdac63f5946aa30032186</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a33ec785e316610370ff4b0619ca081c6</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ade643f70aab45d4968bdfdcac9a5571b</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3e712b89eac0fd6bdae0d45ee32093bc</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a12532da315fd3a0a060052c742208262</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac027354628c3b67ba6e16277e8974aa5</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab4c4c0987bce2b0e678c7687b212bc54</anchor>
      <arglist>(int colIndex) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a4a9c90f85e7b435a546be9c72cdd11ac</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a650b1e93fa0506d5f11a23561e50df10</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2d72605415e6506dbbc01111bedcb9a7</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a8a127eff6b1ae0e800ad6039ff1142e0</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2a6c02d1f55a2f46225d0a5fa03b66a1</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac6f55e398becb2f995f342c1dda492d8</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2f7ccb4840289da8a2604f00e13394b4</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aaea20b850d9985990d25fb5caf5329bb</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a195998db1a5da926130bd6fe93144ac3</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aaf98c008d5abc5af2f50a8182c4fe5b2</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a2bb41a788ddc497a155a6eb460525909</anchor>
      <arglist>(int maxNumRays) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aff6dd777301e3feefe0be46be2e74387</anchor>
      <arglist>(int elementIndex, double elementValue)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aea0422df15377614d9934cce4e0ceb86</anchor>
      <arglist>(double s)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a918fc38cedd52e11c03f5cf101ce1fe4</anchor>
      <arglist>(int elementIndex, double elementValue)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ab26eb753fca0ac04650ef110c17ab90e</anchor>
      <arglist>(int elementIndex, double elementValue)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a21685ed186e30dd92b8cc06d92808ea9</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSetBounds</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a37aefff233ae6638784c2af862e9f81e</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa0dd5fef54d03999d8f2fa7cadcc9dc1</anchor>
      <arglist>(int elementIndex, double elementValue)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a918feed9e75a725a637c4a0e08f1bb7d</anchor>
      <arglist>(int elementIndex, double elementValue)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>afdf50804a16f2b3259e7bf94ec6d37a1</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetBounds</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a060fe5a5f57932dc32ce42809f8a8d60</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>adb30a88b8f6afaa8471157743ac8a105</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetTypes</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a978e825eaae3910188d08da455f170cb</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const char *senseList, const double *rhsList, const double *rangeList)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a56e9f91715aa63c263aab13d46203efb</anchor>
      <arglist>(const double *colsol)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5f966331a098727d8783f737b024faef</anchor>
      <arglist>(const double *rowprice)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a49c9e866b9e67183bebeaaaabff1efa1</anchor>
      <arglist>(int index)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad435813d252120a6886ba50d1be1ad7d</anchor>
      <arglist>(int index)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aeaaf2f6d06373afe47f38a4470a0d8d2</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a232c411ca8ed5b536c3e916938ac7b6f</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a0943eecd48dc5bb43564d0d3f6d0dac5</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac07df4703642a919d53a1d42a20c696a</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a1142a67bb1657dcc012c66ad581bdce3</anchor>
      <arglist>(const int num, const int *colIndices)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a54804d3ea0d4691727e68ca86a27c0c9</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a3b50ec92a58e6dcab48ca0e0207e17c6</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a6957503d520d7d080ba8ef47d2ceece3</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac6a528ebe53957baf5bcd13110fa0353</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aed56c69b5d48406b28549a49da3358f6</anchor>
      <arglist>(const int num, const int *rowIndices)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a7fb7d7fabd8d2880684dfebe53292146</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a628c47507065b5d19c1dec4f4981af89</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a52f10d05e1a4c1f019ebeadcef824b60</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af88f3250e368659286a161fbe5db66fd</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a82c520a822c2c22219b6681b1292d014</anchor>
      <arglist>(const int numcols, const int numrows, const CoinBigIndex *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa90be801bc88d0fc67b7d343ea62f84c</anchor>
      <arglist>(const int numcols, const int numrows, const CoinBigIndex *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ac1ab31a12e1d052aec04267e1d57418b</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a737f65d201081e6c2b70600dee6037ce</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a765b87d7fbc48ab2620b8aec74349804</anchor>
      <arglist>(bool copyData=true) const =0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aa4f040968886fd1077bd639970f7089b</anchor>
      <arglist>(const OsiRowCut &amp;rc)=0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a69285a4985dd37cd55490b6dc3c3719c</anchor>
      <arglist>(const OsiColCut &amp;cc)=0</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiAuxInfo *</type>
      <name>appDataEtc_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a1978a9620c7cd4a9438ecd17f0079c2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>intParam_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5fdc36659906feaa119e703d3b953856</anchor>
      <arglist>[OsiLastIntParam]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>dblParam_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af49b3911bf8c1cd21e54db911679f5cb</anchor>
      <arglist>[OsiLastDblParam]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::string</type>
      <name>strParam_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af4d362fd8baf6f401f8149febdf3095f</anchor>
      <arglist>[OsiLastStrParam]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>hintParam_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>af42b646a11b01420068310af01342a18</anchor>
      <arglist>[OsiLastHintParam]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiHintStrength</type>
      <name>hintStrength_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a52f19d2010e6ed2b62a8a3f9212ffeee</anchor>
      <arglist>[OsiLastHintParam]</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinWarmStart *</type>
      <name>ws_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>aabb06668bd571b1f1dae5155fdf10dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; double &gt;</type>
      <name>strictColSolution_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad21e437db89c995eee190cf6b8fa1496</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiNameVec</type>
      <name>rowNames_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>add7228d3d1a4f2bb8cc7d376dcc380fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiNameVec</type>
      <name>colNames_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a28e3dc870d9712a9e1ae2515dafaf274</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::string</type>
      <name>objName_</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>ad75415555548824117e288b03239ca1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiSolverInterfaceCommonUnitTest</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a5740577958c910d3d0628f18e5256e2d</anchor>
      <arglist>(const OsiSolverInterface *emptySi, const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiSolverInterfaceMpsUnitTest</name>
      <anchorfile>classOsiSolverInterface.html</anchorfile>
      <anchor>a03d9b9f060631444a749c60c7fe47817</anchor>
      <arglist>(const std::vector&lt; OsiSolverInterface * &gt; &amp;vecSiP, const std::string &amp;mpsDir)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSolverInterface::ApplyCutsReturnCode</name>
    <filename>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</filename>
    <member kind="function">
      <type></type>
      <name>ApplyCutsReturnCode</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a47efa84f0854578c017b3ea229ff4610</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ApplyCutsReturnCode</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a592446f8ce2eecc4765129db1e9f140d</anchor>
      <arglist>(const ApplyCutsReturnCode &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>ApplyCutsReturnCode &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a6b5c92a7b7b8481981b17f5b72ffc698</anchor>
      <arglist>(const ApplyCutsReturnCode &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~ApplyCutsReturnCode</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a1b1f4fea1b4afc21039cd5a8b417d673</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumInconsistent</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a4311cf5deee3f892aa74d6c38f8e5dda</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumInconsistentWrtIntegerModel</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a83f8b6ca9dc61666ab17ddff5eb112f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumInfeasible</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>aec6dc8dd5bb3ff327c7549a81da539be</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumIneffective</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>ad9eeec0073744e561f09dc984f61c23c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumApplied</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a1545923f1c4861e9d12e83cb4480e59f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>incrementInternallyInconsistent</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a9690ce60ecdac1b41e4e5a62c5996ad1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>incrementExternallyInconsistent</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>ad1c7d5464c221e669d2ac592d0b73ddd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>incrementInfeasible</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a73ed72026be8b38ef76a313cdd122274</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>incrementIneffective</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>af29474f2448e3668c9bf05d5d0b42775</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>incrementApplied</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a2c418b88ccde58077403de7f8b60ff4f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>intInconsistent_</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>aed8b53ba7b5f6db9ea08ba6abdec5687</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>extInconsistent_</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a2ebd6add0b4e45142b8f3b583623ea73</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>infeasible_</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a1687764fead901ffd9b25a8b89a8c0f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>ineffective_</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>afd1beb637519eacd78dd96d18c157d6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>applied_</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>aab441dbe4b0aef4867124e917fd6c788</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>OsiSolverInterface</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a3f17ae7496b2b06161796f25080eeecf</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>OsiClpSolverInterface</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a472978a8ea3c13575ce8a329edcaa35b</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>OsiGrbSolverInterface</name>
      <anchorfile>classOsiSolverInterface_1_1ApplyCutsReturnCode.html</anchorfile>
      <anchor>a1a90b3608422aabf2620db2dc2c27f0e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSolverResult</name>
    <filename>classOsiSolverResult.html</filename>
    <member kind="function">
      <type>void</type>
      <name>createResult</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>ac748a292b33305c67924e8625131ea4f</anchor>
      <arglist>(const OsiSolverInterface &amp;solver, const double *lowerBefore, const double *upperBefore)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restoreResult</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>af9f01c9f0fdd98a829afc2eb716c8d38</anchor>
      <arglist>(OsiSolverInterface &amp;solver) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinWarmStartBasis &amp;</type>
      <name>basis</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a6c33409a01c990fc606fec1062abc571</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>objectiveValue</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a271de9165758c577f6e6ec17588f52f7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>primalSolution</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a7c8cd9f0417951640defc839723af5bb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>dualSolution</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>aad7bf03e2a601d390f24aad80e457360</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const OsiSolverBranch &amp;</type>
      <name>fixed</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a8fd8484170c78b5a6b4639fe4952e120</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverResult</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>ae1cfe9ef1aecf35f3d861c257fa02617</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverResult</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a226fa8c0dfb66d595950a5c8eb7bd0d1</anchor>
      <arglist>(const OsiSolverInterface &amp;solver, const double *lowerBefore, const double *upperBefore)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSolverResult</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a07984b28e5cfeacd5cc182546b529f61</anchor>
      <arglist>(const OsiSolverResult &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>OsiSolverResult &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>ad66098ccbbcc5e899a1092107fbfba96</anchor>
      <arglist>(const OsiSolverResult &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~OsiSolverResult</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a27d594b15e81ea4d88ee8270a6aab3c9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>objectiveValue_</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a333ece1f0b8029b811343fc24143ea4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinWarmStartBasis</type>
      <name>basis_</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>a5b3a0c2ec8095e1dd6563e5ab86b090e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>primalSolution_</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>aa523abf3c5220e5609b44f46b8a115d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>dualSolution_</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>afc84d14ffbc82f9fd730e6bd4c6d6ecc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>OsiSolverBranch</type>
      <name>fixed_</name>
      <anchorfile>classOsiSolverResult.html</anchorfile>
      <anchor>ad83f1ad78f1d4fee3f37d9eba5bb832e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSOS</name>
    <filename>classOsiSOS.html</filename>
    <base>OsiObject2</base>
    <member kind="function">
      <type></type>
      <name>OsiSOS</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a1a61f28ec69376b4cc4a61f175f24475</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSOS</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>afd6e0d8865db2393e7360250f6d5725a</anchor>
      <arglist>(const OsiSolverInterface *solver, int numberMembers, const int *which, const double *weights, int type=1)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSOS</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a3a2f4dafec6b5b256d9655c23c860117</anchor>
      <arglist>(const OsiSOS &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiObject *</type>
      <name>clone</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a385f4c856b3b42d9202d286e37249a9f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>OsiSOS &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a73f9375a00d59932734c2e920ad16c56</anchor>
      <arglist>(const OsiSOS &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiSOS</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>abf9b1be723f514d1979227fa783fedf5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>infeasibility</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a252801c79aa005379084817617791525</anchor>
      <arglist>(const OsiBranchingInformation *info, int &amp;whichWay) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>feasibleRegion</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a96d5aa13b111aeb669a83cdb1cb92bc1</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>createBranch</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>af2d31a3744f1a4421039fd44aa8bf7da</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiBranchingInformation *info, int way) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>upEstimate</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a27e1a4810fc30c26891b91cb639a9c29</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>downEstimate</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>afecca03d8651c6f2689f63e8a099597c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resetSequenceEtc</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a6f6cf65bd331a8a90512dd137397af5d</anchor>
      <arglist>(int numberColumns, const int *originalColumns)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberMembers</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a8bcdd3ab2209aa3d4364fff0dab834ac</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>members</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>ac887afde9a8419a6250efadd56b712c1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sosType</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a6ed75ad2bb97831620d2e7640f02537f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>setType</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a90922f61070d03ba87350924c5fc6568</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>weights</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>aac8d552c3bf2160314cf6e0918b92e92</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canDoHeuristics</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a08b03d6429117a98b2b63a8a9d558904</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIntegerValued</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a8bf5b306ca32157d179f77d8a81e7410</anchor>
      <arglist>(bool yesNo)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>canHandleShadowPrices</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a3c0786c298cee1b443cace488983c396</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberMembers</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a7ce38ca3b3028ad2aa55be4e85c99361</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>mutableMembers</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a63906f9e76c51b278928da9e4807b232</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSosType</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>ad0cdfc9d81664693da27135f830a68cb</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>mutableWeights</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a8e45174d76e36446b0cd4d61585ffa1f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>members_</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a8fb2338d94b6b48c1b9fc4304a90c093</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>weights_</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>a0c3e3871aa8ecbfbe403c2a739eb0ccf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberMembers_</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>aed0f73da5285dbd1dc333ad6b44755c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>sosType_</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>ac01e28a04647d9cb5cf4a638c71e3670</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>integerValued_</name>
      <anchorfile>classOsiSOS.html</anchorfile>
      <anchor>aa0c4aa7e60bf539360fed4304fccf39a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSOSBranchingObject</name>
    <filename>classOsiSOSBranchingObject.html</filename>
    <base>OsiTwoWayBranchingObject</base>
    <member kind="function">
      <type></type>
      <name>OsiSOSBranchingObject</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>ac0b60d17464a21e248bad43d05d8f793</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSOSBranchingObject</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>ad7cd2b1b1ea7c5a37297935afcd61966</anchor>
      <arglist>(OsiSolverInterface *solver, const OsiSOS *originalObject, int way, double separator)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSOSBranchingObject</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>a86cae8802ee550b7e5c2a7fc79d65270</anchor>
      <arglist>(const OsiSOSBranchingObject &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiSOSBranchingObject &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>abfe45de2a80671eab941ad5666a14b4f</anchor>
      <arglist>(const OsiSOSBranchingObject &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiBranchingObject *</type>
      <name>clone</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>ab965685ce97f82ae30ba1c6015f97edf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiSOSBranchingObject</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>a8e476f9c6c7fe61150059c085a3af724</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>branch</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>a4f0f3b6d29fc8b3316f85de8743c3bfe</anchor>
      <arglist>(OsiSolverInterface *solver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classOsiSOSBranchingObject.html</anchorfile>
      <anchor>a28da60223c4e35d24b41bc3e96c4a2ec</anchor>
      <arglist>(const OsiSolverInterface *solver=NULL)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiSpxSolverInterface</name>
    <filename>classOsiSpxSolverInterface.html</filename>
    <base virtualness="virtual">OsiSolverInterface</base>
    <member kind="enumeration">
      <type></type>
      <name>keepCachedFlag</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_NONE</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5bab7d02d216e7abccae18e93bb1858d8a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_COLUMN</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5bae891303098833fb82645d28c57b9875a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ROW</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5babf3b24feeca0502558ea8d902922caeb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_MATRIX</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5ba23679eb9613eed0ceedbd7a70d735dbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_RESULTS</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5bad1828ba20a39124eda3a6b5b61ec8b62</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_PROBLEM</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5bab54e68215c94ce79569407337046778a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>KEEPCACHED_ALL</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5ba2ff33542ac22d99678ee5b3cbde24f4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_COLUMN</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5ba6490f91cec89d269379390770eb32446</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_ROW</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5babd5aea00ca86216028ab720e8a48118c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_MATRIX</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5baad4aa673fff14bc3bb2e083263f2036e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>FREECACHED_RESULTS</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ade39334d62114cdfaca6533db4ba8d5baa71fe3634156af1d411b1fb76ca5e8f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ad410a0e660b2d773a8f15d5c480fcaa9</anchor>
      <arglist>(double s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a92a236e0eef0958e719555e6f802613f</anchor>
      <arglist>(const double *colsol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aef6cc266a929d6e7a99b602dbe386be0</anchor>
      <arglist>(const double *rowprice)</arglist>
    </member>
    <member kind="function">
      <type>soplex::SoPlex *</type>
      <name>getLpPtr</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a3cd38a0d64e5a3c86857904add09c1a2</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function">
      <type>soplex::SPxOut *</type>
      <name>getSPxOut</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aade9c596e6a1fcbeab51de9aa6563142</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ab03c20dc5e0f9301588baf34cc5708aa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ad2ec593c56ee4a2b676d8b2077a681b2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a92e3a7d3922c4106e0644223a1104174</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a3ea33837fca928fead92e65175c8a927</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>af402ec7ad60db4c26dbb9cc0c1275464</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a7f0f856f72106ae1f56ad10d5dcd2256</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aead25290870fa82189a09cb36170ec58</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a8de5e080836dd617ffc71dae70dd6370</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTimeLimit</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a7f59d780bb4d3e64c3a4c690892e4507</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getTimeLimit</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a992959a6675f1c3cf55739fbdf4e6345</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a4866a63cefc20f238fb6cacb74056738</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a6e6c2ffe17d1093ab1deb65f70324a39</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aaa995944bc1e5b7fd2e787fa3db24f2f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ab9fb936d126375705a58e3f7a57e6823</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ad6d3e1437042ce889c433abc14ca64c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>acec5918a3ccd12d13bbb44fbdb1cfc00</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isTimeLimitReached</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aaf528045109daef18d0007a49919aa4f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a1a12a1725ae6a6ed76d9b975e8f807cb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a074356716307cf8674a49d552ac94d98</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a0320c3045f102c0a68ff99bfb2fde118</anchor>
      <arglist>(const CoinWarmStart *warmstart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a460861d4dbde88e022d31e0455726a38</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aeba25bdb7265aee03cd4026c5415ddd1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ab92ad9ba307aa053f8115c4cd284488c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a62ce4314d326952d3f0b748346751fd2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ac61395b56d862a57aa25aefe26e4f0cf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a73df318bd7a0d74d921ef611883dcfe9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a67922777606fb2c3b2fef7e9adb0d6ca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a945418a9bc98db60b0b27208cc8d8247</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>af05891f7e1a6b743aff31d3035fe9ea1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a37b213a0e9ec749c2d184d1d67a00488</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a4138e8def0b96a190c073c62f62f4c9f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a6967741c0d6044fccff1f3501c0e055f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aaeadbc775745264dc70f73c60ed51f69</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aad27ca2931c27afee685c859abf25d25</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a965c3288dce926745afc14e30fafd9f8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ab0e91818004c4fd272c37b767e24db50</anchor>
      <arglist>(int colNumber) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aa254ffe1ca44edded582886c83e88ed0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a919ac91f8faea85a70bd3f28f83855ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a99f5d4a2f0229807097f4a067fff8bcd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a115be759d259595a63904721b78668b9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a41ddad1e9ca80c406168e6a09b1da042</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a37609061fe885130d2fe03fbf3b05fad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>af7bdc5ab8e1714d2612b4da1a943200f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>af83113cd502c9d58d6989eceb6bdced6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a3886250230b3adb8f30e62a722bb1cf3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a61865974ea38cf040e60dd57ab883542</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a81a4533e16a5c0d6b2785f4bc6fee3c6</anchor>
      <arglist>(int maxNumRays) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aa89f9b8e8d3ca94104c71b21a7413f24</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>afacd9b6890e5fbe8bcd3db9a8e861495</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a11a07b4975c5872c6fea040e26150080</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ae7b6d48eca77ab7b5f393e5b08050af4</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a1ef90703238b7f432fae1964db1dc69b</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ab759a35f087ddf7bc3073ea33d8e90fa</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a878e1d7dbbe85d7bcb669365cc1595e4</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ac94607437ee9ff66028d96c6702249ab</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a3526c825bda64e74352c6495e07f47a5</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a4d759c411234471e479e79038c9aeef7</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a1d8fed284f69baac534a832ca40dd233</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a55b7ebcc429e3ca98fae1fabdbc6b4ac</anchor>
      <arglist>(const int num, const int *colIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>acfdf319778b5010752463d653a772c49</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ac76b02a8fdb1d7ac2e8d9ffe6b97a9e8</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a29aef9ddefa9f650ad304b7d77620c8e</anchor>
      <arglist>(const int num, const int *rowIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a219de0a7fb2fa3d8c0f68a9aeabc7183</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aef83338380cdb891eecbd38c2985e4ab</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a2de445518733ba46cf899223a79e7447</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ae1d12c345398c8179e934f97de582cc4</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>af0353c93c00f5439bf9261ef7573bf3c</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a13ed0405c5b79b78c71e6c6a068b83cf</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a567483449acc83f89869bd87d61d8079</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a4c00988b04db47e6644db2053e6b7251</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSpxSolverInterface</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a227d0f17647c5cae6b31ae600cc6f79b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ae437e86abeed250988f7e6f4c74ca9f5</anchor>
      <arglist>(bool copyData=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiSpxSolverInterface</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a4f2f9a8142251fede76100bd2ab04b98</anchor>
      <arglist>(const OsiSpxSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiSpxSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a65fade5a41e9b8b5ec8851cb2d9bc553</anchor>
      <arglist>(const OsiSpxSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiSpxSolverInterface</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>afa111cb804ae0fe40032452b44474288</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a4e332dcc0a7493bbe6c7f91aad73d44f</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aa233a548e1f2c6cf223bee78d078beba</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>soplex::SPxOut *</type>
      <name>spxout_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>acb1595e6d039a535349deacd912d3314</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>soplex::SoPlex *</type>
      <name>soplex_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a7a7a3856e3cc0911e37cd277d33e5696</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedColRim</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a5944e38bd32a4b650ae4f6be57e39ec2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedRowRim</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a36b8245334e407df4347087203afab53</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedResults</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ab9b48bb6483b1edc640deb1717a5ac33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedMatrix</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>adaed403c91db9803156eb62ab2025265</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedData</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aa527e00c7fe47751e01a4a99d3ef1504</anchor>
      <arglist>(int keepCached=KEEPCACHED_NONE)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeAllMemory</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>acd5dbf7ffddd29dfbb70c997786578c1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>soplex::DIdxSet *</type>
      <name>spxintvars_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>af27465a2c20929518929c6a6d4675af9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>void *</type>
      <name>hotStartCStat_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ad68e0d72ba7b04e0930f167ec10f559b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartCStatSize_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ac72a903a49071e48137c51756164efe0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>void *</type>
      <name>hotStartRStat_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a05de9d000454ebc09305aa90ec4bb3f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartRStatSize_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aeeb76ea1313249c2bcdb091ad227dee1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>hotStartMaxIteration_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a361cfc4d6ef104a76852cdf5bd6a3f33</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>soplex::DVector *</type>
      <name>obj_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a7f1cde08307d585ef7ade9abe468c180</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a881c2c300ae471118b7052e7b315ec65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a5af7bbba7fa1b164b30c269d0f584871</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aa1dd66b39d1e258b65c7281d45d94096</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>soplex::DVector *</type>
      <name>colsol_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ad2f20bae73f6adbdd57a374d4cbaba83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>soplex::DVector *</type>
      <name>rowsol_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a2f755c1e9fb50bbbcb71c9601b8c67e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>soplex::DVector *</type>
      <name>redcost_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>ac3a43339c6998c0840e75f3c9505d16c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>soplex::DVector *</type>
      <name>rowact_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a60bd539077a97fbe0795f9e0c9a470d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a32907d8e6c048aad3bdd2b3df230ba6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByCol_</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>aeebb2a8102cfcfba951cebde09966565</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiSpxSolverInterfaceUnitTest</name>
      <anchorfile>classOsiSpxSolverInterface.html</anchorfile>
      <anchor>a271b1841c85292d3968c61a26ee18e2e</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiTwoWayBranchingObject</name>
    <filename>classOsiTwoWayBranchingObject.html</filename>
    <base>OsiBranchingObject</base>
    <member kind="function" virtualness="pure">
      <type>virtual double</type>
      <name>branch</name>
      <anchorfile>classOsiTwoWayBranchingObject.html</anchorfile>
      <anchor>a56ab72967d59f370fc0e23aeef31f15e</anchor>
      <arglist>(OsiSolverInterface *solver)=0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiXprSolverInterface</name>
    <filename>classOsiXprSolverInterface.html</filename>
    <base virtualness="virtual">OsiSolverInterface</base>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjSense</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>acc14d79cfb8b5b2d5d0b031db69a5ea3</anchor>
      <arglist>(double s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSolution</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a1602fc328683d9d646aa6ca765cfa3fc</anchor>
      <arglist>(const double *colsol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowPrice</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6c9dfb1817302bd391a8805070bb8ed4</anchor>
      <arglist>(const double *rowprice)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialSolve</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a5239427b559bed6678e942fff26c2243</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resolve</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac16a3d73a4bb3dc42b091c49a02fe5cf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>branchAndBound</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ae212c12e43377c6bb7f159cc3bb1e45a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setIntParam</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ad3c2246ab67a792246cc01c615013930</anchor>
      <arglist>(OsiIntParam key, int value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setDblParam</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a12045e0006fa79aac2195ce13de0418b</anchor>
      <arglist>(OsiDblParam key, double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setStrParam</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a7d429bf0823ebeaa329c0ac4ae0303f9</anchor>
      <arglist>(OsiStrParam key, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIntParam</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a1934ce1560705a0f7b15ea430382a864</anchor>
      <arglist>(OsiIntParam key, int &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getDblParam</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a548bd43ab864640bd0c412e0a04dca93</anchor>
      <arglist>(OsiDblParam key, double &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getStrParam</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac601a9023a933ea74ae7e40d53cece4d</anchor>
      <arglist>(OsiStrParam key, std::string &amp;value) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMipStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ad38599e831ee264fcd833ed73f82ec40</anchor>
      <arglist>(bool value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getMipStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a7952fdbbe123f6915e336e246de56ece</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isAbandoned</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6f00f29f99bdc99b68d85cac3d3b247f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenOptimal</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a4e98a4207786e32fdcb201f280c48be6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenPrimalInfeasible</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aa0f38eab9e56aa41cdec5ecb11f160e8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isProvenDualInfeasible</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a9c9fd790ac10424dca31bf61f3f95378</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isPrimalObjectiveLimitReached</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a0d90ebaea273c8cec6e6442f4444b2f1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isDualObjectiveLimitReached</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a836b54c654a62bc2eb074eea46d40f0a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isIterationLimitReached</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a053f6140747e57efcac666a5c3cca4d8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStart *</type>
      <name>getEmptyWarmStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a17c12af1b487ff5b30bedd1d890e88c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>getWarmStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a9967349707bb0051ec35097269a6e4a4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setWarmStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6d6fa43960d460c14cc264e590b12212</anchor>
      <arglist>(const CoinWarmStart *warmstart)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>markHotStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ad366654739f4c43c82b590da3ed3d77a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>solveFromHotStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ae1924bc1966f9590502fb47a438282bb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>unmarkHotStart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a03a9576b0d5009ded521c270497a8491</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumCols</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aabf2ea19f9f5b7cec168ada1bf54531d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumRows</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>adba5c43b362391dd6008fb1a57c466be</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a3006686a2e63102db78ba84271b22c8c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColLower</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ad67f3d07f6c26db33af0ae65673623e4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColUpper</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aabd9ce356c00043dbdfc91f122421122</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const char *</type>
      <name>getRowSense</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a39ae4e2aa8bc13e7559460e90e856ca9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a372cc6083802a6a9128a197687709d0d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowRange</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>abacffaf6932e57e9fd36b9597e4fc175</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowLower</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a1c99db118987d3e5e7a597ed1e92bfca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ace5ed5356a219ff693ea96c054fa2461</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>abf8d832658a6f1b5c1bdcee3a3101aa9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjSense</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a54d6441b8b1b0df8e522295915176e7b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isContinuous</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a009681d52922a16911fd92b007048d07</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aa1ab76aa7ee90f605fb072d88541b826</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a3d9d1a586e142574deecd48310c456d1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getInfinity</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aa32e104e61b7ca0f08f395c05f077e6f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getColSolution</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>af55c90b81781c2bf61ef4a2c647241c5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a89bfcc54b2abdf1a3b43dfc5ef1e485b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a71594e1d3d314bb3a3db61e9a6c01b17</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>abb494662391d4800201a49230ab89ec6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual double</type>
      <name>getObjValue</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a90f2b84d0d8e20fe4fd98a32fac24c49</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getIterationCount</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ae1811b703b3063eff72d06bae74eeae3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getDualRays</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a135e0d05041f6ddbab845cf2970f78d6</anchor>
      <arglist>(int maxNumRays, bool fullRay=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; double * &gt;</type>
      <name>getPrimalRays</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aa2680b83b07b1f138b928c457a732e04</anchor>
      <arglist>(int maxNumRays) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setObjCoeff</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac5e69e8772e63b0adb5954b7602fb4af</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColLower</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a9006ecf63770486b34076bffe92c752d</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColUpper</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aab271975e15ebe8d9c67752db5cf7da6</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColBounds</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a2f37a6da08238a035baccd14aad403d8</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColSetBounds</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a4936aa82838a039985ecc2cea344d9c9</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowLower</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a08483bc5784f6e279709335c6d8176e7</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowUpper</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a55f68da6c0d5002722ed98017ee1f91e</anchor>
      <arglist>(int elementIndex, double elementValue)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowBounds</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac143a0fb3447c4375a13ecb0f7ab2355</anchor>
      <arglist>(int elementIndex, double lower, double upper)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowType</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a1de5fe8621767fc487e5ea503bf15ca7</anchor>
      <arglist>(int index, char sense, double rightHandSide, double range)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetBounds</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a42948ccf931a08c2517c47f9294d3ed7</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const double *boundList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRowSetTypes</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>acbb9ae38ca9fbda85398c1379777491f</anchor>
      <arglist>(const int *indexFirst, const int *indexLast, const char *senseList, const double *rhsList, const double *rangeList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a70de89f81371e9722ec4896985ff8c5d</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>af64a7039a83a4d3991608e9f1d4a7869</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setContinuous</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a5fce49ee325cc57e2a0a0f9c2550fa15</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setInteger</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6497de1867ad58815aff1853259198c8</anchor>
      <arglist>(const int *indices, int len)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCol</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a4a610cf95a11e97032c826b86b58d6ad</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double collb, const double colub, const double obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addCols</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a305205477e251102f8d73a67a7eec8e8</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols, const double *collb, const double *colub, const double *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteCols</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a7d8f252ff60eef85dc88258141eaea7d</anchor>
      <arglist>(const int num, const int *colIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a0c50391d756a4983734274c70a3d8b49</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const double rowlb, const double rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRow</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac2091d0d87699f2c7e202d91586f5e15</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec, const char rowsen, const double rowrhs, const double rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a668a91522da5ca0cb118c43c3e0f09ea</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addRows</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a644bfa8f8ac63bc6008e08f7d0673748</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>abea78f34ef3bdeba21db61cbea34f9d0</anchor>
      <arglist>(const int num, const int *rowIndices)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aca3cf09bca703a9799bc90c6570d9ffa</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a0fcc9698064e55c9c63ee6269d0b981a</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, double *&amp;rowlb, double *&amp;rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ad0012e566384d0182fd9acf787b0dee5</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignProblem</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a7179811bcc88da36207800abc10d979e</anchor>
      <arglist>(CoinPackedMatrix *&amp;matrix, double *&amp;collb, double *&amp;colub, double *&amp;obj, char *&amp;rowsen, double *&amp;rowrhs, double *&amp;rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a500846e4de53c3c248308d82e26d7ff3</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>loadProblem</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a8c725af4856062f4d6573c864f358698</anchor>
      <arglist>(const int numcols, const int numrows, const int *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>readMps</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a72f6361552482ba4db085982f0e4c8b6</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeMps</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a4e77db934e75b9804a82d88efb823b82</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;, double objSense=0.0) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>passInMessageHandler</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a15472b1be8fc3c0d823b0135a9460724</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiXprSolverInterface</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aa803ba250d1459d46a25a6d3f2e9939a</anchor>
      <arglist>(int newrows=50, int newnz=100)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual OsiSolverInterface *</type>
      <name>clone</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a62991011eb51e7cbd8e990c070035803</anchor>
      <arglist>(bool copyData=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>OsiXprSolverInterface</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a80e4ed1432724b8935a0c988031f1719</anchor>
      <arglist>(const OsiXprSolverInterface &amp;)</arglist>
    </member>
    <member kind="function">
      <type>OsiXprSolverInterface &amp;</type>
      <name>operator=</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a0100de7e4c72443b3290c59affa2b903</anchor>
      <arglist>(const OsiXprSolverInterface &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~OsiXprSolverInterface</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aadf1ef4f3566fcc5055aadf908e0dc9c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>version</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a87f42409c0e1544b7521744ed7c9c24d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyRowCut</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a109fdcae5f69e2199e51a1f8f3c63413</anchor>
      <arglist>(const OsiRowCut &amp;rc)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>applyColCut</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>add1cfddfc4a39c73e88ba826c7ebdd3d</anchor>
      <arglist>(const OsiColCut &amp;cc)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a318db7bc0d4a2d991dae6321a6b50b80</anchor>
      <arglist>(const OsiXprSolverInterface &amp;source)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfConstructor</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>acaa8dad53c6c05dd8745fd61bfef59f7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ab3e6eda87532f2d447ec5e9d05b2c6f2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeSolution</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a60180c88a712d3595daacc657dc5d951</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>freeCachedResults</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ae87ab7b6be72cd8f1d1ed73d87101107</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>int</type>
      <name>getNumIntVars</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a4bf9ba59c71f15a7d7b0b3723da32f78</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>getVarTypes</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a50f54cc2dd6c6c511772a8a6144193c2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>activateMe</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ae8f4c4bfe55dcfa234eafdd781c43c91</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>bool</type>
      <name>isDataLoaded</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a41dcf6aa56792e65d7e784391957f51e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>domipstart</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a178dbefc7beb126dfd283ca0898d6d08</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>XPRSprob</type>
      <name>prob_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6510689abea2a950d221d88cd0eac89e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::string</type>
      <name>xprProbname_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac3d8e889d003d1a7239b7b168c8ed49d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aedd8b90b57c202098fdfa4f865dfe35a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CoinPackedMatrix *</type>
      <name>matrixByCol_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>add11a6efb4235a2df1c09297eac026cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a22fe6d586c8b5bee94467bc16f3dcca5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a2318cd6a8d00535a8ab3896119e8c0fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ac8121af41d035ae61308214de4d3205a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>af571789fda0c1eba9306f4cb08a43abb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ae7080bf3992f6a52a4316ad19de2d786</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a0a2fd2ff0d97c51ef8ddac1875ca89e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a67bbf522cf0e0f69c1de846e7addc1af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>objcoeffs_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aa4d125f156f949a5dcc38a22063e56b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>objsense_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6d133db1fc962b8fca0133b3c967bbcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colsol_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a0d77f879b137ecfb53f02d8416e0dea5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowsol_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a532a8820360a67b76757d9f3692f8ade</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowact_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a94e6405a8bc109d0432a5a00d374fbbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>rowprice_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a158163437aac4ed5409e5e00fb36b878</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double *</type>
      <name>colprice_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a054ca63caf1a7a3995587f5ab4734307</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int *</type>
      <name>ivarind_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6909e9505c432a4b8597052c81fae124</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>ivartype_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a615379a8e02932811ca96d1735414701</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>char *</type>
      <name>vartype_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a801022d7a3d6ab9c91df51f7909ff504</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>lastsolvewasmip</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a13b09295f52b56ad2d1d02718d40c552</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static const char *</type>
      <name>logFileName_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a5dcdd4c1b226b9df7114c40834f61ccd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static FILE *</type>
      <name>logFilePtr_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a54c66caf930ae22b6118b8bf00eeb2b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static unsigned int</type>
      <name>numInstances_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a66ff93e3cc45ee06c3715ed754b28d1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static unsigned int</type>
      <name>osiSerial_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a6e18734e7d9c9626acfed628c92f831a</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>OsiXprSolverInterfaceUnitTest</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>aba4a830e25e527cb078ac041d5528e53</anchor>
      <arglist>(const std::string &amp;mpsDir, const std::string &amp;netlibDir)</arglist>
    </member>
    <member kind="function">
      <type>XPRSprob</type>
      <name>getLpPtr</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a87a06fe8f8202ffd4e2870d7f52dda11</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>incrementInstanceCounter</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a1090e9cf681089e0d6744543fb9a0995</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>decrementInstanceCounter</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a24338f0eee27be0ebdc6114e3dbcb972</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static unsigned int</type>
      <name>getNumInstances</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a7c340bf723018d855c8b9f56ba4dd962</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static int</type>
      <name>iXprCallCount_</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>a2722158da4653584b51c1943d4b45168</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static FILE *</type>
      <name>getLogFilePtr</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>ab16654c0d0a1f255e33955e40f19cfe3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setLogFileName</name>
      <anchorfile>classOsiXprSolverInterface.html</anchorfile>
      <anchor>acc245590a2a34f843ae9fb4d6d33ba73</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>OsiUnitTest</name>
    <filename>namespaceOsiUnitTest.html</filename>
    <class kind="class">OsiUnitTest::TestOutcome</class>
    <class kind="class">OsiUnitTest::TestOutcomes</class>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a73c5c8f2b4aff0e91f16df862b887964</anchor>
      <arglist>(const std::string &amp;solverName, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a5b27dabe2c8d4076cdf9c01a6ed696c6</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a26a27b97f6e571baf300c70eaf5b77e3</anchor>
      <arglist>(const std::string &amp;solverName, const std::string &amp;testname, const std::string &amp;testcond)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>failureMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a71144ed72c864ef0e2c88500cbc29e97</anchor>
      <arglist>(const OsiSolverInterface &amp;si, const std::string &amp;testname, const std::string &amp;testcond)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>testingMessage</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a0c822bb3a0ad104812155660c5503ecf</anchor>
      <arglist>(const char *const msg)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>equivalentVectors</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a3b915f44e262c8b214adb1818124b1da</anchor>
      <arglist>(const OsiSolverInterface *si1, const OsiSolverInterface *si2, double tol, const double *v1, const double *v2, int size)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>compareProblems</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a81d6ceb7ab13802d56255f266802c68d</anchor>
      <arglist>(OsiSolverInterface *osi1, OsiSolverInterface *osi2)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>ad5615e81938c2fd1e8969adadebabf5d</anchor>
      <arglist>(const CoinPackedVectorBase &amp;pv, int n, const double *fv)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>processParameters</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a153694396f3ac26853bc2547f858ed63</anchor>
      <arglist>(int argc, const char **argv, std::map&lt; std::string, std::string &gt; &amp;parms, const std::map&lt; std::string, int &gt; &amp;ignorekeywords=std::map&lt; std::string, int &gt;())</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>OsiUnitTestAssertSeverityExpected</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a4952d26b07bd333e08d099cb038827ae</anchor>
      <arglist>(bool condition, const char *condition_str, const char *filename, int line, const Component &amp;component, const std::string &amp;testname, TestOutcome::SeverityLevel severity, bool expected)</arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>verbosity</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>a974eaac2e2b9d0c0be94ecee2999ce9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>haltonerror</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>ae045fc425c77cb73b07f256fd4e68e26</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>TestOutcomes</type>
      <name>outcomes</name>
      <anchorfile>namespaceOsiUnitTest.html</anchorfile>
      <anchor>ad5c84c7610bcbb8a477ffc0faf262fca</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiUnitTest::TestOutcome</name>
    <filename>classOsiUnitTest_1_1TestOutcome.html</filename>
    <member kind="enumeration">
      <type></type>
      <name>SeverityLevel</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a61aacb0d468dbe60a10b04e8c0fa11f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>NOTE</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a61aacb0d468dbe60a10b04e8c0fa11f1aa7847b508b79772c8dad56f20330e294</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PASSED</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a61aacb0d468dbe60a10b04e8c0fa11f1a7bbf47f427e3bca377ef349e2d06ea21</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WARNING</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a61aacb0d468dbe60a10b04e8c0fa11f1a7cab540645d835907b89404dab66a4b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>ERROR</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a61aacb0d468dbe60a10b04e8c0fa11f1aa2350f3132b0819b0223226c3729b489</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>LAST</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a61aacb0d468dbe60a10b04e8c0fa11f1aaf930d785c80387b751a697040bc4e55</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TestOutcome</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>adbdae66ea026ea6420ff6a850eefe58a</anchor>
      <arglist>(const std::string &amp;comp, const std::string &amp;tst, const char *cond, SeverityLevel sev, const char *file, int line, bool exp=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a78bb0a79bc2c4fe046c70543ec395f8d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>component</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>ac727a5631c01646e5c13fd18e11f3ea3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>testname</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a98fa404404d06732a600f25a87338cc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>testcond</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a727ed324434cb632b63d164d2dd5cf40</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>SeverityLevel</type>
      <name>severity</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a11b254620c44d99bb217ccbee78f542e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>expected</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a0771c1bbdda97e0e9946798c42ba9e8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>filename</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a7c9376c6648043a9bae7021b5722243d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>linenumber</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a1fe89179392bf9adca9945050db3739b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static std::string</type>
      <name>SeverityLevelName</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcome.html</anchorfile>
      <anchor>a4f80c52d34d3547e0c092a1c14fff99b</anchor>
      <arglist>[LAST]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>OsiUnitTest::TestOutcomes</name>
    <filename>classOsiUnitTest_1_1TestOutcomes.html</filename>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcomes.html</anchorfile>
      <anchor>abe5a511f2ff5ea6b665facb478c8a0cf</anchor>
      <arglist>(std::string comp, std::string tst, const char *cond, TestOutcome::SeverityLevel sev, const char *file, int line, bool exp=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcomes.html</anchorfile>
      <anchor>afc424e118dfb9dec8c210aa3a1deffbb</anchor>
      <arglist>(const OsiSolverInterface &amp;si, std::string tst, const char *cond, TestOutcome::SeverityLevel sev, const char *file, int line, bool exp=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcomes.html</anchorfile>
      <anchor>a261a09950da1d2369e481cb35d07feee</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getCountBySeverity</name>
      <anchorfile>classOsiUnitTest_1_1TestOutcomes.html</anchorfile>
      <anchor>a3ca55144706d8959786c7eabe8d14c3d</anchor>
      <arglist>(TestOutcome::SeverityLevel sev, int &amp;total, int &amp;expected) const </arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>soplex</name>
    <filename>namespacesoplex.html</filename>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi/</path>
    <filename>dir_27835a19ee1ed02d3fb46851ff0e09db.html</filename>
    <file>config.h</file>
    <file>config_default.h</file>
    <file>config_osi.h</file>
    <file>config_osi_default.h</file>
    <file>OsiAuxInfo.hpp</file>
    <file>OsiBranchingObject.hpp</file>
    <file>OsiChooseVariable.hpp</file>
    <file>OsiColCut.hpp</file>
    <file>OsiCollections.hpp</file>
    <file>OsiConfig.h</file>
    <file>OsiCut.hpp</file>
    <file>OsiCuts.hpp</file>
    <file>OsiPresolve.hpp</file>
    <file>OsiRowCut.hpp</file>
    <file>OsiRowCutDebugger.hpp</file>
    <file>OsiSolverBranch.hpp</file>
    <file>OsiSolverInterface.hpp</file>
    <file>OsiSolverParameters.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCommonTest</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCommonTest/</path>
    <filename>dir_59e0db883be2f01877b624dcf3bc6957.html</filename>
    <file>OsiUnitTests.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCpx</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCpx/</path>
    <filename>dir_b97cb26e6997b6769e7f0bf65bc10011.html</filename>
    <file>OsiCpxSolverInterface.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGlpk</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGlpk/</path>
    <filename>dir_10161e36e6fc112910bf50bb67fc2cac.html</filename>
    <file>OsiGlpkSolverInterface.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGrb</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGrb/</path>
    <filename>dir_f17b43dbea415d21b38e6b095c77919c.html</filename>
    <file>OsiGrbSolverInterface.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiMsk</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiMsk/</path>
    <filename>dir_84dc09d2a0ece457d1aca46274b6c12f.html</filename>
    <file>OsiMskSolverInterface.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiSpx</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiSpx/</path>
    <filename>dir_0b132ef938fde3a1e3aa3b92cebb0fe7.html</filename>
    <file>OsiSpxSolverInterface.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiXpr</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiXpr/</path>
    <filename>dir_123b901edd9dd679881a99b978c7b530.html</filename>
    <file>OsiXprSolverInterface.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/Osi</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCommonTest</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiCpx</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGlpk</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiGrb</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiMsk</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiSpx</dir>
    <dir>/home/tsokalo/workspace/tmaTool/coin-Clp/Osi/src/OsiXpr</dir>
  </compound>
</tagfile>
