\hypertarget{classCoinPresolveAction}{\subsection{Coin\-Presolve\-Action Class Reference}
\label{classCoinPresolveAction}\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}}
}


Abstract base class of all presolve routines.  




{\ttfamily \#include $<$Coin\-Presolve\-Matrix.\-hpp$>$}

Inheritance diagram for Coin\-Presolve\-Action\-:\begin{figure}[H]
\begin{center}
\leavevmode
\includegraphics[height=12.000000cm]{classCoinPresolveAction}
\end{center}
\end{figure}
\subsubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\hyperlink{classCoinPresolveAction_af46054f481c24be2a80a9106bcc8132b}{Coin\-Presolve\-Action} (const \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action} $\ast$\hyperlink{classCoinPresolveAction_ad3d9739db2130d42da2a9901221d61ec}{next})
\begin{DoxyCompactList}\small\item\em Construct a postsolve object and add it to the transformation list. \end{DoxyCompactList}\item 
\hyperlink{Coin__C__defines_8h_a003fd8bf528d56bed3420613d18ed2b1}{void} \hyperlink{classCoinPresolveAction_a8a04cfe528b455468fe8d5333b730e2e}{set\-Next} (const \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action} $\ast$next\-Action)
\begin{DoxyCompactList}\small\item\em modify next (when building rather than passing) \end{DoxyCompactList}\item 
virtual const char $\ast$ \hyperlink{classCoinPresolveAction_a48019a927c518ef3a7655469530456c6}{name} () const =0
\begin{DoxyCompactList}\small\item\em A name for debug printing. \end{DoxyCompactList}\item 
virtual \hyperlink{Coin__C__defines_8h_a003fd8bf528d56bed3420613d18ed2b1}{void} \hyperlink{classCoinPresolveAction_a7f5dd640a0b6fd260946e055f3eb2535}{postsolve} (\hyperlink{classCoinPostsolveMatrix}{Coin\-Postsolve\-Matrix} $\ast$prob) const =0
\begin{DoxyCompactList}\small\item\em Apply the postsolve transformation for this particular presolve action. \end{DoxyCompactList}\item 
virtual \hyperlink{classCoinPresolveAction_a166e9a398b418a06cc7bb8c201626d28}{$\sim$\-Coin\-Presolve\-Action} ()
\begin{DoxyCompactList}\small\item\em Virtual destructor. \end{DoxyCompactList}\end{DoxyCompactItemize}
\subsubsection*{Static Public Member Functions}
\begin{DoxyCompactItemize}
\item 
static \hyperlink{Coin__C__defines_8h_a003fd8bf528d56bed3420613d18ed2b1}{void} \hyperlink{classCoinPresolveAction_a8a6a3658eb450b93709fe78f689671da}{throw\-Coin\-Error} (const char $\ast$error, const char $\ast$ps\-\_\-routine)
\begin{DoxyCompactList}\small\item\em Stub routine to throw exceptions. \end{DoxyCompactList}\end{DoxyCompactItemize}
\subsubsection*{Public Attributes}
\begin{DoxyCompactItemize}
\item 
const \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action} $\ast$ \hyperlink{classCoinPresolveAction_ad3d9739db2130d42da2a9901221d61ec}{next}
\begin{DoxyCompactList}\small\item\em The next presolve transformation. \end{DoxyCompactList}\end{DoxyCompactItemize}


\subsubsection{Detailed Description}
Abstract base class of all presolve routines. 

The details will make more sense after a quick overview of the grand plan\-: A presolve object is handed a problem object, which it is expected to modify in some useful way. Assuming that it succeeds, the presolve object should create a postsolve object, {\itshape i.\-e.}, an object that contains instructions for backing out the presolve transform to recover the original problem. These postsolve objects are accumulated in a linked list, with each successive presolve action adding its postsolve action to the head of the list. The end result of all this is a presolved problem object, and a list of postsolve objects. The presolved problem object is then handed to a solver for optimization, and the problem object augmented with the results. The list of postsolve objects is then traversed. Each of them (un)modifies the problem object, with the end result being the original problem, augmented with solution information.

The problem object representation is \hyperlink{classCoinPrePostsolveMatrix}{Coin\-Pre\-Postsolve\-Matrix} and subclasses. Check there for details. The {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}} class and subclasses represent the presolve and postsolve objects.

In spite of the name, the only information held in a {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}} object is the information needed to postsolve ({\itshape i.\-e.}, the information needed to back out the presolve transformation). This information is not expected to change, so the fields are all {\ttfamily const}.

A subclass of {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}}, implementing a specific pre/postsolve action, is expected to declare a static function that attempts to perform a presolve transformation. This function will be handed a \hyperlink{classCoinPresolveMatrix}{Coin\-Presolve\-Matrix} to transform, and a pointer to the head of the list of postsolve objects. If the transform is successful, the function will create a new {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}} object, link it at the head of the list of postsolve objects, and return a pointer to the postsolve object it has just created. Otherwise, it should return 0. It is expected that these static functions will be the only things that can create new {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}} objects; this is expressed by making each subclass' constructor(s) private.

Every subclass must also define a {\ttfamily postsolve} method. This function will be handed a \hyperlink{classCoinPostsolveMatrix}{Coin\-Postsolve\-Matrix} to transform.

It is the client's responsibility to implement presolve and postsolve driver routines. See Osi\-Presolve for examples.

\begin{DoxyNote}{Note}
Since the only fields in a {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}} are {\ttfamily const}, anything one can do with a variable declared {\ttfamily Coin\-Presolve\-Action$\ast$} can also be done with a variable declared {\ttfamily const} {\ttfamily Coin\-Presolve\-Action$\ast$} It is expected that all derived subclasses of {\ttfamily \hyperlink{classCoinPresolveAction}{Coin\-Presolve\-Action}} also have this property. 
\end{DoxyNote}


Definition at line 155 of file Coin\-Presolve\-Matrix.\-hpp.



\subsubsection{Constructor \& Destructor Documentation}
\hypertarget{classCoinPresolveAction_af46054f481c24be2a80a9106bcc8132b}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!Coin\-Presolve\-Action@{Coin\-Presolve\-Action}}
\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{Coin\-Presolve\-Action}]{\setlength{\rightskip}{0pt plus 5cm}Coin\-Presolve\-Action\-::\-Coin\-Presolve\-Action (
\begin{DoxyParamCaption}
\item[{const {\bf Coin\-Presolve\-Action} $\ast$}]{next}
\end{DoxyParamCaption}
)\hspace{0.3cm}{\ttfamily [inline]}}}\label{classCoinPresolveAction_af46054f481c24be2a80a9106bcc8132b}


Construct a postsolve object and add it to the transformation list. 

This is an `add to head' operation. This object will point to the one passed as the parameter. 

Definition at line 178 of file Coin\-Presolve\-Matrix.\-hpp.

\hypertarget{classCoinPresolveAction_a166e9a398b418a06cc7bb8c201626d28}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!$\sim$\-Coin\-Presolve\-Action@{$\sim$\-Coin\-Presolve\-Action}}
\index{$\sim$\-Coin\-Presolve\-Action@{$\sim$\-Coin\-Presolve\-Action}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{$\sim$\-Coin\-Presolve\-Action}]{\setlength{\rightskip}{0pt plus 5cm}virtual Coin\-Presolve\-Action\-::$\sim$\-Coin\-Presolve\-Action (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)\hspace{0.3cm}{\ttfamily [inline]}, {\ttfamily [virtual]}}}\label{classCoinPresolveAction_a166e9a398b418a06cc7bb8c201626d28}


Virtual destructor. 



Definition at line 195 of file Coin\-Presolve\-Matrix.\-hpp.



\subsubsection{Member Function Documentation}
\hypertarget{classCoinPresolveAction_a8a6a3658eb450b93709fe78f689671da}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!throw\-Coin\-Error@{throw\-Coin\-Error}}
\index{throw\-Coin\-Error@{throw\-Coin\-Error}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{throw\-Coin\-Error}]{\setlength{\rightskip}{0pt plus 5cm}static {\bf void} Coin\-Presolve\-Action\-::throw\-Coin\-Error (
\begin{DoxyParamCaption}
\item[{const char $\ast$}]{error, }
\item[{const char $\ast$}]{ps\-\_\-routine}
\end{DoxyParamCaption}
)\hspace{0.3cm}{\ttfamily [inline]}, {\ttfamily [static]}}}\label{classCoinPresolveAction_a8a6a3658eb450b93709fe78f689671da}


Stub routine to throw exceptions. 

Exceptions are inefficient, particularly with g++. Even with xl\-C, the use of exceptions adds a long prologue to a routine. Therefore, rather than use throw directly in the routine, I use it in a stub routine. 

Definition at line 164 of file Coin\-Presolve\-Matrix.\-hpp.

\hypertarget{classCoinPresolveAction_a8a04cfe528b455468fe8d5333b730e2e}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!set\-Next@{set\-Next}}
\index{set\-Next@{set\-Next}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{set\-Next}]{\setlength{\rightskip}{0pt plus 5cm}{\bf void} Coin\-Presolve\-Action\-::set\-Next (
\begin{DoxyParamCaption}
\item[{const {\bf Coin\-Presolve\-Action} $\ast$}]{next\-Action}
\end{DoxyParamCaption}
)\hspace{0.3cm}{\ttfamily [inline]}}}\label{classCoinPresolveAction_a8a04cfe528b455468fe8d5333b730e2e}


modify next (when building rather than passing) 



Definition at line 180 of file Coin\-Presolve\-Matrix.\-hpp.

\hypertarget{classCoinPresolveAction_a48019a927c518ef3a7655469530456c6}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!name@{name}}
\index{name@{name}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{name}]{\setlength{\rightskip}{0pt plus 5cm}virtual const char$\ast$ Coin\-Presolve\-Action\-::name (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
) const\hspace{0.3cm}{\ttfamily [pure virtual]}}}\label{classCoinPresolveAction_a48019a927c518ef3a7655469530456c6}


A name for debug printing. 

It is expected that the name is not stored in the transform itself. 

Implemented in \hyperlink{classtwoxtwo__action_ab88e43aa5a90afd57b3d02c47aafd5ae}{twoxtwo\-\_\-action}, \hyperlink{classgubrow__action_a2c495eeaec205c194476c3b1e1c1b450}{gubrow\-\_\-action}, \hyperlink{classduprow3__action_a9e5f38c53b6e8c53614dd55626292e0a}{duprow3\-\_\-action}, \hyperlink{classmake__fixed__action_a428b07d1a5245da65703aafa7f5d89eb}{make\-\_\-fixed\-\_\-action}, \hyperlink{classdrop__empty__rows__action_a532876d0173b2c867eb2f6799b67fe67}{drop\-\_\-empty\-\_\-rows\-\_\-action}, \hyperlink{classduprow__action_aa132254972522e156ca10d9c1b02960d}{duprow\-\_\-action}, \hyperlink{classslack__singleton__action_ae9f29a8d4e085e004c27ed31ac37e756}{slack\-\_\-singleton\-\_\-action}, \hyperlink{classsubst__constraint__action_a4fed47311ccd46536232671a3502b7bb}{subst\-\_\-constraint\-\_\-action}, \hyperlink{classdoubleton__action_a8822ceeec5c8d450ca9cbc2bed6b32e0}{doubleton\-\_\-action}, \hyperlink{classdupcol__action_a72aa1aae87260fc0908116775302f3bb}{dupcol\-\_\-action}, \hyperlink{classdrop__empty__cols__action_a4b353603e9f0cb788533bbc9c6283c4b}{drop\-\_\-empty\-\_\-cols\-\_\-action}, \hyperlink{classtripleton__action_adabb758aa5ca42af374897bb83be340a}{tripleton\-\_\-action}, \hyperlink{classremove__fixed__action_a90af2968078197f706201e2bb61ada9b}{remove\-\_\-fixed\-\_\-action}, \hyperlink{classforcing__constraint__action_af201cfb2ba6205a11c43922ca8efa27f}{forcing\-\_\-constraint\-\_\-action}, \hyperlink{classslack__doubleton__action_a13420f970a37fec5a146af189b56a3db}{slack\-\_\-doubleton\-\_\-action}, \hyperlink{classimplied__free__action_a5b6f3e7863512f13d6b2daf044f0338a}{implied\-\_\-free\-\_\-action}, \hyperlink{classdrop__zero__coefficients__action_a77398cd56f948750fe6213d2e14518a5}{drop\-\_\-zero\-\_\-coefficients\-\_\-action}, \hyperlink{classremove__dual__action_ac4e2f8f83166c481e2764e8d3621d870}{remove\-\_\-dual\-\_\-action}, \hyperlink{classdo__tighten__action_acce2a5dcbf86872f823327c3ef4dc2e6}{do\-\_\-tighten\-\_\-action}, \hyperlink{classisolated__constraint__action_a9e33a9404237295fbf028edc8efe817c}{isolated\-\_\-constraint\-\_\-action}, and \hyperlink{classuseless__constraint__action_a1e4ab35bc3066a6ddb895ea5a52347e2}{useless\-\_\-constraint\-\_\-action}.

\hypertarget{classCoinPresolveAction_a7f5dd640a0b6fd260946e055f3eb2535}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!postsolve@{postsolve}}
\index{postsolve@{postsolve}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{postsolve}]{\setlength{\rightskip}{0pt plus 5cm}virtual {\bf void} Coin\-Presolve\-Action\-::postsolve (
\begin{DoxyParamCaption}
\item[{{\bf Coin\-Postsolve\-Matrix} $\ast$}]{prob}
\end{DoxyParamCaption}
) const\hspace{0.3cm}{\ttfamily [pure virtual]}}}\label{classCoinPresolveAction_a7f5dd640a0b6fd260946e055f3eb2535}


Apply the postsolve transformation for this particular presolve action. 



Implemented in \hyperlink{classtwoxtwo__action_a77866303b2caa4056d1223164dd37aa7}{twoxtwo\-\_\-action}, \hyperlink{classgubrow__action_a3b056cac20b779e2ba20f786303c30b0}{gubrow\-\_\-action}, \hyperlink{classmake__fixed__action_a4b4763c7748da261d23959cb92de4b8b}{make\-\_\-fixed\-\_\-action}, \hyperlink{classduprow3__action_a959c0b8cad54ee32e7780735d59ae87e}{duprow3\-\_\-action}, \hyperlink{classdrop__empty__rows__action_ad7db919861939b555088ed0393afb217}{drop\-\_\-empty\-\_\-rows\-\_\-action}, \hyperlink{classduprow__action_aac01dcb7ea5190792172707de476b96a}{duprow\-\_\-action}, \hyperlink{classslack__singleton__action_ae1a7c198258e596127658c6ae47e067a}{slack\-\_\-singleton\-\_\-action}, \hyperlink{classsubst__constraint__action_ac75e991f75081fca54241278eb1b65b3}{subst\-\_\-constraint\-\_\-action}, \hyperlink{classremove__fixed__action_ad34689395e9bd18f7b795f26e607aa7a}{remove\-\_\-fixed\-\_\-action}, \hyperlink{classdoubleton__action_a4fd4a77befc773b5894ed4f42eded046}{doubleton\-\_\-action}, \hyperlink{classdrop__empty__cols__action_af67a4d8d50319ede6c71e6787938e54f}{drop\-\_\-empty\-\_\-cols\-\_\-action}, \hyperlink{classdupcol__action_ab9f852bc6f230af8813496e2cc7d4c4f}{dupcol\-\_\-action}, \hyperlink{classslack__doubleton__action_aeaf8623924b9717943041aa8a38dc83d}{slack\-\_\-doubleton\-\_\-action}, \hyperlink{classtripleton__action_abbcc6c7e8bbe5ac17f4dd3dd9c2dad06}{tripleton\-\_\-action}, \hyperlink{classremove__dual__action_abe3b6c8060fd44f635c626396c14b07b}{remove\-\_\-dual\-\_\-action}, \hyperlink{classforcing__constraint__action_a158d6b4bd886e96a458d2bb356a61bbf}{forcing\-\_\-constraint\-\_\-action}, \hyperlink{classimplied__free__action_a412b7fda979a81f1e6f0b0648a445d58}{implied\-\_\-free\-\_\-action}, \hyperlink{classdrop__zero__coefficients__action_ae90208166b32e759beb9245ca5d1fac1}{drop\-\_\-zero\-\_\-coefficients\-\_\-action}, \hyperlink{classdo__tighten__action_a9a816b0932b2c7657592e9ac8abee341}{do\-\_\-tighten\-\_\-action}, \hyperlink{classisolated__constraint__action_a7746f11b1f3cb91378a3c5bf0a97f72b}{isolated\-\_\-constraint\-\_\-action}, and \hyperlink{classuseless__constraint__action_a1ada69bf17110603addd35397d5e6f1a}{useless\-\_\-constraint\-\_\-action}.



\subsubsection{Member Data Documentation}
\hypertarget{classCoinPresolveAction_ad3d9739db2130d42da2a9901221d61ec}{\index{Coin\-Presolve\-Action@{Coin\-Presolve\-Action}!next@{next}}
\index{next@{next}!CoinPresolveAction@{Coin\-Presolve\-Action}}
\paragraph[{next}]{\setlength{\rightskip}{0pt plus 5cm}const {\bf Coin\-Presolve\-Action}$\ast$ Coin\-Presolve\-Action\-::next}}\label{classCoinPresolveAction_ad3d9739db2130d42da2a9901221d61ec}


The next presolve transformation. 

Set at object construction. 

Definition at line 171 of file Coin\-Presolve\-Matrix.\-hpp.



The documentation for this class was generated from the following file\-:\begin{DoxyCompactItemize}
\item 
/home/tsokalo/workspace/tma\-Tool/coin-\/\-Clp/\-Coin\-Utils/src/\hyperlink{CoinPresolveMatrix_8hpp}{Coin\-Presolve\-Matrix.\-hpp}\end{DoxyCompactItemize}
