\hypertarget{classCoin_1_1ReferencedObject}{\subsection{Coin\-:\-:Referenced\-Object Class Reference}
\label{classCoin_1_1ReferencedObject}\index{Coin\-::\-Referenced\-Object@{Coin\-::\-Referenced\-Object}}
}


\hyperlink{classCoin_1_1ReferencedObject}{Referenced\-Object} class.  




{\ttfamily \#include $<$Coin\-Smart\-Ptr.\-hpp$>$}

\subsubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\hyperlink{classCoin_1_1ReferencedObject_a33f8fa7df67d7f03d1e381fc0f3cdf29}{Referenced\-Object} ()
\item 
virtual \hyperlink{classCoin_1_1ReferencedObject_a5e4ce3e01905b3cf1d14978edaed8a6e}{$\sim$\-Referenced\-Object} ()
\item 
int \hyperlink{classCoin_1_1ReferencedObject_a1f547d3096e09e254eb2b1526f6aab32}{Reference\-Count} () const 
\item 
\hyperlink{Coin__C__defines_8h_a003fd8bf528d56bed3420613d18ed2b1}{void} \hyperlink{classCoin_1_1ReferencedObject_afff3730615f57fc4ac3384bb926c8c33}{Add\-Ref} () const 
\item 
\hyperlink{Coin__C__defines_8h_a003fd8bf528d56bed3420613d18ed2b1}{void} \hyperlink{classCoin_1_1ReferencedObject_abbdf98719bc4f3de55a9e95b6fcd9f0f}{Release\-Ref} () const 
\end{DoxyCompactItemize}


\subsubsection{Detailed Description}
\hyperlink{classCoin_1_1ReferencedObject}{Referenced\-Object} class. 

This is part of the implementation of an intrusive smart pointer design. This class stores the reference count of all the smart pointers that currently reference it. See the documentation for the \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} class for more details.

A \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} behaves much like a raw pointer, but manages the lifetime of an object, deleting the object automatically. This class implements a reference-\/counting, intrusive smart pointer design, where all objects pointed to must inherit off of \hyperlink{classCoin_1_1ReferencedObject}{Referenced\-Object}, which stores the reference count. Although this is intrusive (native types and externally authored classes require wrappers to be referenced by smart pointers), it is a safer design. A more detailed discussion of these issues follows after the usage information.

Usage Example\-: Note\-: to use the \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr}, all objects to which you point M\-U\-S\-T inherit off of \hyperlink{classCoin_1_1ReferencedObject}{Referenced\-Object}.

\begin{DoxyVerb}* 
* In MyClass.hpp...
* 
* #include "CoinSmartPtr.hpp"

* 
* class MyClass : public Coin::ReferencedObject // must derive from ReferencedObject
*    {
*      ...
*    }
* 
* In my_usage.cpp...
* 
* #include "CoinSmartPtr.hpp"
* #include "MyClass.hpp"
* 
* void func(AnyObject& obj)
*  {
*    Coin::SmartPtr<MyClass> ptr_to_myclass = new MyClass(...);
*    // ptr_to_myclass now points to a new MyClass,
*    // and the reference count is 1
*    
*    ...
* 
*    obj.SetMyClass(ptr_to_myclass);
*    // Here, let's assume that AnyObject uses a
*    // SmartPtr<MyClass> internally here.
*    // Now, both ptr_to_myclass and the internal
*    // SmartPtr in obj point to the same MyClass object
*    // and its reference count is 2.
* 
*    ...
* 
*    // No need to delete ptr_to_myclass, this
*    // will be done automatically when the
*    // reference count drops to zero.
* 
*  }   
*  
* \end{DoxyVerb}


Other Notes\-: The \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} implements both dereference operators -\/$>$ \& $\ast$. The \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} does N\-O\-T implement a conversion operator to the raw pointer. Use the Get\-Raw\-Ptr() method when this is necessary. Make sure that the raw pointer is N\-O\-T deleted. The \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} implements the comparison operators == \& != for a variety of types. Use these instead of \begin{DoxyVerb}*    if (GetRawPtr(smrt_ptr) == ptr) // Don't use this
*    \end{DoxyVerb}
 \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr}'s, as currently implemented, do N\-O\-T handle circular references. For example\-: consider a higher level object using Smart\-Ptrs to point to A and B, but A and B also point to each other (i.\-e. A has a \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} to B and B has a \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} to A). In this scenario, when the higher level object is finished with A and B, their reference counts will never drop to zero (since they reference each other) and they will not be deleted. This can be detected by memory leak tools like valgrind. If the circular reference is necessary, the problem can be overcome by a number of techniques\-:

1) A and B can have a method that \char`\"{}releases\char`\"{} each other, that is they set their internal Smart\-Ptrs to N\-U\-L\-L. \begin{DoxyVerb}*        void AClass::ReleaseCircularReferences()
*          {
*          smart_ptr_to_B = NULL;
*          }
*        \end{DoxyVerb}
 Then, the higher level class can call these methods before it is done using A \& B.

2) Raw pointers can be used in A and B to reference each other. Here, an implicit assumption is made that the lifetime is controlled by the higher level object and that A and B will both exist in a controlled manner. Although this seems dangerous, in many situations, this type of referencing is very controlled and this is reasonably safe.

3) This \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} class could be redesigned with the Weak/\-Strong design concept. Here, the \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} is identified as being Strong (controls lifetime of the object) or Weak (merely referencing the object). The Strong \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} increments (and decrements) the reference count in \hyperlink{classCoin_1_1ReferencedObject}{Referenced\-Object} but the Weak \hyperlink{classCoin_1_1SmartPtr}{Smart\-Ptr} does not. In the example above, the higher level object would have Strong Smart\-Ptrs to A and B, but A and B would have Weak Smart\-Ptrs to each other. Then, when the higher level object was done with A and B, they would be deleted. The Weak Smart\-Ptrs in A and B would not decrement the reference count and would, of course, not delete the object. This idea is very similar to item (2), where it is implied that the sequence of events is controlled such that A and B will not call anything using their pointers following the higher level delete (i.\-e. in their destructors!). This is somehow safer, however, because code can be written (however expensive) to perform run-\/time detection of this situation. For example, the \hyperlink{classCoin_1_1ReferencedObject}{Referenced\-Object} could store pointers to all Weak Smart\-Ptrs that are referencing it and, in its destructor, tell these pointers that it is dying. They could then set themselves to N\-U\-L\-L, or set an internal flag to detect usage past this point.

Comments on Non-\/\-Intrusive Design\-: In a non-\/intrusive design, the reference count is stored somewhere other than the object being referenced. This means, unless the reference counting pointer is the first referencer, it must get a pointer to the referenced object from another smart pointer (so it has access to the reference count location). In this non-\/intrusive design, if we are pointing to an object with a smart pointer (or a number of smart pointers), and we then give another smart pointer the address through a R\-A\-W pointer, we will have two independent, A\-N\-D I\-N\-C\-O\-R\-R\-E\-C\-T, reference counts. To avoid this pitfall, we use an intrusive reference counting technique where the reference count is stored in the object being referenced. 

Definition at line 157 of file Coin\-Smart\-Ptr.\-hpp.



\subsubsection{Constructor \& Destructor Documentation}
\hypertarget{classCoin_1_1ReferencedObject_a33f8fa7df67d7f03d1e381fc0f3cdf29}{\index{Coin\-::\-Referenced\-Object@{Coin\-::\-Referenced\-Object}!Referenced\-Object@{Referenced\-Object}}
\index{Referenced\-Object@{Referenced\-Object}!Coin::ReferencedObject@{Coin\-::\-Referenced\-Object}}
\paragraph[{Referenced\-Object}]{\setlength{\rightskip}{0pt plus 5cm}Coin\-::\-Referenced\-Object\-::\-Referenced\-Object (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)\hspace{0.3cm}{\ttfamily [inline]}}}\label{classCoin_1_1ReferencedObject_a33f8fa7df67d7f03d1e381fc0f3cdf29}


Definition at line 159 of file Coin\-Smart\-Ptr.\-hpp.

\hypertarget{classCoin_1_1ReferencedObject_a5e4ce3e01905b3cf1d14978edaed8a6e}{\index{Coin\-::\-Referenced\-Object@{Coin\-::\-Referenced\-Object}!$\sim$\-Referenced\-Object@{$\sim$\-Referenced\-Object}}
\index{$\sim$\-Referenced\-Object@{$\sim$\-Referenced\-Object}!Coin::ReferencedObject@{Coin\-::\-Referenced\-Object}}
\paragraph[{$\sim$\-Referenced\-Object}]{\setlength{\rightskip}{0pt plus 5cm}virtual Coin\-::\-Referenced\-Object\-::$\sim$\-Referenced\-Object (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)\hspace{0.3cm}{\ttfamily [inline]}, {\ttfamily [virtual]}}}\label{classCoin_1_1ReferencedObject_a5e4ce3e01905b3cf1d14978edaed8a6e}


Definition at line 160 of file Coin\-Smart\-Ptr.\-hpp.



\subsubsection{Member Function Documentation}
\hypertarget{classCoin_1_1ReferencedObject_a1f547d3096e09e254eb2b1526f6aab32}{\index{Coin\-::\-Referenced\-Object@{Coin\-::\-Referenced\-Object}!Reference\-Count@{Reference\-Count}}
\index{Reference\-Count@{Reference\-Count}!Coin::ReferencedObject@{Coin\-::\-Referenced\-Object}}
\paragraph[{Reference\-Count}]{\setlength{\rightskip}{0pt plus 5cm}int Coin\-::\-Referenced\-Object\-::\-Reference\-Count (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
) const\hspace{0.3cm}{\ttfamily [inline]}}}\label{classCoin_1_1ReferencedObject_a1f547d3096e09e254eb2b1526f6aab32}


Definition at line 161 of file Coin\-Smart\-Ptr.\-hpp.

\hypertarget{classCoin_1_1ReferencedObject_afff3730615f57fc4ac3384bb926c8c33}{\index{Coin\-::\-Referenced\-Object@{Coin\-::\-Referenced\-Object}!Add\-Ref@{Add\-Ref}}
\index{Add\-Ref@{Add\-Ref}!Coin::ReferencedObject@{Coin\-::\-Referenced\-Object}}
\paragraph[{Add\-Ref}]{\setlength{\rightskip}{0pt plus 5cm}{\bf void} Coin\-::\-Referenced\-Object\-::\-Add\-Ref (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
) const\hspace{0.3cm}{\ttfamily [inline]}}}\label{classCoin_1_1ReferencedObject_afff3730615f57fc4ac3384bb926c8c33}


Definition at line 162 of file Coin\-Smart\-Ptr.\-hpp.

\hypertarget{classCoin_1_1ReferencedObject_abbdf98719bc4f3de55a9e95b6fcd9f0f}{\index{Coin\-::\-Referenced\-Object@{Coin\-::\-Referenced\-Object}!Release\-Ref@{Release\-Ref}}
\index{Release\-Ref@{Release\-Ref}!Coin::ReferencedObject@{Coin\-::\-Referenced\-Object}}
\paragraph[{Release\-Ref}]{\setlength{\rightskip}{0pt plus 5cm}{\bf void} Coin\-::\-Referenced\-Object\-::\-Release\-Ref (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
) const\hspace{0.3cm}{\ttfamily [inline]}}}\label{classCoin_1_1ReferencedObject_abbdf98719bc4f3de55a9e95b6fcd9f0f}


Definition at line 163 of file Coin\-Smart\-Ptr.\-hpp.



The documentation for this class was generated from the following file\-:\begin{DoxyCompactItemize}
\item 
/home/tsokalo/workspace/tma\-Tool/coin-\/\-Clp/\-Coin\-Utils/src/\hyperlink{CoinSmartPtr_8hpp}{Coin\-Smart\-Ptr.\-hpp}\end{DoxyCompactItemize}
