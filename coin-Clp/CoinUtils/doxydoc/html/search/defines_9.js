var searchData=
[
  ['shift_5findex',['SHIFT_INDEX',['../CoinOslC_8h.html#af675973f7434e884b0b80062190b84cb',1,'CoinOslC.h']]],
  ['shift_5fref',['SHIFT_REF',['../CoinOslC_8h.html#a0202f37d00c7a73702d23c69be93b214',1,'CoinOslC.h']]],
  ['slack_5fdoubleton',['SLACK_DOUBLETON',['../CoinPresolveSingleton_8hpp.html#a66c5b30e1d154553dcd1ebb508a19082',1,'CoinPresolveSingleton.hpp']]],
  ['slack_5fsingleton',['SLACK_SINGLETON',['../CoinPresolveSingleton_8hpp.html#a22e4547d3fee426d13bee734e04558a4',1,'CoinPresolveSingleton.hpp']]],
  ['slack_5fvalue',['SLACK_VALUE',['../CoinOslC_8h.html#a66b9bd1f0194646269dc2a2edebefaa4',1,'CoinOslC.h']]],
  ['sparse_5fupdate',['SPARSE_UPDATE',['../CoinOslC_8h.html#a33498026079c143b062f5d57840600b6',1,'CoinOslC.h']]],
  ['stdc_5fheaders',['STDC_HEADERS',['../config_8h.html#a550e5c272cc3cf3814651721167dcd23',1,'config.h']]],
  ['subst_5frow',['SUBST_ROW',['../CoinPresolveSubst_8hpp.html#a8df47926689b7f7deb04b5251a2dc347',1,'CoinPresolveSubst.hpp']]],
  ['swap',['SWAP',['../CoinOslC_8h.html#aedc4b47898d37a614fd1636ddd105992',1,'CoinOslC.h']]]
];
