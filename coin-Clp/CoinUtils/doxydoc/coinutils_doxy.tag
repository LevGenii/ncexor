<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>Coin_C_defines.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>Coin__C__defines_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>COINLIBAPI</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a32c48028a2539f4ea011b328abfb94a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINLINKAGE</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>aeaf9f6584150b9f6167cc07f1b3493d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINLINKAGE_CB</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>add6369a2faf971a270fc68602347daf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void</type>
      <name>Clp_Simplex</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a3d80ed9405ff732c8e1c5f78dfeaa504</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>msgno</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a76838ba33398e08185ffe89b06264b4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int int</type>
      <name>ndouble</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a8317fbe6458c45294bbdfe1724df1828</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int int const double *</type>
      <name>dvec</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a1b78625778d743b4a285343f6cf26a30</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int int const double int</type>
      <name>nint</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a888c3c0b139f4b5b32d43223e4aff206</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int int const double int const int *</type>
      <name>ivec</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a84a50a2923b76f94ee2176929141eb25</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int int const double int const int int</type>
      <name>nchar</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a0fb83b9365ea92ad83a5e88994fd433a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int int const double int const int int char **</type>
      <name>cvec</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>afd71204e6b2b4eb3e6f580bba0a48764</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void</type>
      <name>Sbb_Model</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>af399240390cd8b00b3f3cb872ceab294</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void</type>
      <name>Cbc_Model</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a2098fa54e54a5729abf15398e90d9cc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>CoinBigIndex</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a1d91e832494d1d012e44c52e34d2340f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>typedef</type>
      <name>void</name>
      <anchorfile>Coin__C__defines_8h.html</anchorfile>
      <anchor>a003fd8bf528d56bed3420613d18ed2b1</anchor>
      <arglist>(COINLINKAGE_CB *clp_callback)(Clp_Simplex *model</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinAlloc.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinAlloc_8hpp</filename>
    <includes id="CoinUtilsConfig_8h" name="CoinUtilsConfig.h" local="yes" imported="no">CoinUtilsConfig.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_MEMPOOL_MAXPOOLED</name>
      <anchorfile>CoinAlloc_8hpp.html</anchorfile>
      <anchor>ad2a89cf18554bf791c84d0beb7646064</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinBuild.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinBuild_8hpp</filename>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinFinite_8hpp" name="CoinFinite.hpp" local="yes" imported="no">CoinFinite.hpp</includes>
    <class kind="class">CoinBuild</class>
  </compound>
  <compound kind="file">
    <name>CoinDenseFactorization.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinDenseFactorization_8hpp</filename>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinIndexedVector_8hpp" name="CoinIndexedVector.hpp" local="yes" imported="no">CoinIndexedVector.hpp</includes>
    <includes id="CoinFactorization_8hpp" name="CoinFactorization.hpp" local="yes" imported="no">CoinFactorization.hpp</includes>
    <class kind="class">CoinOtherFactorization</class>
    <class kind="class">CoinDenseFactorization</class>
  </compound>
  <compound kind="file">
    <name>CoinDenseVector.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinDenseVector_8hpp</filename>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <class kind="class">CoinDenseVector</class>
    <member kind="function">
      <type>void</type>
      <name>CoinDenseVectorUnitTest</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a0b491a87f036a026f4e904a927e4db0a</anchor>
      <arglist>(T dummy)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator+</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a001f5a753da31a957fa37dad4170cbf5</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, const CoinDenseVector&lt; T &gt; &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator-</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>af1c2141cfcaedd00134da861d0d0eb9a</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, const CoinDenseVector&lt; T &gt; &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator*</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a79ba24f08d414880c042323222edb818</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, const CoinDenseVector&lt; T &gt; &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator/</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a9458c0613e02fbde70551202a7093416</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, const CoinDenseVector&lt; T &gt; &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator+</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a0c5cd2fafc511f725db71cd9ff7d05a9</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, T value)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator-</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>aa538575601d9e14b305d27d82d5d1fa7</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, T value)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator*</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a9d7fab4dd910938133a90f48efa028c9</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, T value)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator/</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>ae1a4a178057dc406886f4f89b9bc05bb</anchor>
      <arglist>(const CoinDenseVector&lt; T &gt; &amp;op1, T value)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator+</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>afb7250b3410ae83c3704ddbdf9e7ca0f</anchor>
      <arglist>(T value, const CoinDenseVector&lt; T &gt; &amp;op1)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator-</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>a65ab57bea9e30c285bb380103a04ffcf</anchor>
      <arglist>(T value, const CoinDenseVector&lt; T &gt; &amp;op1)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator*</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>ab7014dde89ddf2236c2c8d9016d4c0d2</anchor>
      <arglist>(T value, const CoinDenseVector&lt; T &gt; &amp;op1)</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector&lt; T &gt;</type>
      <name>operator/</name>
      <anchorfile>CoinDenseVector_8hpp.html</anchorfile>
      <anchor>ac2c91f3b461b1b4236dddd68a2f7c1e5</anchor>
      <arglist>(T value, const CoinDenseVector&lt; T &gt; &amp;op1)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinDistance.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinDistance_8hpp</filename>
    <member kind="function">
      <type>void</type>
      <name>coinDistance</name>
      <anchorfile>CoinDistance_8hpp.html</anchorfile>
      <anchor>aaf48ce7c1273db8d8f06d98dc88a586b</anchor>
      <arglist>(ForwardIterator first, ForwardIterator last, Distance &amp;n)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>coinDistance</name>
      <anchorfile>CoinDistance_8hpp.html</anchorfile>
      <anchor>a5911a9216b940891cb8b1df8242630c5</anchor>
      <arglist>(ForwardIterator first, ForwardIterator last)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinError.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinError_8hpp</filename>
    <includes id="CoinUtilsConfig_8h" name="CoinUtilsConfig.h" local="yes" imported="no">CoinUtilsConfig.h</includes>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <class kind="class">CoinError</class>
    <member kind="define">
      <type>#define</type>
      <name>__STRING</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>a375c4dc9f0fb338999de81aab826f9d6</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__GNUC_PREREQ</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>a93a0689feb693ac0e454667845da68d0</anchor>
      <arglist>(maj, min)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinAssertDebug</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>ad3735aa3e7d6f56028531841a1e7c4e2</anchor>
      <arglist>(expression)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinAssertDebugHint</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>a870a605f0b4b1520140acf2dba1e6a97</anchor>
      <arglist>(expression, hint)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinAssert</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>a01053a5f290c0d0dc049383a73590b57</anchor>
      <arglist>(expression)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinAssertHint</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>a21098c24942b02b249e88dcc78fdc62a</anchor>
      <arglist>(expression, hint)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>WindowsErrorPopupBlocker</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>a9693c7a5ea9e94f7e4c806a1319d422c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinErrorUnitTest</name>
      <anchorfile>CoinError_8hpp.html</anchorfile>
      <anchor>af145b267443a0cdbd5cfb2533c3a8da5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinFactorization.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinFactorization_8hpp</filename>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinIndexedVector_8hpp" name="CoinIndexedVector.hpp" local="yes" imported="no">CoinIndexedVector.hpp</includes>
    <class kind="class">CoinFactorization</class>
    <member kind="define">
      <type>#define</type>
      <name>COINFACTORIZATION_BITS_PER_INT</name>
      <anchorfile>CoinFactorization_8hpp.html</anchorfile>
      <anchor>aac80e48203bbdb9c643c604e54750321</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINFACTORIZATION_SHIFT_PER_INT</name>
      <anchorfile>CoinFactorization_8hpp.html</anchorfile>
      <anchor>ac41fe2a0c2ff8ead3bb41ea4752eab54</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINFACTORIZATION_MASK_PER_INT</name>
      <anchorfile>CoinFactorization_8hpp.html</anchorfile>
      <anchor>ab0baa6eee7816510ee6d62c53808dcc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>collectStatistics_</name>
      <anchorfile>CoinFactorization_8hpp.html</anchorfile>
      <anchor>a1fb608bd0f5f937bd01f48749e1ec74f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinFileIO.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinFileIO_8hpp</filename>
    <class kind="class">CoinFileIOBase</class>
    <class kind="class">CoinFileInput</class>
    <class kind="class">CoinFileOutput</class>
  </compound>
  <compound kind="file">
    <name>CoinFinite.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinFinite_8hpp</filename>
    <member kind="function">
      <type>bool</type>
      <name>CoinFinite</name>
      <anchorfile>CoinFinite_8hpp.html</anchorfile>
      <anchor>ab3dcb1e8c014c5d7aa3b1f47fe45c282</anchor>
      <arglist>(double val)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>CoinIsnan</name>
      <anchorfile>CoinFinite_8hpp.html</anchorfile>
      <anchor>a047a257849fbfc69946cebb11e5f178f</anchor>
      <arglist>(double val)</arglist>
    </member>
    <member kind="variable">
      <type>const double</type>
      <name>COIN_DBL_MIN</name>
      <anchorfile>CoinFinite_8hpp.html</anchorfile>
      <anchor>accf559b86533b4581e0b3a71968f7a77</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double</type>
      <name>COIN_DBL_MAX</name>
      <anchorfile>CoinFinite_8hpp.html</anchorfile>
      <anchor>a7472ab60dc63ca905cd73840a76b927d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const int</type>
      <name>COIN_INT_MAX</name>
      <anchorfile>CoinFinite_8hpp.html</anchorfile>
      <anchor>aecc106a6efa338ce736f3cd4b95b0dea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double</type>
      <name>COIN_INT_MAX_AS_DOUBLE</name>
      <anchorfile>CoinFinite_8hpp.html</anchorfile>
      <anchor>ae25f16072926338d6aa07e930e408ba3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinFloatEqual.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinFloatEqual_8hpp</filename>
    <includes id="CoinFinite_8hpp" name="CoinFinite.hpp" local="yes" imported="no">CoinFinite.hpp</includes>
    <class kind="class">CoinAbsFltEq</class>
    <class kind="class">CoinRelFltEq</class>
  </compound>
  <compound kind="file">
    <name>CoinHelperFunctions.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinHelperFunctions_8hpp</filename>
    <includes id="CoinUtilsConfig_8h" name="CoinUtilsConfig.h" local="yes" imported="no">CoinUtilsConfig.h</includes>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinError_8hpp" name="CoinError.hpp" local="yes" imported="no">CoinError.hpp</includes>
    <class kind="class">CoinThreadRandom</class>
    <member kind="define">
      <type>#define</type>
      <name>COIN_RESTRICT</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>aea9d49cf32f1f69616fbf0d218a18048</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_OWN_RANDOM_32</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>aef0f8ff223260ffc6718e297af0111d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinSizeofAsInt</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a9f9305e0b1355a4df29441e445cdc9f2</anchor>
      <arglist>(type)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_DETAIL_PRINT</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ab8029b8a50f1ed0188345a5b87076516</anchor>
      <arglist>(s)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinCopyN</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a15aac48569e8702ab72be267e72eeac4</anchor>
      <arglist>(register const T *from, const int size, register T *to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinCopy</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a0c59c8af78648e716026015a3d3b2de4</anchor>
      <arglist>(register const T *first, register const T *last, register T *to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinDisjointCopyN</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ae5b4946c1a2ff590d1f1e5eb537fb256</anchor>
      <arglist>(register const T *from, const int size, register T *to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinDisjointCopy</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a64f37a5216f6fe7a15266c4b4cd1dbb2</anchor>
      <arglist>(register const T *first, register const T *last, register T *to)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>CoinCopyOfArray</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>aea5e71451af98472e7c4f1b1230fa68b</anchor>
      <arglist>(const T *array, const int size)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>CoinCopyOfArrayPartial</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a56d215b6d821adee79eff1de1f8da003</anchor>
      <arglist>(const T *array, const int size, const int copySize)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>CoinCopyOfArray</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a5c3b8c1b080c3d18695f8672bbeb16ff</anchor>
      <arglist>(const T *array, const int size, T value)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>CoinCopyOfArrayOrZero</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a3ca385b11a53b198673ecc73d06cc7e0</anchor>
      <arglist>(const T *array, const int size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinMemcpyN</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a1b0f172747564155421609d44caceb4a</anchor>
      <arglist>(register const T *from, const int size, register T *to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinMemcpy</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a19933249118887ff023468b6d3dbd122</anchor>
      <arglist>(register const T *first, register const T *last, register T *to)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinFillN</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ad69a0683647f7121a4432ffcee5d0065</anchor>
      <arglist>(register T *to, const int size, register const T value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinFill</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a5f326d157e26852205b083db46aa033b</anchor>
      <arglist>(register T *first, register T *last, const T value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinZeroN</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>afe1a3cee870e697eb74d9d8fcf50dc15</anchor>
      <arglist>(register T *to, const int size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinCheckDoubleZero</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a369f67fd2dd64d8ee6b21b94f0780cfe</anchor>
      <arglist>(double *to, const int size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinCheckIntZero</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ada2499d77a9139c922c405b19c30f966</anchor>
      <arglist>(int *to, const int size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinZero</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ae4ee5f484b682ec5aaca11ffe024ab1e</anchor>
      <arglist>(register T *first, register T *last)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>CoinStrdup</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ab432b2f2de9302a35dbb770085e56a06</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>CoinMax</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a10367757cd432139628c41c286a49b47</anchor>
      <arglist>(register const T x1, register const T x2)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>CoinMin</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a2e9c07cbd18963a8ae8032e61d489c95</anchor>
      <arglist>(register const T x1, register const T x2)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>CoinAbs</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>aee3644bfa92068ab0d02fef327fc6bd0</anchor>
      <arglist>(const T value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>CoinIsSorted</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a2aa6ee983ac1d08c91fa1ff2783aff93</anchor>
      <arglist>(register const T *first, const int size)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>CoinIsSorted</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>afce28fb085f1e08a7aebdf00443ac094</anchor>
      <arglist>(register const T *first, register const T *last)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinIotaN</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>adda876eaecdc45bacdeab07b4b5be78f</anchor>
      <arglist>(register T *first, const int size, register T init)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinIota</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>aacdd22f35c15f7f26df41c1cba634fbe</anchor>
      <arglist>(T *first, const T *last, T init)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>CoinDeleteEntriesFromArray</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a9a2db86dcf1591cbd152d864ca06bb37</anchor>
      <arglist>(register T *arrayFirst, register T *arrayLast, const int *firstDelPos, const int *lastDelPos)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>CoinDrand48</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a735164699fc765fd148448e7ac6f9eff</anchor>
      <arglist>(bool isSeed=false, unsigned int seed=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSeedRandom</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a743a48b0538fc9f5c5d90eb9c32e681a</anchor>
      <arglist>(int iseed)</arglist>
    </member>
    <member kind="function">
      <type>char</type>
      <name>CoinFindDirSeparator</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a51d8d220e461bd860848893aeb4543de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>CoinStrNCaseCmp</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a08f1bc94db363214c0edef67ed195ff6</anchor>
      <arglist>(const char *s0, const char *s1, const size_t len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSwap</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>ae1df5965e00b1fad3514a03824d000dc</anchor>
      <arglist>(T &amp;x, T &amp;y)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>CoinToFile</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>afdbd3777425f82fb0f57fca02c7a8624</anchor>
      <arglist>(const T *array, CoinBigIndex size, FILE *fp)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>CoinFromFile</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a5914299d4c61305688e4a62f6661637f</anchor>
      <arglist>(T *&amp;array, CoinBigIndex size, FILE *fp, CoinBigIndex &amp;newSize)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>CoinStrlenAsInt</name>
      <anchorfile>CoinHelperFunctions_8hpp.html</anchorfile>
      <anchor>a2aed67a2da9ea2c5509d29317652339c</anchor>
      <arglist>(const char *string)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinIndexedVector.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinIndexedVector_8hpp</filename>
    <includes id="CoinFinite_8hpp" name="CoinFinite.hpp" local="yes" imported="no">CoinFinite.hpp</includes>
    <includes id="CoinPackedVectorBase_8hpp" name="CoinPackedVectorBase.hpp" local="yes" imported="no">CoinPackedVectorBase.hpp</includes>
    <includes id="CoinSort_8hpp" name="CoinSort.hpp" local="yes" imported="no">CoinSort.hpp</includes>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <class kind="class">CoinIndexedVector</class>
    <class kind="class">CoinArrayWithLength</class>
    <class kind="class">CoinDoubleArrayWithLength</class>
    <class kind="class">CoinFactorizationDoubleArrayWithLength</class>
    <class kind="class">CoinFactorizationLongDoubleArrayWithLength</class>
    <class kind="class">CoinIntArrayWithLength</class>
    <class kind="class">CoinBigIndexArrayWithLength</class>
    <class kind="class">CoinUnsignedIntArrayWithLength</class>
    <class kind="class">CoinVoidStarArrayWithLength</class>
    <class kind="class">CoinArbitraryArrayWithLength</class>
    <class kind="class">CoinPartitionedVector</class>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INDEXED_TINY_ELEMENT</name>
      <anchorfile>CoinIndexedVector_8hpp.html</anchorfile>
      <anchor>aa7e4ad7451a3a9ae9737b594a6b97bca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INDEXED_REALLY_TINY_ELEMENT</name>
      <anchorfile>CoinIndexedVector_8hpp.html</anchorfile>
      <anchor>a23fa7a9860d33ba5259b49e62443fe19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_PARTITIONS</name>
      <anchorfile>CoinIndexedVector_8hpp.html</anchorfile>
      <anchor>a52708a381d935d9385208477c5e4a094</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinIndexedVectorUnitTest</name>
      <anchorfile>CoinIndexedVector_8hpp.html</anchorfile>
      <anchor>a2c6e6fb4f5b284d426977df27415503c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinLpIO.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinLpIO_8hpp</filename>
    <includes id="CoinPackedMatrix_8hpp" name="CoinPackedMatrix.hpp" local="yes" imported="no">CoinPackedMatrix.hpp</includes>
    <includes id="CoinMessage_8hpp" name="CoinMessage.hpp" local="yes" imported="no">CoinMessage.hpp</includes>
    <class kind="class">CoinLpIO</class>
    <class kind="struct">CoinLpIO::CoinHashLink</class>
    <member kind="typedef">
      <type>int</type>
      <name>COINColumnIndex</name>
      <anchorfile>CoinLpIO_8hpp.html</anchorfile>
      <anchor>aa3d02be12cb2b75806c7d3e69231629f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinLpIOUnitTest</name>
      <anchorfile>CoinLpIO_8hpp.html</anchorfile>
      <anchor>a8a6bcc546bb0e1c14712eae8c9800756</anchor>
      <arglist>(const std::string &amp;lpDir)</arglist>
    </member>
    <member kind="variable">
      <type>const int</type>
      <name>MAX_OBJECTIVES</name>
      <anchorfile>CoinLpIO_8hpp.html</anchorfile>
      <anchor>a877535f0207eed5d133a8f5d73fa5392</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinMessage.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinMessage_8hpp</filename>
    <includes id="CoinMessageHandler_8hpp" name="CoinMessageHandler.hpp" local="yes" imported="no">CoinMessageHandler.hpp</includes>
    <class kind="class">CoinMessage</class>
    <member kind="enumeration">
      <type></type>
      <name>COIN_Message</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_LINE</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da48e591719fce5fa91510d2361ac54fe0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_STATS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da048eca7bf60f8afc676668e9a8aa0b74</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_ILLEGAL</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da303138ef61baa81eec42a4fcd6fe7ff4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_BADIMAGE</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5daac95d540155b6f9faebc5a56a2c2127d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_DUPOBJ</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da311456c2b74a40b8218939fc2117da03</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_DUPROW</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5daaba0cb2bdc47245b664a285d15f7f427</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_NOMATCHROW</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5daa3323376f357caf43836f8ae561f6af3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_NOMATCHCOL</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5dadbc5db34098ce65858d00feeda3f14ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_FILE</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da89d1939a95ff626a9f23885659f08c66</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_BADFILE1</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da08be662aa23918ead0a42877b456549a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_BADFILE2</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da47e97b8af3dfd768357543906d83a1d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_EOF</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da0188a5fd177e8288e18741989dace0b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_RETURNING</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5dad6e81741421e51576511fd0a0c0f45c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MPS_CHANGED</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da6d5a6c8e91884083b13e2f94b628a5b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_SOLVER_MPS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da17de97e9007600a312671326253acb10</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_COLINFEAS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da792781d4800ab6b65aaaa57690470b78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_ROWINFEAS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da52d97a6e9fda41e830e24a4c48de7fb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_COLUMNBOUNDA</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da11fdb00ae28e5aa6a63bcf453f008ad5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_COLUMNBOUNDB</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da9f05d1e1af5478a0a75e4b8383f4c0d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_NONOPTIMAL</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da59887274ef1c9f9f1fc4ab1279a316c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_STATS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da296777588854eb00c6b61aa1d3291aa4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_INFEAS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5dade240420563f793cf84f80bb1b0913ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_UNBOUND</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da372568b6b7e65abe565f38cfaa368f92</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_INFEASUNBOUND</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da3cd635c79297167c952fedae5bc81945</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_INTEGERMODS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da75aba935044219ee13134a5b63e54f0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_POSTSOLVE</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da09e97292a2611cad0b20a19add859364</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_NEEDS_CLEANING</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da0bf0d67e701cf104ac483f40ddae51e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PRESOLVE_PASS</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5dafaac6dda32ed21d077eed8b1d9c6bc77</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_GENERAL_INFO</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5daa832eae2db47a3987db9b4a05b385d52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_GENERAL_INFO2</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5da257fd7e04df2fa1de2bcb1ced08156d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_GENERAL_WARNING</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5dacb17033b9eaa529aa05e8dce98c9d01e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_DUMMY_END</name>
      <anchorfile>CoinMessage_8hpp.html</anchorfile>
      <anchor>a7e009ad64670117f6809a207f3352d5daca8fa4dcd1b8428264cfb44d3ac2727c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinMessageHandler.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinMessageHandler_8hpp</filename>
    <includes id="CoinUtilsConfig_8h" name="CoinUtilsConfig.h" local="yes" imported="no">CoinUtilsConfig.h</includes>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <class kind="class">CoinOneMessage</class>
    <class kind="class">CoinMessages</class>
    <class kind="class">CoinMessageHandler</class>
    <member kind="define">
      <type>#define</type>
      <name>COIN_NUM_LOG</name>
      <anchorfile>CoinMessageHandler_8hpp.html</anchorfile>
      <anchor>a403ed995865af0e5e5c1342dfc27cebd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_MESSAGE_HANDLER_MAX_BUFFER_SIZE</name>
      <anchorfile>CoinMessageHandler_8hpp.html</anchorfile>
      <anchor>a4c1a87b98f5aa32dbfd03af2d1883d2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CoinMessageMarker</name>
      <anchorfile>CoinMessageHandler_8hpp.html</anchorfile>
      <anchor>acfd67913608e11481e195703103d5370</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CoinMessageEol</name>
      <anchorfile>CoinMessageHandler_8hpp.html</anchorfile>
      <anchor>acfd67913608e11481e195703103d5370aa6c332471c68347b8d07ebc8fe5ef875</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CoinMessageNewline</name>
      <anchorfile>CoinMessageHandler_8hpp.html</anchorfile>
      <anchor>acfd67913608e11481e195703103d5370a1e67b46cfbe11814476fcae0973ff5c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>CoinMessageHandlerUnitTest</name>
      <anchorfile>CoinMessageHandler_8hpp.html</anchorfile>
      <anchor>a4035f21309a1b78da27cc505b2767add</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinModel.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinModel_8hpp</filename>
    <includes id="CoinModelUseful_8hpp" name="CoinModelUseful.hpp" local="yes" imported="no">CoinModelUseful.hpp</includes>
    <includes id="CoinMessageHandler_8hpp" name="CoinMessageHandler.hpp" local="yes" imported="no">CoinMessageHandler.hpp</includes>
    <includes id="CoinPackedMatrix_8hpp" name="CoinPackedMatrix.hpp" local="yes" imported="no">CoinPackedMatrix.hpp</includes>
    <includes id="CoinFinite_8hpp" name="CoinFinite.hpp" local="yes" imported="no">CoinFinite.hpp</includes>
    <class kind="class">CoinBaseModel</class>
    <class kind="class">CoinModel</class>
    <member kind="function">
      <type>double</type>
      <name>getFunctionValueFromString</name>
      <anchorfile>CoinModel_8hpp.html</anchorfile>
      <anchor>aef52aaab692c0c0e5f05177b087c69d2</anchor>
      <arglist>(const char *string, const char *x, double xValue)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getDoubleFromString</name>
      <anchorfile>CoinModel_8hpp.html</anchorfile>
      <anchor>a697a3245a0c3d5e58c8cee62587da246</anchor>
      <arglist>(CoinYacc &amp;info, const char *string, const char *x, double xValue)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinModelUseful.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinModelUseful_8hpp</filename>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <class kind="class">CoinModelLink</class>
    <class kind="struct">CoinModelTriple</class>
    <class kind="struct">CoinModelHashLink</class>
    <class kind="struct">symrec</class>
    <class kind="class">CoinYacc</class>
    <class kind="class">CoinModelHash</class>
    <class kind="class">CoinModelHash2</class>
    <class kind="class">CoinModelLinkedList</class>
    <member kind="typedef">
      <type>double(*</type>
      <name>func_t</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>a0123c03962eb08e8bdea334072969309</anchor>
      <arglist>)(double)</arglist>
    </member>
    <member kind="typedef">
      <type>struct symrec</type>
      <name>symrec</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>a3f59e446c33f449c9986fab15daf0850</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rowInTriple</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>a20c31e0753de5c0dc6bf17a47e9a5a0f</anchor>
      <arglist>(const CoinModelTriple &amp;triple)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowInTriple</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>aadd47468b22ee1be8b5db76f40452b5c</anchor>
      <arglist>(CoinModelTriple &amp;triple, int iRow)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>stringInTriple</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>a85b65ab88444db1f7705a1ad5e4a59d2</anchor>
      <arglist>(const CoinModelTriple &amp;triple)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStringInTriple</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>a1d8030ecb17db8a2478bb91766a4ee4e</anchor>
      <arglist>(CoinModelTriple &amp;triple, bool string)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowAndStringInTriple</name>
      <anchorfile>CoinModelUseful_8hpp.html</anchorfile>
      <anchor>ac2ad8869c97d4b2711ad5d76bc07d1e8</anchor>
      <arglist>(CoinModelTriple &amp;triple, int iRow, bool string)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinMpsIO.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinMpsIO_8hpp</filename>
    <includes id="CoinUtilsConfig_8h" name="CoinUtilsConfig.h" local="yes" imported="no">CoinUtilsConfig.h</includes>
    <includes id="CoinPackedMatrix_8hpp" name="CoinPackedMatrix.hpp" local="yes" imported="no">CoinPackedMatrix.hpp</includes>
    <includes id="CoinMessageHandler_8hpp" name="CoinMessageHandler.hpp" local="yes" imported="no">CoinMessageHandler.hpp</includes>
    <includes id="CoinFileIO_8hpp" name="CoinFileIO.hpp" local="yes" imported="no">CoinFileIO.hpp</includes>
    <class kind="class">CoinMpsCardReader</class>
    <class kind="class">CoinSet</class>
    <class kind="class">CoinSosSet</class>
    <class kind="class">CoinMpsIO</class>
    <class kind="struct">CoinMpsIO::CoinHashLink</class>
    <member kind="define">
      <type>#define</type>
      <name>COIN_MAX_FIELD_LENGTH</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>ae3f0eaf3dfc59c28766fbacf60154db0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MAX_CARD_LENGTH</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>a9fb205761a5907615bf59e207df4e674</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>COINColumnIndex</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa3d02be12cb2b75806c7d3e69231629f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>COINRowIndex</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>a33475d9526b64d0fabf3b00f24e9579c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>COINSectionType</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_NO_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a84e60ebffcc6464eab157574b895ec59</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_NAME_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6ae137eedd437776d030e5690dbe8a8d6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_ROW_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a8c84e54d470e908262991acd9b36c940</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_COLUMN_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6ab9aee633f65c5f123c55c5d875182846</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_RHS_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a69dfecc84dfd069dbec9c442fda1f0c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_RANGES_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6adb78e635d836c4a286ab51f3956c49e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_BOUNDS_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a8374a528156b7faae8058d9ef0b14b2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_ENDATA_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6ae2e7c6afd7e21fc2c70a1adbb9607a4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_EOF_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a45edd901c70f4cc8ba34ff5507f532f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_QUADRATIC_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6ab0901d821bf07395e1fa52d208e1dad6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_CONIC_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6af5326ecf40a9cdffd5ec7d3d77097f46</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_QUAD_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6aa7a061b5aefa847068c15fb39c5310d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_SOS_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a32c3450ab0d795b01d62b2c30371d6cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_BASIS_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6aa38a330bad4495deaccca0019736094d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_UNKNOWN_SECTION</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>acb7cb0cd3bf902db737c836f081a7dd6a55bd42a5cf088b343e1ffb33883df281</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>COINMpsType</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_N_ROW</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a3b823e316ad06c95c10254e344096f31</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_E_ROW</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a97fefbadebb35fa0e993eefe131e0173</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_L_ROW</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a02fa05df9308056616121c9ef1209cea</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_G_ROW</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a562f619f14f959427f204fdd2a3cd3b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_BLANK_COLUMN</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a086fa04f1ea18e05245241d15054cb52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_S1_COLUMN</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0adc1ed3de7ccfcad820c9c30e4d905c3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_S2_COLUMN</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a0fc81b167dc7fd99a47cbdfcbf75032e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_S3_COLUMN</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a2815b78835ab086f4851ca93e60af169</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_INTORG</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a7ca6a95405be1afcf75fa382fe5102fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_INTEND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a0865922ecbd7c5b3c55f7fd52061c5f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_SOSEND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a2df61acb11a2cd0bf4b30c694ebea783</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_UNSET_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a4d4179770d0342526af90f08c0b92d0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_UP_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a2636ffbd92e4bc35be94a951e3d7a6f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_FX_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a89a9a31623e138962b167e91ca48246f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_LO_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a0c03139e16019da00d20ebd39f97f042</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_FR_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0ae1b0dc95d49687030a51200438a6fb3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_MI_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0abb7d78c65eebe923dc7daefc1a083240</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_PL_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a0ff3e1c12d3250452349c6459cf4d24a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_BV_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a7e2905e1626fc6e847b9fd50662fe159</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_UI_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a1f97715069cf877f72c41c26630fab29</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_LI_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0ab5ec7f9842eded53ff1e51b4e28b5517</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_BOTH_BOUNDS_SET</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a4d21f606d26e1991d35c3ef5ab2e0c11</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_SC_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0ac99a1469ee26a501dcddadffbcbd6a44</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_S1_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a8ba93941fec08ffc4c8458c2f7a59d34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_S2_BOUND</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a0875b50578a9373b0dbf430b847dec9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_BS_BASIS</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a3af2a35e6475183881b2f0c92235cb1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_XL_BASIS</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0a46cb85dd85b468fbd7a5085df30b668d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_XU_BASIS</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0ae416fab0a32f4f9fc72439b4f7ce5c88</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_LL_BASIS</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0ace84ac73cf21b8bc3b6765d6f1a63006</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_UL_BASIS</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0abe047bede379313daf430607ba41c461</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COIN_UNKNOWN_MPS_TYPE</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>aa838c3eee91abf218d29b3718b0f83d0adf419317d3b5bb3f6b86daab6fda57f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinMpsIOUnitTest</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>ab0da282e1cbdcfba60febd791a541c27</anchor>
      <arglist>(const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinConvertDouble</name>
      <anchorfile>CoinMpsIO_8hpp.html</anchorfile>
      <anchor>ad3c8464acd2b1ef0de9f1e4d77b0a3fb</anchor>
      <arglist>(int section, int formatType, double value, char outputValue[24])</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinOslC.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinOslC_8h</filename>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <member kind="define">
      <type>#define</type>
      <name>CLP_OSL</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a802f2bf6eee6d423ac7f0da1843d086d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>C_EKK_GO_SPARSE</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>abfac9ff497a34ea8d00dcb7a3c641cca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SPARSE_UPDATE</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a33498026079c143b062f5d57840600b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NO_SHIFT</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a2bc3c35d7d12c88b67bee7e224cd8d21</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>c_ekkscpy_0_1</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a92463157fcd3cf6a66d32b16197b7aca</anchor>
      <arglist>(s, ival, array)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>c_ekks1cpy</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a0452de642cc86493281bc7a67972cb08</anchor>
      <arglist>(n, marr1, marr2)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SLACK_VALUE</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a66b9bd1f0194646269dc2a2edebefaa4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>C_EKK_REMOVE_LINK</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>af6c6426592cd30798efed2ca59872e02</anchor>
      <arglist>(hpiv, hin, link, ipivot)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>C_EKK_ADD_LINK</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a378a01997b695610f352711c3959c65b</anchor>
      <arglist>(hpiv, nzi, link, npr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SHIFT_INDEX</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>af675973f7434e884b0b80062190b84cb</anchor>
      <arglist>(limit)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UNSHIFT_INDEX</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a41039d523719c275c808acefcd5f9740</anchor>
      <arglist>(limit)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SHIFT_REF</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a0202f37d00c7a73702d23c69be93b214</anchor>
      <arglist>(arr, ind)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NOT_ZERO</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a3bf395e08253956c78c55ea732ead823</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SWAP</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>aedc4b47898d37a614fd1636ddd105992</anchor>
      <arglist>(type, _x, _y)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UNROLL_LOOP_BODY1</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a6b25359846e6fcd3c564d45769b576a6</anchor>
      <arglist>(code)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UNROLL_LOOP_BODY2</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>acf8fdb0fc786d3e0960f7d85345aaaed</anchor>
      <arglist>(code)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>UNROLL_LOOP_BODY4</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>adead5c1a6142104811b8bb9a05a89393</anchor>
      <arglist>(code)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekkbtrn</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>abbd32bfa5f75a0f79c99171fa3537db4</anchor>
      <arglist>(register const EKKfactinfo *fact, double *dwork1, int *mpt, int first_nonzero)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekkbtrn_ipivrw</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a2889d96b70f6d0d7c2cddabd84df54a9</anchor>
      <arglist>(register const EKKfactinfo *fact, double *dwork1, int *mpt, int ipivrw, int *spare)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekketsj</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>af0ef4a51dc687b1c3f600a787d408fe5</anchor>
      <arglist>(registerEKKfactinfo *fact, double *dwork1, int *mpt2, double dalpha, int orig_nincol, int npivot, int *nuspikp, const int ipivrw, int *spare)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekkftrn</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a38029678d09f3f5a18a8142eb0d2d831</anchor>
      <arglist>(register const EKKfactinfo *fact, double *dwork1, double *dpermu, int *mpt, int numberNonZero)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekkftrn_ft</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a696eac1368fe262b45f9a0e5ff70de84</anchor>
      <arglist>(register EKKfactinfo *fact, double *dwork1, int *mpt, int *nincolp)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkftrn2</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a7524c98f2af09c1aec059ab7788f671a</anchor>
      <arglist>(register EKKfactinfo *fact, double *dwork1, double *dpermu1, int *mpt1, int *nincolp, double *dwork1_ft, int *mpt_ft, int *nincolp_ft)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekklfct</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>ab308ee2ca5f4028614fb358dd810d685</anchor>
      <arglist>(register EKKfactinfo *fact)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekkslcf</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a3b26928d7fd971aea172f9c148b6e229</anchor>
      <arglist>(register const EKKfactinfo *fact)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkscpy</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a843e14d0737d196986b2343e061c4025</anchor>
      <arglist>(int n, const int *marr1, int *marr2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkdcpy</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>af7bee49d9d030f4b3d30e41e03ef6101</anchor>
      <arglist>(int n, const double *marr1, double *marr2)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>c_ekk_IsSet</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a67c19827b0e514be62dc45c2a60246be</anchor>
      <arglist>(const int *array, int bit)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekk_Set</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a701658f7ec1aa65249fe1bb9a0316086</anchor>
      <arglist>(int *array, int bit)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekk_Unset</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>adfb4c0e6a87069efe00096a0aea3f4d3</anchor>
      <arglist>(int *array, int bit)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkzero</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a11dc77e84e07645f6b723e03db357cb9</anchor>
      <arglist>(int length, int n, void *array)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkdzero</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>acfdf468a27f74f2aa15b3dc13a477474</anchor>
      <arglist>(int n, double *marray)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkizero</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>ac4f58ec423f27730224bdb8fc92d3f0d</anchor>
      <arglist>(int n, int *marray)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>c_ekkczero</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a4be6b43932beb10a15104158e1925e19</anchor>
      <arglist>(int n, char *marray)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clp_setup_pointers</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a97c4863fa8359eafe849d0de90454cd4</anchor>
      <arglist>(EKKfactinfo *fact)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clp_memory</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a76ed0ecca876477f3c63b9cb06bb6eab</anchor>
      <arglist>(int type)</arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>clp_double</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>ad0c3388b3b020538a92f687102498f06</anchor>
      <arglist>(int number_entries)</arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>clp_int</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a1aeeb9e7efffb69c942dc53c0e14962a</anchor>
      <arglist>(int number_entries)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>clp_malloc</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a9b885efbce5456ecc45bac0d7cdd090d</anchor>
      <arglist>(int number_entries)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clp_free</name>
      <anchorfile>CoinOslC_8h.html</anchorfile>
      <anchor>a98ca71500e4f0501e6e815df14eb75c9</anchor>
      <arglist>(void *oldArray)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinOslFactorization.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinOslFactorization_8hpp</filename>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinIndexedVector_8hpp" name="CoinIndexedVector.hpp" local="yes" imported="no">CoinIndexedVector.hpp</includes>
    <includes id="CoinDenseFactorization_8hpp" name="CoinDenseFactorization.hpp" local="yes" imported="no">CoinDenseFactorization.hpp</includes>
    <class kind="struct">EKKHlink</class>
    <class kind="struct">_EKKfactinfo</class>
    <class kind="class">CoinOslFactorization</class>
    <member kind="typedef">
      <type>struct _EKKfactinfo</type>
      <name>EKKfactinfo</name>
      <anchorfile>CoinOslFactorization_8hpp.html</anchorfile>
      <anchor>a8ff632f9ab32a69dc02b226b532e9d34</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPackedMatrix.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPackedMatrix_8hpp</filename>
    <includes id="CoinError_8hpp" name="CoinError.hpp" local="yes" imported="no">CoinError.hpp</includes>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinPackedVectorBase_8hpp" name="CoinPackedVectorBase.hpp" local="yes" imported="no">CoinPackedVectorBase.hpp</includes>
    <includes id="CoinShallowPackedVector_8hpp" name="CoinShallowPackedVector.hpp" local="yes" imported="no">CoinShallowPackedVector.hpp</includes>
    <class kind="class">CoinPackedMatrix</class>
    <member kind="function">
      <type>void</type>
      <name>CoinPackedMatrixUnitTest</name>
      <anchorfile>CoinPackedMatrix_8hpp.html</anchorfile>
      <anchor>ab822014d6298cf60c98adcb559d1d987</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPackedVector.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPackedVector_8hpp</filename>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <includes id="CoinPackedVectorBase_8hpp" name="CoinPackedVectorBase.hpp" local="yes" imported="no">CoinPackedVectorBase.hpp</includes>
    <includes id="CoinSort_8hpp" name="CoinSort.hpp" local="yes" imported="no">CoinSort.hpp</includes>
    <class kind="class">CoinPackedVector</class>
    <member kind="define">
      <type>#define</type>
      <name>COIN_DEFAULT_VALUE_FOR_DUPLICATE</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ab20eaf1813ff887cfe62a45b3b56a538</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>sparseDotProduct</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ac68aa3658ffe4ea644436c9f0b473446</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>sortedSparseDotProduct</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>afb7f7faeb739d7d3528e9078464c9083</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinPackedVectorUnitTest</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a7aa27fc9fa40e3fcfea3c639d0530b33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>binaryOp</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a98ce1ef4295a30ed57b75b2ed4e6f7b6</anchor>
      <arglist>(CoinPackedVector &amp;retVal, const CoinPackedVectorBase &amp;op1, double value, BinaryFunction bf)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>binaryOp</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a2973696e8c4191908dbbabb78797f7e9</anchor>
      <arglist>(CoinPackedVector &amp;retVal, double value, const CoinPackedVectorBase &amp;op2, BinaryFunction bf)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>binaryOp</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>aa6fbb0525525dfd32d523c3350364052</anchor>
      <arglist>(CoinPackedVector &amp;retVal, const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2, BinaryFunction bf)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>binaryOp</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ae76bfb9153be03fd3219e48a70d052c1</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, double value, BinaryFunction bf)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>binaryOp</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ac096b607c14997a94cf14db125b63d78</anchor>
      <arglist>(double value, const CoinPackedVectorBase &amp;op2, BinaryFunction bf)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>binaryOp</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a3f290bf00bdb17a62f1ea5e479fc1c5e</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2, BinaryFunction bf)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator+</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a685b5fc15a5c5910b0592b84135c878a</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator-</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ad5e9d13a76a7fd71dfdae08dac9bc8d7</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator*</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ab16f2c7c87b59b60e4ba40d562811e06</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator/</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a6e3db9baed1a85432feffc69d95131cc</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, const CoinPackedVectorBase &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator+</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a335b92f1ee61451905921a199253a7a4</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, double value)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator-</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a45f8d446650bd8dc7b4a21eca53b458b</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, double value)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator*</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a72cc977c551ede2504a5ec3cf8037b47</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, double value)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator/</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a161a89423bc3042cd228351b06005fb1</anchor>
      <arglist>(const CoinPackedVectorBase &amp;op1, double value)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator+</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a1faa0966b2983187e0de1e96957efe2a</anchor>
      <arglist>(double value, const CoinPackedVectorBase &amp;op1)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator-</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>ae9f8fabb59be5c92749fa3f5fa87df3f</anchor>
      <arglist>(double value, const CoinPackedVectorBase &amp;op1)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator*</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>a804d9db7d27bf3cb8da73af8228d612f</anchor>
      <arglist>(double value, const CoinPackedVectorBase &amp;op1)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector</type>
      <name>operator/</name>
      <anchorfile>CoinPackedVector_8hpp.html</anchorfile>
      <anchor>aa68d19a622fbbc58c5dc83529f2a5689</anchor>
      <arglist>(double value, const CoinPackedVectorBase &amp;op1)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPackedVectorBase.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPackedVectorBase_8hpp</filename>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <includes id="CoinError_8hpp" name="CoinError.hpp" local="yes" imported="no">CoinError.hpp</includes>
    <class kind="class">CoinPackedVectorBase</class>
  </compound>
  <compound kind="file">
    <name>CoinParam.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinParam_8hpp</filename>
    <class kind="class">CoinParam</class>
    <namespace>CoinParamUtils</namespace>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>CoinParam_8hpp.html</anchorfile>
      <anchor>aadf7216bb05ff20f3078f7796f23694c</anchor>
      <arglist>(std::ostream &amp;s, const CoinParam &amp;param)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInputSrc</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a437ee486e6f58cad5ea13f3cb3532e3a</anchor>
      <arglist>(FILE *src)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isCommandLine</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a0a2e247f2d799edefef0eed6cea7b087</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteractive</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a93925af7f6c75fe717625c8b37385a4b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getStringField</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a0370af16338883f2f1289c1c0cee4fa6</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getIntField</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>ab1cc490cbb2657785903be303c0a2bf0</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getDoubleField</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>aac3f4f30307501ae40fab327010550bb</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>matchParam</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a13ff379969e4df15a0d427cd48be3d25</anchor>
      <arglist>(const CoinParamVec &amp;paramVec, std::string name, int &amp;matchNdx, int &amp;shortCnt)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getCommand</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a105f78e2e32385a41ec2f6390944ec1e</anchor>
      <arglist>(int argc, const char *argv[], const std::string prompt, std::string *pfx=0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lookupParam</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>ac67d48449be5c2455c4ba4f463c3ebae</anchor>
      <arglist>(std::string name, CoinParamVec &amp;paramVec, int *matchCnt=0, int *shortCnt=0, int *queryCnt=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printIt</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>adf3cb1860afdffcd34d691fea63c2ec6</anchor>
      <arglist>(const char *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shortOrHelpOne</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a6e7f4833f924c1ac5dbbf9be51c96e11</anchor>
      <arglist>(CoinParamVec &amp;paramVec, int matchNdx, std::string name, int numQuery)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shortOrHelpMany</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a817130d7012b494c823fc6e9689eb205</anchor>
      <arglist>(CoinParamVec &amp;paramVec, std::string name, int numQuery)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printGenericHelp</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a77c2516ed11f7154b5db5d0a2dd78a63</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printHelp</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>aced5b09e341f2c76bf00207eb05c631f</anchor>
      <arglist>(CoinParamVec &amp;paramVec, int firstParam, int lastParam, std::string prefix, bool shortHelp, bool longHelp, bool hidden)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPragma.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPragma_8hpp</filename>
  </compound>
  <compound kind="file">
    <name>CoinPresolveDoubleton.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveDoubleton_8hpp</filename>
    <class kind="class">doubleton_action</class>
    <class kind="struct">doubleton_action::action</class>
    <member kind="define">
      <type>#define</type>
      <name>DOUBLETON</name>
      <anchorfile>CoinPresolveDoubleton_8hpp.html</anchorfile>
      <anchor>a5c1e0aba23043a8753b5bd4af74b185c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveDual.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveDual_8hpp</filename>
    <class kind="class">remove_dual_action</class>
  </compound>
  <compound kind="file">
    <name>CoinPresolveDupcol.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveDupcol_8hpp</filename>
    <includes id="CoinPresolveMatrix_8hpp" name="CoinPresolveMatrix.hpp" local="yes" imported="no">CoinPresolveMatrix.hpp</includes>
    <class kind="class">dupcol_action</class>
    <class kind="class">duprow_action</class>
    <class kind="class">duprow3_action</class>
    <class kind="class">gubrow_action</class>
    <class kind="class">twoxtwo_action</class>
    <member kind="define">
      <type>#define</type>
      <name>DUPCOL</name>
      <anchorfile>CoinPresolveDupcol_8hpp.html</anchorfile>
      <anchor>a8a938b743f577a199c446f73043230e2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveEmpty.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveEmpty_8hpp</filename>
    <class kind="class">drop_empty_cols_action</class>
    <class kind="class">drop_empty_rows_action</class>
    <member kind="variable">
      <type>const int</type>
      <name>DROP_ROW</name>
      <anchorfile>CoinPresolveEmpty_8hpp.html</anchorfile>
      <anchor>a19ad4fd8b54f4e77262269460a24742e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const int</type>
      <name>DROP_COL</name>
      <anchorfile>CoinPresolveEmpty_8hpp.html</anchorfile>
      <anchor>a2bbf6105b13fe789d21a19a54255805f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveFixed.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveFixed_8hpp</filename>
    <class kind="class">remove_fixed_action</class>
    <class kind="struct">remove_fixed_action::action</class>
    <class kind="class">make_fixed_action</class>
    <member kind="define">
      <type>#define</type>
      <name>FIXED_VARIABLE</name>
      <anchorfile>CoinPresolveFixed_8hpp.html</anchorfile>
      <anchor>aa75874754fc3ca1fe30840c70907cec4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveForcing.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveForcing_8hpp</filename>
    <includes id="CoinPresolveMatrix_8hpp" name="CoinPresolveMatrix.hpp" local="yes" imported="no">CoinPresolveMatrix.hpp</includes>
    <class kind="class">forcing_constraint_action</class>
    <class kind="struct">forcing_constraint_action::action</class>
    <member kind="define">
      <type>#define</type>
      <name>IMPLIED_BOUND</name>
      <anchorfile>CoinPresolveForcing_8hpp.html</anchorfile>
      <anchor>a6e40d755eeaa232e66be55b4d9de0cb3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveImpliedFree.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveImpliedFree_8hpp</filename>
    <class kind="class">implied_free_action</class>
    <member kind="define">
      <type>#define</type>
      <name>IMPLIED_FREE</name>
      <anchorfile>CoinPresolveImpliedFree_8hpp.html</anchorfile>
      <anchor>a23a60e8d1cabf993f972af74eb44fe70</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveIsolated.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveIsolated_8hpp</filename>
    <includes id="CoinPresolveMatrix_8hpp" name="CoinPresolveMatrix.hpp" local="yes" imported="no">CoinPresolveMatrix.hpp</includes>
    <class kind="class">isolated_constraint_action</class>
  </compound>
  <compound kind="file">
    <name>CoinPresolveMatrix.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveMatrix_8hpp</filename>
    <includes id="CoinPragma_8hpp" name="CoinPragma.hpp" local="yes" imported="no">CoinPragma.hpp</includes>
    <includes id="CoinPackedMatrix_8hpp" name="CoinPackedMatrix.hpp" local="yes" imported="no">CoinPackedMatrix.hpp</includes>
    <includes id="CoinMessage_8hpp" name="CoinMessage.hpp" local="yes" imported="no">CoinMessage.hpp</includes>
    <includes id="CoinTime_8hpp" name="CoinTime.hpp" local="yes" imported="no">CoinTime.hpp</includes>
    <class kind="class">CoinPresolveAction</class>
    <class kind="class">CoinPrePostsolveMatrix</class>
    <class kind="class">presolvehlink</class>
    <class kind="class">CoinPresolveMatrix</class>
    <class kind="class">CoinPostsolveMatrix</class>
    <member kind="define">
      <type>#define</type>
      <name>deleteAction</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>a247935dde761d406c670cc03496a217f</anchor>
      <arglist>(array, type)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PRESOLVEASSERT</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>aed7bd2682adbe1103f07a19db585fba2</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PRESOLVE_STMT</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>abbbe0a1eadb2f9bbf288495adfd5e1fa</anchor>
      <arglist>(s)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PRESOLVE_DETAIL_PRINT</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>a2e41657f79a63541d600091fbd71583b</anchor>
      <arglist>(s)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PRESOLVE_INF</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>a893924e4b8c89248c871be0ce948cd43</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PRESOLVE_SMALL_INF</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>ab6db828390f935c6804819fedd934263</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PRESOLVEFINITE</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>ac955f5a027a862eba9af5e3ffdda11a9</anchor>
      <arglist>(n)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>NO_LINK</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>a302dd036236eaa23b3cfd611d008dc59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>DIE</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>afdd1d1ef5f381cac3196477893a1847f</anchor>
      <arglist>(const char *)</arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>presolve_dupmajor</name>
      <anchorfile>group__PresolveUtilities.html</anchorfile>
      <anchor>ga5ef4f29f36a3daa33bb26217cdcc1d5b</anchor>
      <arglist>(const double *elems, const int *indices, int length, CoinBigIndex offset, int tgt=-1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>coin_init_random_vec</name>
      <anchorfile>group__PresolveUtilities.html</anchorfile>
      <anchor>gacd95d0a7b40d2f51d5d2a9e9437f06a8</anchor>
      <arglist>(double *work, int n)</arglist>
    </member>
    <member kind="variable">
      <type>const double</type>
      <name>ZTOLDP</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>a40fd4ca7a5dbcde27d3dd5245ce95bb5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double</type>
      <name>ZTOLDP2</name>
      <anchorfile>CoinPresolveMatrix_8hpp.html</anchorfile>
      <anchor>a0118ffd9f4082d34e084cefd8d0fbd72</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveMonitor.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveMonitor_8hpp</filename>
    <class kind="class">CoinPresolveMonitor</class>
  </compound>
  <compound kind="file">
    <name>CoinPresolvePsdebug.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolvePsdebug_8hpp</filename>
    <member kind="function">
      <type>void</type>
      <name>postsolve_get_rowcopy</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>gabd1fa82e0e06d7cbba03f45123a2abbb</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj, int *&amp;rowStarts, int *&amp;columns, double *&amp;elements)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveSingleton.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveSingleton_8hpp</filename>
    <class kind="class">slack_doubleton_action</class>
    <class kind="class">slack_singleton_action</class>
    <member kind="define">
      <type>#define</type>
      <name>SLACK_DOUBLETON</name>
      <anchorfile>CoinPresolveSingleton_8hpp.html</anchorfile>
      <anchor>a66c5b30e1d154553dcd1ebb508a19082</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SLACK_SINGLETON</name>
      <anchorfile>CoinPresolveSingleton_8hpp.html</anchorfile>
      <anchor>a22e4547d3fee426d13bee734e04558a4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveSubst.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveSubst_8hpp</filename>
    <includes id="CoinPresolveMatrix_8hpp" name="CoinPresolveMatrix.hpp" local="yes" imported="no">CoinPresolveMatrix.hpp</includes>
    <class kind="class">subst_constraint_action</class>
    <member kind="define">
      <type>#define</type>
      <name>SUBST_ROW</name>
      <anchorfile>CoinPresolveSubst_8hpp.html</anchorfile>
      <anchor>a8df47926689b7f7deb04b5251a2dc347</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>implied_bounds</name>
      <anchorfile>CoinPresolveSubst_8hpp.html</anchorfile>
      <anchor>a6e90a3c69555b66e64dfbaad9a301599</anchor>
      <arglist>(const double *els, const double *clo, const double *cup, const int *hcol, CoinBigIndex krs, CoinBigIndex kre, double *maxupp, double *maxdownp, int jcol, double rlo, double rup, double *iclb, double *icub)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveTighten.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveTighten_8hpp</filename>
    <includes id="CoinPresolveMatrix_8hpp" name="CoinPresolveMatrix.hpp" local="yes" imported="no">CoinPresolveMatrix.hpp</includes>
    <class kind="class">do_tighten_action</class>
    <member kind="define">
      <type>#define</type>
      <name>DO_TIGHTEN</name>
      <anchorfile>CoinPresolveTighten_8hpp.html</anchorfile>
      <anchor>a0e39a3f9496bef93737ea39f7892920f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>const CoinPresolveAction *</type>
      <name>tighten_zero_cost</name>
      <anchorfile>CoinPresolveTighten_8hpp.html</anchorfile>
      <anchor>a60b5a899440b86c403028aff4870034d</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveTripleton.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveTripleton_8hpp</filename>
    <class kind="class">tripleton_action</class>
    <class kind="struct">tripleton_action::action</class>
    <member kind="define">
      <type>#define</type>
      <name>TRIPLETON</name>
      <anchorfile>CoinPresolveTripleton_8hpp.html</anchorfile>
      <anchor>a43ca896a38003f8c7eb3be95b91d92ab</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveUseless.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveUseless_8hpp</filename>
    <class kind="class">useless_constraint_action</class>
    <member kind="define">
      <type>#define</type>
      <name>USELESS</name>
      <anchorfile>CoinPresolveUseless_8hpp.html</anchorfile>
      <anchor>a612122d71eeab5ef460549b943011222</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinPresolveZeros.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinPresolveZeros_8hpp</filename>
    <class kind="struct">dropped_zero</class>
    <class kind="class">drop_zero_coefficients_action</class>
    <member kind="define">
      <type>#define</type>
      <name>DROP_ZERO</name>
      <anchorfile>CoinPresolveZeros_8hpp.html</anchorfile>
      <anchor>ae8f815ce1a4cb9dd97e373dd073a82d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>const CoinPresolveAction *</type>
      <name>drop_zero_coefficients</name>
      <anchorfile>CoinPresolveZeros_8hpp.html</anchorfile>
      <anchor>a4e1aae921b6d557a8041058ce7f5672a</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinRational.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinRational_8hpp</filename>
    <class kind="class">CoinRational</class>
  </compound>
  <compound kind="file">
    <name>CoinSearchTree.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinSearchTree_8hpp</filename>
    <includes id="CoinFinite_8hpp" name="CoinFinite.hpp" local="yes" imported="no">CoinFinite.hpp</includes>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <class kind="class">BitVector128</class>
    <class kind="class">CoinTreeNode</class>
    <class kind="class">CoinTreeSiblings</class>
    <class kind="struct">CoinSearchTreeComparePreferred</class>
    <class kind="struct">CoinSearchTreeCompareDepth</class>
    <class kind="struct">CoinSearchTreeCompareBreadth</class>
    <class kind="struct">CoinSearchTreeCompareBest</class>
    <class kind="class">CoinSearchTreeBase</class>
    <class kind="class">CoinSearchTree</class>
    <class kind="class">CoinSearchTreeManager</class>
    <member kind="enumeration">
      <type></type>
      <name>CoinNodeAction</name>
      <anchorfile>CoinSearchTree_8hpp.html</anchorfile>
      <anchor>ae0689845933bc406ac164c2e4179e65a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CoinAddNodeToCandidates</name>
      <anchorfile>CoinSearchTree_8hpp.html</anchorfile>
      <anchor>ae0689845933bc406ac164c2e4179e65aa8c67b316ac9499a05cad5b5a4e72f391</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CoinTestNodeForDiving</name>
      <anchorfile>CoinSearchTree_8hpp.html</anchorfile>
      <anchor>ae0689845933bc406ac164c2e4179e65aaeb3b6b41cfe8c07856ba86d57d59ea3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>CoinDiveIntoNode</name>
      <anchorfile>CoinSearchTree_8hpp.html</anchorfile>
      <anchor>ae0689845933bc406ac164c2e4179e65aaae61222dabbdefb80a96b0f7cd7a2b3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>CoinSearchTree_8hpp.html</anchorfile>
      <anchor>a2cc8fc4aa9ba6f421ede656bcc26bb4a</anchor>
      <arglist>(const BitVector128 &amp;b0, const BitVector128 &amp;b1)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinShallowPackedVector.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinShallowPackedVector_8hpp</filename>
    <includes id="CoinError_8hpp" name="CoinError.hpp" local="yes" imported="no">CoinError.hpp</includes>
    <includes id="CoinPackedVectorBase_8hpp" name="CoinPackedVectorBase.hpp" local="yes" imported="no">CoinPackedVectorBase.hpp</includes>
    <class kind="class">CoinShallowPackedVector</class>
    <member kind="function">
      <type>void</type>
      <name>CoinShallowPackedVectorUnitTest</name>
      <anchorfile>CoinShallowPackedVector_8hpp.html</anchorfile>
      <anchor>aee2042564a5c00251096f3c0784bd20c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinSignal.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinSignal_8hpp</filename>
    <member kind="typedef">
      <type>void(*</type>
      <name>CoinSighandler_t</name>
      <anchorfile>CoinSignal_8hpp.html</anchorfile>
      <anchor>a5b55d07213caa642476154711ae24cf9</anchor>
      <arglist>)(int)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinSimpFactorization.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinSimpFactorization_8hpp</filename>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <includes id="CoinIndexedVector_8hpp" name="CoinIndexedVector.hpp" local="yes" imported="no">CoinIndexedVector.hpp</includes>
    <includes id="CoinDenseFactorization_8hpp" name="CoinDenseFactorization.hpp" local="yes" imported="no">CoinDenseFactorization.hpp</includes>
    <class kind="class">FactorPointers</class>
    <class kind="class">CoinSimpFactorization</class>
  </compound>
  <compound kind="file">
    <name>CoinSmartPtr.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinSmartPtr_8hpp</filename>
    <class kind="class">Coin::ReferencedObject</class>
    <class kind="class">Coin::SmartPtr</class>
    <namespace>Coin</namespace>
    <member kind="define">
      <type>#define</type>
      <name>dbg_smartptr_verbosity</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a175f709ba6ea978b97fbde2b29ecb293</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinReferencedObject</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a2ce4aac9a873a3d84488b2da508fc6d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinSmartPtr</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a61055f4968267867f7b949517b64d339</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinComparePointers</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a55ac659ebab7fd3e3c4bf70f784bcec7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>ComparePointers</name>
      <anchorfile>namespaceCoin.html</anchorfile>
      <anchor>a0dcec666fa32b8155aa349c90c8403ad</anchor>
      <arglist>(const U1 *lhs, const U2 *rhs)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a05d40d7207c8f9188875b2ded8f61d3f</anchor>
      <arglist>(const Coin::SmartPtr&lt; U1 &gt; &amp;lhs, const Coin::SmartPtr&lt; U2 &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>afc7df9f80a0ef01afb47da43762abc80</anchor>
      <arglist>(const Coin::SmartPtr&lt; U1 &gt; &amp;lhs, U2 *raw_rhs)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a30f445644e3f39278eeabfc7e0fb1e1b</anchor>
      <arglist>(U1 *raw_lhs, const Coin::SmartPtr&lt; U2 &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a133c06d91f74bd9698d6d82a479cdcab</anchor>
      <arglist>(const Coin::SmartPtr&lt; U1 &gt; &amp;lhs, const Coin::SmartPtr&lt; U2 &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>af88134e464f3d0c5ac1e9a219a42fafa</anchor>
      <arglist>(const Coin::SmartPtr&lt; U1 &gt; &amp;lhs, U2 *raw_rhs)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>CoinSmartPtr_8hpp.html</anchorfile>
      <anchor>a71322b123fd6b35589fe7a9a5bf447f9</anchor>
      <arglist>(U1 *raw_lhs, const Coin::SmartPtr&lt; U2 &gt; &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinSnapshot.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinSnapshot_8hpp</filename>
    <includes id="CoinTypes_8hpp" name="CoinTypes.hpp" local="yes" imported="no">CoinTypes.hpp</includes>
    <class kind="class">CoinSnapshot</class>
  </compound>
  <compound kind="file">
    <name>CoinSort.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinSort_8hpp</filename>
    <includes id="CoinDistance_8hpp" name="CoinDistance.hpp" local="yes" imported="no">CoinDistance.hpp</includes>
    <class kind="struct">CoinPair</class>
    <class kind="class">CoinFirstLess_2</class>
    <class kind="class">CoinFirstGreater_2</class>
    <class kind="class">CoinFirstAbsLess_2</class>
    <class kind="class">CoinFirstAbsGreater_2</class>
    <class kind="class">CoinExternalVectorFirstLess_2</class>
    <class kind="class">CoinExternalVectorFirstGreater_2</class>
    <class kind="class">CoinTriple</class>
    <class kind="class">CoinFirstLess_3</class>
    <class kind="class">CoinFirstGreater_3</class>
    <class kind="class">CoinFirstAbsLess_3</class>
    <class kind="class">CoinFirstAbsGreater_3</class>
    <class kind="class">CoinExternalVectorFirstLess_3</class>
    <class kind="class">CoinExternalVectorFirstGreater_3</class>
    <member kind="typedef">
      <type>CoinExternalVectorFirstLess_3&lt; int, int, double, double &gt;</type>
      <name>CoinIncrSolutionOrdered</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>afc86ad11f773a85cf2b0a529a00ee688</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CoinExternalVectorFirstGreater_3&lt; int, int, double, double &gt;</type>
      <name>CoinDecrSolutionOrdered</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>a2146c866f6e8e35e6fb882f3ca056403</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSort_2</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>a190ae808590aaac69b307190648dc7a6</anchor>
      <arglist>(S *sfirst, S *slast, T *tfirst, const CoinCompare2 &amp;pc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSort_2Std</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>a397e55848ea977760138d1105c9b33ff</anchor>
      <arglist>(S *sfirst, S *slast, T *tfirst)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSort_2</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>a890dc9d5d62af3b911341ba71c8c086d</anchor>
      <arglist>(S *sfirst, S *slast, T *tfirst)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinShortSort_2</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>a02bdf437a2bcd84d2c00f5c81aa0c0f2</anchor>
      <arglist>(S *key, S *lastKey, T *array2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSort_3</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>adbad8d3ceb36084c00d097ad2ff0ae9e</anchor>
      <arglist>(S *sfirst, S *slast, T *tfirst, U *ufirst, const CoinCompare3 &amp;tc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CoinSort_3</name>
      <anchorfile>CoinSort_8hpp.html</anchorfile>
      <anchor>adfe292f89aa33bfbf2762cf214713da8</anchor>
      <arglist>(S *sfirst, S *slast, T *tfirst, U *ufirst)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinStructuredModel.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinStructuredModel_8hpp</filename>
    <includes id="CoinModel_8hpp" name="CoinModel.hpp" local="yes" imported="no">CoinModel.hpp</includes>
    <class kind="struct">CoinModelInfo2</class>
    <class kind="class">CoinStructuredModel</class>
    <member kind="typedef">
      <type>struct CoinModelInfo2</type>
      <name>CoinModelBlockInfo</name>
      <anchorfile>CoinStructuredModel_8hpp.html</anchorfile>
      <anchor>a86504a8ba2e17d1d74f9902b0bc46999</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinTime.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinTime_8hpp</filename>
    <class kind="class">CoinTimer</class>
    <member kind="function">
      <type>double</type>
      <name>CoinGetTimeOfDay</name>
      <anchorfile>CoinTime_8hpp.html</anchorfile>
      <anchor>a830698de004dd2c2e603b962f6dd70a6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>CoinWallclockTime</name>
      <anchorfile>CoinTime_8hpp.html</anchorfile>
      <anchor>ab4d8058a243e42b565f795937f7b01e9</anchor>
      <arglist>(double callType=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static double</type>
      <name>CoinCpuTime</name>
      <anchorfile>CoinTime_8hpp.html</anchorfile>
      <anchor>ab77eb86180f611967a08cb568a1aa9ae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static double</type>
      <name>CoinSysTime</name>
      <anchorfile>CoinTime_8hpp.html</anchorfile>
      <anchor>a9b44f1a972b039480ef5b2f97deec563</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static double</type>
      <name>CoinCpuTimeJustChildren</name>
      <anchorfile>CoinTime_8hpp.html</anchorfile>
      <anchor>a0aa0c7cb99c830f0a684b15130e82662</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinTypes.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinTypes_8hpp</filename>
    <includes id="CoinUtilsConfig_8h" name="CoinUtilsConfig.h" local="yes" imported="no">CoinUtilsConfig.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>CoinInt64</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a9b6320e0f19da1e4b46ac6ae091e2d88</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinUInt64</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>abd4d5cdfc0af086940534f7cbaa512d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CoinIntPtr</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a69d45baf736ad9045febb72736749f2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_BIG_INDEX</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a441227f3008693f0cb5ab8833da89ac2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_BIG_DOUBLE</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>aee6ae9b87976c5cbb10851e162e773a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_LONG_WORK</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a7db102b8b7346eb092dce9baf7076454</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>CoinBigIndex</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a1d91e832494d1d012e44c52e34d2340f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>double</type>
      <name>CoinWorkDouble</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a95945f21f97e19504996ac3bc63cf760</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>double</type>
      <name>CoinFactorizationDouble</name>
      <anchorfile>CoinTypes_8hpp.html</anchorfile>
      <anchor>a8ff025b1fd5b64b07b7dc31f394d8c32</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinUtility.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinUtility_8hpp</filename>
    <includes id="CoinSort_8hpp" name="CoinSort.hpp" local="yes" imported="no">CoinSort.hpp</includes>
    <member kind="function">
      <type>CoinPair&lt; S, T &gt;</type>
      <name>CoinMakePair</name>
      <anchorfile>CoinUtility_8hpp.html</anchorfile>
      <anchor>ac0da997b557374b8a09195558c11f049</anchor>
      <arglist>(const S &amp;s, const T &amp;t)</arglist>
    </member>
    <member kind="function">
      <type>CoinTriple&lt; S, T, U &gt;</type>
      <name>CoinMakeTriple</name>
      <anchorfile>CoinUtility_8hpp.html</anchorfile>
      <anchor>aba6960644d6639fcd01dda20976c2a4d</anchor>
      <arglist>(const S &amp;s, const T &amp;t, const U &amp;u)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CoinUtilsConfig.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinUtilsConfig_8h</filename>
    <includes id="config__coinutils__default_8h" name="config_coinutils_default.h" local="yes" imported="no">config_coinutils_default.h</includes>
  </compound>
  <compound kind="file">
    <name>CoinWarmStart.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinWarmStart_8hpp</filename>
    <class kind="class">CoinWarmStart</class>
    <class kind="class">CoinWarmStartDiff</class>
  </compound>
  <compound kind="file">
    <name>CoinWarmStartBasis.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinWarmStartBasis_8hpp</filename>
    <includes id="CoinSort_8hpp" name="CoinSort.hpp" local="yes" imported="no">CoinSort.hpp</includes>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <includes id="CoinWarmStart_8hpp" name="CoinWarmStart.hpp" local="yes" imported="no">CoinWarmStart.hpp</includes>
    <class kind="class">CoinWarmStartBasis</class>
    <class kind="class">CoinWarmStartBasisDiff</class>
  </compound>
  <compound kind="file">
    <name>CoinWarmStartDual.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinWarmStartDual_8hpp</filename>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <includes id="CoinWarmStart_8hpp" name="CoinWarmStart.hpp" local="yes" imported="no">CoinWarmStart.hpp</includes>
    <includes id="CoinWarmStartVector_8hpp" name="CoinWarmStartVector.hpp" local="yes" imported="no">CoinWarmStartVector.hpp</includes>
    <class kind="class">CoinWarmStartDual</class>
    <class kind="class">CoinWarmStartDualDiff</class>
  </compound>
  <compound kind="file">
    <name>CoinWarmStartPrimalDual.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinWarmStartPrimalDual_8hpp</filename>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <includes id="CoinWarmStart_8hpp" name="CoinWarmStart.hpp" local="yes" imported="no">CoinWarmStart.hpp</includes>
    <includes id="CoinWarmStartVector_8hpp" name="CoinWarmStartVector.hpp" local="yes" imported="no">CoinWarmStartVector.hpp</includes>
    <class kind="class">CoinWarmStartPrimalDual</class>
    <class kind="class">CoinWarmStartPrimalDualDiff</class>
  </compound>
  <compound kind="file">
    <name>CoinWarmStartVector.hpp</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>CoinWarmStartVector_8hpp</filename>
    <includes id="CoinHelperFunctions_8hpp" name="CoinHelperFunctions.hpp" local="yes" imported="no">CoinHelperFunctions.hpp</includes>
    <includes id="CoinWarmStart_8hpp" name="CoinWarmStart.hpp" local="yes" imported="no">CoinWarmStart.hpp</includes>
    <class kind="class">CoinWarmStartVector</class>
    <class kind="class">CoinWarmStartVectorDiff</class>
    <class kind="class">CoinWarmStartVectorPair</class>
    <class kind="class">CoinWarmStartVectorPairDiff</class>
  </compound>
  <compound kind="file">
    <name>config.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>config_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_HAS_STDINT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a963ff2eddf7372e90d45688dc491a7b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_MEMPOOL_MAXPOOLED</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ad2a89cf18554bf791c84d0beb7646064</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_SVN_REV</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a9bcbbabfabb6b06b8f7cd168a5e025e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1cbc6bc573dd5f2c97dd63974a1ec013</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_MAJOR</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a227e57c20adb096f2ed55b2d75fb64b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_MINOR</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1de6645b6cf4049e129a2d5775fc2787</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_RELEASE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>af83b7bd6676cc4b9bb85ec70b091a463</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_COINUTILS_CHECKLEVEL</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a2533ae14191b545fd3fe2a426a0fac0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_COINUTILS_VERBOSITY</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a3f1011d1359e2d3d02c9c09cf15f9c83</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_NETLIB</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a6e1a694c8ecdefddf586c7ce148b63f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_SAMPLE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a8ea44e87e1d5c8c821de2055bb250662</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_HAS_ZLIB</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a0cfe6b2bfe36fb0b9d15f93a7d9c8d19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INT64_T</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a26453cc6e3e7c2c5e9ce74488c4e0830</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INTPTR_T</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a3e9faa1d0c9487241fd276387c9e7f56</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_UINT64_T</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a8b4956f399fede07ce699ab6117056b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_CFLOAT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a34d6c379f30f7070f7ae75abb128e5a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_CMATH</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a5ebdfdabf36598943dc9faa5699a1ba5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_DLFCN_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a0ee1617ff2f6885ef384a3dd46f9b9d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_ENDIAN_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a9ef8e61ca569fc24c3e47aeab03498c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_INTTYPES_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ab90a030ff2790ebdc176660a6dd2a478</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_MEMORY_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ae93a78f9d076138897af441c9f86f285</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STDINT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ab6cd6d1c63c1e26ea2d4537b77148354</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STDLIB_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a9e0e434ec1a6ddbd97db12b5a32905e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STRINGS_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a405d10d46190bcb0320524c54eafc850</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STRING_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ad4c234dd1625255dc626a15886306e7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_STAT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ace156430ba007d19b4348a950d0c692b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_TYPES_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a69dc70bea5d1f8bd2be9740e974fa666</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_UNISTD_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a219b06937831d0da94d801ab13987639</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_ZLIB_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>aad5eed50844aa0073171213cb654363d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>aca8570fb706c81df371b7f9bc454ae03</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_BUGREPORT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1d1d2d7f8d2f95b376954d649ab03233</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_NAME</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1c0439e4355794c09b64274849eb0279</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_STRING</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ac73e6f903c16eca7710f92e36e1c6fbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_TARNAME</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>af415af6bfede0e8d5453708afe68651c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>aa326a05d5e30f9e9a4bb0b4469d5d0c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STDC_HEADERS</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a550e5c272cc3cf3814651721167dcd23</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a1c6d5de492ac61ad29aec7aa9a436bbf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>config_coinutils.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>config__coinutils_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_HAS_STDINT_H</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a963ff2eddf7372e90d45688dc491a7b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a1cbc6bc573dd5f2c97dd63974a1ec013</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_MAJOR</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a227e57c20adb096f2ed55b2d75fb64b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_MINOR</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a1de6645b6cf4049e129a2d5775fc2787</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_RELEASE</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>af83b7bd6676cc4b9bb85ec70b091a463</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INT64_T</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a26453cc6e3e7c2c5e9ce74488c4e0830</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INTPTR_T</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a3e9faa1d0c9487241fd276387c9e7f56</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_UINT64_T</name>
      <anchorfile>config__coinutils_8h.html</anchorfile>
      <anchor>a8b4956f399fede07ce699ab6117056b6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>config_coinutils_default.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>config__coinutils__default_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>a1cbc6bc573dd5f2c97dd63974a1ec013</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_MAJOR</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>a227e57c20adb096f2ed55b2d75fb64b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_MINOR</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>a1de6645b6cf4049e129a2d5775fc2787</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COINUTILS_VERSION_RELEASE</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>af83b7bd6676cc4b9bb85ec70b091a463</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INT64_T</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>a26453cc6e3e7c2c5e9ce74488c4e0830</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_UINT64_T</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>a8b4956f399fede07ce699ab6117056b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_INTPTR_T</name>
      <anchorfile>config__coinutils__default_8h.html</anchorfile>
      <anchor>a3e9faa1d0c9487241fd276387c9e7f56</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>config_default.h</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>config__default_8h</filename>
    <includes id="config__coinutils__default_8h" name="config_coinutils_default.h" local="yes" imported="no">config_coinutils_default.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>COIN_COINUTILS_CHECKLEVEL</name>
      <anchorfile>config__default_8h.html</anchorfile>
      <anchor>a2533ae14191b545fd3fe2a426a0fac0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>COIN_COINUTILS_VERBOSITY</name>
      <anchorfile>config__default_8h.html</anchorfile>
      <anchor>a3f1011d1359e2d3d02c9c09cf15f9c83</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>MtxManip</name>
    <title>Presolve Matrix Manipulation Functions</title>
    <filename>group__MtxManip.html</filename>
    <member kind="function">
      <type>void</type>
      <name>presolve_make_memlists</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga4fdf8c2f470833e457bd453857eb5e2d</anchor>
      <arglist>(int *lengths, presolvehlink *link, int n)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>presolve_expand_major</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga85ad25aa69c9d62dedc940811f109061</anchor>
      <arglist>(CoinBigIndex *majstrts, double *majels, int *minndxs, int *majlens, presolvehlink *majlinks, int nmaj, int k)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>presolve_expand_col</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga26285b7734fb16ee66d24fa2ec795aa1</anchor>
      <arglist>(CoinBigIndex *mcstrt, double *colels, int *hrow, int *hincol, presolvehlink *clink, int ncols, int colx)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>presolve_expand_row</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gace2b703fbe1d91a62902a74336134590</anchor>
      <arglist>(CoinBigIndex *mrstrt, double *rowels, int *hcol, int *hinrow, presolvehlink *rlink, int nrows, int rowx)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gaa6df2e05d828f7246d4f6cf69ecdd34f</anchor>
      <arglist>(int tgt, CoinBigIndex ks, CoinBigIndex ke, const int *minndxs)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga6603257c54c40ed5d5535ba9c9304a4f</anchor>
      <arglist>(int row, CoinBigIndex kcs, CoinBigIndex kce, const int *hrow)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_col</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga018bcaad92a79e499daf55fa54a6e329</anchor>
      <arglist>(int col, CoinBigIndex krs, CoinBigIndex kre, const int *hcol)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor1</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gabdfb2edeb344e51aca0a0551d16d4746</anchor>
      <arglist>(int tgt, CoinBigIndex ks, CoinBigIndex ke, const int *minndxs)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row1</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga59c73121ef1cb7f99cc9dab144899349</anchor>
      <arglist>(int row, CoinBigIndex kcs, CoinBigIndex kce, const int *hrow)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_col1</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga779051a846dcaa1745d5aa8df878ed9c</anchor>
      <arglist>(int col, CoinBigIndex krs, CoinBigIndex kre, const int *hcol)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga30af6b47600ca5711fe0c0e72e903e6e</anchor>
      <arglist>(int tgt, CoinBigIndex ks, int majlen, const int *minndxs, const CoinBigIndex *majlinks)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga4cd185437ad58ab99f3184a0e9744642</anchor>
      <arglist>(int row, CoinBigIndex kcs, int collen, const int *hrow, const CoinBigIndex *clinks)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor3</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gaf9e637f7d36420ffcd00954d0b9ff0be</anchor>
      <arglist>(int tgt, CoinBigIndex ks, int majlen, const int *minndxs, const CoinBigIndex *majlinks)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row3</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gaa83a1921b0a08e55eef82ad11d88cb2a</anchor>
      <arglist>(int row, CoinBigIndex kcs, int collen, const int *hrow, const CoinBigIndex *clinks)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_major</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gad732d9c80c4a53d9d0b2117f848d6952</anchor>
      <arglist>(int majndx, int minndx, const CoinBigIndex *majstrts, int *majlens, int *minndxs, double *els)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_many_from_major</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gab0c9ad6aed6f01af891ad404fd5a4a34</anchor>
      <arglist>(int majndx, char *marked, const CoinBigIndex *majstrts, int *majlens, int *minndxs, double *els)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_col</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga891f9ad85b434ad8cb0387290a04c0a7</anchor>
      <arglist>(int row, int col, const CoinBigIndex *mcstrt, int *hincol, int *hrow, double *colels)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_row</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga6adbb54310f32482247aaf19f0baf903</anchor>
      <arglist>(int row, int col, const CoinBigIndex *mrstrt, int *hinrow, int *hcol, double *rowels)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_major2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gae37816e7aefc0987d2615f4096741b48</anchor>
      <arglist>(int majndx, int minndx, CoinBigIndex *majstrts, int *majlens, int *minndxs, int *majlinks, CoinBigIndex *free_listp)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_col2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gab57089922e7b366093e8710965c136ab</anchor>
      <arglist>(int row, int col, CoinBigIndex *mcstrt, int *hincol, int *hrow, int *clinks, CoinBigIndex *free_listp)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>PresolveUtilities</name>
    <title>Presolve Utility Functions</title>
    <filename>group__PresolveUtilities.html</filename>
    <member kind="function">
      <type>double *</type>
      <name>presolve_dupmajor</name>
      <anchorfile>group__PresolveUtilities.html</anchorfile>
      <anchor>ga5ef4f29f36a3daa33bb26217cdcc1d5b</anchor>
      <arglist>(const double *elems, const int *indices, int length, CoinBigIndex offset, int tgt=-1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>coin_init_random_vec</name>
      <anchorfile>group__PresolveUtilities.html</anchorfile>
      <anchor>gacd95d0a7b40d2f51d5d2a9e9437f06a8</anchor>
      <arglist>(double *work, int n)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>PresolveDebugFunctions</name>
    <title>Presolve Debug Functions</title>
    <filename>group__PresolveDebugFunctions.html</filename>
    <member kind="function">
      <type>void</type>
      <name>postsolve_get_rowcopy</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>gabd1fa82e0e06d7cbba03f45123a2abbb</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj, int *&amp;rowStarts, int *&amp;columns, double *&amp;elements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_no_dups</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga08e4d8e2b0f3a1a01705706d17cea73d</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool doCol=true, bool doRow=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_links_ok</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga746a411967809a3cc44296dc00fcabe5</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool doCol=true, bool doRow=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_no_zeros</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga44e2694f711758de1fd1207081ed87d2</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool doCol=true, bool doRow=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_consistent</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>gaebff24e6ce6d30373e7243a760601f9d</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool chkvals=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_threads</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga98a64ab94fccface6907127dfe6e4a84</anchor>
      <arglist>(const CoinPostsolveMatrix *obj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_free_list</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga5e846245e3997fbed40b1c9b799128fc</anchor>
      <arglist>(const CoinPostsolveMatrix *obj, bool chkElemCnt=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_reduced_costs</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga2140877787eaaba96f1247a20ac101db</anchor>
      <arglist>(const CoinPostsolveMatrix *obj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_duals</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga240fa3c02f7b334fd2df38b7270349eb</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_sol</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga0de487312b1bc0b13a65c28f2e4f3f87</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, int chkColSol=2, int chkRowAct=1, int chkStatus=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_sol</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga030acbac86d29771b727e346348842f4</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj, int chkColSol=2, int chkRowAct=2, int chkStatus=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_nbasic</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga5b43580c65f5d379ced3c89b6e2d23c8</anchor>
      <arglist>(const CoinPresolveMatrix *preObj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_nbasic</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>gaec17518929b43c598d12f80ab0dcc480</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>_EKKfactinfo</name>
    <filename>struct__EKKfactinfo.html</filename>
    <member kind="variable">
      <type>double</type>
      <name>drtpiv</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a9528794bf9537bcd97ecae360f4f0cff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>demark</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a2346918798da1a3827ca0d95548c9897</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>zpivlu</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>aea34e12a09bcce799f29721bef15bf5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>zeroTolerance</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>abd0e9522120179eaa9e1bdf99f297f27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>areaFactor</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a75561ab153a5aa42b39b8a6a4c7b5a51</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>xrsadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a40be1dc96243f99b78a95b040480f806</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>xcsadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a1ece3a7dc398af3e95bafced7bc59725</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>xrnadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a463eefda7d361f9d860c866b7a754d33</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>xcnadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ab2604bebb3de6aee0d8df205ede57090</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>krpadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a3c7b8a6699a32654d66640828b52a9c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>kcpadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a8a4105460fd60b2d6264770b3b1eade6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>mpermu</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a530c82ccb790fe7288037fd3ecbb429b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>bitArray</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a023542c21b35eea3b4fe56b48d4ef2a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>back</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a5a818b3788565db0e5b3302bc7c389ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>nonzero</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a0b3ae64a6d204140e9e0676ac0a2c4d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>trueStart</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a85497fcc90c93b8c475f9e9fbe72ed42</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>kadrpm</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a4729870277e674cd9d5b05729e903fa8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>R_etas_index</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a4faacc786702843dc9af66ec77459c43</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>R_etas_start</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a4f5ebefe061f51768423e5c04b4f5732</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>R_etas_element</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a13862d89f2383e809c3f098ee140aeb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>xecadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a7fb8f555c5aaf2001aeaf3b3892b76a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>xeradr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ac32d50ce062d6d771eac039db836c44d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>xeeadr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a3390b0ffe287d12b29a09c7e7c2a1caf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>xe2adr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>acc2bc90b6649b0e52d8c5b7a6bab424d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>EKKHlink *</type>
      <name>kp1adr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a6ed931684c313f6a1fb7623db8aead4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>EKKHlink *</type>
      <name>kp2adr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>aedbb1e83c540db452038aff25672d7fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>kw1adr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a030ee641fabfe80c1ccaa278397e601d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>kw2adr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a0ad2e82c9f9475107aa4f156a3a49bd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>kw3adr</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>aff1f9a4063555323c5bfbbb85bc281d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hpivcoR</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ad81937412ba99b242c7c3d53b0270781</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nrow</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>abef1d36df95b2c8972796375918fac90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nrowmx</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a585436f492fa3a14d4d57a4c0963d495</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>firstDoRow</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a8c40b278710510626df18cc31a67e097</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>firstLRow</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a3a8270935ef3dca0909b17aa1dcb0038</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>maxinv</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a347c9613d39484ed1cff9f4be705b4a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nnetas</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a39b1dac93f620c3c10852a5eaf377767</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>iterin</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a1fd6bb2bd13763cf971b443b80006cb2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>iter0</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>aecb005c0550c6956ad7e277657a9b54a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>invok</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>aafa0499a628a06ad95626dd5eeff20ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nbfinv</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a380423528de5ec475447e307abafc228</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>num_resets</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a85d5f5dd73177608b7115a61620778a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nnentl</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ac74be7502629424c09cb0719a32f4e1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nnentu</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ab10f9892ce65dfc5c6711078b1eafb3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ndenuc</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>af702a247517f5144fbe69896969ca0e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>npivots</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a7c3b41e497bc8d1bca8c138eccc569f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>kmxeta</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a823111f7bf6154858f76ecbbafcf68e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>xnetal</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a3518d760da76762de5628829b1e5ef8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>first_dense</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a2c8dad16ccf3f81fb622e7f4a8ac1012</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>last_dense</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>afb7972f58f4059c5644a58524d19d72e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>iterno</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ab38e4f59c73114501b9fe677c6d1cdf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberSlacks</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a9d850fe6d621eef6f2370cc0f51b20ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>lastSlack</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a67a3f94efada35543191c036b249e9d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>firstNonSlack</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a696a47fa965a63538684a906097a4205</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>xnetalval</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ae37741ade2e5daf6f10aa6ffdb00dbff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>lstart</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a28c3accbd385dba734fff0b2377034f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>if_sparse_update</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ac4c88222fdc1df10e675d23f357ba654</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>packedMode</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a5d439338ab01592badd883510947b14d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>switch_off_sparse_update</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a8dd53e1fec18a34c019e63c0c2e85d41</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nuspike</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a1c5128394ba0ca4ab1d1dee595e46c87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>rows_ok</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a82c31196366b148679ae644eb6a038bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nR_etas</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a9ba252535b93949e8d07c62f43efa91e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>sortedEta</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a58fb7331e5b373b77149b9e450a280e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>lastEtaCount</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a3fb32645d6ce40d5165c17befd0115e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ifvsol</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a172b46c08ee1a1b51c5d7f082193d45c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>eta_size</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>ad55f528ae03462ec137f8152e9d1c55d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>last_eta_size</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a8357dc8ee44dc1199d7b4f0e85cac0cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>maxNNetas</name>
      <anchorfile>struct__EKKfactinfo.html</anchorfile>
      <anchor>a121de046ff9ccd27312a6e92e50c6230</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>BitVector128</name>
    <filename>classBitVector128.html</filename>
    <member kind="function">
      <type></type>
      <name>BitVector128</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a5262d38d20b839fab36b9055064a606b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BitVector128</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a35fbcfe2006fc06e2acf02c2b529413e</anchor>
      <arglist>(unsigned int bits[4])</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~BitVector128</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a629719b77bb1f4b17218ace3700284d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a1545c542f66e0e1d500d4285a1605101</anchor>
      <arglist>(unsigned int bits[4])</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBit</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a35dd0c3705f2f12b1cd44c45b6043537</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearBit</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>acdbd9bb9915fe99ecf6946d8dfe924b7</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>str</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a0b80860f7d3da6fff742067df71bfb62</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend bool</type>
      <name>operator&lt;</name>
      <anchorfile>classBitVector128.html</anchorfile>
      <anchor>a2cc8fc4aa9ba6f421ede656bcc26bb4a</anchor>
      <arglist>(const BitVector128 &amp;b0, const BitVector128 &amp;b1)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinAbsFltEq</name>
    <filename>classCoinAbsFltEq.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinAbsFltEq.html</anchorfile>
      <anchor>ab903a70e3f58fdf8c7d2cfb0cb6b5905</anchor>
      <arglist>(const double f1, const double f2) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinAbsFltEq</name>
      <anchorfile>classCoinAbsFltEq.html</anchorfile>
      <anchor>ad3d2294dd02c72310350f4008699dda0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinAbsFltEq</name>
      <anchorfile>classCoinAbsFltEq.html</anchorfile>
      <anchor>a63e828d4d4ae81f19b497073f9dd32bd</anchor>
      <arglist>(const double epsilon)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinAbsFltEq</name>
      <anchorfile>classCoinAbsFltEq.html</anchorfile>
      <anchor>a8d57c503037937470a84ab4472ba144f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinAbsFltEq</name>
      <anchorfile>classCoinAbsFltEq.html</anchorfile>
      <anchor>afc9f1e1e98462572899c15ad3ae928bb</anchor>
      <arglist>(const CoinAbsFltEq &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>CoinAbsFltEq &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinAbsFltEq.html</anchorfile>
      <anchor>a5a34cf469d27b74b2ccf75bea4e43332</anchor>
      <arglist>(const CoinAbsFltEq &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinArbitraryArrayWithLength</name>
    <filename>classCoinArbitraryArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a2355bea268afcf78fa2f7345211d5fb9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void **</type>
      <name>array</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a944334d3c3c844f8eab799be8e2c55a2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>ab8d6ddf7228d24bc0f16936ef9c3a864</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a45adc477e52f2e3e62b7f640cbbb5ded</anchor>
      <arglist>(int length, int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArbitraryArrayWithLength</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a3f8788ebdd19814d273cb86a61bb335b</anchor>
      <arglist>(int length=1)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArbitraryArrayWithLength</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a92f32e49539965c06ba4c8191930b4ce</anchor>
      <arglist>(int length, int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArbitraryArrayWithLength</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a9b209ff48fde71e9d0dcd515463398c7</anchor>
      <arglist>(int length, int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArbitraryArrayWithLength</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a5fcaae221e8ea584e9fa2df3d9ece8b5</anchor>
      <arglist>(const CoinArbitraryArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArbitraryArrayWithLength</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a21d95d603fedd92ddfcbe405ae4d6982</anchor>
      <arglist>(const CoinArbitraryArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinArbitraryArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a271fb31eea88f32004c60607fc0de971</anchor>
      <arglist>(const CoinArbitraryArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>aab1a5c73dc46b18879d3425d7bbcd4aa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rawSize</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a679f41ec96dad83194101985df7ad5e0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>switchedOn</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>aa0a773c5ef4ac1dfb04579fbe771a96c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>capacity</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>afd75961574a8ad08f350f59e56ef9152</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCapacity</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a509844bbb87ce1d9bd3513741ef43424</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>array</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>aa9e76fe3b7e99a817f8a7c7d85177436</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>ac017ea3a2896135f470f99a3c7df11ca</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>switchOff</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a1136a29eafce1964c0af914a8d3db95a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>switchOn</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a24dc2109a80c0199454e8722697198e3</anchor>
      <arglist>(int alignment=3)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPersistence</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a7aeb9bd236e1ec0e7587d8ce1fa96bfd</anchor>
      <arglist>(int flag, int currentLength)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a975810784075902568908ea2209b841e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a4a7d6ea7582a2dbd95704b56e1b92c1f</anchor>
      <arglist>(CoinArrayWithLength &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>extend</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a7c2cbe6f403d93cbab12e64de91b3f66</anchor>
      <arglist>(int newSize)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a869c59fc6c1785df968eef2c88dddbec</anchor>
      <arglist>(long sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>conditionalDelete</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a6e8ce9697169c04a58e16c63975cc74f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArrayWithLength</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a3a4cf561f2b22afbe7f073abebd2b86b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArrayWithLength</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a3110eed750331f96c09df05fab16a8ce</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArrayWithLength</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>ae1328d77d60c17a687e856f9587b1a00</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArrayWithLength</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a28fdd8521b744221d4a05a9bf9ee2c00</anchor>
      <arglist>(const CoinArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinArrayWithLength</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a84264973819ce76755403ff426ee5469</anchor>
      <arglist>(const CoinArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>aaca3630c33d7a39b612a8a49b6ab573d</anchor>
      <arglist>(const CoinArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copy</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a2dc5c0fdc8028885a8351ed06c9ac793</anchor>
      <arglist>(const CoinArrayWithLength &amp;rhs, int numberBytes=-1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>allocate</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a4392afdd6e4a7a0cb40c10e78d8083f9</anchor>
      <arglist>(const CoinArrayWithLength &amp;rhs, int numberBytes)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinArrayWithLength</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a8517b84946ad37582542c8055db2a51f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getArray</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a770b1253440c5bab53725d6eef17bedb</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reallyFreeArray</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a0a0ad6ae4ab944f174e13cab6a01e6a7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getCapacity</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a103d611a3a2305ea4893755e3b2a51ba</anchor>
      <arglist>(int numberBytes, int numberIfNeeded=-1)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>lengthInBytes_</name>
      <anchorfile>classCoinArbitraryArrayWithLength.html</anchorfile>
      <anchor>a348cd29c825889c0bfc129a836668d8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>array_</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a3f5ef91feb4e2d28d67037bd69a6c0c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>size_</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a9ade29b238280ae7f08ebcfa920774bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>offset_</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a800d087c728e975c29cd9d95a914d31f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>alignment_</name>
      <anchorfile>classCoinArrayWithLength.html</anchorfile>
      <anchor>a7a00b9585081899acf8a6a323d9365d0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinArrayWithLength</name>
    <filename>classCoinArrayWithLength.html</filename>
  </compound>
  <compound kind="class">
    <name>CoinBaseModel</name>
    <filename>classCoinBaseModel.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinBaseModel</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a94c175340e02f3fb96ad2f22fd4db00f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBaseModel</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>aa59f8ec96eb6e329ee1f87d413d81384</anchor>
      <arglist>(const CoinBaseModel &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinBaseModel &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>aa07fd47c9133f3fa84045eafe93e8ebd</anchor>
      <arglist>(const CoinBaseModel &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual CoinBaseModel *</type>
      <name>clone</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a8742e30ac5f7afc47b5e1ac282c98840</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinBaseModel</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a1add41faa656fe228fbf31a8c5c9239a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRows</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a4e1fed9668052324509395b45dea2172</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColumns</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>af41a464a36bc8197eb85fc169f08edfc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual CoinBigIndex</type>
      <name>numberElements</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a9d7e4b23c5b2a01df9679254271f4f89</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>objectiveOffset</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a60f44072929cb7542bfc1db463e38afd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjectiveOffset</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a0130340e89dd1f653c3aebc896e0f473</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>optimizationDirection</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a03a81fc4eeb081c817dd81060c516eb5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOptimizationDirection</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a9d696d3d56bb27d238de49546cbd005b</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>logLevel</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>aa75adbac9bce7d04750691934d1b376a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLogLevel</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a0e51de0ff044c6ca3464389fabb53a5a</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getProblemName</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>ac82f4e3d46ad4cbc515f1d6fd15a0b25</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setProblemName</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a2531864108e4a9e4963cf9408748946b</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setProblemName</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a348696b0c94d2cd8073ef414f1c3817b</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getRowBlock</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a23315e250844281c6ef56f3d36e0fb9d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowBlock</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a1ba112d4b0f6f415fde53a96859e8e55</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getColumnBlock</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>ab70b0d72d5caa4d6bf2a18382aa8cffa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnBlock</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a6fc6e87a71af8fb067bd8b6bcbda2341</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMessageHandler</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a19600a7d84ee579a303ff556ab3d116f</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberRows_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a8403d807345ab6ff93a0d294170a99be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a76f07a67ee7f2cd504d1f93cf6d9ed83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>optimizationDirection_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>af702c1b24eb8be4ebe8aa799614581b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>objectiveOffset_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>ad49bafdf3556529b849ef6f67e228bdd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>problemName_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>acbcc919adbdd6097acd55683ce02b89e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>rowBlockName_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a9ce5e6010bb7587bce163119a64404c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>columnBlockName_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>adf01fe85bf5a8ca304e5a07fb802c598</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessageHandler *</type>
      <name>handler_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a8ca583ee4a2d3a04e682b723156188ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessages</type>
      <name>messages_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>a746ea80055caf4276f24edcd37d6b58b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>logLevel_</name>
      <anchorfile>classCoinBaseModel.html</anchorfile>
      <anchor>aa566834589a7774770dce194ac5f1edc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinBigIndexArrayWithLength</name>
    <filename>classCoinBigIndexArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a5dde72441b7c496ac1fe20291bdf8ecf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex *</type>
      <name>array</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a1622c40fc3ab65382ec12e159a432c9a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a88362f613ff093f70a00328b92319ea6</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a353896fab233934c68a19f0b7dd83552</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBigIndexArrayWithLength</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a178582ae29d2390c50b38119f9fdfa8d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBigIndexArrayWithLength</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a6d1ccce3d3026a4eea9c366bb8eabbe9</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBigIndexArrayWithLength</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a5729094fe1c92ae5e7e64889d46aa539</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBigIndexArrayWithLength</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a334b3b8dd0d5961b1df103d6e7a634ca</anchor>
      <arglist>(const CoinBigIndexArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBigIndexArrayWithLength</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>a85d56b7624fcbad871f02589fb308a0e</anchor>
      <arglist>(const CoinBigIndexArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndexArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinBigIndexArrayWithLength.html</anchorfile>
      <anchor>ae73ca08f174d8e9b2243513846ec6f28</anchor>
      <arglist>(const CoinBigIndexArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinBuild</name>
    <filename>classCoinBuild.html</filename>
    <member kind="function">
      <type>void</type>
      <name>addRow</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a2f9c1358dc51563a5421ee868443c646</anchor>
      <arglist>(int numberInRow, const int *columns, const double *elements, double rowLower=-COIN_DBL_MAX, double rowUpper=COIN_DBL_MAX)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addColumn</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a3b192ee1e2309764196caca55f2a55b8</anchor>
      <arglist>(int numberInColumn, const int *rows, const double *elements, double columnLower=0.0, double columnUpper=COIN_DBL_MAX, double objectiveValue=0.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addCol</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a2bf4c30bf06f52808bc3d1b14f73285a</anchor>
      <arglist>(int numberInColumn, const int *rows, const double *elements, double columnLower=0.0, double columnUpper=COIN_DBL_MAX, double objectiveValue=0.0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRows</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>aa4da069ce43d6e586e821a0be3542744</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColumns</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a9fedbfe68718f8027a786baa149e6ead</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElements</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a5d4bc500f893902327634b7d97999555</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>row</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>ad123bdf15e5b452a66978c093f8a1d6f</anchor>
      <arglist>(int whichRow, double &amp;rowLower, double &amp;rowUpper, const int *&amp;indices, const double *&amp;elements) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>currentRow</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>aaa1e832355393e17105ddfbadc45bd08</anchor>
      <arglist>(double &amp;rowLower, double &amp;rowUpper, const int *&amp;indices, const double *&amp;elements) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCurrentRow</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a31c08d54ceecab19dea33bd7068e9807</anchor>
      <arglist>(int whichRow)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>currentRow</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a55e2bdd863f20ded0de4116fc34c0c4f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>column</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a08d3b96842d0a1e252069ff113d17d04</anchor>
      <arglist>(int whichColumn, double &amp;columnLower, double &amp;columnUpper, double &amp;objectiveValue, const int *&amp;indices, const double *&amp;elements) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>currentColumn</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a6349c6496dffbbe8ebb0909da27f18a4</anchor>
      <arglist>(double &amp;columnLower, double &amp;columnUpper, double &amp;objectiveValue, const int *&amp;indices, const double *&amp;elements) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCurrentColumn</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a03aa7aa2a35bba8b70067c376400b6c1</anchor>
      <arglist>(int whichColumn)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>currentColumn</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a35444b680b35aaee8af1d4e88c8af4eb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>type</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>aeda7fb4961b6ff6444de0f5fc3de5c15</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBuild</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a9111f2c5b7d5c3dcb73ec5b121e12675</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBuild</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>acac8ceafcf8021445ce8fc73c66ae71a</anchor>
      <arglist>(int type)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinBuild</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a48ce12d7036c7d067866f8e0f3972d64</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinBuild</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a6737be92815dc0a4348a664163eca983</anchor>
      <arglist>(const CoinBuild &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinBuild &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinBuild.html</anchorfile>
      <anchor>a0fda9f95a82abbf1d730f4fa0ccbbba4</anchor>
      <arglist>(const CoinBuild &amp;)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinDenseFactorization</name>
    <filename>classCoinDenseFactorization.html</filename>
    <base>CoinOtherFactorization</base>
    <member kind="function">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a48f88e1ba79bb83e034b88b9a79fa740</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfInitialize</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a094b2d6f3bbb2fa7dff321a02bcc750a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>aa196c9fedfaf44a35a164ccc69745c38</anchor>
      <arglist>(const CoinDenseFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDenseFactorization</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>ac4b6b63e5ce60fe1b5423bac54719042</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDenseFactorization</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>ab78a0718f5517ae2d257876eb00f5267</anchor>
      <arglist>(const CoinDenseFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinDenseFactorization</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a6a8498d9b5947cf890b4af8104565314</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseFactorization &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a7525f3ecbbe5e658f8c72cafca80d872</anchor>
      <arglist>(const CoinDenseFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinOtherFactorization *</type>
      <name>clone</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a9ec2c957c30880de8ff9161892d297e8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getAreas</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>ade1ba5350861043a62f9f4f42d43e9e1</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex maximumL, CoinBigIndex maximumU)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>preProcess</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>aae70366f8b633fb779315098dff0d705</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>factor</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a069ac33c80a1c0fc007f06e094778ae8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>postProcess</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a4a6c9d529f2f6f5382cb885feb8a5cea</anchor>
      <arglist>(const int *sequence, int *pivotVariable)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>makeNonSingular</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a56be7a711ec784703d0d1436f6d4f242</anchor>
      <arglist>(int *sequence, int numberColumns)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>numberElements</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a801118c5cf36541df99e2ba63fb902da</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>maximumCoefficient</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>aa2fa1902897221ff7ba02c90530a90fd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>replaceColumn</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>ae0f75de89bc478ef04a1f4318b3c00c0</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int pivotRow, double pivotCheck, bool checkBeforeModifying=false, double acceptablePivot=1.0e-8)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumnFT</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>ae2093b9b0e393854d37408f862ef4b22</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumn</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a5270c474612214e370d9bcd25e01b420</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateTwoColumnsFT</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>aba4c15bf92dea1b0a40096e1b5967a36</anchor>
      <arglist>(CoinIndexedVector *regionSparse1, CoinIndexedVector *regionSparse2, CoinIndexedVector *regionSparse3, bool noPermute=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumnTranspose</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>aeb68790f740c79ce0877f7aba6f6bd9b</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearArrays</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a2f2162b58a1afe7d7d473cec89cae05c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>indices</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>abac922a9d0873cfc26cd720f764b9c5e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>permute</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a69c6d5cf56052b9ecedab784aa6474b1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinOtherFactorization</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ae3ec6d1fd23ff9a4bed236f022d6ab00</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinOtherFactorization</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a64705d3670a678969fcb33dd459c43df</anchor>
      <arglist>(const CoinOtherFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinOtherFactorization</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ab622f0789be78f342abae03a06f28e38</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinOtherFactorization &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a0497893a0c2b4a1bdf55719ec676764a</anchor>
      <arglist>(const CoinOtherFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>status</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a543ecaa8f71a33a5668f9ec866c7e283</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStatus</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a1e1191df0d49bb2fddef8ac87567b9a6</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>pivots</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>afe428d9841e9e7fdaee7873f4354347a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPivots</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a7ac5798695dba93ebd0da7585cc33c76</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberRows</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a61139d9f5ab66edc3f83dcb855263683</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRows</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a6203979a9783a5b3922dcb81bd6a642b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColumns</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a269e046368513f6b4a66c984d42fb058</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberGoodColumns</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a91193070173928928a49ae4090b2ca22</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>relaxAccuracyCheck</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ad0bc173bce43bc4c59c7f835269ce506</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getAccuracyCheck</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a424bebd17a6e5d3f96bd353dcf135cb7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumPivots</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a33ebfd72f98398321e06bf8af25772ba</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>maximumPivots</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a74d78609bc087cace3db69d19d6be083</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>pivotTolerance</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a96d756cb058bd3ccb3a478eabf8c318e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pivotTolerance</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a1e86b493d82d4487f9c2d8163efe939c</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>zeroTolerance</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a10f5061da0c7f1f68fc028385bce9a4e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>zeroTolerance</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ae034269cce73054057e1c101bf1024c7</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>slackValue</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ac718b2c8d5ea7838ec75874a7b4c456d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>slackValue</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>af9107f0761ca410f5cf57421f76a05ba</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinFactorizationDouble *</type>
      <name>elements</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a6909a2c65ec8aa8d151fd802ea83adb8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>pivotRow</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a9b86af0738050fe1b0e205c7f25c389f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinFactorizationDouble *</type>
      <name>workArea</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a6b62dd19cdcc96f9a5e7eb94afbad467</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>intWorkArea</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a36bd86797523442419d99f3d268d1bca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>numberInRow</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a6d28eb08ffa7eed27736bed2723d6340</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>numberInColumn</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a4cc5986df3abbf193e59190a4628b75f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinBigIndex *</type>
      <name>starts</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a0364d75213949e4a1980d29fc24aea4f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>permuteBack</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ad9d1f46cf7a96897972f62be00d07fde</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>solveMode</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>aa71b54b3fd6515667e1d798fa9ccc2a9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSolveMode</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>aad3c62edc715f51de76ce0f143ab419e</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>wantsTableauColumn</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a7e4d79f56bb4710d53dace2dfa57ff21</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setUsefulInformation</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>aad4e9f874f51d2c24318b0eba22a9f9d</anchor>
      <arglist>(const int *info, int whereFrom)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>checkPivot</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>ad7e23d27e57d6f85647a6e5bf123d432</anchor>
      <arglist>(double saveFromU, double oldPivot) const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinDenseFactorizationUnitTest</name>
      <anchorfile>classCoinDenseFactorization.html</anchorfile>
      <anchor>a228c18fbaa7afe0ec6ae8548d11775b2</anchor>
      <arglist>(const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>pivotTolerance_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a206604f13b56937d78e368fc51772acb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>zeroTolerance_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a38dc91855c6e0f5a8c6cb5342035bad2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>slackValue_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a1e5781ae177a455e500a6397d72b938d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>relaxCheck_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ab7cb4f76ba17709a1c0c1c58db71b209</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>factorElements_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a3a8bfd230c39bad846d963c94199ef24</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberRows_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a4a2fef6fd733d5c4dee28c4c4e15b729</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a8f09ab7cc69850c8ef7dd6d67173c0bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberGoodU_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ae30e4a0e727896e099d68a633d84d234</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maximumPivots_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a6d73d51e2e0ad57f4d720e4c6a7fe016</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberPivots_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a23610b4846593f5c1b82d095b5367a13</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>status_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a9b39ef3d4e57dd581dac3b9ae31b1a8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maximumRows_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a62887b0655047c28c651d2e4c01b0b2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>maximumSpace_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a872571e728bb106a1a379561701b442a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>pivotRow_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>afbf648862cefe15112c9538c50d4561b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDouble *</type>
      <name>elements_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a876a838bc31887c691f928379e232f66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDouble *</type>
      <name>workArea_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a80d1933ed7c7804ce7a7bcd3af92c22c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>solveMode_</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>abc5f3c5663f0fffc92963e1cca533225</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinDenseVector</name>
    <filename>classCoinDenseVector.html</filename>
    <templarg>T</templarg>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a9474140c714c1122523c48214cafb41b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a54ee9129d43995c014ede851b3bdb4d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const T *</type>
      <name>getElements</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a7ec967a5db4840070d320603472c3339</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>getElements</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a93546d5ecc0a3bd23ab295c175171156</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a98d9914ce20aaba6fa65a3adc6543b6b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinDenseVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>af7cc725cd9b3c0706f0e616f6b8f4f9b</anchor>
      <arglist>(const CoinDenseVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type>T &amp;</type>
      <name>operator[]</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a969560c752198e5944a72edac580453f</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVector</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>ae33460289855c3c1c6599971a30bc4eb</anchor>
      <arglist>(int size, const T *elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setConstant</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>ad9952e9d4267d58cb61405030e931239</anchor>
      <arglist>(int size, T elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setElement</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a88eaf4043c58b0dbd0e527041329ecb2</anchor>
      <arglist>(int index, T element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a5c2e6a41e49c6d04a6375653215b97c5</anchor>
      <arglist>(int newSize, T fill=T())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>append</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a851f278a45a140f3d20362205d4b721e</anchor>
      <arglist>(const CoinDenseVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>oneNorm</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a8a2eb5bdd42e4867441a1f3f6ca9add1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>twoNorm</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a1879e574a89e6aba6cad57053b038bb4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>infNorm</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>ad3aa03c19fcbc1207ade7d3a81940800</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>sum</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a2b7c95576321396a974cb1331338cfa2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>scale</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>afe51762d4ff36492247e4c88740890e5</anchor>
      <arglist>(T factor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator+=</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a55b427816bc701cc76cae813123ff74a</anchor>
      <arglist>(T value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator-=</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a3e55d173a32835a749738da1497bbc82</anchor>
      <arglist>(T value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator*=</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a4e90251c1fc11ec0d19c1e5d2f8ea9d7</anchor>
      <arglist>(T value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator/=</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a18c560c27cb019f095165da819ae6f22</anchor>
      <arglist>(T value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDenseVector</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a5ad2b3ae069a473b02e2542b3a5f3c10</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDenseVector</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a98475f546616884a3d2ffdc0d6a67221</anchor>
      <arglist>(int size, const T *elems)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDenseVector</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a59c2154eaff28f86412473adb66922b7</anchor>
      <arglist>(int size, T element=T())</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDenseVector</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>a016187192b25862391d9bce3f660ae2d</anchor>
      <arglist>(const CoinDenseVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinDenseVector</name>
      <anchorfile>classCoinDenseVector.html</anchorfile>
      <anchor>acf064893116798247c317c920e69f46a</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinDoubleArrayWithLength</name>
    <filename>classCoinDoubleArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>a955b22eac62e618b802bad2483c5f262</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>array</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>ad5b76eff36f9455503f5748eb1e454cf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>a8575cf7916ebcdec1a2e18a24d58bf2e</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>a8509b811f00d3e048100d2bae6ac9b34</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDoubleArrayWithLength</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>a245095546900224249fadce14d2f76e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDoubleArrayWithLength</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>ac1d10e42f75a1f8d06b49e24753fd2f7</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDoubleArrayWithLength</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>a3435e59dc76a89db05934a8f189749a4</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDoubleArrayWithLength</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>adf931ef26914697907d02bf9091ebe0a</anchor>
      <arglist>(const CoinDoubleArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinDoubleArrayWithLength</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>af88c28edfcdb913dddb1cfdffed77979</anchor>
      <arglist>(const CoinDoubleArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinDoubleArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinDoubleArrayWithLength.html</anchorfile>
      <anchor>ac6a82503373e300ecd5271967a254501</anchor>
      <arglist>(const CoinDoubleArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinError</name>
    <filename>classCoinError.html</filename>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>message</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a180139b4cfc7cb3b0a2c40646037ccee</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>methodName</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a36ff36a0b67590732b360556ad9e101b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>className</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a71a92467e37b88e25e9f1fd44b157647</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>fileName</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>aae4fa989aa8fb2ad01b0c37768df6e79</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lineNumber</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a45014b7b61810d91ace51910ea321b49</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a727ed0a5f3d07c6ac2a07922f067d71c</anchor>
      <arglist>(bool doPrint=true) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinError</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a618012942a8bec993c6277d0717c430b</anchor>
      <arglist>(std::string message__, std::string methodName__, std::string className__, std::string fileName_=std::string(), int line=-1)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinError</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a78297887f73fe45bc3e3683f8f1b3664</anchor>
      <arglist>(const CoinError &amp;source)</arglist>
    </member>
    <member kind="function">
      <type>CoinError &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>ad610ec4663551780d3f50b34ea5a76b6</anchor>
      <arglist>(const CoinError &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinError</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a1868b3c5544d9dacabef4982675a3293</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static bool</type>
      <name>printErrors_</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>a6cb60db8382de5cc79c3f8e7efb253f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinErrorUnitTest</name>
      <anchorfile>classCoinError.html</anchorfile>
      <anchor>af145b267443a0cdbd5cfb2533c3a8da5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinExternalVectorFirstGreater_2</name>
    <filename>classCoinExternalVectorFirstGreater__2.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinExternalVectorFirstGreater__2.html</anchorfile>
      <anchor>a90dd3e2075cbfafd5cc093cd03ff80f6</anchor>
      <arglist>(const CoinPair&lt; S, T &gt; &amp;t1, const CoinPair&lt; S, T &gt; &amp;t2) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinExternalVectorFirstGreater_2</name>
      <anchorfile>classCoinExternalVectorFirstGreater__2.html</anchorfile>
      <anchor>a67d222fc581000d27e27d23b5276bbbd</anchor>
      <arglist>(const V *v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinExternalVectorFirstGreater_3</name>
    <filename>classCoinExternalVectorFirstGreater__3.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinExternalVectorFirstGreater__3.html</anchorfile>
      <anchor>ac3b2e2ca54ef5647e62d500758098a40</anchor>
      <arglist>(const CoinTriple&lt; S, T, U &gt; &amp;t1, const CoinTriple&lt; S, T, U &gt; &amp;t2) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinExternalVectorFirstGreater_3</name>
      <anchorfile>classCoinExternalVectorFirstGreater__3.html</anchorfile>
      <anchor>a0d841c883c6ed56c75261af1b8387f66</anchor>
      <arglist>(const V *v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinExternalVectorFirstLess_2</name>
    <filename>classCoinExternalVectorFirstLess__2.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinExternalVectorFirstLess__2.html</anchorfile>
      <anchor>a7e1899e52982a8f0fff104d992bb34e6</anchor>
      <arglist>(const CoinPair&lt; S, T &gt; &amp;t1, const CoinPair&lt; S, T &gt; &amp;t2) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinExternalVectorFirstLess_2</name>
      <anchorfile>classCoinExternalVectorFirstLess__2.html</anchorfile>
      <anchor>acb8480481e05176908f94e97fc8099d7</anchor>
      <arglist>(const V *v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinExternalVectorFirstLess_3</name>
    <filename>classCoinExternalVectorFirstLess__3.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinExternalVectorFirstLess__3.html</anchorfile>
      <anchor>a35ae28617b71e53255177dc05bb24b6b</anchor>
      <arglist>(const CoinTriple&lt; S, T, U &gt; &amp;t1, const CoinTriple&lt; S, T, U &gt; &amp;t2) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinExternalVectorFirstLess_3</name>
      <anchorfile>classCoinExternalVectorFirstLess__3.html</anchorfile>
      <anchor>ac278ca306ced222e50f9921a0225774a</anchor>
      <arglist>(const V *v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFactorization</name>
    <filename>classCoinFactorization.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinFactorization</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a0ed7e20aac9fe1b5ba13b95263d6bc85</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorization</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a753818e06d69c3a62d1efcdcb3757a81</anchor>
      <arglist>(const CoinFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinFactorization</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1e69c87bd761149a672808035a0def3a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>almostDestructor</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aad2ae76feb32af3442ac740c0622777a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>show_self</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a52c58de44bdd2a7243684a712fe0f852</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>saveFactorization</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ada4f53b086c4dc9b833164277ebfe5b8</anchor>
      <arglist>(const char *file) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>restoreFactorization</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a11cde537ce11961c5aae457a848cef6d</anchor>
      <arglist>(const char *file, bool factor=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sort</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>acca6448bfe60d43b3b35f9d6b726583a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinFactorization &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a59675f2990ac38f9a4aa198cb24b9d25</anchor>
      <arglist>(const CoinFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>factorize</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>adc51223a097da78d5aa6c38defca38fd</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, int rowIsBasic[], int columnIsBasic[], double areaFactor=0.0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>factorize</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8c0c80e2008712453689d7086ee2be26</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex numberElements, CoinBigIndex maximumL, CoinBigIndex maximumU, const int indicesRow[], const int indicesColumn[], const double elements[], int permutation[], double areaFactor=0.0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>factorizePart1</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aaeb092984c16111533455ff9ea7f5deb</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex estimateNumberElements, int *indicesRow[], int *indicesColumn[], CoinFactorizationDouble *elements[], double areaFactor=0.0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>factorizePart2</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af2e4f64a29bb4163cfd58e6927ec5a30</anchor>
      <arglist>(int permutation[], int exactNumberElements)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>conditionNumber</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad7da452c37f68fd6bc569e6364a2f443</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>status</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8deadf046c78f1c14b779d55920a9a8b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStatus</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa99a1b24d818afc20f4c050ddf5e6e08</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>pivots</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a99df7c29f011ffb5cebdc21a4375a862</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPivots</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9faf98e3f4733467585edc854f66b291</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>permute</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a47e5aa9602d7599e514271ee1c146125</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>pivotColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a46d0c359c78daa2641f961d9d272f4c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationDouble *</type>
      <name>pivotRegion</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3b4a87de4208811102c7367af030f33b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>permuteBack</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af098dd0baeb98cae2a0d2121f353fb3b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>lastRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a441dff8ab7086404db6cfb6adf51ff09</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>pivotColumnBack</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a92310f242e4c6dac4d875d7e6f0209e3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex *</type>
      <name>startRowL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac115d21cfc819210a41425d316c88499</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex *</type>
      <name>startColumnL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a022235e8776c5f22201940c4e0e1f851</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>indexColumnL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afb3fff4d1154738f7b8f66d4fdc563fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>indexRowL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab2e4d2d13b35bade1c5bf0f0b58b4b79</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationDouble *</type>
      <name>elementByRowL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a555374808e249764b31bdc1f9f3f2497</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRowsExtra</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a0cfe0b3a79a6f0f938135ccd7d1cf9a4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberRows</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad226e1220f3b17dfdf673a8d45ed0e26</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRows</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a15fe793e9c767497fbf530c8687847a3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad60fe90e0b0c46c5f79bb39fe781c0fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>baseL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a725489d548e0eed698e9796f9441b8dc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumRowsExtra</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a62fe74dd4f325ccc5c59187bf4b2a960</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColumns</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a235498bbca1e18acaa23bdf5ad94abb8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberElements</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a90a135db446e3f4d483b6faae48c81ba</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberForrestTomlin</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aaf3ee39bb65165e540ca36123f500437</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberGoodColumns</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a45f2348d21492419907f272b51fda4b7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>areaFactor</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab726bd9899f7226da9443e4ab849398c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>areaFactor</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa689c8bc44b47b7a0d2fab962f27aee5</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>adjustedAreaFactor</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a54f8c60c1715905055815bbc9209079b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>relaxAccuracyCheck</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a50b4e0e3c0ef3d542683a365835f4ba9</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getAccuracyCheck</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5c61c53216a9aad63672084c83183a6d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>messageLevel</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af43055c92a5d4c20ab86113a508f4267</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>messageLevel</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a234466d2eb881917a0e9af16c39f1c15</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumPivots</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9ed8e5093008a0365a264291d9ea9cb1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>maximumPivots</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1d324c036c2217ca35578445b46b0753</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>denseThreshold</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a89342dca1d06bc36c77bc5ad05da7c3d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDenseThreshold</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a6c48d4791a5ca5af83239769f50f93f9</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>pivotTolerance</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>acb232a3bc31bc241eb78da75df07aec2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pivotTolerance</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1fb53a524d21ea4268fd5074ca5b288a</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>zeroTolerance</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa40b4b412c1b2db85813f946b960d89a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>zeroTolerance</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a26e2a73ce4a6c80eba2a7242298bcf46</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>slackValue</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a7270cf5721c0892f67cb3bdb32a2e630</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>slackValue</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac253117939a280a03188d70a4593de62</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>maximumCoefficient</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5c6a4e5bd1fead58d5585b1ec27e0b15</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>forrestTomlin</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af5109f235703d1801335f20863411960</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setForrestTomlin</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a7acb8bded1ac76b4602d45a1383df116</anchor>
      <arglist>(bool value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>spaceForForrestTomlin</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aede1cd1cb650fa89a8de55adad4606ee</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberDense</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aab536e4fb6e10904124857392a160f30</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElementsU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad6b782bf49a1584dc233275cfa814d55</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberElementsU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5caa47315e58add4a5395214f2a863cd</anchor>
      <arglist>(CoinBigIndex value)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>lengthAreaU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a70aaadad3d6b387764cefccf1a042ab8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElementsL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a11dfdd78adb2d6c1975bbd4ea134c1d3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>lengthAreaL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9a11faa0a0a0d70c0af4fcfe1aa6c04b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElementsR</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a2cd1655e445a12409989bc7cda3fedf9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberCompressions</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a42b0a9daf473ea482e13c8f89e5a24d8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>numberInRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afd2169e77473b3a1f1d38ad4e946a4db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>numberInColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a413f1c2dfe681bff6a57520ff3dfeb6d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationDouble *</type>
      <name>elementU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3ad6de28c6d7d31a073b9af3710f1677</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>indexRowU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a118d0c4a22d52d81ab35e8dc1809939b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex *</type>
      <name>startColumnU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9d482a0b85f0b661486862e8fac06487</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumColumnsExtra</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a2fc5eb00d60fc01fe2c1481b2dd8759d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>biasLU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3f07c78e3db792ed90789a9d980ea2b4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBiasLU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aeb8e0382c9fca0a68f78ccb12be132e9</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>persistenceFlag</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4d8000228328723cb9fa4a055ad9f4e3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPersistenceFlag</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad1151d04b7ed3ec7e27f5419411da2a7</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>replaceColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a01a5b5be71dd511360e93fc6e658f2c0</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int pivotRow, double pivotCheck, bool checkBeforeModifying=false, double acceptablePivot=1.0e-8)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>replaceColumnU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1e194bd6590a03bd4aee9916b722ad25</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinBigIndex *deleted, int internalPivotRow)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>updateColumnFT</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afeded4a00b19460ec3e50d91230e88fd</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>updateColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4baf1eb460fb7a38bafe18159d16f5b7</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>updateTwoColumnsFT</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a562168a65e560065d53b6df7ded5b31b</anchor>
      <arglist>(CoinIndexedVector *regionSparse1, CoinIndexedVector *regionSparse2, CoinIndexedVector *regionSparse3, bool noPermuteRegion3=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>updateColumnTranspose</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9c7fdec6c3682c0ce782ea80c201d285</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>goSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac7d6ae39d324722225f0781d96dc191d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sparseThreshold</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4a4d9295a5e7dc969a4b890be965828d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sparseThreshold</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a842bf20f2b2d8d27941b02aa46edcdd3</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearArrays</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aeb256bf176ab22c36f2951fc794edf60</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>add</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a48d92cfbb02d1aa8cd162c9fdc6b76b4</anchor>
      <arglist>(CoinBigIndex numberElements, int indicesRow[], int indicesColumn[], double elements[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a66a96f8ab8c553a669db8d9aade0377c</anchor>
      <arglist>(CoinBigIndex numberElements, int indicesRow[], double elements[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa2db26b36c4a867111e90d389b6afc94</anchor>
      <arglist>(CoinBigIndex numberElements, int indicesColumn[], double elements[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>deleteColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a70cffda332b06bb7b0dbe707d336b3c6</anchor>
      <arglist>(int Row)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>deleteRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a79175ec489ee488f825b102317b98068</anchor>
      <arglist>(int Row)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>replaceRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8565849dbf936be7902965f0cf3bd179</anchor>
      <arglist>(int whichRow, int numberElements, const int indicesColumn[], const double elements[])</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>emptyRows</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a7eb089e88b849526bcf7a47937f9790c</anchor>
      <arglist>(int numberToEmpty, const int which[])</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a46719d6985c0d29ff356a700ee41e369</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>collectStatistics</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad1562a3d2cdd2db16037af57fce10767</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCollectStatistics</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1b3453b79d95881cd3c564faec26fc47</anchor>
      <arglist>(bool onOff) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab22a2916336d403e41cfc2c0ba5afd74</anchor>
      <arglist>(int type=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfInitialize</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af1e64c26100c8fc88c1b83d86d4ac785</anchor>
      <arglist>(int type)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a780cdf044d237a2234d6833de8312846</anchor>
      <arglist>(const CoinFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetStatistics</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>acb7a3f583e7df79614fb63242d57d7a8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinFactorizationUnitTest</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3c4ac880c6815eb1545f1c974c956316</anchor>
      <arglist>(const std::string &amp;mpsDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getAreas</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a93f7731c88191eddcab0bf790f8469f8</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex maximumL, CoinBigIndex maximumU)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>preProcess</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad673495de11503849338544d0c7cfc6e</anchor>
      <arglist>(int state, int possibleDuplicates=-1)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>factor</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afeb3da33f1bbf19790f5c984b6831a07</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>replaceColumnPFI</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af893cf673e6101d768efd8105dd4926c</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int pivotRow, double alpha)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>factorSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac9a04f5a09f5348b854238c7b2fbffb6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>factorSparseSmall</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a566ce1a28801d1fd3388303bf8437072</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>factorSparseLarge</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9bd7f1c635d5688561f15880b0f99570</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>factorDense</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a6a646bc2710b996988f4c3f2342ae044</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>pivotOneOtherRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab82eb7741527db873c5be74c47150d08</anchor>
      <arglist>(int pivotRow, int pivotColumn)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>pivotRowSingleton</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9d82ccb7c7221f0e367feb2993884eb9</anchor>
      <arglist>(int pivotRow, int pivotColumn)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>pivotColumnSingleton</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae506d0baa4116baba3c7d91d9e6c0b02</anchor>
      <arglist>(int pivotRow, int pivotColumn)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>getColumnSpace</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a89f6cce8d68917cf40500dc945bd4fef</anchor>
      <arglist>(int iColumn, int extraNeeded)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>reorderU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4787d1205a2818eb90abfa8054e1d942</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>getColumnSpaceIterateR</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5eb822b756307c538b5c026113e807ef</anchor>
      <arglist>(int iColumn, double value, int iRow)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>CoinBigIndex</type>
      <name>getColumnSpaceIterate</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a7d900d3a04a6afe737a1892e1a6e472f</anchor>
      <arglist>(int iColumn, double value, int iRow)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>getRowSpace</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afae83d80bd5c58d6caf420a214031a36</anchor>
      <arglist>(int iRow, int extraNeeded)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>getRowSpaceIterate</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a460149043b625e52bf60689f7aae6e39</anchor>
      <arglist>(int iRow, int extraNeeded)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>checkConsistency</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5ddb7373c5a193ba2298265d55710bbe</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>addLink</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>adc127d20d2079c3dbdf7769b596e69b0</anchor>
      <arglist>(int index, int count)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>deleteLink</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9ec14da936ea79296f55489b0842895c</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>separateLinks</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4aa3e720b7efde9d093c01632687a4d8</anchor>
      <arglist>(int count, bool rowsFirst)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>cleanup</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a112f9d3a7be65e310922d5d9f84642cd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab7455751b3f986a4e18543d846b5f3c9</anchor>
      <arglist>(CoinIndexedVector *region, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnLDensish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4e8fee119f0bbb58476fc58e7d9f92c3</anchor>
      <arglist>(CoinIndexedVector *region, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnLSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae4494dedb6831085b74bc72a8cb5b37f</anchor>
      <arglist>(CoinIndexedVector *region, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnLSparsish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5c1a39b3cea8f26973a7ad56039beb7a</anchor>
      <arglist>(CoinIndexedVector *region, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnR</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae65ef3099e829fc457df265cf422e746</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnRFT</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa5566ecf39a6401037c72b20e4c70c43</anchor>
      <arglist>(CoinIndexedVector *region, int *indexIn)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa2eee572b3c065a1647792b68dba173c</anchor>
      <arglist>(CoinIndexedVector *region, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnUSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae50facb5aa18b687ea513c339822f009</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnUSparsish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae1c10055191198ff7b56f3118fb76856</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int *indexIn) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>updateColumnUDensish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9f2c5ccebcb246a99d92eecb23d9e1ac</anchor>
      <arglist>(double *COIN_RESTRICT region, int *COIN_RESTRICT regionIndex) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateTwoColumnsUDensish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a444c7a37bac4e4b7d27657a4950f0c03</anchor>
      <arglist>(int &amp;numberNonZero1, double *COIN_RESTRICT region1, int *COIN_RESTRICT index1, int &amp;numberNonZero2, double *COIN_RESTRICT region2, int *COIN_RESTRICT index2) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnPFI</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4ec33184fb036d13a3ff61c718566c65</anchor>
      <arglist>(CoinIndexedVector *regionSparse) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>permuteBack</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a651b2b895538418a03a73a62d134b5b7</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *outVector) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposePFI</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af8b35b6543ed9fe234f62f0f8608ecdf</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeU</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a37c2dc5e580fb8558eb5d5190fa94696</anchor>
      <arglist>(CoinIndexedVector *region, int smallestIndex) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeUSparsish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a41bd906af0116f74c284a64156ae4e6b</anchor>
      <arglist>(CoinIndexedVector *region, int smallestIndex) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeUDensish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a2f435d16bc802651f7575053aac1793f</anchor>
      <arglist>(CoinIndexedVector *region, int smallestIndex) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeUSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae497fb41480f643e484e8b45837b1b51</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeUByColumn</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ada5bec546cd497fb0fe206d9a8fb022b</anchor>
      <arglist>(CoinIndexedVector *region, int smallestIndex) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeR</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a2c4cdb51e532fdfecd965d1a9d2ea4ff</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeRDensish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>accbf2bdc9375589169fe0a3089aa969a</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeRSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae94ef9c298db833d4f147f2643826fe1</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeL</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa76a2b41d5b4e263ff06e265a0c57c57</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeLDensish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a92142a9273e98c5815cbb2a2378687b8</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeLByRow</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a020f8148266ecba242548ea398ba1f69</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeLSparsish</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a93f27ce473ab517f9bbd0a3ffa3afb28</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateColumnTransposeLSparse</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a471cd6522f188a34ef768ff06f74e932</anchor>
      <arglist>(CoinIndexedVector *region) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>checkPivot</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4ab6a2105c43697272b398f3b348384f</anchor>
      <arglist>(double saveFromU, double oldPivot) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>pivot</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1f3fb309af65d015b512495064368e20</anchor>
      <arglist>(int pivotRow, int pivotColumn, CoinBigIndex pivotRowPosition, CoinBigIndex pivotColumnPosition, CoinFactorizationDouble work[], unsigned int workArea2[], int increment2, T markRow[], int largeInteger)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>pivotTolerance_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9f3dec9e96451e20f5a2959e8299ec90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>zeroTolerance_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a57a47177a652fd6f4348618b2097382d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>slackValue_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9d68ecbee962a07fbffe91e9ae12cc11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>areaFactor_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3056a160dde9ea5cc6024c7b1da4ae3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>relaxCheck_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>acc7b21028ae12dc9bbf71bd9c1435c7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberRows_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a842da4321ff591c284f6acd236e5df39</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberRowsExtra_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a26ff364ef39ae5e1ddffc07bebb2c1cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maximumRowsExtra_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a62ce659a49f8c7eba4abf3d6652b999c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a705e089b715696d57489a2f83d0361d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberColumnsExtra_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4d0e26c368f522c3945f9b24e0476081</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maximumColumnsExtra_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a18e50f358854edacf2e8e70c83bc6846</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberGoodU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a43073c719541a9b7b687ebe954d28fd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberGoodL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a0c774fa13c03b572a8635dd546377bcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maximumPivots_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a20203c59817e08e4c826c00962896a50</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberPivots_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac737e5c5178e0b3af3f83405958cb8b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>totalElements_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab553170e02fd06f1b92e8c08fec9dda7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>factorElements_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab2ba7282feaf8ad1d8650b73bdb2e376</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>pivotColumn_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a481a42d31dbddb295dd7c1407454323f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>permute_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8986f4eb3a82d95ca146085c628fe37a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>permuteBack_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>abd6ab23b35d058063aec83e679d03ceb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>pivotColumnBack_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a50db7f339448b32683bd0d8c8f0ae91e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>status_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a73a23a4dc778e59572f5d686cf30ebb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberTrials_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9d857f29e26684da7360603405d68c20</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndexArrayWithLength</type>
      <name>startRowU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a6c72b573b91541f9faf7836c0325df6e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>numberInRow_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aa9c3aad663572c78bb78d0baee9681d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>numberInColumn_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3658c8e16912d5539650b9ec0bef9dd3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>numberInColumnPlus_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac81dabe044d21bdac9c54264de68f304</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>firstCount_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab4d8ec2394c6c94da9c59e86699ad953</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>nextCount_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a7820e2ac0d7b5b66a646fd976fc79d89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>lastCount_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a861fe7f56d356b7331ff4b746ff787ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>nextColumn_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ac1d49cbe3a74dd9a2b3f4f89a8254846</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>lastColumn_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>adb72d75181f7ad218b1ac12c76910bb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>nextRow_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a99166b6a56baf81f44e2e832bee4193e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>lastRow_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae426c21a043b43e8ffb4c0b144e67ccc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>saveColumn_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a35ac52e196338f4b0eb7752588bb7b92</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>markRow_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5586bb0533b867d87eded8a99e839e7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>messageLevel_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a19800201e1b9e438dc9d145e2f60d246</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>biggerDimension_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a7c400a3d59d363ad7a616d11c72445bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>indexColumnU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aacd788f77a02ffc5bbfec7cfa949e243</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>pivotRowL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a11ae7c81645abe2cd234a26aad732656</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDoubleArrayWithLength</type>
      <name>pivotRegion_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a966cbf7e4aa8ff85a081f278b021544b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberSlacks_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>abf0d9b569537f258b54cd057d9fab499</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aed020eb3d825f99224036561a83972f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>maximumU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8fe1cbfeb93faa3877553bf75b984710</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>lengthU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8b83df7e9cad7c1fade0e2aa538139ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>lengthAreaU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae999424df6b2fbc81dd0a1d571cd6e1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDoubleArrayWithLength</type>
      <name>elementU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1eb6ad2161087c2a7ea7a7d68498ff07</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>indexRowU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a97a5acde9747e62a3d5b4e9ae2823d9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndexArrayWithLength</type>
      <name>startColumnU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a170e76c9a51c11d19b0d65ead9e20277</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndexArrayWithLength</type>
      <name>convertRowToColumnU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afe91398c31e8d6dd6d8e5b80065c41c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>numberL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9bc5ef668dd827676408742471317284</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>baseL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3af714b062fc4273c9d3b3f38d0b4e6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>lengthL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3f5285a84f92fbb727abb6af1e8cfc56</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>lengthAreaL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab502a008d13c868c7c30b89629eed986</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDoubleArrayWithLength</type>
      <name>elementL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae1ddcddf6849f2c003c74f16d94834d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>indexRowL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aeae2fef08e3ee1c8b77e0b4d3fcf8563</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndexArrayWithLength</type>
      <name>startColumnL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a33e910d44bee2834b373ec0fdf1aab2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>doForrestTomlin_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad1944b8cf1db0d0f5de6d120db5c9225</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a984a898b44d53de29995d96bd17b16b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>lengthR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>afdb605e32a57ae74e840897bd846ba17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>lengthAreaR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad09bc9051a1654ae46f073075db1004c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDouble *</type>
      <name>elementR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a3e91593ff2118668895b313c855647fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>indexRowR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af4096a6c07309ee0ccb7e3bd788b8a62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndexArrayWithLength</type>
      <name>startColumnR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a2c5545cc3f6cd7f0f6b0623495788724</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>denseArea_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a32c52b7888980dd7dfc753d28b449b97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>denseAreaAddress_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>abecee95fe9cb1e9fa11192c21877bda3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>densePermute_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8841d6e4b8635e91522322932bd9d2da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberDense_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a21aa7bd7a76e37c65069b3fb1fdef5d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>denseThreshold_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5bde2182b347dd32d049ba30c6b3afdb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDoubleArrayWithLength</type>
      <name>workArea_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae568462f980723288e50b86d350882f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinUnsignedIntArrayWithLength</type>
      <name>workArea2_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1bc4efe2839b322fe2471d9e475f1ebe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>numberCompressions_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a92ca3297a698d98aee7058147c3ca678</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>sparseThreshold_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>af46bede14e2237add7e7b99c370684eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>sparseThreshold2_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9ce98125731994116a749fc3af47ba8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndexArrayWithLength</type>
      <name>startRowL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ae91262a15e31997719744911fd57a605</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>indexColumnL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1d4c8024071c5182e00aa1f5358484a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFactorizationDoubleArrayWithLength</type>
      <name>elementByRowL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4f6a60a452caf90547ea3e2c7e894b23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinIntArrayWithLength</type>
      <name>sparse_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1f2d51724edeaf2c75249b671dde0ab6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>biasLU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab53b4ee5a4cace99a114c5511ac904e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>persistenceFlag_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>aea16d9400b31c0407cb3f230ad7e9bec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranCountInput_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a516df8eaaed152d0e7822bff390ef393</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranCountAfterL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a9fce2149b7b2c04947a29757b3f5a589</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranCountAfterR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a1793462460b86a1264cb57935acb061b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranCountAfterU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a5dd96ff88d16abb9a32591543880f5d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranCountInput_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a08239b4c7d97f2e199308f6f4acaeb75</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranCountAfterU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a188f49339d27d059ef7fbe5c2b6141bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranCountAfterR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ab4c7709d315d21be22d58842dec96ca6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranCountAfterL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a4b682d4856c93ad44f621b9f2ebf99b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberFtranCounts_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a6dca410b8388abbf51b3e60f966d4159</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberBtranCounts_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>ad4d9869eee3b8fb9869db8afb9826ff6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranAverageAfterL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a09b58e4b548a0f7dfeb19d731c52d5c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranAverageAfterR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a0cdd8ef0243949aae7a1ceca25a2422a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ftranAverageAfterU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a8efecff58ff2d4a8ce5475830e2a07b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranAverageAfterU_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a88197885ed12da73611df82791a8d06f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranAverageAfterR_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a76ef8579deee6285671bdea29afa1fd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>btranAverageAfterL_</name>
      <anchorfile>classCoinFactorization.html</anchorfile>
      <anchor>a6a26269b53b04567e347ea4747328acf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFactorizationDoubleArrayWithLength</name>
    <filename>classCoinFactorizationDoubleArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a75e8ccc9786000ac4591da51b7e83e75</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationDouble *</type>
      <name>array</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a029c081d0a98d28159b6c861b82d0eda</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>ad0934e3007c35b93a4d7b3e43db68cfe</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationDouble *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a07a9be5ccace9b4ccd536b667a2e86a6</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a21f583967a5b362e080d84e899601271</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a6173335efa1dc75f5a39fd6e1fa56b2a</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a85111213d40051e61b155d4081ddb858</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a2be21d3cdd246c8a57678d6118bb8afe</anchor>
      <arglist>(const CoinFactorizationDoubleArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>ab4efb5af8abafcf2c07c947d250b4f50</anchor>
      <arglist>(const CoinFactorizationDoubleArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationDoubleArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinFactorizationDoubleArrayWithLength.html</anchorfile>
      <anchor>a998ad72bfd66da1db28a560d075b720e</anchor>
      <arglist>(const CoinFactorizationDoubleArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFactorizationLongDoubleArrayWithLength</name>
    <filename>classCoinFactorizationLongDoubleArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>ab2459f020fa88ed375a46b509d7b721b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>long double *</type>
      <name>array</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>a1c6124860777f993c7c7790e1d0404fb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>a068303c2978a8a3ebe54601d10961f4b</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>long double *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>ab11b6c363cb3cdaa873db963da9ab16b</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationLongDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>ae3ddaf829c441f63fa28bca4a8441361</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationLongDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>ad9ac7d003893320e10caacec5a8d8bbb</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationLongDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>aca93c762e678dff6587b5ded646a1620</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationLongDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>a2d72acd9619f31770fc123c8332fd82f</anchor>
      <arglist>(const CoinFactorizationLongDoubleArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFactorizationLongDoubleArrayWithLength</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>a3b510b338e10f2ff486097a0fdab42c7</anchor>
      <arglist>(const CoinFactorizationLongDoubleArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinFactorizationLongDoubleArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinFactorizationLongDoubleArrayWithLength.html</anchorfile>
      <anchor>a4462a756b6d22c19dec232ff5219924b</anchor>
      <arglist>(const CoinFactorizationLongDoubleArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFileInput</name>
    <filename>classCoinFileInput.html</filename>
    <base>CoinFileIOBase</base>
    <member kind="function">
      <type></type>
      <name>CoinFileInput</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>a5496f8103557774843b3605372507aab</anchor>
      <arglist>(const std::string &amp;fileName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinFileInput</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>a03d2c05fb9feccc43f75f1df89cbf75b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>read</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>ae1c8307f888cb9eb92d6b15ebb2e55b6</anchor>
      <arglist>(void *buffer, int size)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual char *</type>
      <name>gets</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>a580f583416242cf637ccbdcba5f43c7c</anchor>
      <arglist>(char *buffer, int size)=0</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFileIOBase</name>
      <anchorfile>classCoinFileIOBase.html</anchorfile>
      <anchor>a8dcbcfa6fe91f5d444a7ccedc934f4f2</anchor>
      <arglist>(const std::string &amp;fileName)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinFileIOBase</name>
      <anchorfile>classCoinFileIOBase.html</anchorfile>
      <anchor>a93c6dcdf7794e97be89e948d0a9250b2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getFileName</name>
      <anchorfile>classCoinFileIOBase.html</anchorfile>
      <anchor>a22af85eb8a7b2450611ffc6e7ffa748e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getReadType</name>
      <anchorfile>classCoinFileIOBase.html</anchorfile>
      <anchor>a8467c132eaa01c9356723e791e40a3c2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>haveGzipSupport</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>adb317e251f626865866f79302ce115cf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>haveBzip2Support</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>a95096a7cea3d1e030c6ff7aac42c8951</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static CoinFileInput *</type>
      <name>create</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>acb98f0bd729938d27538868c6a909b0d</anchor>
      <arglist>(const std::string &amp;fileName)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fileAbsPath</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>ad071472d5188169d786852db94966be7</anchor>
      <arglist>(const std::string &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fileCoinReadable</name>
      <anchorfile>classCoinFileInput.html</anchorfile>
      <anchor>a0069ea6abd83427fc110c414e18f4142</anchor>
      <arglist>(std::string &amp;name, const std::string &amp;dfltPrefix=std::string(&quot;&quot;))</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>readType_</name>
      <anchorfile>classCoinFileIOBase.html</anchorfile>
      <anchor>aa6694606f99d9295375569d4b3faf523</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFileIOBase</name>
    <filename>classCoinFileIOBase.html</filename>
  </compound>
  <compound kind="class">
    <name>CoinFileOutput</name>
    <filename>classCoinFileOutput.html</filename>
    <base>CoinFileIOBase</base>
    <member kind="enumeration">
      <type></type>
      <name>Compression</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a3ff0f28bcebcc957d282f4bc59c2d921</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COMPRESS_NONE</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a3ff0f28bcebcc957d282f4bc59c2d921a7a0c336b28a96471f82bba1715c2aebe</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COMPRESS_GZIP</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a3ff0f28bcebcc957d282f4bc59c2d921ad1b06338b9c433bad2c00258656bf1a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>COMPRESS_BZIP2</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a3ff0f28bcebcc957d282f4bc59c2d921a983bce457941f5b5dc3230db5dedd997</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinFileOutput</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>aca0278930f07fe870075a3ef10d01e77</anchor>
      <arglist>(const std::string &amp;fileName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinFileOutput</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>acaaf0fefb7fea304875cc57c03bcb748</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>write</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a8534cfa07592514329ea6cf283f61680</anchor>
      <arglist>(const void *buffer, int size)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>puts</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a6faf5213dcdf1a96abe0465a74bfe9a1</anchor>
      <arglist>(const char *s)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>puts</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>a9ab4c68decfe3814cf876fcf55f70c04</anchor>
      <arglist>(const std::string &amp;s)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>compressionSupported</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>aba6e85bd525214863e5669d2a0bd2cba</anchor>
      <arglist>(Compression compression)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static CoinFileOutput *</type>
      <name>create</name>
      <anchorfile>classCoinFileOutput.html</anchorfile>
      <anchor>acb568141c5207764f2b016bbbdd87cb3</anchor>
      <arglist>(const std::string &amp;fileName, Compression compression)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstAbsGreater_2</name>
    <filename>classCoinFirstAbsGreater__2.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstAbsGreater__2.html</anchorfile>
      <anchor>a0b0ccf9cdf7f1ca2a7a181d0231899af</anchor>
      <arglist>(CoinPair&lt; S, T &gt; t1, CoinPair&lt; S, T &gt; t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstAbsGreater_3</name>
    <filename>classCoinFirstAbsGreater__3.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstAbsGreater__3.html</anchorfile>
      <anchor>a83378601178137796254c0a9921edf8e</anchor>
      <arglist>(const CoinTriple&lt; S, T, U &gt; &amp;t1, const CoinTriple&lt; S, T, U &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstAbsLess_2</name>
    <filename>classCoinFirstAbsLess__2.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstAbsLess__2.html</anchorfile>
      <anchor>a071a3d70b172dfe874ea04fb21db8ebf</anchor>
      <arglist>(const CoinPair&lt; S, T &gt; &amp;t1, const CoinPair&lt; S, T &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstAbsLess_3</name>
    <filename>classCoinFirstAbsLess__3.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstAbsLess__3.html</anchorfile>
      <anchor>a65f85e27c5174f6b3483c3a2b9495340</anchor>
      <arglist>(const CoinTriple&lt; S, T, U &gt; &amp;t1, const CoinTriple&lt; S, T, U &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstGreater_2</name>
    <filename>classCoinFirstGreater__2.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstGreater__2.html</anchorfile>
      <anchor>a462d3ba02d9458ec95b2efc7e6b4c2e9</anchor>
      <arglist>(const CoinPair&lt; S, T &gt; &amp;t1, const CoinPair&lt; S, T &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstGreater_3</name>
    <filename>classCoinFirstGreater__3.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstGreater__3.html</anchorfile>
      <anchor>a1b2db56e60547a434552c156299434e0</anchor>
      <arglist>(const CoinTriple&lt; S, T, U &gt; &amp;t1, const CoinTriple&lt; S, T, U &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstLess_2</name>
    <filename>classCoinFirstLess__2.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstLess__2.html</anchorfile>
      <anchor>ae00366bbaef2e93e8cb3c9abe1b4c4b9</anchor>
      <arglist>(const CoinPair&lt; S, T &gt; &amp;t1, const CoinPair&lt; S, T &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinFirstLess_3</name>
    <filename>classCoinFirstLess__3.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinFirstLess__3.html</anchorfile>
      <anchor>ad044a68852aa3cb404e098e4b9c77bba</anchor>
      <arglist>(const CoinTriple&lt; S, T, U &gt; &amp;t1, const CoinTriple&lt; S, T, U &gt; &amp;t2) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinIndexedVector</name>
    <filename>classCoinIndexedVector.html</filename>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a4aa5b4ec7c7bd15fc85a9802b95dea84</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a7b9ffea80ab4c58ae676babc61b1f011</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8333eeed72e9acad4793b2d2ef395b7b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>denseVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8752a56d07571d4f3126371dc48b1eb5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDenseVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8768238d155519533c86e5e0ed54aa11</anchor>
      <arglist>(double *array)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIndexVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a3eac8d1f43e6be27c457be45b42aa104</anchor>
      <arglist>(int *array)</arglist>
    </member>
    <member kind="function">
      <type>double &amp;</type>
      <name>operator[]</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aa639fb75dd78f56e3ce9f96ec3c9f14a</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumElements</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a08d821525f43fd4f52ab6ddeaea60362</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ad978f4735bd4f250e3fb706aa30aea03</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>empty</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a4aabbdcf8590123b06b952ab57bb3c1b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinIndexedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aadcb6c9e46770125f8a874407832fbd0</anchor>
      <arglist>(const CoinIndexedVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinIndexedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a2b8d2b1e74ed9b26684caa9d2725bbeb</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copy</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>add77edb79b607b220e5f58d0973a8cf9</anchor>
      <arglist>(const CoinIndexedVector &amp;rhs, double multiplier=1.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>borrowVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a89dc48a5b0b98a667b318a72081d2ac4</anchor>
      <arglist>(int size, int numberIndices, int *inds, double *elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>returnVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a086ef87987d449f0f8bc447bef2fe50d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a197a94e7991bd8f5c0a1038a8718a43a</anchor>
      <arglist>(int numberIndices, const int *inds, const double *elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aa056264aa77b0978e58659ef842f3405</anchor>
      <arglist>(int size, int numberIndices, const int *inds, const double *elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setConstant</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aadf85dfcbe57ab5c86570c293ff52ba5</anchor>
      <arglist>(int size, const int *inds, double elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFull</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a72870c0ee047fd1ca05db3e7963a4184</anchor>
      <arglist>(int size, const double *elems)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setElement</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a45f9f8fa6fa7446dc47d8b412d7902cb</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ade7950ab8ba3add50b09ee3b45ecee61</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>quickInsert</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a7211870d1421fd345d64704cd144b322</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ab47a3358e14c363b9659903767d5e0d9</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>quickAdd</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ade038e06d936449bff697729d27e45fd</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>quickAddNonZero</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>afe894407cc3b64f046eac99dfdf1e344</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>zero</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a5fb39252c1d2b896d377595b58913b18</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>clean</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a1f3941ca044382afebfa6ce5b69674c8</anchor>
      <arglist>(double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>cleanAndPack</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aae983eae398bf15004f174f12d979bf6</anchor>
      <arglist>(double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>cleanAndPackSafe</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a3825637de6419eb1eee22d44a77e0b13</anchor>
      <arglist>(double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPacked</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a25e2ef988ea5c58409666b8bb2304e62</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkClear</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a7e39bc282b79cc57e9994afd13595364</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkClean</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a6c16e6a05aa60ce7613469768b6ea6d4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scan</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a9591ea3c0aab830b3728b3d04d83c9ac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scan</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a72c144dee2d5fccc932e174f407d1df4</anchor>
      <arglist>(int start, int end)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scan</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>af34740db826cbc2c1e85c758eed2f2f2</anchor>
      <arglist>(double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scan</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a603839ba8d7d575e63046478a4d9656e</anchor>
      <arglist>(int start, int end, double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scanAndPack</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a9f2179ab5a8ead34abc80135afe03fed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scanAndPack</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a89defe756c558f60d4faf06378a09f2b</anchor>
      <arglist>(int start, int end)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scanAndPack</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a914a20dc8a9692249930d43aa7a76896</anchor>
      <arglist>(double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scanAndPack</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a22412544badfc1b3192175286d44fa04</anchor>
      <arglist>(int start, int end, double tolerance)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createPacked</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a3f7caf8939718c8a32cd937f52826b4e</anchor>
      <arglist>(int number, const int *indices, const double *elements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createUnpacked</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ae19b6ae6b78d522c1f39c1aa8e15bb9c</anchor>
      <arglist>(int number, const int *indices, const double *elements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createOneUnpackedElement</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>af675ec9a521ef61a2e33c5526c391c5b</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>expand</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8e4d1adda13c8a6165233cbda8d28386</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>append</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a1b97db6f995087b04079ca07edddfb97</anchor>
      <arglist>(const CoinPackedVectorBase &amp;caboose)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>append</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8645386ea5a9051d6d94f5663691d277</anchor>
      <arglist>(const CoinIndexedVector &amp;caboose)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>append</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a4e1e58b7dbcf2f6d8193005ff88f53df</anchor>
      <arglist>(CoinIndexedVector &amp;other, int adjustIndex, bool zapElements=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ad5849a7ed1fb11ea16970940dfadd6fe</anchor>
      <arglist>(int i, int j)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>truncate</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>acd6e6c3d9a643da6107a7f462a0cb9d2</anchor>
      <arglist>(int newSize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aa7cd851ffa3303d6fc01f9e9dcbdda1a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator+=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a896c98f2c5cdc0c9861e2b71ad0c355d</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator-=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aa0f8b92d6bb2343a4fc3034c7f3aa082</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator*=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a996938b91cd3fac82a30818bc12b6d05</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator/=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>af5db31e4351df734e73638fd96a5239f</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a957ffe5b8861955326143da74ab187a8</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a3f9c94cf1fc3a3d635309edea904166b</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a30e14050e92d0c7f8e47f2e951011283</anchor>
      <arglist>(const CoinIndexedVector &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ab8b2b251ac47ddc573f4128304a7f304</anchor>
      <arglist>(const CoinIndexedVector &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isApproximatelyEqual</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8a3ac5cc2fea1900cb920dabffa4fbb8</anchor>
      <arglist>(const CoinIndexedVector &amp;rhs, double tolerance=1.0e-8) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMaxIndex</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ab017889f654beab76b276673e814d5f3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMinIndex</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a00065ed6544aba3e174cf3cbb5bf6ca8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sort</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a4c233ce1d8db5ca92e5340e03aa2ba46</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortIncrIndex</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a4a1600c4f2eed8d58ff636d1208c81ce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortDecrIndex</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ac537fcdd3ce920747279ca77b6d4984c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortIncrElement</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a5c352ccdb786e972612ec5cda2b513b0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortDecrElement</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a1923f13f2bd42f6c5365ec6bc3db2d43</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortPacked</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a24f6216a2983b1f46e2683777ff2f27b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinIndexedVector</type>
      <name>operator+</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a6110b3572e20e45b632f314e2cd59963</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinIndexedVector</type>
      <name>operator-</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a164d68295e9ee2bf5dd24acdd540334f</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinIndexedVector</type>
      <name>operator*</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ac68ae0bbb5b1c12ea3877dfff0a9a121</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>CoinIndexedVector</type>
      <name>operator/</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ac03b7402d06bea9fcebc164a6c301c13</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator+=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a3e9f3dc9324d413405f49bbb4d8668bf</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator-=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a9c86dcf0e2953fc0e7a9f3be563dd39d</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator*=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a6934cf57236b5e03489114f0884b7b5e</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator/=</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>adc5932b1443bace375eb9421cbf58afc</anchor>
      <arglist>(const CoinIndexedVector &amp;op2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserve</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ab91a3591d027263dc558adb6f02d0f50</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>capacity</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ad059a56552923ab05976a7c7bbc77a5e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCapacity</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ae25d4bc5bcd5ffcf205b3009db414294</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPackedMode</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a2d286bd4b699c19d17e5bca4bbd8dfbd</anchor>
      <arglist>(bool yesNo)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>packedMode</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a5e3ba0619f28e3d7ce90b8ad1ae43429</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a8c51a43228c0a9deceb2000587b09e47</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a2871912afb4097dc86ccd3dcb2c2a957</anchor>
      <arglist>(int size, const int *inds, const double *elems)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ae19c26304e44b45c13665e1986400c2a</anchor>
      <arglist>(int size, const int *inds, double element)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a10e4d92bca3c0efe82be01dfb5adba56</anchor>
      <arglist>(int size, const double *elements)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a90879e478ccfe2761c23679b609c2c21</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a14c42d410505589f31d4bb70e8968632</anchor>
      <arglist>(const CoinIndexedVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ace41b102e23a13d883255c9f8ce4b98c</anchor>
      <arglist>(const CoinIndexedVector *)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a77fb3194a6539e28e00683900334c3d9</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinIndexedVector</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a99722ad398b1a59947f2c97ff253bf02</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>indices_</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a069d37d81aceeb13d73b605e83e952f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>elements_</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a47fb35ac2734855021dbcbf2791c23b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>nElements_</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ae6000c7d6f12827ef71d535220b9cb96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>capacity_</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>ab2f195183839aed2c3012a93e60fe6cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>offset_</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>aa79e65490f2b799d311282611f2f9d28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>packedMode_</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a4b0c42d575f5e332b6895ce07f05913f</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinIndexedVectorUnitTest</name>
      <anchorfile>classCoinIndexedVector.html</anchorfile>
      <anchor>a2c6e6fb4f5b284d426977df27415503c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinIntArrayWithLength</name>
    <filename>classCoinIntArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>a1bf2c42531ebfb1aa9dec71c46f77597</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>array</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>ad32af7b0fbbc4850caf0b5a52bda7286</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>a98a366b25d055d3b9b12fe62051e94ec</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>a5b56760c5e9b94f10835ba81e4a14b00</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIntArrayWithLength</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>af66764c73b97d8648944b87f13c3a28a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIntArrayWithLength</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>a8df8eca61084b114ced10a956fceb0c4</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIntArrayWithLength</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>aee474c22eabd13f44c67a516ef8e7186</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIntArrayWithLength</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>a5b16ad8b6d9611a49eddebf39af88727</anchor>
      <arglist>(const CoinIntArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinIntArrayWithLength</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>a12238fad31a05160bccc29b40984c1d8</anchor>
      <arglist>(const CoinIntArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinIntArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinIntArrayWithLength.html</anchorfile>
      <anchor>ab7a7ed51b2c9abc3ff4d22bd195ac86f</anchor>
      <arglist>(const CoinIntArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinLpIO</name>
    <filename>classCoinLpIO.html</filename>
    <class kind="struct">CoinLpIO::CoinHashLink</class>
    <member kind="function">
      <type>void</type>
      <name>convertBoundToSense</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac25396f743b8fe46e79bd4aca55b0511</anchor>
      <arglist>(const double lower, const double upper, char &amp;sense, double &amp;right, double &amp;range) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinLpIO</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aecd2ff02f0bcae477f505b8981f0f1f4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>afb8586195f2d31618c6af1f902a4fb1d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a274ee6f2d5ff9f2fd6e9f89b7d906962</anchor>
      <arglist>(const CoinLpIO &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinLpIO &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6a2ee5ca2e31394bbc82eb9fb9c68264</anchor>
      <arglist>(const CoinLpIO &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinLpIO</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8e8153da09eb6c5fab54f30a6638c0e8</anchor>
      <arglist>(const CoinLpIO &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinLpIO</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6258f817026291dbe119f82b4c589d60</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freePreviousNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ada9cfd7b5027775fd8be79edfcd08702</anchor>
      <arglist>(const int section)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>freeAll</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ae15261723a8c0bdf1f7ebb9bd7a65d70</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getProblemName</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ab295044b5b4e924742bd9b6f11d3a4b7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setProblemName</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a852d47ff81b4929006f0865a262daa0e</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumCols</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a35ff8888aa7d4ce5dcfeac2e2ce76dfd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumRows</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ae552017a9331789e41673c8ce1e125b5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a09fd1bf11981af67d68d1315db130fbd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColLower</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aae51e4e4a844ada1d943568d3365bcc2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColUpper</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8c8b25c927486b3450bf7b5ea549134a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowLower</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac1636ffca9089c91eaa2400e9e54331d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ad27e74c3747a5f75dd0a75562cf65b3c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRowSense</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a83840370907f161247ed930fa1a0fcc2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a118eaef348e37b882fa0de1f9d8d1f8c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowRange</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a249123839a6b014ed039abd98b258d1f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int</type>
      <name>getNumObjectives</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6545b49ac87b61231552987ffe553e92</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a70a4f6a51951f714e7f903dfa7e641a8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a5617f95efc75bb5a0fdaa700f1a01ead</anchor>
      <arglist>(int j) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af66aee3d37ef8c4c69e4d1352695e9f8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aac82e06ad88062f4abc34a05ad5e54bb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getObjName</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af4117a8e997d846fd766f2c034155da2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getObjName</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a3d4e9df0610c7cd0304c2637bf0a7f93</anchor>
      <arglist>(int j) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getPreviousRowNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a1b3a7253ca568d48d211dc22480d097d</anchor>
      <arglist>(char const *const *prev, int *card_prev) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getPreviousColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a9f38c019c384c832d1cd755b7b54238a</anchor>
      <arglist>(char const *const *prev, int *card_prev) const </arglist>
    </member>
    <member kind="function">
      <type>char const *const *</type>
      <name>getRowNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aa562e78ccf103362365138aeecaafcd1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>char const *const *</type>
      <name>getColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a235f546a5e322f88d69ec20dee8678a2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowName</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a7c3fc3e63c2e4baa2500e7deb106668f</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnName</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a7e36d63e96701c781972c3da11ae13c6</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rowIndex</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a43e21820f9219b4afb46d08920e88a1b</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>columnIndex</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8f3d1d06c48dfb85d62d87c8a036f11b</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>objectiveOffset</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>add9acc7b9b6e6f909bb52568d06d7be4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>objectiveOffset</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a388a473049f3deea9e1db18891548244</anchor>
      <arglist>(int j) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjectiveOffset</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a815643db58bc0d999d3e9b4eb143b305</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjectiveOffset</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a41e68b3cb793b1bd31211b65a85afb58</anchor>
      <arglist>(double value, int j)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteger</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a868cd58ee0eb6f922d9124fcedf0ff4e</anchor>
      <arglist>(int columnNumber) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>integerColumns</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ae60fafae15bcc52bf2afc4336b44669f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getInfinity</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a49b144f74c5bd75fbbdb2d4006c78ae1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInfinity</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a136471274550c42b7cb20df69ebb7467</anchor>
      <arglist>(const double)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getEpsilon</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>addfde12d2bf011ef7fa5fbcfed323804</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setEpsilon</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a424cac7da9ab820fe847cec1d9e302a5</anchor>
      <arglist>(const double)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumberAcross</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a678b39b929c0cd6eb241cd3eb4f4835a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberAcross</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>afb04cad58c94e75b180f46e01590fd43</anchor>
      <arglist>(const int)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDecimals</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6cebb0cd107e98cf3c8c2bf692071cac</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDecimals</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a5a079e7853710cba7ed107e62b4a2cc1</anchor>
      <arglist>(const int)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLpDataWithoutRowAndColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a568473b714e3f9ea3b2d1cabaf796b3e</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double *collb, const double *colub, const double *obj_coeff, const char *integrality, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLpDataWithoutRowAndColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aebec770798b1f9d189f95059e698ea4e</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double *collb, const double *colub, const double *obj_coeff[MAX_OBJECTIVES], int num_objectives, const char *integrality, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>is_invalid_name</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aab40073e6f46f363608f43e3b93fba93</anchor>
      <arglist>(const char *buff, const bool ranged) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>are_invalid_names</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a9c3d770487cb117cf95367f0237b25f1</anchor>
      <arglist>(char const *const *vnames, const int card_vnames, const bool check_ranged) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDefaultRowNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>abdc7e5fdbd0a9a5d9e1fe1850ace3bb2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDefaultColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a27395b950e59b2025b56e83508f97dcc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLpDataRowAndColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ad60d0183cb320e1d3646459b581f9591</anchor>
      <arglist>(char const *const *const rownames, char const *const *const colnames)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a436c6a9693168d950dd61c856d771bb5</anchor>
      <arglist>(const char *filename, const double epsilon, const int numberAcross, const int decimals, const bool useRowNames=true)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a2e13f16d9fed8678b8211817ba434967</anchor>
      <arglist>(FILE *fp, const double epsilon, const int numberAcross, const int decimals, const bool useRowNames=true)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a1ee383e0ba692ed88c082743a2908e95</anchor>
      <arglist>(const char *filename, const bool useRowNames=true)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6298953227e08352b3d087dc85e4e221</anchor>
      <arglist>(FILE *fp, const bool useRowNames=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a5dcdf10ec0f147edb08bc04613419e94</anchor>
      <arglist>(const char *filename, const double epsilon)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af854b4cc3765623279b476bd76f2dab5</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a54c1870363484faa918036668026f1be</anchor>
      <arglist>(FILE *fp, const double epsilon)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>readLp</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a225d39be8a89b6adcf02b34e39dfb511</anchor>
      <arglist>(FILE *fp)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a37082e11b509c8546bce51d75382771b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadSOS</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6fda1f919d8f579cc4bcbef6631ea0f0</anchor>
      <arglist>(int numberSets, const CoinSet *sets)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadSOS</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac4b9bead07e73de65ef906cffec73693</anchor>
      <arglist>(int numberSets, const CoinSet **sets)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberSets</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>abff49811c0558a660d1d229b44497a96</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinSet **</type>
      <name>setInformation</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a4d224e50b57c51c54d1a206ab602fa14</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>passInMessageHandler</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ad69ca594c49dde9aeb41a297817b6f9e</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>newLanguage</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6cf4bdce3e06fc576f6c31b7666e1d5b</anchor>
      <arglist>(CoinMessages::Language language)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLanguage</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac8dc8783b505ae5e7a4f44c891496cf9</anchor>
      <arglist>(CoinMessages::Language language)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler *</type>
      <name>messageHandler</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aec7072feb20af7cc59efc3f7318c1e2d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinMessages</type>
      <name>messages</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ab18222dc4ee49f867d71d3025ffb2a6a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinMessages *</type>
      <name>messagesPointer</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a5b14d2eb2a4bb0027b6146db4bbbd6d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>startHash</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a3b7decb41e9a42d1d9d7d44b21fae03a</anchor>
      <arglist>(char const *const *const names, const COINColumnIndex number, int section)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>stopHash</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af3a3006b41a85ec0e95cc57bf1b92bc9</anchor>
      <arglist>(int section)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>COINColumnIndex</type>
      <name>findHash</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8c6f0e8a615e1a570a70ef26229b603d</anchor>
      <arglist>(const char *name, int section) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>insertHash</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a589d12dee4d5464085f9f099cc19d0fa</anchor>
      <arglist>(const char *thisName, int section)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>out_coeff</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a68117e2b48075feb67e5caf15bf16b1e</anchor>
      <arglist>(FILE *fp, double v, int print_1) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>find_obj</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a0c4494410ebfbb9c10de64ce85f5690a</anchor>
      <arglist>(FILE *fp) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>is_subject_to</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a2a3e56acf1a411afc83c4e1eed89e469</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>first_is_number</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a81db68494fd12c066d1210ec116b6e5c</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>is_comment</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6756e7eff1a2ec3f3f4416fbb61d9c46</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>skip_comment</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a3e6f9b65fc415efe977b7e2ec87e159d</anchor>
      <arglist>(char *buff, FILE *fp) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>scan_next</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a2bfa8278aed579609216847af8b4ac0d</anchor>
      <arglist>(char *buff, FILE *fp) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>is_free</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ae6e8584a2a329bc0c16ca0aadb154f39</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>is_inf</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a7f23924f8cf897009bac87e913e52269</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>is_sense</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a0419bc0c8a164b982c17ce2f449c7dec</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>is_keyword</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af7f9f962fccb91d3794d0111b599912b</anchor>
      <arglist>(const char *buff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>read_monom_obj</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a940f0cfc512ac7f72907517357d55118</anchor>
      <arglist>(FILE *fp, double *coeff, char **name, int *cnt, char **obj_name, int *num_objectives, int *obj_starts)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>read_monom_row</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a2e4ef5bbe56decf7895c13853c50cec2</anchor>
      <arglist>(FILE *fp, char *start_str, double *coeff, char **name, int cnt_coeff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>realloc_coeff</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a43ead6ac1063e13d8989fc441fcb97d8</anchor>
      <arglist>(double **coeff, char ***colNames, int *maxcoeff) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>realloc_row</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a7d0ae82fcb09b42acf5cd59e8c2c23b8</anchor>
      <arglist>(char ***rowNames, int **start, double **rhs, double **rowlow, double **rowup, int *maxrow) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>realloc_col</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a9a3d815968fdab28c73573c849d49fd3</anchor>
      <arglist>(double **collow, double **colup, char **is_int, int *maxcol) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>read_row</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a54c0cf4500ad7f2328e3bc64aa9b272f</anchor>
      <arglist>(FILE *fp, char *buff, double **pcoeff, char ***pcolNames, int *cnt_coeff, int *maxcoeff, double *rhs, double *rowlow, double *rowup, int *cnt_row, double inf) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>checkRowNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8fc9417009291cb91c249eb90a3aad1b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>checkColNames</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a42cadc5635887776f2c62c20b06e3b9d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>problemName_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac792cf25635bbc81b58351939a705c21</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessageHandler *</type>
      <name>handler_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6b3fa52a7ab3882f068fedda9bf2ae9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>defaultHandler_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a4944b8b8e6e642945d8b17388c64467e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessages</type>
      <name>messages_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a4e9171b09fdaec9ae7f49ba88b97808c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberRows_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8308fe53b719299b81ba5fc615759ea5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8c5d4e06deb0dda61e0e7e6069377349</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberElements_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a3ce9115cb4ee54a9d2a0ef2e9c8e5873</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinPackedMatrix *</type>
      <name>matrixByColumn_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a0286538a4ce0549b0d25ede833e8b9d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af74f0fdd97ea2d5e772f54b19757a652</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a69069b905bd64bc2a9a2bb57a02efa9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ad25c6b2e3efa5394b8c74d5e1fb4adba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a946a4483578c638a371e84578235bf8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a690097cf8c647b0fe0bf4090e497818b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a3577705e1fd53fb821adf6339472b8c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a1e84194cf9ea32f17a61724a516276d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a2f324822215f8420a4c43a73fff27fb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>objective_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ae0c66cdad36e8dff43939c7cede7bf82</anchor>
      <arglist>[MAX_OBJECTIVES]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>num_objectives_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6e130a5390a4494e69e2e7dc544c7cec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>objectiveOffset_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a077ee775d777dfff0443628b6b7e6145</anchor>
      <arglist>[MAX_OBJECTIVES]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>integerType_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aa00ea81fb29d69d19089f3876abbea74</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinSet **</type>
      <name>set_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a62f3c0f91e528c096705e6880e208d2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberSets_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a17f62e720f402af5e81c726c2649461b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>fileName_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a0f30ec0a721937b3556da4873c8ed90b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>infinity_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a744a65cfe41d6411b8c7b4f2bc0b3cfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>epsilon_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af917450abff353b63b9d13ee7c9e1c60</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberAcross_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aa3767ec696f43878a0f3009c8149bee9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>decimals_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>af1cb4a3a4a8deec342154ee7bc466f38</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>objName_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac405f89d8168d0595c25aed43ff1509a</anchor>
      <arglist>[MAX_OBJECTIVES]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char **</type>
      <name>previous_names_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>ac476d2299154d96e0949251d933078ca</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>card_previous_names_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>abaac0d61d3621977da72ac21b52318a7</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char **</type>
      <name>names_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a6180e5ca73125a6fe9671f618ae6b700</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maxHash_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>acb64c8501c36b07978ae6b4395447ac9</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberHash_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>aec1280cb9465d62526876e6cbc813f85</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinHashLink *</type>
      <name>hash_</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a592aad51c1d1acffe5d0933621c97919</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinLpIOUnitTest</name>
      <anchorfile>classCoinLpIO.html</anchorfile>
      <anchor>a8a6bcc546bb0e1c14712eae8c9800756</anchor>
      <arglist>(const std::string &amp;lpDir)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinLpIO::CoinHashLink</name>
    <filename>structCoinLpIO_1_1CoinHashLink.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>index</name>
      <anchorfile>structCoinLpIO_1_1CoinHashLink.html</anchorfile>
      <anchor>a7835297ba6e25d09f17a83661d40f76d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>next</name>
      <anchorfile>structCoinLpIO_1_1CoinHashLink.html</anchorfile>
      <anchor>ace6c4c867c3d47407e43afadda90b4b7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinMessage</name>
    <filename>classCoinMessage.html</filename>
    <base>CoinMessages</base>
    <member kind="function">
      <type></type>
      <name>CoinMessage</name>
      <anchorfile>classCoinMessage.html</anchorfile>
      <anchor>a1be0ddc7193b7910e61d8dc23cdf9aeb</anchor>
      <arglist>(Language language=us_en)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMessages</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a6ebf4101b38ebf9a016ddbbc404e76c5</anchor>
      <arglist>(int numberMessages=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinMessages</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a9c6fc6c541dc42b28197188c7f236ad5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMessages</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a2b5551fb35fb3b16dad4424f24019140</anchor>
      <arglist>(const CoinMessages &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessages &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>aca622eec3137a8edc0f4adce43997056</anchor>
      <arglist>(const CoinMessages &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a83f7a6ae100955950e066fe1562d329e</anchor>
      <arglist>(int messageNumber, const CoinOneMessage &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>replaceMessage</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>afeda80c135db2e252a793cc38cc938ee</anchor>
      <arglist>(int messageNumber, const char *message)</arglist>
    </member>
    <member kind="function">
      <type>Language</type>
      <name>language</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a4159ebef7bb3b82e4068cde639a9cc69</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLanguage</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a7df9a9563d8af7810b92caf16a9630e6</anchor>
      <arglist>(Language newlanguage)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDetailMessage</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a6fde0f4b866733aa974656c5907ba2c4</anchor>
      <arglist>(int newLevel, int messageNumber)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDetailMessages</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a09cfcc9b510b6666b4df17ec3d1d1254</anchor>
      <arglist>(int newLevel, int numberMessages, int *messageNumbers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDetailMessages</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a1b1af90e796f4281c95b22eb24ec00d1</anchor>
      <arglist>(int newLevel, int low, int high)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getClass</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>afd7317c99ee229943e7b5dd583f19670</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toCompact</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a03e0d2a477dd9e7e936bee93fa4d3f76</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fromCompact</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>ac247947ff3a9ba73210dbdd692a22e2e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>Language</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a61fe25d8a8334b4f928f51460646f96f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>us_en</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a61fe25d8a8334b4f928f51460646f96faad5a6991bebfefa9b46eb53ccc6c1bef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>uk_en</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a61fe25d8a8334b4f928f51460646f96fad65f130afa08e3459fb1cd23a6dcb34e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>it</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a61fe25d8a8334b4f928f51460646f96fa0df7b2f1b55becbcfe129966446c3ccf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberMessages_</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a1f996654dc715352504476b662ee82b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Language</type>
      <name>language_</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a38d8fc6a26c72e221d75512b36472a1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>source_</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a703affe2ab96a0c9c607bbe891069838</anchor>
      <arglist>[5]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>class_</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>ab6236427c101ad6449509a5252ee36d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>lengthMessages_</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>ae6a529acb8d057eb1c611594ee180216</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinOneMessage **</type>
      <name>message_</name>
      <anchorfile>classCoinMessages.html</anchorfile>
      <anchor>a11fe03bec408cd98f8ef489b52099965</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinMessageHandler</name>
    <filename>classCoinMessageHandler.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>print</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a764f95dbbea93833cf8278ed9cbded05</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>checkSeverity</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>afebfd1840bf3ac3874c21aa2f996cc22</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMessageHandler</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a34c3e45079d87782adad75a4700f0e3b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMessageHandler</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>acdafdefb9682b70f9223d6814462184f</anchor>
      <arglist>(FILE *fp)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinMessageHandler</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ad8ab6a88c01f5ae4464d6b2bdddbf776</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMessageHandler</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a9854a65d48eb1cb0626da1faf8d61214</anchor>
      <arglist>(const CoinMessageHandler &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a38e7cfaae9b1ed533ee9649367c99301</anchor>
      <arglist>(const CoinMessageHandler &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinMessageHandler *</type>
      <name>clone</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a03a15db8c2039ebfa72932edda5764bd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>detail</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a4d2718fc4855c1f993200b6ccf2346db</anchor>
      <arglist>(int messageNumber, const CoinMessages &amp;normalMessage) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>logLevel</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aa5ccc5abca2c404590768f214ad3c46b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLogLevel</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a02055a662eedd49aaedca5bdd7868ea9</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>logLevel</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ae4b58913c1695dbc0fbe7407d15ad7de</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLogLevel</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a3dae1d818a0b9dca7bd3c4004ecf4583</anchor>
      <arglist>(int which, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPrecision</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a18eb13136c7dff6dd73006d15da7d753</anchor>
      <arglist>(unsigned int new_precision)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>precision</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a96db147da218bba118f69b25a4cda54d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPrefix</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ab20902b2947b2fcdde4d7c24baf41691</anchor>
      <arglist>(bool yesNo)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>prefix</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ae3345112757e1b85e097955701d3ab35</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>doubleValue</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>af4a9f270217371feecb4c552120d999f</anchor>
      <arglist>(int position) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberDoubleFields</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ae1ad359c1ed736ccb82538fb5fe82f53</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>intValue</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ada0c9900c48984d79999913d6dab9ccf</anchor>
      <arglist>(int position) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberIntFields</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a9cc5ceed521fd7925d53c01f1c289442</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>char</type>
      <name>charValue</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ab9b64421aafd5795ce994f24f3e632b3</anchor>
      <arglist>(int position) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberCharFields</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a4e8b7ab0cafbe513af8b09a3a15cf196</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>stringValue</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a9d5f7a224b4bc562415c40458279e28c</anchor>
      <arglist>(int position) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberStringFields</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a1304623897c1395ace502fe52323e5bf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinOneMessage</type>
      <name>currentMessage</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a272930e00be4f391d993e23e84d054aa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>currentSource</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a624b6013710779eefadca5f674ff642f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>messageBuffer</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a650acf467db011ab544aab42991cab56</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>highestNumber</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a2a90fa889d29c2b0df0b5b96c4d1e6cb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>FILE *</type>
      <name>filePointer</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aeb670204fa94925ca190583ef98a3e0e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFilePointer</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ac9dd948a6ba2281400eee6a6422fa755</anchor>
      <arglist>(FILE *fp)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>message</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aff6a7f9bd7d2b5a21798e46c3db94b99</anchor>
      <arglist>(int messageNumber, const CoinMessages &amp;messages)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>message</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ac180b18111ba5a8236f4b8dc9d6c73bf</anchor>
      <arglist>(int detail=-1)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>message</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aae531ece442d2713079341a96988ae98</anchor>
      <arglist>(int externalNumber, const char *source, const char *msg, char severity, int detail=-1)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a1299da24d6195f100fabd37c5fd1a6a8</anchor>
      <arglist>(int intvalue)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a67113ef531b190e1d7d41456aea71950</anchor>
      <arglist>(double doublevalue)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aacbd0dd85e9f930aef4e446dea7e38a9</anchor>
      <arglist>(const std::string &amp;stringvalue)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a1705b51ec35de97a1086a2f57fac133f</anchor>
      <arglist>(char charvalue)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a759b84598c985d1374cb4e08ea0a0630</anchor>
      <arglist>(const char *stringvalue)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ae09312dc1706c73f61467b8bdc2da1d6</anchor>
      <arglist>(CoinMessageMarker)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>finish</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a47c6931e7146a78448fa42da74f354cc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler &amp;</type>
      <name>printing</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a1010f920141a9c79d52089b51f81bace</anchor>
      <arglist>(bool onOff)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; double &gt;</type>
      <name>doubleValue_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a2d4ce0a6dd84ae2ad7ba8bc60920e0ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; int &gt;</type>
      <name>longValue_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a4f9a77fc07977b62a5baaf8cea67a6ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; char &gt;</type>
      <name>charValue_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a1a3758d9968ac4e730eb43fca1cd0481</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; std::string &gt;</type>
      <name>stringValue_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aba8abab7a013bfb1faae63bc309c143d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>logLevel_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a2d845cdeade09d07001a146ee60f74a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>logLevels_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a0cdcb554aea6cee17c83f3f157e47a44</anchor>
      <arglist>[COIN_NUM_LOG]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>prefix_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ae864e184a6f310bb8679afb542f0cbda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinOneMessage</type>
      <name>currentMessage_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a11b2193860adea7bdbcd7f4725d877f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>internalNumber_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a0df79a5887f3acaba60f29300cf62d59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>format_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a8ebfb40fa19e735fd5550f3660e9a04c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char</type>
      <name>messageBuffer_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aba18ce12f2461761bb806c2880ed3395</anchor>
      <arglist>[COIN_MESSAGE_HANDLER_MAX_BUFFER_SIZE]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>messageOut_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aa841b75943cf14bc4f48c19ddcff2b5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>source_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ab86a01dcffd58043249ac93c69468e60</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>printStatus_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>af09b8c653b342d02977308cf6b21d99f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>highestNumber_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aa26b4afe4e8f1e51a0f21f7c252e1144</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>FILE *</type>
      <name>fp_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>ace7ae004b8a9f492f7ddad5f57d9166d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char</type>
      <name>g_format_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a6c4978d268f4a9daef21d7adc0ebdc01</anchor>
      <arglist>[8]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>g_precision_</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>aa8895c3b12ebb97604c704ca4c929a68</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend bool</type>
      <name>CoinMessageHandlerUnitTest</name>
      <anchorfile>classCoinMessageHandler.html</anchorfile>
      <anchor>a4035f21309a1b78da27cc505b2767add</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinMessages</name>
    <filename>classCoinMessages.html</filename>
  </compound>
  <compound kind="class">
    <name>CoinModel</name>
    <filename>classCoinModel.html</filename>
    <base>CoinBaseModel</base>
    <member kind="function">
      <type>int</type>
      <name>computeAssociated</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a21e0704f23e7fdd65296685d086c326b</anchor>
      <arglist>(double *associated)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedMatrix *</type>
      <name>quadraticRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a7e5af55be8057f64a3926d4b34574241</anchor>
      <arglist>(int rowNumber, double *linear, int &amp;numberBad) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>replaceQuadraticRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>acbe47d84eaa928d277bb037a099be336</anchor>
      <arglist>(int rowNumber, const double *linear, const CoinPackedMatrix *quadraticPart)</arglist>
    </member>
    <member kind="function">
      <type>CoinModel *</type>
      <name>reorder</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a9eb80bfb0e353c52fdc4e8b4b63d3eb9</anchor>
      <arglist>(const char *mark) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>expandKnapsack</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab2065b19076da65c28639935464f8eb3</anchor>
      <arglist>(int knapsackRow, int &amp;numberOutput, double *buildObj, CoinBigIndex *buildStart, int *buildRow, double *buildElement, int reConstruct=-1) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCutMarker</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>adb2e570f21449373a5990e49774a2732</anchor>
      <arglist>(int size, const int *marker)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPriorities</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ae697cfffa06b43007d871ea7b312ca3c</anchor>
      <arglist>(int size, const int *priorities)</arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>priorities</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ae9d73771fbb447ef75625fe47f8fe023</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalIndices</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a4072c688a70c745b7ad368e009a321ea</anchor>
      <arglist>(const int *row, const int *column)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>afeb05c0163907ceb934246c8673dbfb9</anchor>
      <arglist>(int numberInRow, const int *columns, const double *elements, double rowLower=-COIN_DBL_MAX, double rowUpper=COIN_DBL_MAX, const char *name=NULL)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>abc0699e593edcc2d60441c68e645da72</anchor>
      <arglist>(int numberInColumn, const int *rows, const double *elements, double columnLower=0.0, double columnUpper=COIN_DBL_MAX, double objectiveValue=0.0, const char *name=NULL, bool isInteger=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addCol</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a4dd6c6d8bf50e9c9d1f07f7d8758bf54</anchor>
      <arglist>(int numberInColumn, const int *rows, const double *elements, double columnLower=0.0, double columnUpper=COIN_DBL_MAX, double objectiveValue=0.0, const char *name=NULL, bool isInteger=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator()</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a7ab295cc27a1af63616539828d82bc9f</anchor>
      <arglist>(int i, int j, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a44c4c0000cf3ff1118c1bfb37585ed7f</anchor>
      <arglist>(int i, int j, double value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a10a6638f7dd6b05e7fd788dad76b436d</anchor>
      <arglist>(int whichRow, int *column, double *element)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a0b26686e14e1cc220f1537e8bac0d1ff</anchor>
      <arglist>(int whichColumn, int *column, double *element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setQuadraticElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a395d216694e248219d09afb249a2c812</anchor>
      <arglist>(int i, int j, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator()</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>afae07acf0ecc930eb959816cfd26640e</anchor>
      <arglist>(int i, int j, const char *value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a9ee9723e1d7630e9fbf7c5cbcab615d7</anchor>
      <arglist>(int i, int j, const char *value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>associateElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a64c6c681aa090dcacfdd925929ce2433</anchor>
      <arglist>(const char *stringValue, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab14a3f28d78662f8df72ff66172e2f6b</anchor>
      <arglist>(int whichRow, double rowLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aeab8448a7ccf06f4792554b2a7d0c477</anchor>
      <arglist>(int whichRow, double rowUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowBounds</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a10aac83a89850f793805880fa5661037</anchor>
      <arglist>(int whichRow, double rowLower, double rowUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aefe2a18b05ca3c451df9ada092e146ab</anchor>
      <arglist>(int whichRow, const char *rowName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a33708b14fe15a85ccaccefb01c18dd9b</anchor>
      <arglist>(int whichColumn, double columnLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>adfc6d715da42833fa41f1e9d3ba6c72e</anchor>
      <arglist>(int whichColumn, double columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnBounds</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2bc2f252014797e8b7ec823a43ff10ca</anchor>
      <arglist>(int whichColumn, double columnLower, double columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ade64092b9c352877a1a1398226299b9d</anchor>
      <arglist>(int whichColumn, double columnObjective)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a8185ef965ea27b4446864c11815ff485</anchor>
      <arglist>(int whichColumn, const char *columnName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa2481d3526799a452cf4e9d852cbf10f</anchor>
      <arglist>(int whichColumn, bool columnIsInteger)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a1c2d8e03125e1ac4b82bd13281187a32</anchor>
      <arglist>(int whichColumn, double columnObjective)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a21cd6be9b90df537a605d54c07a6d3a6</anchor>
      <arglist>(int whichColumn, bool columnIsInteger)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a73766d58b93c2d76150df6b6914214db</anchor>
      <arglist>(int whichColumn)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setContinuous</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a692e5edff6fa900f49a0ec4de3807676</anchor>
      <arglist>(int whichColumn)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>acdfbad62d782fb8715d242a9ae7449cc</anchor>
      <arglist>(int whichColumn, double columnLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab580b2c35c3acf175cdc3afffda6802a</anchor>
      <arglist>(int whichColumn, double columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColBounds</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a8756b6d438541c7e869dba509aca8e40</anchor>
      <arglist>(int whichColumn, double columnLower, double columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a63346e505984bb0be76759a675c9d73b</anchor>
      <arglist>(int whichColumn, double columnObjective)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a412dee576ec936c26e7012bdc389db30</anchor>
      <arglist>(int whichColumn, const char *columnName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>afdf4c28cccd8ce0a6aba5adaba4690a3</anchor>
      <arglist>(int whichColumn, bool columnIsInteger)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a320c75ac2c32c16e4e7314265ca5d076</anchor>
      <arglist>(int whichRow, const char *rowLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a67fe11b44119db2fcb8e9c4a017ac314</anchor>
      <arglist>(int whichRow, const char *rowUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a58f807247a49ed7500ad192ee2f3e73a</anchor>
      <arglist>(int whichColumn, const char *columnLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a4b3df78103e0c41102d81c454a48d1f0</anchor>
      <arglist>(int whichColumn, const char *columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a92772bbdf76d41092436044b0c597e54</anchor>
      <arglist>(int whichColumn, const char *columnObjective)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3f05cd3e8ab0542891df69f39a57fcb7</anchor>
      <arglist>(int whichColumn, const char *columnIsInteger)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ac9a73725d282f1af004c54d3bea76e4b</anchor>
      <arglist>(int whichColumn, const char *columnObjective)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a9312e91c1f5554bcb2a396f4f79742ac</anchor>
      <arglist>(int whichColumn, const char *columnIsInteger)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a5e2dce33aa49d444131057967c2095cb</anchor>
      <arglist>(int whichRow)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a50d85869e465bd8007d140afb5b9be4a</anchor>
      <arglist>(int whichColumn)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteCol</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ad31fd2d0a05e4c9c9a456ef081259f39</anchor>
      <arglist>(int whichColumn)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>deleteElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a96079eacf6fb547370bdc8bada3726a4</anchor>
      <arglist>(int row, int column)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteThisElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a7e53e6b1b66b73aa065c98e95cf69c0d</anchor>
      <arglist>(int row, int column, int position)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>packRows</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a4940075ac08d36e6b4e272ebab76f9ce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>packColumns</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a64e88c576e88da649667a7e0492f62e1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>packCols</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aeba70c678b5e853e549d19acb97c910a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>pack</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aec8e9848e60e9158f3dc812a6924fd14</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab142c8d19ac1ecea0652622d2247a43a</anchor>
      <arglist>(int numberColumns, const double *objective)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3cfbdfca51de9e26c4fb21e762919427</anchor>
      <arglist>(int numberColumns, const double *columnLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3d95e6d0055eab4eeedb7f02acfd72b2</anchor>
      <arglist>(int numberColumns, const double *columnLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a62dad29e26243e883fe717697b051674</anchor>
      <arglist>(int numberColumns, const double *columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a5cb6d1ef2e3eb1aa350b19fabbbe0499</anchor>
      <arglist>(int numberColumns, const double *columnUpper)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a6c294b160b5398c857d4491ba8e0cbcc</anchor>
      <arglist>(int numberRows, const double *rowLower)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa1dde95782b0b169d41a4aa670021085</anchor>
      <arglist>(int numberRows, const double *rowUpper)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeMps</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ac8067e82269f604c9d895a354473d7b0</anchor>
      <arglist>(const char *filename, int compression=0, int formatType=0, int numberAcross=2, bool keepStrings=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>differentModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>afc2fe4912de302902bdeebff108a33b3</anchor>
      <arglist>(CoinModel &amp;other, bool ignoreNames)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>passInMatrix</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ad72f9e66511915c722305ac47f8dc70f</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>convertMatrix</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a56aca340e5ef8cdcd1405585b5ae074e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>packedMatrix</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a63e2b572ea38ecdd4a9879db32b4b7ab</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>originalRows</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a1a6bf1b8b09ec58b5248360eb9c89f2c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>originalColumns</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab50a60576bb7c4cc140f4dae8b84e231</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElements</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2ebd5d68df4ed8f3501e84ac3991af0e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinModelTriple *</type>
      <name>elements</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a018224ecd24a95c8825eb2b0433dfc83</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>operator()</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>abe18773b512df42fd5e550fb03f51fea</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a206881a99b1f24ab3f7df5eae21844d2</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>operator()</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a8ba7a350d4416ea0ca35c41a7822d3db</anchor>
      <arglist>(const char *rowName, const char *columnName) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a0e4e2b0f74082da339fa890f92dc6e76</anchor>
      <arglist>(const char *rowName, const char *columnName) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getQuadraticElement</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3658e3f2e1779a28538f77c78751afad</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getElementAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab8ace5eaf648b8a3d6c885507bfd9358</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>pointer</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a86605d19f26b3e2aef4a3762abf61940</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>position</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>acaf98d89cf9c9afc951a8949cf404d4a</anchor>
      <arglist>(int i, int j) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>firstInRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a0701003cb2267359718d0a1574abe958</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>lastInRow</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab396539d9cd8a4ec2de1d36892e612b0</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>firstInColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aeea00815b39d8103784c4f328812ebaf</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>lastInColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a796cd7685cb00cf18703af8ed46c8127</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>next</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a44823abcfe3da216c1ca72d9b60d0aba</anchor>
      <arglist>(CoinModelLink &amp;current) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>previous</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ae2a207d3eabc79272994c88e1ae2ef49</anchor>
      <arglist>(CoinModelLink &amp;current) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>firstInQuadraticColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a50e132a6dfea1184e7dce49e6b773549</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink</type>
      <name>lastInQuadraticColumn</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aba9da49dff186356137afeb7e9959297</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getRowLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab6ac672661ec0de8df9ee931714124a4</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getRowUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a169a28031f3cfc4c54c27fe9b8fae641</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRowName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>adfde7f6ec11de17ee02bdace3fad51a3</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>rowLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab479b2767fc4014ccdedeffa37c95490</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>rowUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a77fcd97a0d73bf2ae2e9a57edd224e6d</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a90afb21ca3a3d7907bbba7bccb6aa7a9</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getColumnLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ace8f9dfcbe2f7d4c9eb2bd24f960abd0</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getColumnUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>af395b16bbf3441e6f3a493d944e2fe5d</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getColumnObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a373f21fada7272eb36b75172c5bc043e</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColumnName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a19bf1c28ca2189282578c4050f75385b</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getColumnIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a0ae63f9131baa415815a6c35b104e67d</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>columnLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa1077107e6cd5c42dfbde47544e845aa</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>columnUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a60819e0cb850e504bb0a6b1b3791d182</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>columnObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a34fdd6c12c79cd5a7093a5a8a630ecc3</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>objective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ace1d623577e46278a7b9ce84d657edb5</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>af44b402dc4294f60f56dcb9e1311780e</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>columnIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a526c596dbb96a7b8991bb4b723f5e27e</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa50a0d13b416ca115026154f631db288</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getColLower</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>abe763801945ed5cae2ecd948ca4be094</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getColUpper</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aaa5339954dbbd1e2793881f370d62e47</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getColObjective</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a136bc4f116577c33e9ea74ba50512a0b</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColName</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>abb805fc5cc75c3fb9c147e52c17bbd22</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getColIsInteger</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2d5c6d8b0dcf19097ed9d48b29e995a4</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRowLowerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a96e91b74f95e4bb767a2587aacf82497</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRowUpperAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa90be79fee19ea185da44d3b6a6f80dd</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowLowerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aabc2d488fdea85652fd4caf02438f9c0</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowUpperAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ad23a7db078bd25666500fbe4d76cc06a</anchor>
      <arglist>(int whichRow) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColumnLowerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aab0775e640ec72294c7f5ffffc07c536</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColumnUpperAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a50bdea3695b9033e9785c09b426c198d</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColumnObjectiveAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>abf2417c2f98cf84405d7644921407aa1</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColumnIsIntegerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a106235ca54e1e585ff57eadbedb4fed5</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnLowerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a84243d2917eac079228881357aa461e0</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnUpperAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>abe4e0ef7430b1b7b825bb75961fb960e</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnObjectiveAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>af5d9fd726f445484d97b010494b9efdb</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>objectiveAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a21e6621107b2d4d02d4a91b7318c650a</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnIsIntegerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a72927e2e6055f8da3a19ec3928702a21</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>isIntegerAsString</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a92d2f48a6761b6cdf0c32d6fb5e62a45</anchor>
      <arglist>(int whichColumn) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>row</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aeaa1a815a2c088dcaea9eda6bd8378f7</anchor>
      <arglist>(const char *rowName) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>column</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a6fffef92b502bd4188936f2e5437183d</anchor>
      <arglist>(const char *columnName) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>type</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa97dda752444064ba492dcfb1850d563</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>unsetValue</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ad6c85df5efb3d3ea76c91ab4329172f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>createPackedMatrix</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a8a0ae28180ea2bf8ba015a04a042c817</anchor>
      <arglist>(CoinPackedMatrix &amp;matrix, const double *associated)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countPlusMinusOne</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3a645a3147f2504022340952605ad076</anchor>
      <arglist>(CoinBigIndex *startPositive, CoinBigIndex *startNegative, const double *associated)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createPlusMinusOne</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ade552695e2da73444741f86bcf36ee3e</anchor>
      <arglist>(CoinBigIndex *startPositive, CoinBigIndex *startNegative, int *indices, const double *associated)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>createArrays</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2316cea8301a525d0bb281f1a15c04ad</anchor>
      <arglist>(double *&amp;rowLower, double *&amp;rowUpper, double *&amp;columnLower, double *&amp;columnUpper, double *&amp;objective, int *&amp;integerType, double *&amp;associated)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>stringsExist</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>af1149b586fbe2388b8f50fc9bf6e0274</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinModelHash *</type>
      <name>stringArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>af2ba60d1889a54b35c1b0709f7d26018</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>associatedArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a1fc628f0f6ffaff419e0ce05f6b3b9da</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>rowLowerArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3ec7f777bbc4f59029a9275ab69cd3f7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>rowUpperArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>acd1e8542913b6058916ceaa4155dd6bd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>columnLowerArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a5e7375fc9015c873ad33830ea6e2cd88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>columnUpperArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2249e4b661709cb91b5d24c958a76810</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>objectiveArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a54bdf171a8c5a6ea7ffddd698c484e72</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>integerTypeArray</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a015f79b3ea0340a43ec5f51bf531550c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinModelHash *</type>
      <name>rowNames</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ab3337d6da28f0c6a505d72b01d5a852f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinModelHash *</type>
      <name>columnNames</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2503f3422ff20eb43987053f84390985</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>zapRowNames</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a7eb8b13ca76085d8ec141724e863d4f2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>zapColumnNames</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a59125938d2323ee4321c4289630dabe9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>cutMarker</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a5e12009771d45b4415792d61be64023e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>optimizationDirection</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a2d9ca579be87bed4b9a1a8f9ac8d72ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOptimizationDirection</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aba64405c436d802905b79944adcc7582</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>moreInfo</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a11126aec19b26293cf99dc8325b7f546</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMoreInfo</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a622850444caef355f04d5ca2869e17ba</anchor>
      <arglist>(void *info)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>whatIsSet</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>acb255a8006e21a3c08f35d59b1d71c92</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadBlock</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa656aeafdf7de25d45e76a84a9aa4401</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadBlock</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a65442d88e805e3eadebca70e5d8b5bda</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadBlock</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a72c48c2c64b3839756a21caf6289e135</anchor>
      <arglist>(const int numcols, const int numrows, const CoinBigIndex *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadBlock</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>afa0fd028f72700a285deab9be811e496</anchor>
      <arglist>(const int numcols, const int numrows, const CoinBigIndex *start, const int *index, const double *value, const double *collb, const double *colub, const double *obj, const char *rowsen, const double *rowrhs, const double *rowrng)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a720b5508ef99b234fe642bab20b1fb44</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>af77b0e3fe61d82b4af735fb749a65c7a</anchor>
      <arglist>(int firstRows, int firstColumns, int firstElements, bool noNames=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a1b881dc2cc6cd3f50fefb08f4ef515fc</anchor>
      <arglist>(const char *fileName, int allowStrings=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a21d1f94b43864bec4230440159ea7ed0</anchor>
      <arglist>(int nonLinear, const char *fileName, const void *info)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>aa7a765ce28475d64d1e63eca048fb21e</anchor>
      <arglist>(int numberRows, int numberColumns, const CoinPackedMatrix *matrix, const double *rowLower, const double *rowUpper, const double *columnLower, const double *columnUpper, const double *objective)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinBaseModel *</type>
      <name>clone</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a3e2a7e045fbba81bdb750aee0bbcf712</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a915650575732b17a6e47a3bc709f4bb5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModel</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>afb5674c77a93a6a599efbf3ccd551b07</anchor>
      <arglist>(const CoinModel &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinModel &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>a1342776a95312b17bfcb0af8211e34db</anchor>
      <arglist>(const CoinModel &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>validateLinks</name>
      <anchorfile>classCoinModel.html</anchorfile>
      <anchor>ad34655396abb338ba59e6015869f817b</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinModelHash</name>
    <filename>classCoinModelHash.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinModelHash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a400093e816c4ecabb5cfc0264742fbb7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinModelHash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>abeb03f33a6ff22163ee1769dd96f34bd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModelHash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a874ac1f7f159454566aaa15556eca5e5</anchor>
      <arglist>(const CoinModelHash &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinModelHash &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>aa662566b5d73a16b2dd073a3b4b56d9b</anchor>
      <arglist>(const CoinModelHash &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a148579d36951f4e24ae11883089f743b</anchor>
      <arglist>(int maxItems, bool forceReHash=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberItems</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>ac7061f6182b4a1c2646b20cf989eec25</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberItems</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>acc433c408cdc6e66d85bec5382defc20</anchor>
      <arglist>(int number)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumItems</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>adb87fc46c7b5fdd3a4a11ee6a807bd64</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *const *</type>
      <name>names</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a3e15ce2cc9e74a62865e001349571cc4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>hash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a044963a6d534d4adafec5ea9ad751abd</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addHash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a683228626d215594f19779fa28db5af1</anchor>
      <arglist>(int index, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteHash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>aeace674dc2610e3b24aa3bc97e68945e</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a7e94e7e77e141a3c2694b905c9021f92</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>getName</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a3c2dac0ba8b5188ce8c4d0f2e9d9baf3</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setName</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>a30ce61d2960b82910d0dd914cea5da09</anchor>
      <arglist>(int which, char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>validateHash</name>
      <anchorfile>classCoinModelHash.html</anchorfile>
      <anchor>ab1073c2ba4de50ac0b0d1a87da91a7e3</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinModelHash2</name>
    <filename>classCoinModelHash2.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinModelHash2</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>aaeb9a0ce7df7476d2cf36d48895fab99</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinModelHash2</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>acbe666467c0720cb0c9b2b03543df15f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModelHash2</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>abd3cf93b766bdd42a15ea705209ad79c</anchor>
      <arglist>(const CoinModelHash2 &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinModelHash2 &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>a24f8f202bfe8b41d077ed31f7a6f9f5b</anchor>
      <arglist>(const CoinModelHash2 &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>a16544ab30d9d5b00b04f18d247c98f72</anchor>
      <arglist>(int maxItems, const CoinModelTriple *triples, bool forceReHash=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberItems</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>a88d088a219e56dea1e828c01a3568d7a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumberItems</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>a26660a71cbe7efe37c61bb8a6820bb8b</anchor>
      <arglist>(int number)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumItems</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>acf17c1a7a93c81db029a378854f141eb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>hash</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>aed550196ec013802178e2b074144733d</anchor>
      <arglist>(int row, int column, const CoinModelTriple *triples) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addHash</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>a06736d01d38d7f9a054028268a7466d4</anchor>
      <arglist>(int index, int row, int column, const CoinModelTriple *triples)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteHash</name>
      <anchorfile>classCoinModelHash2.html</anchorfile>
      <anchor>a64dee255dae1fc4a2550ea357007ac8a</anchor>
      <arglist>(int index, int row, int column)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinModelHashLink</name>
    <filename>structCoinModelHashLink.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>index</name>
      <anchorfile>structCoinModelHashLink.html</anchorfile>
      <anchor>a5a01f9746ffa24ac3b06f906a6ad1bc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>next</name>
      <anchorfile>structCoinModelHashLink.html</anchorfile>
      <anchor>afa81298c27e13a93ef895d0f52d7a19b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinModelInfo2</name>
    <filename>structCoinModelInfo2.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinModelInfo2</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>a6d880003edddf330df13892de3b00220</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>rowBlock</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>a75c6c295a0e2ae072d2a96e0941c63a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>columnBlock</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>abea58f22add89a5e5bae0f7aff9441bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>matrix</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>ae068f4ba78954f91b6e0336478d971a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>rhs</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>aeb58ec96ee64ed7e42dcb3e72a8ee84b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>rowName</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>a04c420ed1a5be4ce7466eaba190ec31d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>integer</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>a42bef0463df456edfdf4283e83045c2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>bounds</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>adc45535f9d5a854e31fbc48262f6865e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>columnName</name>
      <anchorfile>structCoinModelInfo2.html</anchorfile>
      <anchor>ad9fa42150caf34c649f4fd212c3fc7a4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinModelLink</name>
    <filename>classCoinModelLink.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinModelLink</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a26e274f019187293f30daa72bed2ca50</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinModelLink</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a01f3817573270f6d495f45fdd95adf26</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModelLink</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>aff949b8b591af9460d67c0f98b8a6736</anchor>
      <arglist>(const CoinModelLink &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinModelLink &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a068e19f51d184216b9a5079cd9edd817</anchor>
      <arglist>(const CoinModelLink &amp;)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>row</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a6c11857c49bd5e2f9fa3a05144ea4a1a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>column</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>aca0076662a87c2118832b8bc1187b8e0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>value</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a6c587bf1f59bf5311fb12f11ce342fe3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>element</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a3b5152126433f73fa057a85e3acce00f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>position</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a68f242374d57a0f459255631d942144a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>onRow</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a7ccb8f08f7e5cc7f13a885cd3530baee</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRow</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>ace3ce3d1d1f146205034a5a887c0bbb4</anchor>
      <arglist>(int row)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>af7f5eeeadc3310f40025474912cb0f87</anchor>
      <arglist>(int column)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setValue</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a303b8161291e7eafda0fd8c5f5195cc2</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setElement</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a2a6e685e7adef93711f7563bcd33a206</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPosition</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>aff92fed65f0ffac0b6597f3a1de3bb60</anchor>
      <arglist>(int position)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOnRow</name>
      <anchorfile>classCoinModelLink.html</anchorfile>
      <anchor>a8f1f81e9ba3a90eb6589ea0a229e6470</anchor>
      <arglist>(bool onRow)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinModelLinkedList</name>
    <filename>classCoinModelLinkedList.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinModelLinkedList</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>ace0c0a17dafaa9c9f4b450d0bb28aa92</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinModelLinkedList</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a19991b570b42829334ebd0f47961e7ff</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinModelLinkedList</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a92b4df9dbc7efd2b76552eaf5e605451</anchor>
      <arglist>(const CoinModelLinkedList &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinModelLinkedList &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a8aeb1ea03a681d9ea9f1c92b4aecc379</anchor>
      <arglist>(const CoinModelLinkedList &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resize</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>af49333d2dc3e1dc469eeea96b8b3a0f7</anchor>
      <arglist>(int maxMajor, int maxElements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>create</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a9e53085edd05fb68799873281f8ed17d</anchor>
      <arglist>(int maxMajor, int maxElements, int numberMajor, int numberMinor, int type, int numberElements, const CoinModelTriple *triples)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberMajor</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a895c507d1ef00a388a7cdbd921a9bff1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumMajor</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a95a4ca3af039c56366a694ecd360f81c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberElements</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>addc1a6de1a88a85f60e493258e0c5cdd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>maximumElements</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a9c88c3daaff63d5e2a2d36a0f1c1e596</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>firstFree</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a0cd372fc6256d8fe396a1bb5421dcc5a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lastFree</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>aaadf5769562b854e57659b6d7ffba8b9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>first</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a3a8b2ea2bde30222f98a353076ad70d7</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>last</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>aa72c300df8c90d380d9778c109d4ecc8</anchor>
      <arglist>(int which) const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>next</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a1133aec156c1be8e1caea42836aa8f6e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>previous</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a61a3cbb19fe0ec1d876292ee223fc503</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addEasy</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a7839be1bc646c3a36172a3c7420ad656</anchor>
      <arglist>(int majorIndex, int numberOfElements, const int *indices, const double *elements, CoinModelTriple *triples, CoinModelHash2 &amp;hash)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addHard</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a9b94248891e0e4f126cef2dcf8f40ef5</anchor>
      <arglist>(int minorIndex, int numberOfElements, const int *indices, const double *elements, CoinModelTriple *triples, CoinModelHash2 &amp;hash)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addHard</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>aa8b595fe0447f3cac126417214389dab</anchor>
      <arglist>(int first, const CoinModelTriple *triples, int firstFree, int lastFree, const int *nextOther)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteSame</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>ae20fa703ab03e0b536a5d3286557f898</anchor>
      <arglist>(int which, CoinModelTriple *triples, CoinModelHash2 &amp;hash, bool zapTriples)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>updateDeleted</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a09a260b1fa28d27816c15b4faeb4da70</anchor>
      <arglist>(int which, CoinModelTriple *triples, CoinModelLinkedList &amp;otherList)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteRowOne</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>aee10123676149207045dbf6083c523b3</anchor>
      <arglist>(int position, CoinModelTriple *triples, CoinModelHash2 &amp;hash)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>updateDeletedOne</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>ae5615790981fc6b24aaaed35c697709a</anchor>
      <arglist>(int position, const CoinModelTriple *triples)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fill</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>ac767fb5f74150e8bbc7df4afd45c301a</anchor>
      <arglist>(int first, int last)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>synchronize</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>ac16ca50a02383397f35feaceedea3b17</anchor>
      <arglist>(CoinModelLinkedList &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>validateLinks</name>
      <anchorfile>classCoinModelLinkedList.html</anchorfile>
      <anchor>a505017952f30a5267bc3cb2952d7a51f</anchor>
      <arglist>(const CoinModelTriple *triples) const </arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinModelTriple</name>
    <filename>structCoinModelTriple.html</filename>
    <member kind="variable">
      <type>unsigned int</type>
      <name>row</name>
      <anchorfile>structCoinModelTriple.html</anchorfile>
      <anchor>ad0a6739d4058078c1b35875b37a63e92</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>column</name>
      <anchorfile>structCoinModelTriple.html</anchorfile>
      <anchor>aebd95b2674cf97a69b4b8a8b58430b0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>value</name>
      <anchorfile>structCoinModelTriple.html</anchorfile>
      <anchor>a6541001609bd55b47eae293bf5664225</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinMpsCardReader</name>
    <filename>classCoinMpsCardReader.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinMpsCardReader</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>aba0bfeedfba8bb81cb85bc67254b09c5</anchor>
      <arglist>(CoinFileInput *input, CoinMpsIO *reader)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinMpsCardReader</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>accbee6a3ae338e8f1db4cf1dad1272a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>COINSectionType</type>
      <name>readToNextSection</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>aee5727775fb10eab281c86820f355c2b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>COINSectionType</type>
      <name>nextField</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>ab2e1813c269b81e4142d46bbc2a0d37f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>nextGmsField</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a759b458e4750a1587e5e088e71fb7842</anchor>
      <arglist>(int expectedType)</arglist>
    </member>
    <member kind="function">
      <type>COINSectionType</type>
      <name>whichSection</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>af882fcb5aa6e80a49e2a4de5cba433e9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setWhichSection</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a66a5521f700dad39eed5fbc36c974edd</anchor>
      <arglist>(COINSectionType section)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>freeFormat</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a6cfb5ee459c5d61510882b433d8af174</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFreeFormat</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a6f2157cb1592c3693950ac501fe78cce</anchor>
      <arglist>(bool yesNo)</arglist>
    </member>
    <member kind="function">
      <type>COINMpsType</type>
      <name>mpsType</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a9fca267cf184c98e218cd17f77aa3c2a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>cleanCard</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a108f414ad05157de0b700c496a5574d1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowName</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>aca5f4b5deeb516658d8d0f56ee3f1288</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnName</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>aa95b6ec1142b3999b2c8ec2e181380f0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>value</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a8664ab6f7f5f2793f44bcdafd5f8d0e6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>valueString</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>afd75554379471cf08c5fb7954faee677</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>card</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>ab223bb447617d310bc67098f0f678111</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>mutableCard</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a820818b62643a83b162dd6a102330406</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPosition</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>afb333948543d474142f8893792f4241d</anchor>
      <arglist>(char *position)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>getPosition</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a565ce14c21549b2b0b7f8762b0acfb94</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>cardNumber</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a810feef5cd2db6f4dc20fb6c19fc13c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinFileInput *</type>
      <name>fileInput</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>acfc1aed0d3551da69f30212af8719f43</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStringsAllowed</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a766c1830e48f85abf479655fb319b8ba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>value_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a2612531fc67ca259f351ec7788f71e7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char</type>
      <name>card_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a4990acd2d868a40be754dcdff6a1ce6b</anchor>
      <arglist>[MAX_CARD_LENGTH]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>position_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>ad7cf6c93d0ebd77b12182a27f0816d4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>eol_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a2080d651886557509eb335e21e79a1a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>COINMpsType</type>
      <name>mpsType_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a3385c7514b088e36b4fefebb971055ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char</type>
      <name>rowName_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>aeb7f70c22eb128db88453262d54d0079</anchor>
      <arglist>[COIN_MAX_FIELD_LENGTH]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char</type>
      <name>columnName_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>afbe673f98cf3edc98f22a1510f201c7b</anchor>
      <arglist>[COIN_MAX_FIELD_LENGTH]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinFileInput *</type>
      <name>input_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>afe36cd8303226f03955e9a1ffd4c20e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>COINSectionType</type>
      <name>section_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>ac9e0909084594efae4d8bfbe61073ba5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>cardNumber_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a281d4c65a67e5f0948894bd1a4502f65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>freeFormat_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a11d2cefd936d9ca48d9ca47f3936e53b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>ieeeFormat_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>ab462a163bfde65a5d70d5a5a289120d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>eightChar_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a5520377fe2c450bfaaf635dab09ddd28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMpsIO *</type>
      <name>reader_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a9dcbe03b96603ae4d1af9b5b9417ace9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessageHandler *</type>
      <name>handler_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a14512b0ce55ad162c1cf05c29d3d0715</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessages</type>
      <name>messages_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a1a442944a09988cda473c0b1f0b384b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char</type>
      <name>valueString_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>ad2dcc6729f4eed83a76d6e5220b4836d</anchor>
      <arglist>[COIN_MAX_FIELD_LENGTH]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>stringsAllowed_</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a30e6b490fd0da3c362d7dcc933dd7b2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>osi_strtod</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a377111ca5828ea25566dc7c22a2d890a</anchor>
      <arglist>(char *ptr, char **output, int type)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>osi_strtod</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>aa45675fe7c87f61c8ca4f3b2aa99a08e</anchor>
      <arglist>(char *ptr, char **output)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>strcpyAndCompress</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a825afa4c23692482b887401414719357</anchor>
      <arglist>(char *to, const char *from)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static char *</type>
      <name>nextBlankOr</name>
      <anchorfile>classCoinMpsCardReader.html</anchorfile>
      <anchor>a942cd3289e4a5949fb5b407fd2e9a9d2</anchor>
      <arglist>(char *image)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinMpsIO</name>
    <filename>classCoinMpsIO.html</filename>
    <class kind="struct">CoinMpsIO::CoinHashLink</class>
    <member kind="function">
      <type>int</type>
      <name>getNumCols</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aab54cb3965067d7a7a45a5a3e31bdd3b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumRows</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ae6d4eec7aee445316842cf197b8d7129</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a9c35a425289d40136cd0f123de825039</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColLower</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a795815b13e0bcdd7f75253d78a11992c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColUpper</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad4ecc7473e2df90f1323fb017065e425</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRowSense</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a031921e60c7ca030198cb8899a7f5eb8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a25de64eb92d7a8ebe13207f4e29292c4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowRange</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a3644206c77d2f0dc376abb15864c70a5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowLower</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a50e58265098531807fa3f810e231444a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>af04ac265013a6e1299400418382d48d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a93e05c81fb9fb63fc5bb3abed62c0733</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a167721680b82ec1c794f0e03763e8ff5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a4c19553f4e5ac7377019cb5d842b6686</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isContinuous</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a88cf63fbe88f2f231aecffb9c3581686</anchor>
      <arglist>(int colNumber) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteger</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac43d3cbaa8116cf215d49adfbf5a2bba</anchor>
      <arglist>(int columnNumber) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>integerColumns</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aebe21a9af01c94d3157c513775a5ac7f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad3ea1654a496253ccc718f24993bcbf4</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a3381b6c345a33512d2c9fd71ff15e216</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rowIndex</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a1cfe69eab9b8d203888f613aca43ac43</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>columnIndex</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a541994fbf95b8b94d136c329c3446636</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>objectiveOffset</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a07f6e71ad40bb6e954d0794677b26455</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjectiveOffset</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a6dd3043efb18ab2d8734de7d80cb395f</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getProblemName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad3412607a1e9c27091ef6d8a8356f6c1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getObjectiveName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>abb06e920c05b3a013fb97f0c18a75d1f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRhsName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac1b3a9d4323b53b22ca3702c8a78ecfe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getRangeName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a245b6a521e16918a6a9feffd1acc6c0c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getBoundName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aefd1ecc4efb58fc8939b2059433f05a1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberStringElements</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ada00ae534f6c4f2120be32ee7018549d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>stringElement</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a33de67fae0e37d0c5242ac9779f876f5</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMpsData</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a8b632bff092f5ffd8f4658caf3303411</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double infinity, const double *collb, const double *colub, const double *obj, const char *integrality, const double *rowlb, const double *rowub, char const *const *const colnames, char const *const *const rownames)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMpsData</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a41cf078775366f934eb0aceecee7ce1d</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double infinity, const double *collb, const double *colub, const double *obj, const char *integrality, const double *rowlb, const double *rowub, const std::vector&lt; std::string &gt; &amp;colnames, const std::vector&lt; std::string &gt; &amp;rownames)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMpsData</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>af132418628377fa0fe98bf4b553c138b</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double infinity, const double *collb, const double *colub, const double *obj, const char *integrality, const char *rowsen, const double *rowrhs, const double *rowrng, char const *const *const colnames, char const *const *const rownames)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMpsData</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac21e607335f525b5a134106f041bd01c</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double infinity, const double *collb, const double *colub, const double *obj, const char *integrality, const char *rowsen, const double *rowrhs, const double *rowrng, const std::vector&lt; std::string &gt; &amp;colnames, const std::vector&lt; std::string &gt; &amp;rownames)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyInIntegerInformation</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aa27329153df14b7b1f80140f92b5ed46</anchor>
      <arglist>(const char *integerInformation)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setProblemName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>abb71546477f56294e30f7fafb34961de</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjectiveName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a89cd53f4212e910b8e32775ec05a8314</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInfinity</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a51a616cb7d96a48fb90b32a30cc9f656</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getInfinity</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>afa3bcea1f837328b338ea49344befbed</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDefaultBound</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a525834166fb9ec2a5ec1e35662b57ef4</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDefaultBound</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac12e0fa4ce0ac51de754cacf60997808</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>allowStringElements</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a5822073a2521dac739ddc1912532da8f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAllowStringElements</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac13774f500d4b12f4b4d7ca7177ad7f3</anchor>
      <arglist>(int yesNo)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getSmallElementValue</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a30276b15d3ff608289893193d7eefb87</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSmallElementValue</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ab2d8c97a7cb22ce200f08937045816f8</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFileName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>af42186ed8de7f64c45e0716ebbe4cba2</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getFileName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a62c12a9a66091c5805bed1434a4b5a93</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a1ecc185060d9da986081c2bccab539f8</anchor>
      <arglist>(const char *filename, const char *extension=&quot;mps&quot;)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aff860de8c7bc749d25045303d39db055</anchor>
      <arglist>(const char *filename, const char *extension, int &amp;numberSets, CoinSet **&amp;sets)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>af5d138c0be1e3ea6d563b4d5cc51c0ee</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a9de52234635d5f5882f05f0027a9593a</anchor>
      <arglist>(int &amp;numberSets, CoinSet **&amp;sets)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readBasis</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a00cec245b361cd4351b861fec5c5bb6a</anchor>
      <arglist>(const char *filename, const char *extension, double *solution, unsigned char *rowStatus, unsigned char *columnStatus, const std::vector&lt; std::string &gt; &amp;colnames, int numberColumns, const std::vector&lt; std::string &gt; &amp;rownames, int numberRows)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readGms</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>abc60f3bc529255de66b2f1208171013c</anchor>
      <arglist>(const char *filename, const char *extension=&quot;gms&quot;, bool convertObjective=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readGms</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>af5c78a7bccc4713f26465e68fe21d011</anchor>
      <arglist>(const char *filename, const char *extension, int &amp;numberSets, CoinSet **&amp;sets)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readGms</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ae8d896d9b605e93efd55a644bfe3f06c</anchor>
      <arglist>(int &amp;numberSets, CoinSet **&amp;sets)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readGMPL</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a8b8370b68e455a992cbbd2572a0f25ea</anchor>
      <arglist>(const char *modelName, const char *dataName=NULL, bool keepNames=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a1fa1346728804ddba8a66dfea15c2323</anchor>
      <arglist>(const char *filename, int compression=0, int formatType=0, int numberAcross=2, CoinPackedMatrix *quadratic=NULL, int numberSOS=0, const CoinSet *setInfo=NULL) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinMpsCardReader *</type>
      <name>reader</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>acf35f7b99a655cf1194a6940e5943c45</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readQuadraticMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a83c27c17b4f6a2639e490438d5c302ab</anchor>
      <arglist>(const char *filename, int *&amp;columnStart, int *&amp;column, double *&amp;elements, int checkSymmetry)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readConicMps</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>abca50f488b8920b7efe8263e92ea24bd</anchor>
      <arglist>(const char *filename, int *&amp;columnStart, int *&amp;column, int *&amp;coneType, int &amp;numberCones)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setConvertObjective</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a6b4627ac648d996be23a62263eceb516</anchor>
      <arglist>(bool trueFalse)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>copyStringElements</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a9fc87faf2a0b94e8dcce380953a5c21b</anchor>
      <arglist>(const CoinModel *model)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMpsIO</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a37c291192ec7bc5bda605f438d8349bd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinMpsIO</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a5e37d0414403a4b03060f590953f7e8c</anchor>
      <arglist>(const CoinMpsIO &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinMpsIO &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a71bb80e9283c78f177b641b4995d5ecf</anchor>
      <arglist>(const CoinMpsIO &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinMpsIO</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>afbdbb4b7818f07d0b21811fd1c64896b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>passInMessageHandler</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad0d00ea5b5db4acb10c889e2fbf462f4</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>newLanguage</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a2864310ce5cf30e88ad49b1feca4d20d</anchor>
      <arglist>(CoinMessages::Language language)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLanguage</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a2103a322d78bd7cda5276b8c3bf131bb</anchor>
      <arglist>(CoinMessages::Language language)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler *</type>
      <name>messageHandler</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aad8802cabcba8f33e8489961b8315322</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinMessages</type>
      <name>messages</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aefa681800dc88aa3c0befdd25726f0b9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinMessages *</type>
      <name>messagesPointer</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a4cc57c34734d7cf4876e7299b717798b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseRedundantInformation</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a495f2e99a6cdc54b3211f4f712652f86</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseRowInformation</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac0db887193fec59a1bf4bba2c41d8064</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseColumnInformation</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a145d2df1ad0c3fb478f66f1841f9b5c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseIntegerInformation</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a22908ccf52ba1461dd7de07fced7637a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseRowNames</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a8717b73d04b1ddfd67c18db07faa7734</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseColumnNames</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad49c195f6eacc98d05879755d65225f5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>releaseMatrixInformation</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a6a7f2fa98a4f6a4712b43562d75fc4e6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>setMpsDataWithoutRowAndColNames</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>afc839b922cb15396b087a86947919329</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, const double infinity, const double *collb, const double *colub, const double *obj, const char *integrality, const double *rowlb, const double *rowub)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>setMpsDataColAndRowNames</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aab5804fd84de7ca93c8aa4c351cd3d95</anchor>
      <arglist>(const std::vector&lt; std::string &gt; &amp;colnames, const std::vector&lt; std::string &gt; &amp;rownames)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>setMpsDataColAndRowNames</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>af6621b711944bbc6719b86e96d49cab8</anchor>
      <arglist>(char const *const *const colnames, char const *const *const rownames)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a9680b8558f5cfbdf7e81716197534002</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac4b9a4b03b79d4843e38a3296022e1fd</anchor>
      <arglist>(const CoinMpsIO &amp;)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>freeAll</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aa70dba7037188e12b762a712fa7f49b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>convertBoundToSense</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac39c50a0d29889dc8ceaea056db7e8a9</anchor>
      <arglist>(const double lower, const double upper, char &amp;sense, double &amp;right, double &amp;range) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>convertSenseToBound</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a73e3b7b26c6149e2b524f5f368f2d500</anchor>
      <arglist>(const char sense, const double right, const double range, double &amp;lower, double &amp;upper) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>dealWithFileName</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a0ad86eabaa42551c8b52272169b8a7a2</anchor>
      <arglist>(const char *filename, const char *extension, CoinFileInput *&amp;input)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>addString</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a09ca8874692f0ee7f0082befaef52e71</anchor>
      <arglist>(int iRow, int iColumn, const char *value)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>decodeString</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a84312f29a5f81fc8e5dfb89e66b5fb38</anchor>
      <arglist>(int iString, int &amp;iRow, int &amp;iColumn, const char *&amp;value) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>startHash</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a9a08ad50469b2c712035944ac0d2e45c</anchor>
      <arglist>(char **names, const int number, int section)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>startHash</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a41fc03d75eea2ef39b5a55cfab02b66c</anchor>
      <arglist>(int section) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>stopHash</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a35ef32836b55e832b283b354958ca87e</anchor>
      <arglist>(int section)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>findHash</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac82e45eef6b7d1556d5bdcd656f2eaca</anchor>
      <arglist>(const char *name, int section) const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>problemName_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a8f377120f6827201ead7d24f7102b290</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>objectiveName_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aec246317beef245ca80c7ea42c09bf85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>rhsName_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a931811dda82d0ab28b80ec52eabe9170</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>rangeName_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a902fa748df5dc82ef43a2ce38f3c0bc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>boundName_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a288bef1840b3d80f4569329dbb956270</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberRows_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>adf62c1e637c5227adf156c122f3ad198</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberColumns_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>adf19be906c1b9c309cc488d687d5be17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>numberElements_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aeb7c5b5e33c5d9f89daadd4bdbb086dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>rowsense_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>adc159b7ed83cce6931d199aa73d4fcda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rhs_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aa921bede228bcbbf8e942f79b12d8b83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rowrange_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a6075a8694d702ad5d03cd622654fe228</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinPackedMatrix *</type>
      <name>matrixByRow_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ac4f81620c4a9fea03e59d07866ec02e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinPackedMatrix *</type>
      <name>matrixByColumn_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad37a3529c4afebf0035e5f210e2036b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rowlower_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aa78adbb1baaf01cc4de6fccb1b8044e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>rowupper_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a6e71a98e74c5fac5248f13f6b58afcd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>collower_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a65c335ca57b2d888489657858594c115</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>colupper_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a62b5e67333ea0e288b9ac67bbcb2af30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>objective_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aaa09e2f3f35c0b0b1dddd9976d63d2d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>objectiveOffset_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aaae6332b292d628c65448633155fa420</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>integerType_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aa9156d6c64e6064e9e5de9062b773698</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char **</type>
      <name>names_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a9e101f3eeaa8ea65482745a2dae542c5</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>fileName_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a5174b1390af1484d9b60b2099b93c0d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberHash_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aadd0b79755d66b3555c82e3cbfda6539</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinHashLink *</type>
      <name>hash_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a7f4921f69ea313e71ba07285acfb203c</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>defaultBound_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a0286eae321d4ee0ac5b34319a4c9b11c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>infinity_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aff47e6b9551ffa4aaf0c9406b7456fa6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>smallElement_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a81b8b79deea4056764ac69affa488735</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessageHandler *</type>
      <name>handler_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a022dea94877c2f0274a1d25e7b390e97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>defaultHandler_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a5936a15e0dffcd6247dce508e3028ce2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMessages</type>
      <name>messages_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad0dc1829e7258e0d1f7c2fe1828d2d37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinMpsCardReader *</type>
      <name>cardReader_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a8f7d114670cda3646e65e6fd78efe95f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>convertObjective_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a014f91c3c6418b7a3da69c57784541f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>allowStringElements_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ad8a72cc60734adfc2ed611cb080c47b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maximumStringElements_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a0af0d9c660a59b5c6095f323f4b434b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberStringElements_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>a72331d9d6e276f261aef868a9d3782a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char **</type>
      <name>stringElements_</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>aa2066263c7fd3ce95e909288ed42a047</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinMpsIOUnitTest</name>
      <anchorfile>classCoinMpsIO.html</anchorfile>
      <anchor>ab0da282e1cbdcfba60febd791a541c27</anchor>
      <arglist>(const std::string &amp;mpsDir)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinMpsIO::CoinHashLink</name>
    <filename>structCoinMpsIO_1_1CoinHashLink.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>index</name>
      <anchorfile>structCoinMpsIO_1_1CoinHashLink.html</anchorfile>
      <anchor>a66818e11c899513f7ec88ea9c85be4ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>next</name>
      <anchorfile>structCoinMpsIO_1_1CoinHashLink.html</anchorfile>
      <anchor>a9140033ab220287170e8b35e396e51a8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinOneMessage</name>
    <filename>classCoinOneMessage.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinOneMessage</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>adebcf36b3749a037c34a0649f5b13e3d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinOneMessage</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>aaf2033129ad0c222af35fe6c5790513f</anchor>
      <arglist>(int externalNumber, char detail, const char *message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinOneMessage</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a4fb2fb5679472070cfd851fa0384035a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinOneMessage</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>af7c1ee4ed42d309b075b28267ca1192f</anchor>
      <arglist>(const CoinOneMessage &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinOneMessage &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>ae8a904fd9e83e26fc9313a94a98d8302</anchor>
      <arglist>(const CoinOneMessage &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>replaceMessage</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a58a6cc21f8e212229ccfe928939f50cb</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>externalNumber</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>aa725e679cc8d6389fbe4d581a8c036b6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setExternalNumber</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a48fa5741eb4edb0618ad0aa2a3ec8f8b</anchor>
      <arglist>(int number)</arglist>
    </member>
    <member kind="function">
      <type>char</type>
      <name>severity</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a68896e953966c227856280895880cc90</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDetail</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a78d6246733441b0ba8cc4450ba9e9d2e</anchor>
      <arglist>(int level)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>detail</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>acec2ac53a5aedfb02b0c76b790f29fff</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>message</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>ad1ae97d7e2b30018ca6aea576fea7269</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>externalNumber_</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>acc89ebd3d246982ef68e1fcd16059f23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>detail_</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a0464de7b899b938b93e6b6852bf768e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>severity_</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>ab1a1e7f2d4705682ac36b40b7cc8ddd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>message_</name>
      <anchorfile>classCoinOneMessage.html</anchorfile>
      <anchor>a5f6a981e1d5dcd542a4c40de37d290e3</anchor>
      <arglist>[400]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinOslFactorization</name>
    <filename>classCoinOslFactorization.html</filename>
    <base>CoinOtherFactorization</base>
    <member kind="function">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ad378de9d4393aecc67052f106be21a3e</anchor>
      <arglist>(bool clearFact=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfInitialize</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a2f41a605b9a035c600846331160dde27</anchor>
      <arglist>(bool zapFact=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a826b4dab47216a677b78dc3151096726</anchor>
      <arglist>(const CoinOslFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinOslFactorization</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a064465de199e7b3250e7f8af026ba76a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinOslFactorization</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ae565f22e933b06bff0d0b3757390f8a2</anchor>
      <arglist>(const CoinOslFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinOslFactorization</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>aa5f1aa2faac7925b6e442c15ee2b7714</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinOslFactorization &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a3324156b606e2e32e2633194a4c47f91</anchor>
      <arglist>(const CoinOslFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinOtherFactorization *</type>
      <name>clone</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a6facc612cbea98939566f70768d19d06</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getAreas</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a2f730d8861e868c9593b079aead9d48d</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex maximumL, CoinBigIndex maximumU)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>preProcess</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a1cfc5bca8f3e8cf48521be9d27751c9b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>factor</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a40231b5155928532caedf9e86e4eeb75</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>postProcess</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ae3391432c36304ca219d6cfb66556207</anchor>
      <arglist>(const int *sequence, int *pivotVariable)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>makeNonSingular</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>aec168f304033b6e702b8c25ea4d1d50e</anchor>
      <arglist>(int *sequence, int numberColumns)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>factorize</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>accef33070e3d676123ec5d2a08377d63</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, int rowIsBasic[], int columnIsBasic[], double areaFactor=0.0)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>numberElements</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ab5abef394a462e1cba1a41ef56ceb34b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinFactorizationDouble *</type>
      <name>elements</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a50e9481a7ad9df1bef0d247cb1817b06</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>pivotRow</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>af3f63c4186b5897c500055100a7ace1a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinFactorizationDouble *</type>
      <name>workArea</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a09526447f48418e4af6365c435b38bc2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>intWorkArea</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a40fa85bd09215e901bbd715b6ea96e6c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>numberInRow</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a9da74e30f0857988e26b9b569718268e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>numberInColumn</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>acc310017d0b47a778bfcc19b8828d8c0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinBigIndex *</type>
      <name>starts</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ab28a661399161dffebfbbd96149e1bec</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>permuteBack</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a2a06fd6658ff5c1244d95e60d3646834</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>wantsTableauColumn</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ae988f46067bee3628dafdaf629c4201a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setUsefulInformation</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>af05bbea2521707a2092ce2d546af4aaa</anchor>
      <arglist>(const int *info, int whereFrom)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>maximumPivots</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ae29d797f75c194b1abb9cf204863f15b</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>maximumCoefficient</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a0319e2c1c6f7a4bce6a9249fae18dbb9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>conditionNumber</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a9620bd15afb989408dd234950c5400f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>clearArrays</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a2b300cb295257d1310cf3e69eb4ebfbb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>replaceColumn</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a31fc3ef50355fcf35b4964b49a773337</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int pivotRow, double pivotCheck, bool checkBeforeModifying=false, double acceptablePivot=1.0e-8)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumnFT</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ad52ac2f1dd165b50a8f0cbcd13270b39</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumn</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a722bb70e56c4bb43de76581c24554e9d</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateTwoColumnsFT</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a81a713a00721a8121fded9ca5ecdf927</anchor>
      <arglist>(CoinIndexedVector *regionSparse1, CoinIndexedVector *regionSparse2, CoinIndexedVector *regionSparse3, bool noPermute=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumnTranspose</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a6d1210e836b3c326b1143c8abd80955c</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>indices</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a40116e83168f4a218e7da693c6c19b55</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>permute</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a86b5da620f88f658696e57223e1286ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>checkPivot</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a82866c5238f38dbcb935e6ad058a4891</anchor>
      <arglist>(double saveFromU, double oldPivot) const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>EKKfactinfo</type>
      <name>factInfo_</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>ad22aee25a49897d93aa44265aa4ab1ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinOslFactorizationUnitTest</name>
      <anchorfile>classCoinOslFactorization.html</anchorfile>
      <anchor>a5241a09a2b4837cc6c71f8f86be3de4c</anchor>
      <arglist>(const std::string &amp;mpsDir)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinOtherFactorization</name>
    <filename>classCoinOtherFactorization.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual CoinOtherFactorization *</type>
      <name>clone</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a9858b54173b9d247c9f6eba4b3ee1d50</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>clearArrays</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>af3122b3f3ce0b882851ec2d7aeebb5c5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int *</type>
      <name>indices</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a16450f04d0482e2ef72ada538b0987e9</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int *</type>
      <name>permute</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a244aea677011d5807190a16aa1c2b2b4</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>numberElements</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a10ded9066934ecb2561f8803ff5efb53</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>getAreas</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>acb4c8c61f910dd8a733bed8da3b555d0</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex maximumL, CoinBigIndex maximumU)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>preProcess</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>ab39033bf6b53301ca416d01e8e17e18c</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>factor</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>af28bad5c2ccbed67052703304b06a75d</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>postProcess</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a97c3527f72f4787aad43b26b1c4b76c3</anchor>
      <arglist>(const int *sequence, int *pivotVariable)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>makeNonSingular</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a62e6c329e76038d6370e54831bc90e48</anchor>
      <arglist>(int *sequence, int numberColumns)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>replaceColumn</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>aca5dee1ba5b23ee09a58dfbd5d7b3151</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int pivotRow, double pivotCheck, bool checkBeforeModifying=false, double acceptablePivot=1.0e-8)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>updateColumnFT</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>aa73984283de07d80acc44b3c2dfddc8d</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>updateColumn</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a7eeb0a4c08bbfb9788cfbe8437d8d4e5</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>updateTwoColumnsFT</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a67408168c6bfbee45e4e6472921e76f9</anchor>
      <arglist>(CoinIndexedVector *regionSparse1, CoinIndexedVector *regionSparse2, CoinIndexedVector *regionSparse3, bool noPermute=false)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>updateColumnTranspose</name>
      <anchorfile>classCoinOtherFactorization.html</anchorfile>
      <anchor>a9ff9ec4c1fba59c1f95e5aa563441733</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2) const =0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPackedMatrix</name>
    <filename>classCoinPackedMatrix.html</filename>
    <member kind="function">
      <type>double</type>
      <name>getExtraGap</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aec19e827425a25e2b2f5344b34d7aba1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getExtraMajor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6eb9746ced0ffb12bd22e62ada7091c3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserve</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6f2c62bdd2f0e15c582e232294144640</anchor>
      <arglist>(const int newMaxMajorDim, const CoinBigIndex newMaxSize, bool create=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aeace140b7b1cbdaefcfe1e3966ef6784</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isColOrdered</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab41f11feb09aacbcb92b2dee61687973</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>hasGaps</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>abe5c05f88a2ff11aadca7895f12244a4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>getNumElements</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6b90bc4de71a9114a7ba5bda8dc0181f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumCols</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a004b368c95a3dc123622f57d4e32c633</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumRows</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a9b71b862957fc8832574bd484f63c80c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getElements</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a8057ffe31eacb969b4eccdb99fb2e14c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ac7c35995bdc9c1a1e9442cdae69ca3c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getSizeVectorStarts</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a163da34001f706e0362ff0d3f3335e81</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getSizeVectorLengths</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6989305a517dc474d9fbe6d99e59a3ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinBigIndex *</type>
      <name>getVectorStarts</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab56aafd1df8d44a01e8d11e85c45e752</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getVectorLengths</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a9bb4a6d15c4b02d7f125f9867c3488c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>getVectorFirst</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad3edbd86e2d4d24a201300403c758954</anchor>
      <arglist>(const int i) const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>getVectorLast</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a648dc932ab526415ff2a93a0afdc170a</anchor>
      <arglist>(const int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getVectorSize</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a57df9626b4ed14d55a465137179598ab</anchor>
      <arglist>(const int i) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinShallowPackedVector</type>
      <name>getVector</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa9fbebb0d5541db2a204966b48acb61d</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>getMajorIndices</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a73305551b5ad87525f69f5759ed6bf19</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDimensions</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a1b63e9ffc940a55b927abec0240be2cd</anchor>
      <arglist>(int numrows, int numcols)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setExtraGap</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a83e3d34599cd8b4f88712f7fa5e8daee</anchor>
      <arglist>(const double newGap)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setExtraMajor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a98b476cb993b1a4204d013680e625a5d</anchor>
      <arglist>(const double newMajor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendCol</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a7d5536b2e697c4ea54c70b92c8918e65</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendCol</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a645ef23fce2b19d93365cd8ad4cf4739</anchor>
      <arglist>(const int vecsize, const int *vecind, const double *vecelem)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendCols</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa1bc89d33c5b04ac8db724b4a8ef1c14</anchor>
      <arglist>(const int numcols, const CoinPackedVectorBase *const *cols)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>appendCols</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab6f8da2da7a4a52b6260235335530f8b</anchor>
      <arglist>(const int numcols, const CoinBigIndex *columnStarts, const int *row, const double *element, int numberRows=-1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendRow</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a7165dd99b16c8a26f3774198e16e2fea</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendRow</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a8af6449df995e04d23711ec18b4129fb</anchor>
      <arglist>(const int vecsize, const int *vecind, const double *vecelem)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendRows</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a10a7bc251f7aec2e38469936d0d508e1</anchor>
      <arglist>(const int numrows, const CoinPackedVectorBase *const *rows)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>appendRows</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6802cf18c879b7e33732af16eb7f4a0d</anchor>
      <arglist>(const int numrows, const CoinBigIndex *rowStarts, const int *column, const double *element, int numberColumns=-1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>rightAppendPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af7487b1422721f4c25149602e26f8673</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>bottomAppendPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>addcb47c9ec1957af5be32ab01fdfd224</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteCols</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af6c2708b075eeb4151e976c698c4fbb5</anchor>
      <arglist>(const int numDel, const int *indDel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteRows</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>acbbbeca5bd806aa0cdfe4baada443ba2</anchor>
      <arglist>(const int numDel, const int *indDel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>replaceVector</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a18aed65965d86299d6b9d0cc378a6541</anchor>
      <arglist>(const int index, const int numReplace, const double *newElements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>modifyCoefficient</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af1ddf65b41190599e169a5bfbbd3ead5</anchor>
      <arglist>(int row, int column, double newElement, bool keepZero=false)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getCoefficient</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a224d51f09a8d58b8b6253a08028a516a</anchor>
      <arglist>(int row, int column) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>compress</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a88ac7ca6ca4003c3d9a6ad5883bd37b5</anchor>
      <arglist>(double threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>eliminateDuplicates</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a1049e0775c779f851ce96d246d341496</anchor>
      <arglist>(double threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>orderMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab14c4ea525078afafd358781ac54045a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>cleanMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ae622fe4a7d48a34a4a447cc83e596f59</anchor>
      <arglist>(double threshold=1.0e-20)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>removeGaps</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aba328e11f0de1928d53e962f23eaf7fa</anchor>
      <arglist>(double removeValue=-1.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>submatrixOf</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>afe8aedda19a17eff020201dd8867ad8f</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const int numMajor, const int *indMajor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>submatrixOfWithDuplicates</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aac1d9e74dc9b29dfc1ad1bc48fb76b20</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const int numMajor, const int *indMajor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyOf</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a5b7e4c8f87812a287676a23cc22f832e</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyOf</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>adb1988ad13070345575d348d50c3d253</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const CoinBigIndex numels, const double *elem, const int *ind, const CoinBigIndex *start, const int *len, const double extraMajor=0.0, const double extraGap=0.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyReuseArrays</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a4370b9a2992f1e3ebbe9294055a543b6</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reverseOrderedCopyOf</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aabee2687807cdf8c723ddc9247dbcda1</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a716c5b3653bb6a919a46ba298b8b194e</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const CoinBigIndex numels, double *&amp;elem, int *&amp;ind, CoinBigIndex *&amp;start, int *&amp;len, const int maxmajor=-1, const CoinBigIndex maxsize=-1)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedMatrix &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a76c95cc884e53e4aa63a2f829c72a4c4</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reverseOrdering</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a28bf8e24c6f617a6c35a6ef765c0267e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>transpose</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aeeeeea63dcf68f90fbeb895298fe1df2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ac26f5fce66511f0e71e4284b7244bb54</anchor>
      <arglist>(CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>times</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a9720e6552100069784d7cc141c9900c0</anchor>
      <arglist>(const double *x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>times</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a30951e0d4c5c47eb1e324831af65a1c4</anchor>
      <arglist>(const CoinPackedVectorBase &amp;x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>transposeTimes</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a87c76c0870bfc73aad9f2055ce5010e9</anchor>
      <arglist>(const double *x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>transposeTimes</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6c706dc5c65c0821fb53d3ce19fb708e</anchor>
      <arglist>(const CoinPackedVectorBase &amp;x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>countOrthoLength</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a42cd6fcae4a8b78defd4b2c81ed65489</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>countOrthoLength</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a60403ee2ec418a84afd1a1e97a9771bc</anchor>
      <arglist>(int *counts) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMajorDim</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a7eb9a47b2901a240511e7383a9883657</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMajorDim</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a74a478b5a5fafad6c958280b45e8114d</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMinorDim</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a3dc04d925a08fe6bd95ffd2786a35e95</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMinorDim</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa6dd5a0b045e5f598551edac13033dde</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMaxMajorDim</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a328db3923f293e8be6c7ae950fd65f0e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dumpMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aebbd0588745e3ceaabdedc72387c19d5</anchor>
      <arglist>(const char *fname=NULL) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printMatrixElement</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad02ca4a325e353f2677f28c92576704a</anchor>
      <arglist>(const int row_val, const int col_val) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMajorVector</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a648cfa7781cd971e06eae902c4465454</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMajorVector</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad2922674c0b5d7f2aa25e8be9db95f78</anchor>
      <arglist>(const int vecsize, const int *vecind, const double *vecelem)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMajorVectors</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a8eec7b82c676a247497f197333794e28</anchor>
      <arglist>(const int numvecs, const CoinPackedVectorBase *const *vecs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMinorVector</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a99e0eefe3d62daa677d3f1079be712ca</anchor>
      <arglist>(const CoinPackedVectorBase &amp;vec)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMinorVector</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a494033dcce072f337f61e5dc80d2153b</anchor>
      <arglist>(const int vecsize, const int *vecind, const double *vecelem)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMinorVectors</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad3e865b2c72fffe86db9bf5ad14d14de</anchor>
      <arglist>(const int numvecs, const CoinPackedVectorBase *const *vecs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendMinorFast</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab0833899e9f235c090c6f280a8a7b123</anchor>
      <arglist>(const int number, const CoinBigIndex *starts, const int *index, const double *element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>majorAppendSameOrdered</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a24ccf7d57957f9764a1c36f47f97f513</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>minorAppendSameOrdered</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a40d52e9b4eb43fa7ea98ddb8264ca602</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>majorAppendOrthoOrdered</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a5182e5d305f9a86aa9f9310e2b991282</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>minorAppendOrthoOrdered</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>acee6d8e0a29986ffa2776a7d5accb514</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteMajorVectors</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a8602b344a2e7ff9b5fbe623ad3a13120</anchor>
      <arglist>(const int numDel, const int *indDel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteMinorVectors</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a78c66100bfa60957d5c9ac9c1e582fe8</anchor>
      <arglist>(const int numDel, const int *indDel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>timesMajor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a1faad5ec6fd67e225f3d4d31be252968</anchor>
      <arglist>(const double *x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>timesMajor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa08dc8c91979a557c1f16db46161daa4</anchor>
      <arglist>(const CoinPackedVectorBase &amp;x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>timesMinor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>adf54c1cc78d273a343116865b93f4c10</anchor>
      <arglist>(const double *x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>timesMinor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a5c223515329f509b205d8e4a48dc749c</anchor>
      <arglist>(const CoinPackedVectorBase &amp;x, double *y) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a3a1282797d07dcccfee7c56bd257c57f</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs, const FloatEqual &amp;eq) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent2</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a208239a1be9dab345643d2ec1e10cde0</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ac96abafe777cbdd400038bdc47c65747</anchor>
      <arglist>(const CoinPackedMatrix &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>getMutableElements</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a3faa1820a3ddde750ec0c7a5699a8ec7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>getMutableIndices</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a1a114b35ab3ae47beb98982040c53a5c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex *</type>
      <name>getMutableVectorStarts</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a37651245b07ede8fba035f256c15fc28</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>getMutableVectorLengths</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa43d176229bfca68dcb2c97564d9ff45</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumElements</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a7238d113be874326f3582ac4c28383e3</anchor>
      <arglist>(CoinBigIndex value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nullElementArray</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a0679772effe09df3f9109b1a5c38523b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nullStartArray</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af33be6c43d9b70eab632718ee006b68e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nullLengthArray</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a2fe29028955df7b03ed392ff329ac3be</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>nullIndexArray</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a16b1d1382a0f0738a1161dab84e2886c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad1fa09e351531684e2c20c6ceb7f84de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ac37b61c530629bb584d90c0338559147</anchor>
      <arglist>(const bool colordered, const double extraMajor, const double extraGap)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ae52e4f64642aa484c1256f288e9bf699</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const CoinBigIndex numels, const double *elem, const int *ind, const CoinBigIndex *start, const int *len, const double extraMajor, const double extraGap)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af44f25f36d6bd121d784f805a0477b90</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const CoinBigIndex numels, const double *elem, const int *ind, const CoinBigIndex *start, const int *len)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aea43ff86d8de5913172c47cfd0e23c36</anchor>
      <arglist>(const bool colordered, const int *rowIndices, const int *colIndices, const double *elements, CoinBigIndex numels)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab96fd1b7fafd745e46ea314a7c2ed9b4</anchor>
      <arglist>(const CoinPackedMatrix &amp;m)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa66824be9525e5a8dcf46b0893bc022e</anchor>
      <arglist>(const CoinPackedMatrix &amp;m, int extraForMajor, int extraElements, bool reverseOrdering=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a6caf6379823b3eeb946bf9df23b49cff</anchor>
      <arglist>(const CoinPackedMatrix &amp;wholeModel, int numberRows, const int *whichRows, int numberColumns, const int *whichColumns)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinPackedMatrix</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a4e2f01003e8bfb993365e9416ffb1de4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>verifyMtx</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a1f12116416e1b39a30daeabe6d44c438</anchor>
      <arglist>(int verbosity=1, bool zeroesAreError=false) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a4c2936ffc034ef41b22c019a75ac55c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfCopyOf</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ae652c7e30fc318d502e8cf8df3bfe7f9</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const CoinBigIndex numels, const double *elem, const int *ind, const CoinBigIndex *start, const int *len, const double extraMajor=0.0, const double extraGap=0.0)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfCopyOfNoGaps</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af3206af9919adaec741d53dd01c9dcff</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const double *elem, const int *ind, const CoinBigIndex *start)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfOpEqual</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aa8a1ce5e056990f3655b3b79be379ab5</anchor>
      <arglist>(const bool colordered, const int minor, const int major, const CoinBigIndex numels, const double *elem, const int *ind, const CoinBigIndex *start, const int *len)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>resizeForAddingMajorVectors</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>add032f3ea6b607cca8072687f187a969</anchor>
      <arglist>(const int numVec, const int *lengthVec)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>resizeForAddingMinorVectors</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a1e735d613b22d727587c7e743e81a466</anchor>
      <arglist>(const int *addedEntries)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>appendMajor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af95417e565d537df0d5b6ff41d43bbf1</anchor>
      <arglist>(const int number, const CoinBigIndex *starts, const int *index, const double *element, int numberOther=-1)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>appendMinor</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a559caedc837c363e308dcefc663add6a</anchor>
      <arglist>(const int number, const CoinBigIndex *starts, const int *index, const double *element, int numberOther=-1)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>colOrdered_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad39d5efd36270cbbed06b29386dc6f03</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>extraGap_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>aca5a49e8305ee36b2bc3ecfd8c1ee156</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>extraMajor_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a822fa4a7d286b11f9b2f82e03a142516</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>element_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a371b60b4cd972ede8f4f59424950e493</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>index_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a95b117d727282f12d2866b72d96a7115</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex *</type>
      <name>start_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>af6088f6eba2d36e9d01c05c8ec8f0169</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>length_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a4329b56325773ceb261a3133ae80f987</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>majorDim_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>afbb3e4fc653470b7c2f6a1788ef7095d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>minorDim_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a2f2e323fb5aa8c89991c66b496f7b833</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>size_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a0e19f209173f03b950652477fc3a2ca7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maxMajorDim_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>a2bfd01623e7d2b80f78c51d09ecfd84f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CoinBigIndex</type>
      <name>maxSize_</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ad045812e72b0e66599a2df728e5af0a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinPackedMatrixUnitTest</name>
      <anchorfile>classCoinPackedMatrix.html</anchorfile>
      <anchor>ab822014d6298cf60c98adcb559d1d987</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPackedVector</name>
    <filename>classCoinPackedVector.html</filename>
    <base>CoinPackedVectorBase</base>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a2cf41486149c3bef4af297baec8a9c6c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a1a1f68f2cee972b24a2fad1b64056ada</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getElements</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a48889b40f87817393dd87e644c7e56dc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a1ee7c2e9d9a4e6c3149cf42347a0b696</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getVectorNumElements</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>ab8ca45438c962dd8325d6609ace212d3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getVectorIndices</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a0a02b50401d10c30f04eaa7e901b5f56</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getVectorElements</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a2037c43cab5dc668a513c731304a90ef</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>getElements</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a0b3a74838f2461380f85974aa827d4e6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getOriginalPosition</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a792daa013d0d14a67470d2b81cac2371</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a009dba1b2ff09674708722522da8d9a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a5baa4b22d40648a1d5e478e32d46292c</anchor>
      <arglist>(const CoinPackedVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinPackedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a35fa5d6c7fc69d63114c8cd348821635</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>aed86dc50cb3faaf90159b2ccc24c0c20</anchor>
      <arglist>(int size, int *&amp;inds, double *&amp;elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a1db69076fe0829f1ee8dbbf1e3e2fc1c</anchor>
      <arglist>(int size, const int *inds, const double *elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setConstant</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a26898ab3c8f6e4e061efb5ccc34f4709</anchor>
      <arglist>(int size, const int *inds, double elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFull</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a3561f070138c2248dc8b5352ac0b12a6</anchor>
      <arglist>(int size, const double *elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFullNonZero</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a4c24c435570e562af56542ddb074adc1</anchor>
      <arglist>(int size, const double *elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setElement</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a7ff4fa28e1093b5055fe9a465374f162</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>insert</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a57efbd8f7df05af51f703ba45f293fe1</anchor>
      <arglist>(int index, double element)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>append</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a2641f031a186cb1f439964dcd0c78e89</anchor>
      <arglist>(const CoinPackedVectorBase &amp;caboose)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a88dfdb5f95e02e66d3cf5332601f94f9</anchor>
      <arglist>(int i, int j)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>truncate</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a998983f1ab0b050576ca8baa7b726e4d</anchor>
      <arglist>(int newSize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator+=</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>ae3293bcf715733beb70490db485bb30e</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator-=</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a5cbd3b2e2ed092a6b32fce903d0336e0</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator*=</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a2d9c0ca20857ac2cdc273b2485d66311</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>operator/=</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a97f317713bdeddcdd25d9051281a2af8</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sort</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a73921585354eeb8d97b79dbdf921ae6a</anchor>
      <arglist>(const CoinCompare3 &amp;tc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortIncrIndex</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>af33f81085ae75486f927d7e636f6df5f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortDecrIndex</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a281aa099e5784655ef55ee58b8f0af77</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortIncrElement</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a750d2adfccb09946132238f30d783b8d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortDecrElement</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>aa258812d2ddbc9a93b0c022baeb02d83</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortOriginalOrder</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>ad44591a2d31a95ce679df6747665c8e5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserve</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>ae14febcc1ba3465a8409f0c8f1eadddd</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>capacity</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>ac116ea44f0882dcd2127ba632c0a7464</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a72e605d8e426a3a6dba6f810cd8c3c38</anchor>
      <arglist>(bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>afe4875a500c7b3b93c68c2b4204a7c2a</anchor>
      <arglist>(int size, const int *inds, const double *elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a73f29bd0fe7afe595f546a2fd5d2b075</anchor>
      <arglist>(int capacity, int size, int *&amp;inds, double *&amp;elems, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a08717cd3aca8be8fb0cc225780d472f3</anchor>
      <arglist>(int size, const int *inds, double element, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>af0a0b941baf1c09d054b13f44696f294</anchor>
      <arglist>(int size, const double *elements, bool testForDuplicateIndex=COIN_DEFAULT_VALUE_FOR_DUPLICATE)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>ac540a00ce8422708f03a843cf416e208</anchor>
      <arglist>(const CoinPackedVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a4fb057230b82040b5ee7f59fb0f05050</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinPackedVector</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>aaadbaaaef10b412ba7c25cb36414cb40</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTestForDuplicateIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>af4de219b35d657e98d8c29717eea685a</anchor>
      <arglist>(bool test) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTestForDuplicateIndexWhenTrue</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>afdbdba348a35ea278eac7f899cb7a962</anchor>
      <arglist>(bool test) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>testForDuplicateIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a4fc888040e249dde69141ba4c6cf29cf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTestsOff</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a28d4e42be4ece336e4e015bcb3f5dd8d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double *</type>
      <name>denseVector</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a40898d89eb2d7bf0e0cde9c1a7634f24</anchor>
      <arglist>(int denseSize) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>operator[]</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a13e109857530d9f02a0b07349610cd14</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMaxIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>ae7f70f2aad863f9234c744ae3c571233</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMinIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a1660ea519668438aea03e7a4f33ed315</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>duplicateIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a5a0a017bf2ffe35870d59d98149b8d55</anchor>
      <arglist>(const char *methodName=NULL, const char *className=NULL) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isExistingIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a5e961eb67c1ae8ff36500b22ea4d8756</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>af90a03aced74e5874436499e7965651f</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a1ef8aafa65073376a2b6eb418c886728</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>aadd476437bce23e583022d82c947a4f2</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>compare</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>aaf51f05c5fc1de8faf792e935392fd94</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a9ab7cbf00c6dffc2c986460b46296741</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs, const FloatEqual &amp;eq) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEquivalent</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>ae4ba4bf760cda6b0c7e6b4772ef9ea3c</anchor>
      <arglist>(const CoinPackedVectorBase &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>dotProduct</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a4dc175cf43ed21d4f6b0701d4b41ef16</anchor>
      <arglist>(const double *dense) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>oneNorm</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>add42c820274f01e2fcfebd55325360b7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>normSquare</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a64d53f9cf099541f4214d01bdf5e8917</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>twoNorm</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a68f3ca3cfb2a0f4cfeadc587d934294c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>infNorm</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a953944328dd20eeddbe640770b404152</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>sum</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a3ee476e0754a433f3b55bea96a69c0c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinPackedVectorBase</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a03b554f2e65911157c5c22fe217affe9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinPackedVectorUnitTest</name>
      <anchorfile>classCoinPackedVector.html</anchorfile>
      <anchor>a7aa27fc9fa40e3fcfea3c639d0530b33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>findMaxMinIndices</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a9318fba446c6e70981938996b6658080</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>std::set&lt; int &gt; *</type>
      <name>indexSet</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>ad8069aea73ac0d5db8d90f3751e6f1b9</anchor>
      <arglist>(const char *methodName=NULL, const char *className=NULL) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>clearIndexSet</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a0841f8a33397b2e03401a8d6c7893903</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>clearBase</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>af1d7e4e0b161c262fc199b7292879403</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>copyMaxMinIndex</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a1c8b0030c402bedb41fe7540e1ea6666</anchor>
      <arglist>(const CoinPackedVectorBase &amp;x) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinPackedVectorBase</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>acad56ee67c7dc22e35177a4db617506b</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPackedVectorBase</name>
    <filename>classCoinPackedVectorBase.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a65d73fc887025247e6fa3597f9b73b78</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a2c64a28cd9dd017a487e46a3351f355c</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const double *</type>
      <name>getElements</name>
      <anchorfile>classCoinPackedVectorBase.html</anchorfile>
      <anchor>a53e937ca748faf4951fb00d87a234f0e</anchor>
      <arglist>() const =0</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinPair</name>
    <filename>structCoinPair.html</filename>
    <templarg>S</templarg>
    <templarg>T</templarg>
    <member kind="function">
      <type></type>
      <name>CoinPair</name>
      <anchorfile>structCoinPair.html</anchorfile>
      <anchor>adab844006ee2b7f17e2351aa3543783f</anchor>
      <arglist>(const S &amp;s, const T &amp;t)</arglist>
    </member>
    <member kind="variable">
      <type>S</type>
      <name>first</name>
      <anchorfile>structCoinPair.html</anchorfile>
      <anchor>a828052600873e456420a019de8c5adde</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>T</type>
      <name>second</name>
      <anchorfile>structCoinPair.html</anchorfile>
      <anchor>add50afed176e87c8756b14fbf0f4d8be</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinParam</name>
    <filename>classCoinParam.html</filename>
    <member kind="enumeration">
      <type></type>
      <name>CoinParamType</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>coinParamInvalid</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070af2ab46cb0368a5e727f0f2e728c23623</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>coinParamAct</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070a5e10f16971482d0c3aa8ab8f6796073e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>coinParamInt</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070a5792e74a86968042f47d871e1547265c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>coinParamDbl</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070a8e82dd755ab5dfcd1f55afc8ebff25b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>coinParamStr</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070acffd89e8a1e7e840ec22facd03ca099b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>coinParamKwd</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a60d26b7002047f5828ee1f4f200a3070a4ef00b99335b3642153cdf8812473038</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int(*</type>
      <name>CoinParamFunc</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a2948f6fdcb9626771c70992b077658a5</anchor>
      <arglist>)(CoinParam *param)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ae5edada506fd3509b0373780ad8837e3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a455b46924dbc18de3eb1cd07e9c39a6e</anchor>
      <arglist>(std::string name, std::string help, double lower, double upper, double dflt=0.0, bool display=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a49f4682425fe0200bd0758732751e0d6</anchor>
      <arglist>(std::string name, std::string help, int lower, int upper, int dflt=0, bool display=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a6f967d3283297189b18eea2003db2aea</anchor>
      <arglist>(std::string name, std::string help, std::string firstValue, int dflt, bool display=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a99dac5d0907b01d4b3b8d40efb3f778f</anchor>
      <arglist>(std::string name, std::string help, std::string dflt, bool display=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a82426692069ecb83a4e635c4ee22de39</anchor>
      <arglist>(std::string name, std::string help, bool display=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a93db3a67f27d722e76c7d1d0ec3feab7</anchor>
      <arglist>(const CoinParam &amp;orig)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinParam *</type>
      <name>clone</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>adb0fe1c601a4893a203d79cfce43ba93</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinParam &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aca661e70838ee833758aa2cb2556adba</anchor>
      <arglist>(const CoinParam &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a8729faa88b70b569507840054bbaf6d2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendKwd</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a555d09a3825f88eef97e5daa176d9168</anchor>
      <arglist>(std::string kwd)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>kwdIndex</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a6e198df2a38bb139703d2568501500bd</anchor>
      <arglist>(std::string kwd) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>kwdVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a1417859993c16d910e49a709f43cc672</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setKwdVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a6473ac4cae00e27b6b49c5bbcb6fde46</anchor>
      <arglist>(int value, bool printIt=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setKwdVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a92a719f34a5738c55ce50ced603749c8</anchor>
      <arglist>(const std::string value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printKwds</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aac4147b42dba08407f750f71b3f5470d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStrVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a1375bb6e6e08c464bd70140e686e5ee5</anchor>
      <arglist>(std::string value)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>strVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a4f8ff227d80234fdd4c0414f9fb1cd86</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDblVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aa745130465baad9e2255afa9aa22a0d5</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>dblVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ac0c95338c1bd55e2065f97d7c57b9e4e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIntVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ad079727d0eddd69ec572f55c34c7f9fc</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>intVal</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a0e3b077bb6f3949d2c1fb3e2b0224b16</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShortHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ab13d08f5fca8c2546e666665fb6737c0</anchor>
      <arglist>(const std::string help)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>shortHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aaf001260f92e6d85b81885c12669f02a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLongHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a0d081439478c716d6ccee883de4fa771</anchor>
      <arglist>(const std::string help)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>longHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a849568b8bbbabe423434cb4afe0d9106</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printLongHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a298c2bca33edceeb682508452417a3ed</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinParamType</type>
      <name>type</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>afd0ef03cbc1a866f958613fe2484dd1b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setType</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a7d3685ac3523d86478d42e3a604b542c</anchor>
      <arglist>(CoinParamType type)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>name</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>abeae8f9310ff007b75013d8b282770f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setName</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a450660e48b25bb16c6c46634f4449391</anchor>
      <arglist>(std::string name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>matches</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a0d03e153c6f6358a71fda7f6274f0485</anchor>
      <arglist>(std::string input) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>matchName</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aaf407ddeb577e2fb6a7c01ce98243345</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDisplay</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a811a98c947b580e6a175499bcfc55860</anchor>
      <arglist>(bool display)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>display</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a57c3e04559965c09df4e945a1a6e1019</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinParamFunc</type>
      <name>pushFunc</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a73412734211314216fb908725d0d993f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPushFunc</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a377bd8961e22ff9fe5c1492988b10cf7</anchor>
      <arglist>(CoinParamFunc func)</arglist>
    </member>
    <member kind="function">
      <type>CoinParamFunc</type>
      <name>pullFunc</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a29ab30fa0d7ab92afebc5caf24540f93</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPullFunc</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a16df75257e9ec568f1496d6393da4cdf</anchor>
      <arglist>(CoinParamFunc func)</arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; CoinParam * &gt;</type>
      <name>CoinParamVec</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a497f18802a41daf5e1d48ca640b9f03b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aadf7216bb05ff20f3078f7796f23694c</anchor>
      <arglist>(std::ostream &amp;s, const CoinParam &amp;param)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInputSrc</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ae0ef0ee0f0e42df13dbdd9a84d904511</anchor>
      <arglist>(FILE *src)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isCommandLine</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a2cfcf5bc33c45a1b28b538cf53327d89</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteractive</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ae87322eec4fe85a0642579a3d988bd47</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getStringField</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aedcbab3f6aae92467a32a8cafb4089d2</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getIntField</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a2392dec81656c42aedab3b8c9b63e86e</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getDoubleField</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>acd51550d805264ee0557c55e6d47efe2</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>matchParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a2b85d788a859575de83e1e1eb2aa78d5</anchor>
      <arglist>(const CoinParamVec &amp;paramVec, std::string name, int &amp;matchNdx, int &amp;shortCnt)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getCommand</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>aee581da3d8ce0d3ffe5bcb4b9512528b</anchor>
      <arglist>(int argc, const char *argv[], const std::string prompt, std::string *pfx=0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lookupParam</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>adea5d3f966a67b28be6b1e8f85c57f42</anchor>
      <arglist>(std::string name, CoinParamVec &amp;paramVec, int *matchCnt=0, int *shortCnt=0, int *queryCnt=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printIt</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a89c7c2cf8022931a30b1f57ee7bd1826</anchor>
      <arglist>(const char *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shortOrHelpOne</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ac74f9ba5b0aef6e88f326a64347a3bb6</anchor>
      <arglist>(CoinParamVec &amp;paramVec, int matchNdx, std::string name, int numQuery)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shortOrHelpMany</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>ab03dd3f7961d5d237782ed3a462d54af</anchor>
      <arglist>(CoinParamVec &amp;paramVec, std::string name, int numQuery)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printGenericHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a8872f16bc6d65d7d3892816452ed4d82</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printHelp</name>
      <anchorfile>classCoinParam.html</anchorfile>
      <anchor>a1b6f32a5974021f3f25e94c608889a91</anchor>
      <arglist>(CoinParamVec &amp;paramVec, int firstParam, int lastParam, std::string prefix, bool shortHelp, bool longHelp, bool hidden)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPartitionedVector</name>
    <filename>classCoinPartitionedVector.html</filename>
    <base>CoinIndexedVector</base>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>aabec2e84ff0a5950638833d9625ab2a1</anchor>
      <arglist>(int partition) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumPartitions</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a52683ef9f8ffed426c33016481303048</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a682fc951abf0213dcbd1e1da5bbfccb9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>startPartition</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a6a43ea5069ee03b9b59cf723417b1579</anchor>
      <arglist>(int partition) const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>startPartitions</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>ac94a51f931bef8423fa4c8b0330ea786</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumElementsPartition</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>aaecff125882ed7e3449157ab73c03dbd</anchor>
      <arglist>(int partition, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTempNumElementsPartition</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a931f4faac86aa1212cab6b75a59a4f6a</anchor>
      <arglist>(int partition, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>computeNumberElements</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a0d7089fa88db50a6a7ece069e8fa0d9b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>compact</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a514dc379ec48b1e82e1ea477632e3d74</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reserve</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a642a7ec3bff058829458afbe31e4797a</anchor>
      <arglist>(int n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPartitions</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a83962ac995000604aa0ea0aedb252d5b</anchor>
      <arglist>(int number, const int *starts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearAndReset</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>ad83ea4ace3e16af87b00efc58f978e65</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearAndKeep</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>accd03cb62553d26c827735dc34e1b0d1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearPartition</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a8238f180e95316d32df1736d018ed885</anchor>
      <arglist>(int partition)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkClear</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>aa585873b17acbab350e0f8de5a0ec3e1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkClean</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a965cef7975806e11372206e486abeff0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>scan</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a673a302dcf630bc4ff72554fd18b8e0a</anchor>
      <arglist>(int partition, double tolerance=0.0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a7445cd171203bd6903b2155b28c8d01b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sort</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a712303fa6d0cf75fb75eceafa4cf58b6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a041cee18ca4253e0fdab50111b98b0df</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a8e79161dd75b73ccbc9a5f9295eb1422</anchor>
      <arglist>(int size, const int *inds, const double *elems)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a9c2d30995195727b82fca009f532344e</anchor>
      <arglist>(int size, const int *inds, double element)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>ac211e9541cff2b9a29401590ac33d420</anchor>
      <arglist>(int size, const double *elements)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>ab15e09623dec183e9e3b1f0477bda236</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a12a213b2e42e02d71cd2ca80579cb6a5</anchor>
      <arglist>(const CoinPartitionedVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a0e7ad616d91cc3749490f735e81d1798</anchor>
      <arglist>(const CoinPartitionedVector *)</arglist>
    </member>
    <member kind="function">
      <type>CoinPartitionedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a10b72e6be3e9a506e358b611111874a4</anchor>
      <arglist>(const CoinPartitionedVector &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinPartitionedVector</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a46e73f73b7ed4520f543659c0702e137</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>startPartition_</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a087f071af51f499d0af466d068d7d86b</anchor>
      <arglist>[COIN_PARTITIONS+1]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberElementsPartition_</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>ade6c342ad772afc1d6ab51d7a42af2cf</anchor>
      <arglist>[COIN_PARTITIONS]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberPartitions_</name>
      <anchorfile>classCoinPartitionedVector.html</anchorfile>
      <anchor>a7b765dd47dd1eb9d2cafac466fdaa623</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPostsolveMatrix</name>
    <filename>classCoinPostsolveMatrix.html</filename>
    <base>CoinPrePostsolveMatrix</base>
    <member kind="function">
      <type></type>
      <name>CoinPostsolveMatrix</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a7da68d51d217b18ab8d72d4f1f2e48f8</anchor>
      <arglist>(int ncols_alloc, int nrows_alloc, CoinBigIndex nelems_alloc)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPostsolveMatrix</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a0e7ea8de3740b1c871c5ae1509a184c1</anchor>
      <arglist>(ClpSimplex *si, int ncols0, int nrows0, CoinBigIndex nelems0, double maxmin_, double *sol, double *acts, unsigned char *colstat, unsigned char *rowstat)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPostsolveMatrix</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a9702d018605d15f82d01d35a8e2a190c</anchor>
      <arglist>(OsiSolverInterface *si, int ncols0, int nrows0, CoinBigIndex nelems0, double maxmin_, double *sol, double *acts, unsigned char *colstat, unsigned char *rowstat)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignPresolveToPostsolve</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a7a72a9affe6370e8feb593aee554b1f6</anchor>
      <arglist>(CoinPresolveMatrix *&amp;preObj)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinPostsolveMatrix</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a216d68019818730a89305cd3744de809</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>check_nbasic</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>ac2a39a227852513aa308300f159ea12c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPrePostsolveMatrix</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ac05b0052a2f4a7aa808a059b27e73f76</anchor>
      <arglist>(int ncols_alloc, int nrows_alloc, CoinBigIndex nelems_alloc)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPrePostsolveMatrix</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a3266bdb05f0141912e39e3b5487fc481</anchor>
      <arglist>(const OsiSolverInterface *si, int ncols_, int nrows_, CoinBigIndex nelems_)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPrePostsolveMatrix</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a518aeac9aabb6dfce47a261778fc76c1</anchor>
      <arglist>(const ClpSimplex *si, int ncols_, int nrows_, CoinBigIndex nelems_, double bulkRatio)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinPrePostsolveMatrix</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a42342bbba9829593c1f76ae5c65ed154</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>afda0c7ac2c5b48093d19d9a07daf2c7e</anchor>
      <arglist>(int sequence, Status status)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>getRowStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a0b6ff586ba85771619d8cfc54b25652c</anchor>
      <arglist>(int sequence) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>rowIsBasic</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a8b63188e86b37215e62340e38a6b3ceb</anchor>
      <arglist>(int sequence) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa1a6f6ab5f78d239e1a9a42772d409e4</anchor>
      <arglist>(int sequence, Status status)</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>getColumnStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a143ef93126be1e777ec7a20b0bb29726</anchor>
      <arglist>(int sequence) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>columnIsBasic</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ac811dacee21b6fc1de18e4724391c12b</anchor>
      <arglist>(int sequence) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowStatusUsingValue</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a40ab63f9b2fe2a8787e586c25c844576</anchor>
      <arglist>(int iRow)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnStatusUsingValue</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a93082aa9ac1fe7cae4dfdbbccc511623</anchor>
      <arglist>(int iColumn)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStructuralStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a80048af2a86422486f7736e05cf95e31</anchor>
      <arglist>(const char *strucStatus, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setArtificialStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aeb5ee92d0711ceeed44f1a2d388b9aaa</anchor>
      <arglist>(const char *artifStatus, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a6f975d041a4ff7c0c017c3e888ea9e93</anchor>
      <arglist>(const CoinWarmStartBasis *basis)</arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStartBasis *</type>
      <name>getStatus</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa5115703bf18a80aa2b2f586eed9ea80</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>columnStatusString</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aab55e05a4728c14fa0ce75a8beff4540</anchor>
      <arglist>(int j) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>rowStatusString</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ab91036faba433808b30ff80f27859ac1</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjOffset</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a52b22232894384988804f5275ff297e7</anchor>
      <arglist>(double offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjSense</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a8e62e2e68fb879fc48104b77fe547c9b</anchor>
      <arglist>(double objSense)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPrimalTolerance</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a19f75ede650e4354d8678b7037208d52</anchor>
      <arglist>(double primTol)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDualTolerance</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>acb8cc99c567a2fbecd4f29d92d1f375a</anchor>
      <arglist>(double dualTol)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColLower</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a489624035abc9f981342b45036183bf2</anchor>
      <arglist>(const double *colLower, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColUpper</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ab20f7858300bb8c8d891560845607289</anchor>
      <arglist>(const double *colUpper, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColSolution</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>afc2068b288b7a62cd85342cc90bbb6d5</anchor>
      <arglist>(const double *colSol, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCost</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a2d92bec886fe18442e96e2f4053d6f09</anchor>
      <arglist>(const double *cost, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setReducedCost</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>afd2c9aa70f95f00fb2792edf47fabcfe</anchor>
      <arglist>(const double *redCost, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowLower</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a26bc1e976e0797c6aa3eae7c10af57d0</anchor>
      <arglist>(const double *rowLower, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowUpper</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a211eeaa4fd0d309446619d870d121f51</anchor>
      <arglist>(const double *rowUpper, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowPrice</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>add4f143c1bd9a42c39c84905e3e37468</anchor>
      <arglist>(const double *rowSol, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowActivity</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ae57e6769d96ad291ee247469eb393e04</anchor>
      <arglist>(const double *rowAct, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumCols</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>af19f712ca91c6842aea1be56ef69c6d5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumRows</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ad69a41056e45e1ca9cd97bec251cea9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumElems</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a1180ebf137484d81805ee90f5b1f2252</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinBigIndex *</type>
      <name>getColStarts</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a7d6a5e7087f4e08f75135de6fd57fb03</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getColLengths</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a3791759985f884abd841d0a4028f9157</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getRowIndicesByCol</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ad6fb6198764371d57e2880258edf2c28</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getElementsByCol</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>abfef5b93f02e0aba91cb6b026125b6b9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColLower</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a66c9247f6401739324019b96b6d78e38</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColUpper</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aeabf5919faee05c3ff0b239edda2b585</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getCost</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a855343d054b5c4239fcdb7da678e9ea2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowLower</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a6d9cbc9786a2d07b9c53492aff3f77a9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a481fb6e2b60d2305c1d44adce072a50a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColSolution</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a69f4c771e0e9786632f291420bb2b05a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a4aeed7f890f85bf551f8d9a20d92a79e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ac9c30cebcefe4f016e774895e5787664</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ad7e65c1b8abd6cbfef0a72950ae03e5a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countEmptyCols</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a1732cf45ca156c96bf3f6110ce8b8d7b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinMessageHandler *</type>
      <name>messageHandler</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a43a6c86d5820f2f39a0a505ad1bf77eb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMessageHandler</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ae335f4e27b9515532c7cf331266c5214</anchor>
      <arglist>(CoinMessageHandler *handler)</arglist>
    </member>
    <member kind="function">
      <type>CoinMessages</type>
      <name>messages</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aee71d9339d3b7ec8c9e650ea08eef680</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex</type>
      <name>free_list_</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a66ea368beea9f9b7424265aab16824f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>maxlink_</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>aaf764c5ca205d298443d9bf43167960c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex *</type>
      <name>link_</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a418de40a02a8db5cf6ef962a316bc65e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>cdone_</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>ad408938504ba1fded708f531cab73ae7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>rdone_</name>
      <anchorfile>classCoinPostsolveMatrix.html</anchorfile>
      <anchor>a28a4be43ad9ca734bc4af96c6c25de29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncols_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a0e09a64af996c7cfad3d8911d1bc1903</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nrows_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a2e9c5f53f716fdabe88fa210b49e73e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex</type>
      <name>nelems_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a2bc5e7c2d9a10b99f6cd3ba6ca10dc6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncols0_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a6a37c7d0fc9e58b60e442edb966cee87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nrows0_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a4456b311a92c4afc8b589a13c609eb53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex</type>
      <name>nelems0_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a69457fbc65723d73bbb60900c12b7b0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex</type>
      <name>bulk0_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a29aafba59d7cf80314fdd986a40d9edf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>bulkRatio_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a86269bff3976d9bc1c4c29dc245cb1a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex *</type>
      <name>mcstrt_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aedf7855180e382d9dcba398efbfc7cbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hincol_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>adabcfd45750974f6dadb925d3198f0cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hrow_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>af54b74d05f3ffff10be12a6fa03c0d2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>colels_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>acb883d4a0d9856970dbb120737acdad1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>cost_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a1fb4cc840d2d2a619566afd7d8f34981</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>originalOffset_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ad9fd7b23a337989bd77bd72e1484f589</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>clo_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a5b080359d92a64e0d435a3a90c31082b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>cup_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a1604fde98bc50cb0668ba377a63aa16e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rlo_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a0554de318be95a246e713f1247d62cf9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rup_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>af8b4a7d97c9a884d006724dbe121bbf1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>originalColumn_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa95c722999bcae82b19a503acdce582b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>originalRow_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a6fc96b3db782f6d17c6addff35861320</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ztolzb_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a55098c91845a179937500338683ad7fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ztoldj_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a24114d11e2d69ba84eda4f96f00cad3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>maxmin_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a0eb0b5bc9531222c5bcb411a26715533</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>sol_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a2fde1699d45fda4cd6d625a6990d1c45</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowduals_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ae4168def10bb6f8bcd4656e957e51b28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>acts_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ae86049bd8c64aff80605e035ecc41528</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rcosts_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ab9b22f1432fc58ff3749bb12dfa3a2c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned char *</type>
      <name>colstat_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ac333f4ade74850ae444b585665b0003c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned char *</type>
      <name>rowstat_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aff3fd7dcce1859653951ecdabc6e2277</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinMessageHandler *</type>
      <name>handler_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>ab19604064dfc70464d425f4a84148361</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>defaultHandler_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a00ae8f1cbe16b141f474e09b806dcef8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinMessage</type>
      <name>messages_</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>a7511f2796278c2056540b3308a2c9032</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_col</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga018bcaad92a79e499daf55fa54a6e329</anchor>
      <arglist>(int col, CoinBigIndex krs, CoinBigIndex kre, const int *hcol)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga30af6b47600ca5711fe0c0e72e903e6e</anchor>
      <arglist>(int tgt, CoinBigIndex ks, int majlen, const int *minndxs, const CoinBigIndex *majlinks)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga4cd185437ad58ab99f3184a0e9744642</anchor>
      <arglist>(int row, CoinBigIndex kcs, int collen, const int *hrow, const CoinBigIndex *clinks)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor3</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gaf9e637f7d36420ffcd00954d0b9ff0be</anchor>
      <arglist>(int tgt, CoinBigIndex ks, int majlen, const int *minndxs, const CoinBigIndex *majlinks)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row3</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gaa83a1921b0a08e55eef82ad11d88cb2a</anchor>
      <arglist>(int row, CoinBigIndex kcs, int collen, const int *hrow, const CoinBigIndex *clinks)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_major2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gae37816e7aefc0987d2615f4096741b48</anchor>
      <arglist>(int majndx, int minndx, CoinBigIndex *majstrts, int *majlens, int *minndxs, int *majlinks, CoinBigIndex *free_listp)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_col2</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gab57089922e7b366093e8710965c136ab</anchor>
      <arglist>(int row, int col, CoinBigIndex *mcstrt, int *hincol, int *hrow, int *clinks, CoinBigIndex *free_listp)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_threads</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga98a64ab94fccface6907127dfe6e4a84</anchor>
      <arglist>(const CoinPostsolveMatrix *obj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_free_list</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga5e846245e3997fbed40b1c9b799128fc</anchor>
      <arglist>(const CoinPostsolveMatrix *obj, bool chkElemCnt=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_reduced_costs</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga2140877787eaaba96f1247a20ac101db</anchor>
      <arglist>(const CoinPostsolveMatrix *obj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_duals</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga240fa3c02f7b334fd2df38b7270349eb</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_sol</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga030acbac86d29771b727e346348842f4</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj, int chkColSol=2, int chkRowAct=2, int chkStatus=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_nbasic</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>gaec17518929b43c598d12f80ab0dcc480</anchor>
      <arglist>(const CoinPostsolveMatrix *postObj)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>statusName</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aad6c3ded1d2b0eba2fff0deb2d06f05d</anchor>
      <arglist>(CoinPrePostsolveMatrix::Status status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_make_memlists</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga4fdf8c2f470833e457bd453857eb5e2d</anchor>
      <arglist>(int *lengths, presolvehlink *link, int n)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>presolve_expand_major</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga85ad25aa69c9d62dedc940811f109061</anchor>
      <arglist>(CoinBigIndex *majstrts, double *majels, int *minndxs, int *majlens, presolvehlink *majlinks, int nmaj, int k)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>presolve_expand_col</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga26285b7734fb16ee66d24fa2ec795aa1</anchor>
      <arglist>(CoinBigIndex *mcstrt, double *colels, int *hrow, int *hincol, presolvehlink *clink, int ncols, int colx)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>presolve_expand_row</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gace2b703fbe1d91a62902a74336134590</anchor>
      <arglist>(CoinBigIndex *mrstrt, double *rowels, int *hcol, int *hinrow, presolvehlink *rlink, int nrows, int rowx)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gaa6df2e05d828f7246d4f6cf69ecdd34f</anchor>
      <arglist>(int tgt, CoinBigIndex ks, CoinBigIndex ke, const int *minndxs)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga6603257c54c40ed5d5535ba9c9304a4f</anchor>
      <arglist>(int row, CoinBigIndex kcs, CoinBigIndex kce, const int *hrow)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_minor1</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gabdfb2edeb344e51aca0a0551d16d4746</anchor>
      <arglist>(int tgt, CoinBigIndex ks, CoinBigIndex ke, const int *minndxs)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_row1</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga59c73121ef1cb7f99cc9dab144899349</anchor>
      <arglist>(int row, CoinBigIndex kcs, CoinBigIndex kce, const int *hrow)</arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>presolve_find_col1</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga779051a846dcaa1745d5aa8df878ed9c</anchor>
      <arglist>(int col, CoinBigIndex krs, CoinBigIndex kre, const int *hcol)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_major</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gad732d9c80c4a53d9d0b2117f848d6952</anchor>
      <arglist>(int majndx, int minndx, const CoinBigIndex *majstrts, int *majlens, int *minndxs, double *els)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_many_from_major</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>gab0c9ad6aed6f01af891ad404fd5a4a34</anchor>
      <arglist>(int majndx, char *marked, const CoinBigIndex *majstrts, int *majlens, int *minndxs, double *els)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_col</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga891f9ad85b434ad8cb0387290a04c0a7</anchor>
      <arglist>(int row, int col, const CoinBigIndex *mcstrt, int *hincol, int *hrow, double *colels)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_delete_from_row</name>
      <anchorfile>group__MtxManip.html</anchorfile>
      <anchor>ga6adbb54310f32482247aaf19f0baf903</anchor>
      <arglist>(int row, int col, const CoinBigIndex *mrstrt, int *hinrow, int *hcol, double *rowels)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>Status</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa103148594116d4889514cc8a3464280</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>isFree</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa103148594116d4889514cc8a3464280ac0c7bdb1b872ed4f547096849b9542a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>basic</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa103148594116d4889514cc8a3464280a97cb8ba567db0b5bd6cceb65dba65cfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>atUpperBound</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa103148594116d4889514cc8a3464280a914cf79a3e0c9b456a3fecd9fed01bf5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>atLowerBound</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa103148594116d4889514cc8a3464280a498941cbb41568eb88dbd7d71ee168d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>superBasic</name>
      <anchorfile>classCoinPrePostsolveMatrix.html</anchorfile>
      <anchor>aa103148594116d4889514cc8a3464280a81ab946629425eeb5b02b7602c01a542</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPrePostsolveMatrix</name>
    <filename>classCoinPrePostsolveMatrix.html</filename>
  </compound>
  <compound kind="class">
    <name>CoinPresolveAction</name>
    <filename>classCoinPresolveAction.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinPresolveAction</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>af46054f481c24be2a80a9106bcc8132b</anchor>
      <arglist>(const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNext</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>a8a04cfe528b455468fe8d5333b730e2e</anchor>
      <arglist>(const CoinPresolveAction *nextAction)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const char *</type>
      <name>name</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>a48019a927c518ef3a7655469530456c6</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>postsolve</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>a7f5dd640a0b6fd260946e055f3eb2535</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinPresolveAction</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>a166e9a398b418a06cc7bb8c201626d28</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>throwCoinError</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>a8a6a3658eb450b93709fe78f689671da</anchor>
      <arglist>(const char *error, const char *ps_routine)</arglist>
    </member>
    <member kind="variable">
      <type>const CoinPresolveAction *</type>
      <name>next</name>
      <anchorfile>classCoinPresolveAction.html</anchorfile>
      <anchor>ad3d9739db2130d42da2a9901221d61ec</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPresolveMatrix</name>
    <filename>classCoinPresolveMatrix.html</filename>
    <base>CoinPrePostsolveMatrix</base>
    <member kind="function">
      <type></type>
      <name>CoinPresolveMatrix</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>abbceb4417de74c7f92e52598df63af51</anchor>
      <arglist>(int ncols_alloc, int nrows_alloc, CoinBigIndex nelems_alloc)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPresolveMatrix</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ab5df6afba98cfff90c554050b6e62ce9</anchor>
      <arglist>(int ncols0, double maxmin, ClpSimplex *si, int nrows, CoinBigIndex nelems, bool doStatus, double nonLinearVariable, double bulkRatio)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>update_model</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a0829d5ff6d6046ce5d61729a436685f6</anchor>
      <arglist>(ClpSimplex *si, int nrows0, int ncols0, CoinBigIndex nelems0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPresolveMatrix</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a374774b6d55412fbe9147cf6a906ea53</anchor>
      <arglist>(int ncols0, double maxmin, OsiSolverInterface *si, int nrows, CoinBigIndex nelems, bool doStatus, double nonLinearVariable, const char *prohibited, const char *rowProhibited=NULL)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>update_model</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a5c788525d18a00e8af40471beb614992</anchor>
      <arglist>(OsiSolverInterface *si, int nrows0, int ncols0, CoinBigIndex nelems0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinPresolveMatrix</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a30cad12765b14b6565a1a1292b46162f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>change_bias</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a0cccabe0d3702a4ee7ce3683cca4fb90</anchor>
      <arglist>(double change_amount)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>statistics</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a4f0f4d9e4bf58a0816e27fef6047b0da</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>feasibilityTolerance</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ad080ad320e741cd6708ae4a02cda26d1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFeasibilityTolerance</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aaceaed25261293d6cfb7b462d3a5bed7</anchor>
      <arglist>(double val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>status</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aa6694128cdb68acdd7b854fdd238205e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStatus</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a5957634d10727a80531118e758707ce8</anchor>
      <arglist>(int status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPass</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a95f15a6319bdfe470338ec613e3cbc7f</anchor>
      <arglist>(int pass=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMaximumSubstitutionLevel</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a16eec5e7d8196c7c26e502592a450cc2</anchor>
      <arglist>(int level)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>recomputeSums</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a352a8cc3c05157fde467988e8b9b991a</anchor>
      <arglist>(int whichRow)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initializeStuff</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a709af07e90bef9395ccc78dbf6e00f16</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteStuff</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aafcf14a0e6f3ae6bef0326f3861ac5c1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMatrix</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a87b3a8a1892c3a556aa13767dd9c8529</anchor>
      <arglist>(const CoinPackedMatrix *mtx)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>countEmptyRows</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>af89d125c1e9d50e75eebfe6ddee13c33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVariableType</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a54c2721140070c23ced0d3d4542a40a4</anchor>
      <arglist>(int i, int variableType)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVariableType</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>af2841c36d44aab3848a84593c5f0ac72</anchor>
      <arglist>(const unsigned char *variableType, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVariableType</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aa3b269f97331ad121f241aec649f58c9</anchor>
      <arglist>(bool allIntegers, int lenParam)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAnyInteger</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aff6e58a39876f5f3d9cf06889d92affa</anchor>
      <arglist>(bool anyInteger=true)</arglist>
    </member>
    <member kind="function">
      <type>const CoinBigIndex *</type>
      <name>getRowStarts</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a54fad22a842c84c6fa4870c7cf72bab0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>getColIndicesByRow</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ac1212acba5f8b5467ca6f5837f72e259</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getElementsByRow</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a71692e54a3441f46fcae69cf6febb3b9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteger</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ad86a13eeae4e201a920b78767f9b7086</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>anyInteger</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>af6913d6f261503cd773eed69fdfbe09e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>presolveOptions</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a0612844a63e64934ae56ce2405af4f34</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPresolveOptions</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ab5b0f4f1a7224db71977f6fa19f91958</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initColsToDo</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ac79e1b4e9348c3d337c4bf26eb57ebca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stepColsToDo</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a87103ec308c0a8bcdb7ac533f3f903f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColsToDo</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a715352a937aaf71f6d56f2cca7ac6656</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>colChanged</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>af20b39faa21dbd2a053b52f90d98e40d</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unsetColChanged</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a2057cdc4381ab3c15e324a5f874e3f94</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColChanged</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aa771482e3e759272fcdb687e96d36f19</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addCol</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a245ff553bfa1c95d629129045b86c372</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>colProhibited</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a92e0230ab9c2e7c98c3fb8d9e13f1983</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>colProhibited2</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a41e2bcf0a4fd5a6a5882055cff4c4eaf</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColProhibited</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a925925e5ef078a6290617edf94e949a2</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>colUsed</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ac9d1c23dadd45677fdd9e603820f0173</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColUsed</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a288cf117e8a95f026f68361ea2d08657</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unsetColUsed</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a4afd5640eee120b1d79bcea45c0be9df</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>colInfinite</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a3a19e8d9db90ef37d8642195af8d0fb8</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unsetColInfinite</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a1b367d0d1dc110b8532f09014bdea425</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColInfinite</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>acf3c6096afaaa0611446eea5f1e0ff20</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initRowsToDo</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a9ac2f7bc81843f7e12dc7c9f7129b2d9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>stepRowsToDo</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a11b0f85d42323b4521c5d4b68d772ee7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRowsToDo</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ab7e6a23dc8f5032f5f64f7a1e6140c9e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>rowChanged</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a618f36469cef3b194314836d94a03dc3</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unsetRowChanged</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a6c81d4bcf1efb34595d9b142be8e7b82</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowChanged</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a0a1965f906cba3e31d68d63bc400e4c3</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addRow</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aac1b3c8a9076169747e16230aee0d171</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>rowProhibited</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a81c4187a4fc404807b16c3f34d2b322a</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>rowProhibited2</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a6e0ed35bf81dc7d562ec60df10a3e74b</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowProhibited</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a3934a0ba3f968d7bbffbbe3310ed96f3</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>rowUsed</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>abace1e134ebee52de1bf77ef966230b7</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowUsed</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a28654dccba672f27ed572ef9c8204a8e</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unsetRowUsed</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a44e2d82b835db8fc63d379bdba4cb70c</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>anyProhibited</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>af1704d1e82e5ce036cb3d449d254ce4d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAnyProhibited</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a53fffcdb205f5a24faf5282f7638645d</anchor>
      <arglist>(bool val=true)</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>dobias_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a32c172d3f6cef2b8f019f6e456a3968b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned char *</type>
      <name>integerType_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a68e822d19c0e73f35cbfff3b78e3da2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>anyInteger_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a3c7c83a5c3325d43c525ccbc43310f35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>tuning_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aba1977ca2367ef3d5dbedefbc0cf33c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>startTime_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a5548ed337a65d142180e562899880ce3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>feasibilityTolerance_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a100ed915a80d5fd67f4ae72b54c42883</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>status_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a99bd70368723341a0d945c8d67eadd4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>pass_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a9ad09c50b561428070f5a29c561cd9a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>maxSubstLevel_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ad4878a5c7103dd7445521c636318046f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>presolvehlink *</type>
      <name>clink_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a7911a4dbfb1ef2c593d8c3a8f0bb0e9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>presolvehlink *</type>
      <name>rlink_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aec35d54b181db4e01045d937fc21f7a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>CoinBigIndex *</type>
      <name>mrstrt_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a78d805b3c6fb4183a61611faf498a9a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hinrow_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a53350cc2e2c946f496ff428a45190c94</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowels_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a68b07c57319d08bdb6e8eaedad772b12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>hcol_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a22fdaa2146b400fdc1fe57398f39110a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned char *</type>
      <name>colChanged_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>af085b9e5fed50eca3779cd882ede2312</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>colsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ac99150f4db110498674d8c1a4ba05238</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberColsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a6536c142183e8b21ca92b1530110a8df</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>nextColsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ae1f58cc69c7e08906393ac88a7741b2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberNextColsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a416fbca5d0ece3f4e94791bada2bc1fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned char *</type>
      <name>rowChanged_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a7c58107776e9b55aae5df0fff9b2ff03</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>rowsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>adc09b7fa1dde41c0301a36b23fe5b542</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberRowsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a6172dd749be2aa685eee2fb5ea326cb2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>nextRowsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>abac5c83497f9589fc178b1f190803faa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numberNextRowsToDo_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aa9578ff3201ce9ab405e50c06e8e2938</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>presolveOptions_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ab7b1e1cef29e58bb511ba798cd3eb711</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>anyProhibited_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aeed84606bf0fc7ca7d2ea82b15492b4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>usefulRowInt_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aa42c58030bdc655192ae159ebbf7f828</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>usefulRowDouble_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a22d801ee2ae7a45d78b8b043ec0fcef5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>usefulColumnInt_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a714cab4e08189e8ae78fc127492629df</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>usefulColumnDouble_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aeeda0d2f5f65889c4f6d639f2bb48e86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>randomNumber_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>abc4715ed8ed4275891ad00214ba56ba1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>infiniteUp_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>aa619fa7dcacc887599e484c937f476e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>sumUp_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ae6c55769e6347bc084740fe131a29cfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>infiniteDown_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ac0f340ef67cad0e46be3dd32dade2708</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>sumDown_</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>ab82db3400f6bd56a3da442e5400e9f63</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend void</type>
      <name>assignPresolveToPostsolve</name>
      <anchorfile>classCoinPresolveMatrix.html</anchorfile>
      <anchor>a09e5fc9ae8f388cd5db6fec7a6efa0b5</anchor>
      <arglist>(CoinPresolveMatrix *&amp;preObj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_no_dups</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga08e4d8e2b0f3a1a01705706d17cea73d</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool doCol=true, bool doRow=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_links_ok</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga746a411967809a3cc44296dc00fcabe5</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool doCol=true, bool doRow=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_no_zeros</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga44e2694f711758de1fd1207081ed87d2</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool doCol=true, bool doRow=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_consistent</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>gaebff24e6ce6d30373e7243a760601f9d</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, bool chkvals=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_sol</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga0de487312b1bc0b13a65c28f2e4f3f87</anchor>
      <arglist>(const CoinPresolveMatrix *preObj, int chkColSol=2, int chkRowAct=1, int chkStatus=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>presolve_check_nbasic</name>
      <anchorfile>group__PresolveDebugFunctions.html</anchorfile>
      <anchor>ga5b43580c65f5d379ced3c89b6e2d23c8</anchor>
      <arglist>(const CoinPresolveMatrix *preObj)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinPresolveMonitor</name>
    <filename>classCoinPresolveMonitor.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinPresolveMonitor</name>
      <anchorfile>classCoinPresolveMonitor.html</anchorfile>
      <anchor>a57969f75cdfb9cb25e8cf75ac8b7bd6e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPresolveMonitor</name>
      <anchorfile>classCoinPresolveMonitor.html</anchorfile>
      <anchor>ace0b7ad9bf329344057de92df5044606</anchor>
      <arglist>(const CoinPresolveMatrix *mtx, bool isRow, int k)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinPresolveMonitor</name>
      <anchorfile>classCoinPresolveMonitor.html</anchorfile>
      <anchor>a20e75bbc7e85e10bc0e03d10f04de2c7</anchor>
      <arglist>(const CoinPostsolveMatrix *mtx, bool isRow, int k)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkAndTell</name>
      <anchorfile>classCoinPresolveMonitor.html</anchorfile>
      <anchor>a479403b0525cc8c3858dd159bb268564</anchor>
      <arglist>(const CoinPresolveMatrix *mtx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>checkAndTell</name>
      <anchorfile>classCoinPresolveMonitor.html</anchorfile>
      <anchor>add9d6047596d7db0ff54acc364147fcf</anchor>
      <arglist>(const CoinPostsolveMatrix *mtx)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinRational</name>
    <filename>classCoinRational.html</filename>
    <member kind="function">
      <type>long</type>
      <name>getDenominator</name>
      <anchorfile>classCoinRational.html</anchorfile>
      <anchor>a14dc6787a59fc6511239f4f753acba0f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>long</type>
      <name>getNumerator</name>
      <anchorfile>classCoinRational.html</anchorfile>
      <anchor>a265cf6e5033d3c3890aa60234daeba04</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinRational</name>
      <anchorfile>classCoinRational.html</anchorfile>
      <anchor>a1f104a7a137c971984cb112861e3e788</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinRational</name>
      <anchorfile>classCoinRational.html</anchorfile>
      <anchor>a2a379966edd053f81392d3e1abbf4691</anchor>
      <arglist>(long n, long d)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinRational</name>
      <anchorfile>classCoinRational.html</anchorfile>
      <anchor>a00035eca25a1079156ce3a4c2380e813</anchor>
      <arglist>(double val, double maxdelta, long maxdnom)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinRelFltEq</name>
    <filename>classCoinRelFltEq.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>classCoinRelFltEq.html</anchorfile>
      <anchor>aa6d1eec5b4c8e4d3618f111db2bf76e0</anchor>
      <arglist>(const double f1, const double f2) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinRelFltEq</name>
      <anchorfile>classCoinRelFltEq.html</anchorfile>
      <anchor>ad240c88b0a5da1761aae2bba40320188</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinRelFltEq</name>
      <anchorfile>classCoinRelFltEq.html</anchorfile>
      <anchor>ac2ead93cc4a94e24136c082422222877</anchor>
      <arglist>(const double epsilon)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinRelFltEq</name>
      <anchorfile>classCoinRelFltEq.html</anchorfile>
      <anchor>a154fba334c4cc79dc08a4a3ad7e6b9e5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinRelFltEq</name>
      <anchorfile>classCoinRelFltEq.html</anchorfile>
      <anchor>a00cddd2450b31171a738ae801a9e33ba</anchor>
      <arglist>(const CoinRelFltEq &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>CoinRelFltEq &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinRelFltEq.html</anchorfile>
      <anchor>a0ea15accf1d3a1920989e754c5a15297</anchor>
      <arglist>(const CoinRelFltEq &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSearchTree</name>
    <filename>classCoinSearchTree.html</filename>
    <templarg></templarg>
    <base>CoinSearchTreeBase</base>
    <member kind="function">
      <type></type>
      <name>CoinSearchTree</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>a97fcd0f595cacb062719160cba1c50dc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSearchTree</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>a08036108f849c28e2f432f8aa95fe822</anchor>
      <arglist>(const CoinSearchTreeBase &amp;t)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSearchTree</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>aef52af94f6291675abc835ebb09615b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>compName</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>a7f1ec756ad61fbbcdbc31d41e803775f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSearchTreeBase</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>aa69bcb8049bab98e9535412b56a58eca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const std::vector&lt; CoinTreeSiblings * &gt; &amp;</type>
      <name>getCandidates</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a0ff74ea154693ff8964ea6354d044767</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a8b5f240c83392b3a38f069c2470e5119</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>ae84f503e5169e516a4bce8e6a11068fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numInserted</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a97b243446125835d26b8dda9c68fa143</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinTreeNode *</type>
      <name>top</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a95fea113bc69eb2b0080f62aabb34a2a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pop</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a8ac3f8052a1c22b3a98b0f05d90672fb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>aeb2abcf03236e98ccc9535db54a191a5</anchor>
      <arglist>(int numNodes, CoinTreeNode **nodes, const bool incrInserted=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>aae63dbdf9d07e6df543f771d566b1b07</anchor>
      <arglist>(const CoinTreeSiblings &amp;sib, const bool incrInserted=true)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>realpop</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>ae5d0c11184d2f4462e637b2786637a13</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>fixTop</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>a5298c8af524fa98d0c7047bf3fb2c6b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>realpush</name>
      <anchorfile>classCoinSearchTree.html</anchorfile>
      <anchor>a1d1c968767b745af80b0b0df55fe087d</anchor>
      <arglist>(CoinTreeSiblings *s)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinSearchTreeBase</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a9efce88d485488ab135566a262138834</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; CoinTreeSiblings * &gt;</type>
      <name>candidateList_</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>ac5b0b4ca5447ca73ee504732f7b77d64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numInserted_</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a5830acee62d3898cb7e4d5df63f0a583</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>size_</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>aebc727a043c2264dac3dfe401b2674f5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSearchTreeBase</name>
    <filename>classCoinSearchTreeBase.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual const char *</type>
      <name>compName</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a9af1c62c7235541d3efb4ac58d3d4b64</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>realpop</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a32896234e7d4ca1e1a28bdb183c13c78</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>realpush</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>ae0dd0bf1be29a4fb77eaf41cbeac1dce</anchor>
      <arglist>(CoinTreeSiblings *s)=0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>fixTop</name>
      <anchorfile>classCoinSearchTreeBase.html</anchorfile>
      <anchor>a3ad22e666d8bed17257f81bd1ac22f4a</anchor>
      <arglist>()=0</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinSearchTreeCompareBest</name>
    <filename>structCoinSearchTreeCompareBest.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>structCoinSearchTreeCompareBest.html</anchorfile>
      <anchor>ab078149641832d0febd1fd76dc7c4f7e</anchor>
      <arglist>(const CoinTreeSiblings *x, const CoinTreeSiblings *y) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const char *</type>
      <name>name</name>
      <anchorfile>structCoinSearchTreeCompareBest.html</anchorfile>
      <anchor>a519f9125702533f961d7bf280cfa3395</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinSearchTreeCompareBreadth</name>
    <filename>structCoinSearchTreeCompareBreadth.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>structCoinSearchTreeCompareBreadth.html</anchorfile>
      <anchor>a86a4bc2d246cbb67d27068cd80ec9d4d</anchor>
      <arglist>(const CoinTreeSiblings *x, const CoinTreeSiblings *y) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const char *</type>
      <name>name</name>
      <anchorfile>structCoinSearchTreeCompareBreadth.html</anchorfile>
      <anchor>ac76ebd85d25f7270f3eba4dd792a5571</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinSearchTreeCompareDepth</name>
    <filename>structCoinSearchTreeCompareDepth.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>structCoinSearchTreeCompareDepth.html</anchorfile>
      <anchor>a2e4e70b793f50978a5fec824b5718531</anchor>
      <arglist>(const CoinTreeSiblings *x, const CoinTreeSiblings *y) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const char *</type>
      <name>name</name>
      <anchorfile>structCoinSearchTreeCompareDepth.html</anchorfile>
      <anchor>a560f146119836c5a10dec4b0cb9ba3fb</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>CoinSearchTreeComparePreferred</name>
    <filename>structCoinSearchTreeComparePreferred.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>operator()</name>
      <anchorfile>structCoinSearchTreeComparePreferred.html</anchorfile>
      <anchor>ae963fb111d1df973e33c5e42c5ac7244</anchor>
      <arglist>(const CoinTreeSiblings *x, const CoinTreeSiblings *y) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const char *</type>
      <name>name</name>
      <anchorfile>structCoinSearchTreeComparePreferred.html</anchorfile>
      <anchor>afb12056e12d58a067898cbdfa77dc863</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSearchTreeManager</name>
    <filename>classCoinSearchTreeManager.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinSearchTreeManager</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a09daa85b8cef3c601d3de72bc6c3aa5d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSearchTreeManager</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a4ab76a5f408e158e1e091a71efc52792</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTree</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a44d893853bb80aba39428dee6fe4dc9a</anchor>
      <arglist>(CoinSearchTreeBase *t)</arglist>
    </member>
    <member kind="function">
      <type>CoinSearchTreeBase *</type>
      <name>getTree</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a4f255d806146c82e654bc250a2bd6584</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>empty</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a886d4801aebb7f6442f02f4759106143</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>size</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>ac8366c2560295c72cb28f23c5962d30a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>numInserted</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a55e6d508f0a98909e6dc458020bf82f4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinTreeNode *</type>
      <name>top</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a8b024e7c446a3353f90027b6c6809e53</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pop</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a3b4416ad9c055f3dc1d4da71e0adb669</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>ab4e97b17d9cffdac640773ae8957e06d</anchor>
      <arglist>(CoinTreeNode *node, const bool incrInserted=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a7b5f00757eaf7d508c17cfca15f5912d</anchor>
      <arglist>(const CoinTreeSiblings &amp;s, const bool incrInserted=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>push</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a1576658b13d5b3064a026a221e238295</anchor>
      <arglist>(const int n, CoinTreeNode **nodes, const bool incrInserted=true)</arglist>
    </member>
    <member kind="function">
      <type>CoinTreeNode *</type>
      <name>bestQualityCandidate</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>abfc9734df33ddcad881cc35322c892d5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>bestQuality</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>a7573504827402a9959bd2ec08406d8a2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>newSolution</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>aef37d845c55ac66bedd0e9cd22fb29f7</anchor>
      <arglist>(double solValue)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reevaluateSearchStrategy</name>
      <anchorfile>classCoinSearchTreeManager.html</anchorfile>
      <anchor>ae5aead8d75bcb973737d0604afe129cf</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSet</name>
    <filename>classCoinSet.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinSet</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>a8c5bb96bafc0995d2ab17a23239e0674</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSet</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>ac739d81b0952a016eba51269b11776b2</anchor>
      <arglist>(int numberEntries, const int *which)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSet</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>a46bcbde10c236e2b7c8c9e0dadf32a6b</anchor>
      <arglist>(const CoinSet &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinSet &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>ae9fc7f7fb1a277759bc3c139f99c9634</anchor>
      <arglist>(const CoinSet &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSet</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>aafb671c3c064a74548283d98b6a9f904</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberEntries</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>adaab812902cc6b05186ce92590d86371</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>setType</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>a65cc1922dd5f9b1cb5208e371610cceb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const int *</type>
      <name>which</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>ac0c81db37a458ebb417601fa7ced89e0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>weights</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>ac9180cf9cb9a47117ec20622935bde24</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberEntries_</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>afa56271dfcf7d7286510dbf61f5414d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>setType_</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>a59aa03b049b28f0c0ab1096373df256b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>which_</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>ae14229367555777128765bcfbae99df0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>weights_</name>
      <anchorfile>classCoinSet.html</anchorfile>
      <anchor>af4dafd09a988b5002094b6ac60528abe</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinShallowPackedVector</name>
    <filename>classCoinShallowPackedVector.html</filename>
    <base>CoinPackedVectorBase</base>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a065f87bbf0cff635b61a43c83ac554ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const int *</type>
      <name>getIndices</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a07cb2e7632f37cd69b89fe0bbe925010</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const double *</type>
      <name>getElements</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a42baf0b21446a65245c2eb178283610a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a7817ab55411a7a82dcb267443b2aa92a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinShallowPackedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>aff93a1a3f105a299d669a74a571ba605</anchor>
      <arglist>(const CoinShallowPackedVector &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>CoinShallowPackedVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>ab84de222bf4cd0fbec0e521c9eafdff4</anchor>
      <arglist>(const CoinPackedVectorBase &amp;x)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVector</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a5f2cd556e8fdb127ad7e1f80342f6180</anchor>
      <arglist>(int size, const int *indices, const double *elements, bool testForDuplicateIndex=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinShallowPackedVector</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a5bb7590a03cbc3ce19b445a4282ad1dc</anchor>
      <arglist>(bool testForDuplicateIndex=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinShallowPackedVector</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a84b58fab007b1ef868216419bd11ca5a</anchor>
      <arglist>(int size, const int *indices, const double *elements, bool testForDuplicateIndex=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinShallowPackedVector</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>abaa33dd21387d53773332ca6f76c6986</anchor>
      <arglist>(const CoinPackedVectorBase &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinShallowPackedVector</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a4bf885bea60b51c4d3ea99a432f5d0bf</anchor>
      <arglist>(const CoinShallowPackedVector &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinShallowPackedVector</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>a07cb5be43662fac96a0572fc28fc9c4e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>af6b945ac616b28c1074ce895419a1960</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinShallowPackedVectorUnitTest</name>
      <anchorfile>classCoinShallowPackedVector.html</anchorfile>
      <anchor>aee2042564a5c00251096f3c0784bd20c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSimpFactorization</name>
    <filename>classCoinSimpFactorization.html</filename>
    <base>CoinOtherFactorization</base>
    <member kind="function">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5188ba97c052c09c3735834cb3aa9c8c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfInitialize</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a7f84aaa7d0a2024c35c49ffd89e68113</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a7f7e02bf74f43ce18502edc8d648406e</anchor>
      <arglist>(const CoinSimpFactorization &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>factorize</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a8c982c65580c52abe3207eb5ab6ac311</anchor>
      <arglist>(int numberOfRows, int numberOfColumns, const int colStarts[], const int indicesRow[], const double elements[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>mainLoopFactor</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a8e71f40cedfd6fea2e824d109dae8227</anchor>
      <arglist>(FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyLbyRows</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a4553ed1c87cc5446dc8fc702f0f3a087</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyUbyColumns</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a2f5e7ce0e3f58def95943923d7530d59</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findPivot</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1b2746a7a42aa55d497813357982757b</anchor>
      <arglist>(FactorPointers &amp;pointers, int &amp;r, int &amp;s, bool &amp;ifSlack)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findPivotShCol</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af6c9113bdfc298000726e2f218595040</anchor>
      <arglist>(FactorPointers &amp;pointers, int &amp;r, int &amp;s)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findPivotSimp</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aaca90d9ab3bad64379167e88c4ba0c02</anchor>
      <arglist>(FactorPointers &amp;pointers, int &amp;r, int &amp;s)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>GaussEliminate</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a7e432904aa407223e8947cdba16fdcb3</anchor>
      <arglist>(FactorPointers &amp;pointers, int &amp;r, int &amp;s)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findShortRow</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1b0066df4dfacc7471b504d1a1538619</anchor>
      <arglist>(const int column, const int length, int &amp;minRow, int &amp;minRowLength, FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findShortColumn</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5d94892e6571b12b758f20d70e6e0d76</anchor>
      <arglist>(const int row, const int length, int &amp;minCol, int &amp;minColLength, FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>findMaxInRrow</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aa1286b28347cad12f6f95b0428f41b91</anchor>
      <arglist>(const int row, FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pivoting</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ac118e27bc9ac1fa8300ac6624829a9f2</anchor>
      <arglist>(const int pivotRow, const int pivotColumn, const double invPivot, FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>updateCurrentRow</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aafd5eed6fe4f78e7916ba923d75dd142</anchor>
      <arglist>(const int pivotRow, const int row, const double multiplier, FactorPointers &amp;pointers, int &amp;newNonZeros)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>increaseLsize</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aa67a9ca2088b084b1ab0495e28578ea4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>increaseRowSize</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a14ec15c93257f4cff8828566572e7bc2</anchor>
      <arglist>(const int row, const int newSize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>increaseColSize</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a0a5d2e3f6ef5b3c767d0cf13018a109b</anchor>
      <arglist>(const int column, const int newSize, const bool b)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enlargeUrow</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a0a844f767fcb0d6b2619fdb05b16523b</anchor>
      <arglist>(const int numNewElements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enlargeUcol</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a556e0d82106e7d83c5b143baed8c3715</anchor>
      <arglist>(const int numNewElements, const bool b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findInRow</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a0974e56a4a60acf2e909c1441b7231fd</anchor>
      <arglist>(const int row, const int column)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>findInColumn</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>abbc209b93ef7636e614ee725d3bc8495</anchor>
      <arglist>(const int column, const int row)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>removeRowFromActSet</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a15da3ef2bb967d0e4def5a51f81d7f7f</anchor>
      <arglist>(const int row, FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>removeColumnFromActSet</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ace0bf87c546eb1c4543afc6fcddf2e96</anchor>
      <arglist>(const int column, FactorPointers &amp;pointers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>allocateSpaceForU</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ab82a6f74debd69c81a10c46e8abf9ef2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>allocateSomeArrays</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ab733f2d92a4ab6008ed694462deb5a2f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialSomeNumbers</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a3c6a67a3f73e7d171c93d8307b0ab847</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Lxeqb</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aabaffca82252f048b2985b93c55a7ddd</anchor>
      <arglist>(double *b) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Lxeqb2</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a053bb902823f6e773f877523e41a2537</anchor>
      <arglist>(double *b1, double *b2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Uxeqb</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a4a9d517e1ff2e6151e335c8e575c67a6</anchor>
      <arglist>(double *b, double *sol) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Uxeqb2</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a41a84ad288aa727172a4880c5e01c144</anchor>
      <arglist>(double *b1, double *sol1, double *sol2, double *b2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>xLeqb</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ad16628609807bd62b7b3917217ba7314</anchor>
      <arglist>(double *b) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>xUeqb</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a951fd7f1ce1ad06df7a9382315cbbee6</anchor>
      <arglist>(double *b, double *sol) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>LUupdate</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a31cd1bd8a77559ad5a1a6094ed9f81f1</anchor>
      <arglist>(int newBasicCol)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>newEta</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af4371eaa945540641bddeca38da519a7</anchor>
      <arglist>(int row, int numNewElements)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>copyRowPermutations</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a0b551dcbd2939cfe63368005928a11c4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Hxeqb</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a909ee6d8a71ab510f0def1f2ab49bb2a</anchor>
      <arglist>(double *b) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Hxeqb2</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1d0ab2e2fe08eacbd077c1befa10a8b8</anchor>
      <arglist>(double *b1, double *b2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>xHeqb</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a530876ce0605fd7ed15c922a179c42b9</anchor>
      <arglist>(double *b) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>ftran</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ae865ccbd8d7ccd0c947783f3175514c4</anchor>
      <arglist>(double *b, double *sol, bool save) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>ftran2</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a8f5e3c872fbded01b096fb443ef17138</anchor>
      <arglist>(double *b1, double *sol1, double *b2, double *sol2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>btran</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1fbf3b8d6ebec5e492ec50b0f22b4f0d</anchor>
      <arglist>(double *b, double *sol) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSimpFactorization</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af38b17976ce6cf5e8887becc85daf10b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSimpFactorization</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aad57c22e4f34d57b75723bd095597855</anchor>
      <arglist>(const CoinSimpFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSimpFactorization</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1f898c39b549af02198c147ebf68d72d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinSimpFactorization &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a2657ec49d8d63b56e6c065614bfc8a97</anchor>
      <arglist>(const CoinSimpFactorization &amp;other)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinOtherFactorization *</type>
      <name>clone</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a417bf9838e5d3efd2fbe166712012f5b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getAreas</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a40b79a475bc297ab358d1c742b0bf17e</anchor>
      <arglist>(int numberRows, int numberColumns, CoinBigIndex maximumL, CoinBigIndex maximumU)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>preProcess</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a2ec6519406a6f88ce88fcf573d7a5137</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>factor</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a3523bf0e49a2e041d3ad47546a7755ae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>postProcess</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>abdf056e51ac22b037b274142921a946f</anchor>
      <arglist>(const int *sequence, int *pivotVariable)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>makeNonSingular</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a276cd9bce9c040fb1cf9eee5b89f91ed</anchor>
      <arglist>(int *sequence, int numberColumns)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>numberElements</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a7cd5769fef81cb13d5f463b94e978dc4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>maximumCoefficient</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ae40d9e994a90990344b3e7ff9ca3eb7e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>replaceColumn</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a798ddf938f34301cbe0ec369a1c68dd9</anchor>
      <arglist>(CoinIndexedVector *regionSparse, int pivotRow, double pivotCheck, bool checkBeforeModifying=false, double acceptablePivot=1.0e-8)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumnFT</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a705db2ca2fd37039608e411e8cb372ac</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumn</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aadc6d093cd82bedb4a3c559cc4f4cf01</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateTwoColumnsFT</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a62a6ad0318bf9c3229a179431ade6e7c</anchor>
      <arglist>(CoinIndexedVector *regionSparse1, CoinIndexedVector *regionSparse2, CoinIndexedVector *regionSparse3, bool noPermute=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>upColumn</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af726767bfb7dd8b64613514697983e4f</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2, bool noPermute=false, bool save=false) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>updateColumnTranspose</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aa7afe72258acef05b2c7a1aded01c2d3</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>upColumnTranspose</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a9d289992c9ea5be878e0303e3e884604</anchor>
      <arglist>(CoinIndexedVector *regionSparse, CoinIndexedVector *regionSparse2) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearArrays</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ab8d8fa4b19272a67b8962848cf6ec675</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int *</type>
      <name>indices</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a16bcdb6b22ac7168169a574c3b692453</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>permute</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a3beb6730c4739523fd442f532c56d194</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>checkPivot</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a4a2a7d74fb32b4366d04b62db414ee92</anchor>
      <arglist>(double saveFromU, double oldPivot) const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>denseVector_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a9897d5fd0b4d5f5dbaa2d2cd8741e228</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>workArea2_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>abb33a2e46f00f1bd059eeb7871817167</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>workArea3_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>acd503be0dd3172e43bba04f8e58e6166</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>vecLabels_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af77a2e579ceb4cd989f7812a9da51c61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>indVector_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5930e4dc1b9df97dd7fd9ea11a98474c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>auxVector_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a79be4573ad6dbf3fd7efde2f57da766f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>auxInd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5951b5d689f92f53d1e2019e2f7d7a77</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>vecKeep_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a56d9dbb7095fbe91468e3334ab7bd39a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>indKeep_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af798dc743bc6ba7d18ab6b4bae8c6c33</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>keepSize_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a04dadba6419ef60e73e037cd339c31f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>LrowStarts_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a8a425065ff467b8b96224d2ac08c0cf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>LrowLengths_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>abbbe7b94dbf6181e0a5cc9daf42efeaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>Lrows_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aaa85f181b8d77ca94d22898d61c080eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>LrowInd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>adba9c7b393af5f8c426a1ad7b5ad5bbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>LrowSize_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ae90182728c728d980ac979835632a513</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>LrowCap_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aecf55bec073528db52e4e74f1a5b6e10</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>LcolStarts_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a639fc2532e398b175327aac7628d1c15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>LcolLengths_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a114219cde7e3bdf940ad49e680d4fab5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>Lcolumns_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a60f338db51e75173bce1c144fad64094</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>LcolInd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a71de3f2201827d6ee1a2ae070a398593</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>LcolSize_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af74ee5ff171bc6d670f6a5d6c89e1d90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>LcolCap_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ad6d3ce99a99e86fa54fc5379b44c0780</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>UrowStarts_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a90ced5775ad5b7e37cf62932f0d6e9f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>UrowLengths_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ab5c8b1c0dcb141454261fa8785c733f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>Urows_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ad5c06c3f268374971b6cf6afe22cffcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>UrowInd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a05ae7208df32075f18783a9df76eb861</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>UrowMaxCap_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ac4c16dea993cb7ef1bed5b0d693c1b11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>UrowEnd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af94c3397ed1d3b88c1fe3d1607326a0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>firstRowInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a028e86362369a57f38f70732eb4fd9cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>lastRowInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ad67246a7b2a62dac0de92dc88b659087</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>prevRowInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a4bf643fe497d8b1f5898a6503e22d29f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>nextRowInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a54fdf9215b45e25506683f79cc940ed6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>UcolStarts_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a8f43c12dc16d59c382a662b78c78f6f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>UcolLengths_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af388609e7db27cec9d173c10a65f80be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>Ucolumns_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>af704cf6a95aa48472b2f14ba9915b47c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>UcolInd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a3f69e4bb5fbd065674f33fe10162e5ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>prevColInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a6e866853b2db7473bb7016498c9275cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>nextColInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5c4b42ae06b95ea406d8e4963a9f3245</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>firstColInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a26444fd9e82a21d9d39714e7a6ba6013</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>lastColInU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>abf9bd41e26ff4d8890222cd3533f5f39</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>UcolMaxCap_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a673f9b214a7ecdf77b8536008989ddee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>UcolEnd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a10735e73422806240c278bd55780e992</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>colSlack_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a15af9ff29c178d11e56f401b1b13f908</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>invOfPivots_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a38568b216a57b45823d81260f53b05a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>colOfU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aef5e0dbf996a7953a8c0161b4141bca5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>colPosition_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a34b769ff220c4ea37af38f21ff2bf982</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>rowOfU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a83dc277d0fc7d77241a89bde7342fc64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>rowPosition_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a18a055a64ba9c3941a9f72af277bfdce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>secRowOfU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5b5cd4ee5826ba6e747b55011a2ba141</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>secRowPosition_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1238e2398b41a010a5805596894f324b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>EtaPosition_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aca7da64ec469830f197aa5b17a87a3a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>EtaStarts_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ab570c98952da82eed4797a519cf7e219</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>EtaLengths_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a6d96e6305fbb64daaea5177f47b6ee3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int *</type>
      <name>EtaInd_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1fe475fd651d81e77c13ea8208623256</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double *</type>
      <name>Eta_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a66d31a496bbadd953bbc6fac073e2b7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>EtaSize_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a82813f865568acb6de9737b45fdda74e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>lastEtaRow_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aa32d3c6df2be3dd31bfdc5f7431ac90b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maxEtaRows_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a3de5cae063752122b57a665778406f0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>EtaMaxCap_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a33a024347f31b7e8fc41e58c8398de05</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>minIncrease_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5dfacc67286045c0ad249d00eef254bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>updateTol_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a5ae53fa2659bf6a169f3680b523f27a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>doSuhlHeuristic_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a75253a37ee005982f5b7401bbb24bef1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>maxU_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a1fa22b2a7ac3a63f8c3a9c29b9ad27e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>maxGrowth_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a34a88800bf640886700a218592ee0c73</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>double</type>
      <name>maxA_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a4319f7b8f1851633a74d0b193584e11b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>pivotCandLimit_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>ab7183d07a2ddb34f069914caade9db8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numberSlacks_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a80923d91196a866ea868fa60a40567ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>firstNumberSlacks_</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>aa9183b711e4af88af1ea6e91420aed9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinSimpFactorizationUnitTest</name>
      <anchorfile>classCoinSimpFactorization.html</anchorfile>
      <anchor>a00da4596b47b7735e33a063813eec828</anchor>
      <arglist>(const std::string &amp;mpsDir)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSnapshot</name>
    <filename>classCoinSnapshot.html</filename>
    <member kind="function">
      <type>int</type>
      <name>getNumCols</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>afd340294217ebb76bb080b8d11fe10f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumRows</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a90709c2496d4e5eaa53cb6b4be461dfa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumElements</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>af566484ed797fcd69710138b41ea670b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumIntegers</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>af933dcbd05d7e7debc95c5425263aaef</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColLower</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a869b6468eb6388e8035f045de5718b96</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColUpper</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>afb49714dbadd30e8ceddd10d46f022c6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowLower</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aa4c2d80b4c042e3c4646791ceb7400c3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowUpper</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ad4440e533507cc0475cef2f680ca6905</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRightHandSide</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a97dc986e677b48857562dbd9aee73254</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getObjCoefficients</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a90559af5bd7e205ed2c34091f81b50fd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getObjSense</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>adf3d47557bf0ab9d436f1f33011d63be</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isContinuous</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a8c4a7eefce95a10c19c1f1d3a0ae2c82</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isBinary</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aad1d370ec83988238d7c861c24e62274</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteger</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a5e92d97ecb5ada0269579c01ee9972e9</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isIntegerNonBinary</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a17847fa18792ae3ab8d860bb76b4e5b5</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isFreeBinary</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ac28fa3ebc594f5e2f5f888e5fd1d5a4c</anchor>
      <arglist>(int colIndex) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getColType</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a63872f2c99014cb3697500b100fa2b86</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getMatrixByRow</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a21d9f55b2f2c7b3e64b7d31abe5f7179</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getMatrixByCol</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ae599d42e9a69d8e2189db91c46faacae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getOriginalMatrixByRow</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ad13225fbfcf8890bd3c73036bbb94209</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const CoinPackedMatrix *</type>
      <name>getOriginalMatrixByCol</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>af6964fe0b443634b7c454be9c0d9adf2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getColSolution</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a52809f4d4518b53ce9f6748a7288d81b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowPrice</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a5526005ffd6a34b9171ad3b54962a9e7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getReducedCost</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ae12ec99a262b8340fdb6be36a33746f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getRowActivity</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ad93b97a9e46c043ee9f6ee674f31fdca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>getDoNotSeparateThis</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a03b23b7e829c20332e3fe3925fb22d51</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getInfinity</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>afabfd1eb1530508bddfa68d709569a93</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getObjValue</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>abecd4a3a1c4e0eb3293f85613c7fe8db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getObjOffset</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>af01941a363fa8aedd47314bf9674bf2f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getDualTolerance</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>abaeda5984eb0f49a78e450da8bb767ca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getPrimalTolerance</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>abfaafb3a19907826c6e34972193df498</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getIntegerTolerance</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ae98d93f39dce45d4a3b2862d6590ccab</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getIntegerUpperBound</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a5598283b63ecf6edc2b0af7c69dea256</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getIntegerLowerBound</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aa786263502da4e35b00b5a1603e30bf7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadProblem</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>afb833c79da53fa065400338488b08ee3</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *collb, const double *colub, const double *obj, const double *rowlb, const double *rowub, bool makeRowCopy=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumCols</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>afcb2f883d5a91f34a437c255f145ee59</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumRows</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>af2f0472d253fa5109d48265b635e7a19</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumElements</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aaf23e3a8c00f0ed9e33252a0a3b6fb72</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setNumIntegers</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aea176388ac802e0d0adb34f5b0ca2945</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColLower</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a246a0f67ec1438ca26f08a021c583da4</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColUpper</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a6a422a41dabe7e8377a50e7058ae661c</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowLower</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a125517219b1733c6d1e92514ecc5a77e</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowUpper</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a2bf8e738e89dad97f8ab5cc7dbdcc82e</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRightHandSide</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a5442f28896c1878624e72e35a64b0482</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createRightHandSide</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aa3525898f6bef0a94669b977b3cf6fed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjCoefficients</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aca4aeee68156ff7c947fb3ce93965f71</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjSense</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aab90d5231558ac96506d7959fe2ae07c</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColType</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aff8f380e6b12e69a71eb045003302eee</anchor>
      <arglist>(const char *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMatrixByRow</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a5290154d2ddd7c674204c27fdc9c3283</anchor>
      <arglist>(const CoinPackedMatrix *matrix, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createMatrixByRow</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a4b0a787d642b8159c7d3e135d79af68f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMatrixByCol</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>acc156d3fec7891ea4d4e9db5bad19a80</anchor>
      <arglist>(const CoinPackedMatrix *matrix, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalMatrixByRow</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aec1c99aea53af4f7c316377deebdb520</anchor>
      <arglist>(const CoinPackedMatrix *matrix, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOriginalMatrixByCol</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aae9378645f0c453b611fa7bbd7679bd1</anchor>
      <arglist>(const CoinPackedMatrix *matrix, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColSolution</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a6f428f6e98b46fb9fba4cfd75b95190d</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowPrice</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a93ac3c9e6355fbe8c00a22c2d28ae60e</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setReducedCost</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ac0adae9a561cff55ee0e4a7e2d79c325</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowActivity</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a450e2394a814f7d0cdedcbf79cdbfc5b</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDoNotSeparateThis</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a252fa9afe1d7e7b3a8b7e42363ef5e0b</anchor>
      <arglist>(const double *array, bool copyIn=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInfinity</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a9a7574e6044a6478f7efb75bd6f92ab3</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjValue</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a0d5384abbba5256faae539012ab877da</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObjOffset</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a44896e7389e41d9f1bf1fb4c92108bdc</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDualTolerance</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a1972580887658201072bbcd5dd657a52</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPrimalTolerance</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a8d081a24101e3e067e963cf068903a3d</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIntegerTolerance</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>af4c0e197bc723ea61f882f46c595f1ce</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIntegerUpperBound</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a569c6cab225c07ac1454c23105527302</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIntegerLowerBound</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a13e0a65db7ac07e5c5e953f899421745</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSnapshot</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>aea94804809b87b4ff2dc4712fb5f945a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinSnapshot</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>ae0348c55a0afa2540a1c8bb3edca6722</anchor>
      <arglist>(const CoinSnapshot &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinSnapshot &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a8f7e05815470b720d14d16b988913dc6</anchor>
      <arglist>(const CoinSnapshot &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSnapshot</name>
      <anchorfile>classCoinSnapshot.html</anchorfile>
      <anchor>a89d4834fd934c69c35834d8d85d98417</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinSosSet</name>
    <filename>classCoinSosSet.html</filename>
    <base>CoinSet</base>
    <member kind="function">
      <type></type>
      <name>CoinSosSet</name>
      <anchorfile>classCoinSosSet.html</anchorfile>
      <anchor>aeeb85b3dd1a962fd107244bbfff5c308</anchor>
      <arglist>(int numberEntries, const int *which, const double *weights, int type)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinSosSet</name>
      <anchorfile>classCoinSosSet.html</anchorfile>
      <anchor>a2580ba679142e7b5a985385bac364cd7</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinStructuredModel</name>
    <filename>classCoinStructuredModel.html</filename>
    <base>CoinBaseModel</base>
    <member kind="function">
      <type>int</type>
      <name>addBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a6131f6226f0336fc74a4f0c40e802f5d</anchor>
      <arglist>(const std::string &amp;rowBlock, const std::string &amp;columnBlock, const CoinBaseModel &amp;block)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a438f30e2d4d2106ac687ddc6194ef249</anchor>
      <arglist>(const CoinBaseModel &amp;block)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>aad4d406e94588b91e18153bc2dcdefa7</anchor>
      <arglist>(const std::string &amp;rowBlock, const std::string &amp;columnBlock, CoinBaseModel *block)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>ac376704f8e1551e76cd77d35e71236c9</anchor>
      <arglist>(const std::string &amp;rowBlock, const std::string &amp;columnBlock, const CoinPackedMatrix &amp;matrix, const double *rowLower, const double *rowUpper, const double *columnLower, const double *columnUpper, const double *objective)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>writeMps</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>ac1634722746dcfaa50d9fafd477e7434</anchor>
      <arglist>(const char *filename, int compression=0, int formatType=0, int numberAcross=2, bool keepStrings=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readSmps</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>accbcbe67891c19ef9fc42d2ca600cd1b</anchor>
      <arglist>(const char *filename, bool keepNames=false, bool ignoreErrors=false)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>decompose</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a723df08988caa017f489059efbb930a4</anchor>
      <arglist>(const CoinModel &amp;model, int type, int maxBlocks=50, const char **starts=NULL)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>decompose</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a69a15626f1c573e8c2b6816944ae1df4</anchor>
      <arglist>(const CoinPackedMatrix &amp;matrix, const double *rowLower, const double *rowUpper, const double *columnLower, const double *columnUpper, const double *objective, int type, int maxBlocks=50, int *starts=NULL, double objectiveOffset=0.0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberRowBlocks</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a59d6b3017c2de1cea8cb1710d72189f4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberColumnBlocks</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a91e8c8245754a0fdfe1b0f554244c69e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElementBlocks</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a21c732162b5fd9774630f05247ac9539</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>CoinBigIndex</type>
      <name>numberElements</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>aa67de342da09043dc6a7e154ef1799ca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getRowBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a982d4fc973c784bf658c2a6feb419b25</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRowBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a9906f0de0c0671bf110eb6fd4fba32cb</anchor>
      <arglist>(int i, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addRowBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>aa0d8e6c864712ccf736b4c797cdce85b</anchor>
      <arglist>(int numberRows, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>rowBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a3b345acff1ad4df9a7865c92f4a52599</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getColumnBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a29e411e2f6c5809c5a6e4b5ba7755e11</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumnBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a746a1b070c85a6ef32c649b9d75a19ce</anchor>
      <arglist>(int i, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addColumnBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>acb35c20dda336f325c485ce9871b8281</anchor>
      <arglist>(int numberColumns, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>columnBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>ab45b7b2d371e034ce161c76dd776cca8</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinModelBlockInfo &amp;</type>
      <name>blockType</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a0bd5e71e2125d032a35fdadf72470956</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>CoinBaseModel *</type>
      <name>block</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>ab98a024234ab2b10d226a6731764f0f7</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinBaseModel *</type>
      <name>block</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a0ade312a0ae0543c829da5628d3c4a57</anchor>
      <arglist>(int row, int column) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModel *</type>
      <name>coinBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>af7e9590e3b224da14ff591ba65e38da4</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>const CoinBaseModel *</type>
      <name>coinBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>ac63cd3c615155cf376cad85270714863</anchor>
      <arglist>(int row, int column) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>blockIndex</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a89a45cee36dda9f4146477bf986fcfb7</anchor>
      <arglist>(int row, int column) const </arglist>
    </member>
    <member kind="function">
      <type>CoinModel *</type>
      <name>coinModelBlock</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a4a2397222204c0e1e13aa927d968bfca</anchor>
      <arglist>(CoinModelBlockInfo &amp;info)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCoinModel</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a89196815c355eb292662cfdfeb66f51d</anchor>
      <arglist>(CoinModel *block, int iBlock)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>refresh</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>afa62732973f011fd8ed08ea61afc8083</anchor>
      <arglist>(int iBlock)</arglist>
    </member>
    <member kind="function">
      <type>CoinModelBlockInfo</type>
      <name>block</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>abb6e5bcd6d978a444e1fd4d6dcbff98b</anchor>
      <arglist>(int row, int column, const double *&amp;rowLower, const double *&amp;rowUpper, const double *&amp;columnLower, const double *&amp;columnUpper, const double *&amp;objective) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>optimizationDirection</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>afa79703b0799939671d4264f7c9471c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOptimizationDirection</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a579a81509ad55116d065571f88ba00ad</anchor>
      <arglist>(double value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinStructuredModel</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a43555e4edadcbceee0919cdcd82dd460</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinStructuredModel</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a9d7213ac99a6710c235d0549a8819d47</anchor>
      <arglist>(const char *fileName, int decompose=0, int maxBlocks=50)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinStructuredModel</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>acd102d9d906617b8040575df3ed1c03e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinStructuredModel</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a3db2abebcc7909387e6d57ba87f29349</anchor>
      <arglist>(const CoinStructuredModel &amp;)</arglist>
    </member>
    <member kind="function">
      <type>CoinStructuredModel &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>ac047892149e0f921c7a9f4e28dd13b3f</anchor>
      <arglist>(const CoinStructuredModel &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinBaseModel *</type>
      <name>clone</name>
      <anchorfile>classCoinStructuredModel.html</anchorfile>
      <anchor>a9ab59a23334253f9fd42c41b7e1b0769</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinThreadRandom</name>
    <filename>classCoinThreadRandom.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinThreadRandom</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>a05d78a4a11f0bf5183e5471d3d0895c7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinThreadRandom</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>aa88e1036ca2caac2a85e9d3bee6f2a48</anchor>
      <arglist>(int seed)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinThreadRandom</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>ab94ba754b7f2cc8332399c731548f7d9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinThreadRandom</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>ae9fe29732ed66af99422f17cdd8c1e16</anchor>
      <arglist>(const CoinThreadRandom &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinThreadRandom &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>ab0d35179e276ee92a14a12ca5fdab7d0</anchor>
      <arglist>(const CoinThreadRandom &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSeed</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>ae70742ec2e16b2704544c63d9f1af5da</anchor>
      <arglist>(int seed)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getSeed</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>aff4e144ae0d7a2a2bb24179028bb4585</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>randomDouble</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>a633fbe066de1dd3e65887f500a424825</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>randomize</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>a98f38c9a96830beb6f152da088481821</anchor>
      <arglist>(int n=0)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>unsigned int</type>
      <name>seed_</name>
      <anchorfile>classCoinThreadRandom.html</anchorfile>
      <anchor>af3fb9a475948429693d1ae0a49e57481</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinTimer</name>
    <filename>classCoinTimer.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinTimer</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a215866a82a6d0dc257d16b478b9881f7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinTimer</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a8a25cf0fe89f596e6384998d9b3b5117</anchor>
      <arglist>(double lim)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>restart</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>aeabeca2d362847a25d6ba71c05271b9f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>ac2dccd9d9325926f3a4fadb117949d96</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a01b08e624f50a372195a56ea460a6c8e</anchor>
      <arglist>(double lim)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isPastPercent</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a49c83b143b9e05f01f24953dba5cd153</anchor>
      <arglist>(double pct) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isPast</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a40789fa7e9534ca735793acd7a2df257</anchor>
      <arglist>(double lim) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isExpired</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a9981459a6db90ecaa6b7e7fdc8d4ca17</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>timeLeft</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a2b0481caa62f2e5cba5308e34ee2e23d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>timeElapsed</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a7716277bc64bda444a3fd017fd74fc26</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setLimit</name>
      <anchorfile>classCoinTimer.html</anchorfile>
      <anchor>a104ac4be8fc638f13350c6afce0d8d78</anchor>
      <arglist>(double l)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinTreeNode</name>
    <filename>classCoinTreeNode.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinTreeNode</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>ae5f9f394b3f0e81967712ec5dd5be8bc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDepth</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a8a041fd24ad9f467f1c43fe91a0d61f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getFractionality</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>afc1a21b38168fb6e01eafcfe00cc78a4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getQuality</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>aeeff74a82f8d1a10fc8468d1bb4776b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getTrueLB</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a85464a2a365b43d5e265ec0a1acdd130</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>BitVector128</type>
      <name>getPreferred</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>adae7a451d80fe4c25c84c1c963c3b21b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDepth</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a92b92707542b758d39a2e11a97d84110</anchor>
      <arglist>(int d)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFractionality</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>aee8078efd81d6ec9d7ceb66de517cbb0</anchor>
      <arglist>(int f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setQuality</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a9c6ac2b5f047202dbe4e6c2d5924c653</anchor>
      <arglist>(double q)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTrueLB</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a329d97f0d7793084e71e8a935f9c8cbc</anchor>
      <arglist>(double tlb)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreferred</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a77c2bb558f737b162c467d6ba584eaf1</anchor>
      <arglist>(BitVector128 p)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinTreeNode</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a0d064b03274562e640ac0a58d9c5a8eb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinTreeNode</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a8205e8033bc9aeaa83562e292a5a4b94</anchor>
      <arglist>(int d, int f=-1, double q=-COIN_DBL_MAX, double tlb=-COIN_DBL_MAX, BitVector128 p=BitVector128())</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinTreeNode</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a73eaedfffc03b10f0c00fdca70669e5d</anchor>
      <arglist>(const CoinTreeNode &amp;x)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>CoinTreeNode &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinTreeNode.html</anchorfile>
      <anchor>a61b27c813dc5c2fe39473adcb924d655</anchor>
      <arglist>(const CoinTreeNode &amp;x)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinTreeSiblings</name>
    <filename>classCoinTreeSiblings.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinTreeSiblings</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>af0736ceecb7adaf0351a541f7f93fdf5</anchor>
      <arglist>(const int n, CoinTreeNode **nodes)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinTreeSiblings</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>a7e494ab40d48d0bb6a0ab1064ccbbfd9</anchor>
      <arglist>(const CoinTreeSiblings &amp;s)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinTreeSiblings</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>a9df4f1bad09d5f80e063b80e83768b2d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CoinTreeNode *</type>
      <name>currentNode</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>ac0c10070842fd6df260c48d3700d3cfa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>advanceNode</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>a347356e69962fb6de24e58a15066fb66</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>toProcess</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>a2893f6b79e7659439c2146eb92eb688a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>af950e6dbd3a2883e4b8c496d71d55cbc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printPref</name>
      <anchorfile>classCoinTreeSiblings.html</anchorfile>
      <anchor>a1a35983b156021722ed6b41591291f62</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinTriple</name>
    <filename>classCoinTriple.html</filename>
    <templarg>S</templarg>
    <templarg>T</templarg>
    <templarg>U</templarg>
    <member kind="function">
      <type></type>
      <name>CoinTriple</name>
      <anchorfile>classCoinTriple.html</anchorfile>
      <anchor>aaf4985b9165050e633bfdbc460224dcd</anchor>
      <arglist>(const S &amp;s, const T &amp;t, const U &amp;u)</arglist>
    </member>
    <member kind="variable">
      <type>S</type>
      <name>first</name>
      <anchorfile>classCoinTriple.html</anchorfile>
      <anchor>aa3fc0fa5ca78ce561a8cc4c0a58fbfd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>T</type>
      <name>second</name>
      <anchorfile>classCoinTriple.html</anchorfile>
      <anchor>a532dcfbf811f509c42aa749bdeb1b74e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>U</type>
      <name>third</name>
      <anchorfile>classCoinTriple.html</anchorfile>
      <anchor>af3d7b7c4c91cde8474bb7b232b9b6e8e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinUnsignedIntArrayWithLength</name>
    <filename>classCoinUnsignedIntArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a6a9e64c66d24235b2796a87721220693</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>unsigned int *</type>
      <name>array</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>ad3efe7bb215af2181542a09036f261fc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a3e68095718e161c2ee5cf01f8819390f</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int *</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>abc9af3d6bf3c84497237672ff5b7ccd4</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinUnsignedIntArrayWithLength</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a6fb6e7d545738db854fbc2a1d3956143</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinUnsignedIntArrayWithLength</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a29c53e9d4724b527124f3f8757d13faf</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinUnsignedIntArrayWithLength</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>ada5830399968dce9ce8c587ea26f2cd5</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinUnsignedIntArrayWithLength</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a85ee69356a8f80a614ad3e86f102ba85</anchor>
      <arglist>(const CoinUnsignedIntArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinUnsignedIntArrayWithLength</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a8d2608da9ef8d1650503b5fcbe7e1ab9</anchor>
      <arglist>(const CoinUnsignedIntArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinUnsignedIntArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinUnsignedIntArrayWithLength.html</anchorfile>
      <anchor>a576ced789ca6a463f52a89766454deff</anchor>
      <arglist>(const CoinUnsignedIntArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinVoidStarArrayWithLength</name>
    <filename>classCoinVoidStarArrayWithLength.html</filename>
    <base>CoinArrayWithLength</base>
    <member kind="function">
      <type>int</type>
      <name>getSize</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a0a03eaa785f4b2cbe8d4109a23d9d4e9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void **</type>
      <name>array</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>abc2e1b549b17239feb6c8a7a206897e0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSize</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a702b042a1cefc3e26d0278e412b49366</anchor>
      <arglist>(int value)</arglist>
    </member>
    <member kind="function">
      <type>void **</type>
      <name>conditionalNew</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>ab69c861684244de3b06b2aca34d6d522</anchor>
      <arglist>(int sizeWanted)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinVoidStarArrayWithLength</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>aa43a68c210b51010d0d5a0b9e0e29b19</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinVoidStarArrayWithLength</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a4553f346c51aed5e784e6f12f912decb</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinVoidStarArrayWithLength</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a8ef1ed72d4b0f4c605ffc09cf9aa4526</anchor>
      <arglist>(int size, int mode)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinVoidStarArrayWithLength</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a5f882192d572a7cc277c92a892674b3e</anchor>
      <arglist>(const CoinVoidStarArrayWithLength &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinVoidStarArrayWithLength</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a4f28827e9303197e2424ca063727d3cb</anchor>
      <arglist>(const CoinVoidStarArrayWithLength *rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinVoidStarArrayWithLength &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinVoidStarArrayWithLength.html</anchorfile>
      <anchor>a5b1315419fcc31e7159f98a7d02c4974</anchor>
      <arglist>(const CoinVoidStarArrayWithLength &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStart</name>
    <filename>classCoinWarmStart.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStart</name>
      <anchorfile>classCoinWarmStart.html</anchorfile>
      <anchor>a15c90cb5b58fdf1f82b248f5a881b775</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual CoinWarmStart *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStart.html</anchorfile>
      <anchor>ab0384ae2925d55d9b761217736213d99</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>generateDiff</name>
      <anchorfile>classCoinWarmStart.html</anchorfile>
      <anchor>abbf3a8854e288e1f8cbd31b53b730ed9</anchor>
      <arglist>(const CoinWarmStart *const ) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyDiff</name>
      <anchorfile>classCoinWarmStart.html</anchorfile>
      <anchor>aa58a8f1502ef307fe6b8d5856fa546ee</anchor>
      <arglist>(const CoinWarmStartDiff *const )</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartBasis</name>
    <filename>classCoinWarmStartBasis.html</filename>
    <base virtualness="virtual">CoinWarmStart</base>
    <member kind="enumeration">
      <type></type>
      <name>Status</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ab92819b1d7d9c4a8946740dd4b7dc036</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>isFree</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ab92819b1d7d9c4a8946740dd4b7dc036adc4b9ef41e3b0d6f17befeb6103495be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>basic</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ab92819b1d7d9c4a8946740dd4b7dc036a76fa578628e6e7ec0938d5430e448945</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>atUpperBound</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ab92819b1d7d9c4a8946740dd4b7dc036aebf532878ca48a4dbb02febbe2141775</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>atLowerBound</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ab92819b1d7d9c4a8946740dd4b7dc036aef9710307fd2bf33b9d26f739b9b68ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>CoinTriple&lt; int, int, int &gt;</type>
      <name>XferEntry</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ab394ad8df317975f6309ad7402d06028</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; XferEntry &gt;</type>
      <name>XferVec</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a8cf13843d9a880a23941d38c9a6ab205</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumStructural</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>aa0ecca58c7010e80afd97fceb56bd939</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumArtificial</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a1d7db7ae88b4e5954a7876c6fcd66584</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numberBasicStructurals</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a87c55175f51d3269276dda79d116c439</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>getStructStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a4e86dd5a34023f739b448139268c351a</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStructStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>aa7776bb574c148d10731649ffaa43870</anchor>
      <arglist>(int i, Status st)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>getStructuralStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a4ba42cc6de6152897ed0a27cec77e95e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getStructuralStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a7679e661ef1f1f7c2678a623d4158a61</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>getArtificialStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a294d4ec1539262d426cfb7bcf01d30e4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Status</type>
      <name>getArtifStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>aa37619c9c40fdd3f36d9b5638a291622</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setArtifStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a28076c37cd494dd73135df30e272d1da</anchor>
      <arglist>(int i, Status st)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getArtificialStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a5f8d929e4ab1ab4603cc5a533f19b559</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>generateDiff</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>af14969058b18183a4d1b4278e22a164c</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyDiff</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a280b0a42b20d0b1414907f9ef52e007c</anchor>
      <arglist>(const CoinWarmStartDiff *const cwsdDiff)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setSize</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ac05260b8cadaf5da69dbad9e372f658a</anchor>
      <arglist>(int ns, int na)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>resize</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a4b853ef71e1454b4e11256cafadb1084</anchor>
      <arglist>(int newNumberRows, int newNumberColumns)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>compressRows</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a1900b89de75b23e33123dbf7dae2c563</anchor>
      <arglist>(int tgtCnt, const int *tgts)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteRows</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a6cc8bf2cfe3864a1ca12e7de18b5b909</anchor>
      <arglist>(int rawTgtCnt, const int *rawTgts)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deleteColumns</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a5aa3ed93168a122ce07ba862f80a307c</anchor>
      <arglist>(int number, const int *which)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>mergeBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ac37defd64abe177525bd9730da62b7cf</anchor>
      <arglist>(const CoinWarmStartBasis *src, const XferVec *xferRows, const XferVec *xferCols)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a46f5e94c0c37086bd2633b13188bcbff</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ae801fb9ed1836b1c79313f8eb6cba6da</anchor>
      <arglist>(int ns, int na, const char *sStat, const char *aStat)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a0c4d96de388e1abf9680434d8264bd24</anchor>
      <arglist>(const CoinWarmStartBasis &amp;ws)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a6a20a56d9d71a9643b918652afc5a527</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>ae249d0c33b54c848e4f4cec88847d74f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartBasis &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a63d47b8b444bf4c3e3d2683d33535a53</anchor>
      <arglist>(const CoinWarmStartBasis &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>assignBasisStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a9e051309763b4f7d42f60735327874af</anchor>
      <arglist>(int ns, int na, char *&amp;sStat, char *&amp;aStat)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a6e0c34d07e39cc41174157ba3f3d2d67</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fullBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>aed2859d2ab3d4fe22b88b21b25f99ed3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fixFullBasis</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>aded3cf3ad0a1aefc3ff8f49136174f2d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numStructural_</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a17d00470a594429606ee2903b77bc151</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>numArtificial_</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a8d66c356ce3a3f4b3340dff9f777214a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>maxSize_</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a21a668d7b2fe0685ef009e1850c458e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>structuralStatus_</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a7f25e0d42b2e2bdca4eaf12ace00ddb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>char *</type>
      <name>artificialStatus_</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>acde9861772e728d78250c22935714dae</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStartBasis::Status</type>
      <name>getStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a4d4e8fda47eeac660dccc92e31f83bb4</anchor>
      <arglist>(const char *array, int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStatus</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>a46c2e9b90fe722655860b8934574ffc2</anchor>
      <arglist>(char *array, int i, CoinWarmStartBasis::Status st)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>statusName</name>
      <anchorfile>classCoinWarmStartBasis.html</anchorfile>
      <anchor>aae5804228d0914dc4f19725caa6e1dcc</anchor>
      <arglist>(CoinWarmStartBasis::Status status)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartBasisDiff</name>
    <filename>classCoinWarmStartBasisDiff.html</filename>
    <base virtualness="virtual">CoinWarmStartDiff</base>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a7aa55494a579f9897b6b7c833d70513b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartBasisDiff &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a92da0f8d04ec7a6e10b8b6f4ceabe233</anchor>
      <arglist>(const CoinWarmStartBasisDiff &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartBasisDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a8c0f83f4a21ea3821ba0854936789e4e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartDiff</name>
      <anchorfile>classCoinWarmStartDiff.html</anchorfile>
      <anchor>a7cd06b48b5c9cd9dd939fdfb6a16aa6f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartBasisDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>af81c61d20907cc10eac30b023f93a2fe</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartBasisDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a608429fb4bd6f2a05d079941ceacfb1e</anchor>
      <arglist>(const CoinWarmStartBasisDiff &amp;cwsbd)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartBasisDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a8ee528c628b639a5e37874dd8246f9bc</anchor>
      <arglist>(int sze, const unsigned int *const diffNdxs, const unsigned int *const diffVals)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartBasisDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a6d571941b35f8c1fa387a321a1aa625d</anchor>
      <arglist>(const CoinWarmStartBasis *rhs)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend CoinWarmStartDiff *</type>
      <name>CoinWarmStartBasis::generateDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>aada6f6752d35c07f20672cf6c60cdb72</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinWarmStartBasis::applyDiff</name>
      <anchorfile>classCoinWarmStartBasisDiff.html</anchorfile>
      <anchor>a53aa276661635c90e77e0270934abe72</anchor>
      <arglist>(const CoinWarmStartDiff *const diff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartDiff</name>
    <filename>classCoinWarmStartDiff.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual CoinWarmStartDiff *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartDiff.html</anchorfile>
      <anchor>a6821b75ad54bd3e0e322019e691dd119</anchor>
      <arglist>() const =0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartDual</name>
    <filename>classCoinWarmStartDual.html</filename>
    <base virtualness="virtual">CoinWarmStart</base>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a57423ae20ab2323edd1415cd316a4cbf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>dual</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a582961f566b82176893bbbd3a92c52b3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignDual</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>ad354e33a75e66a2f26eab4f85fa677d1</anchor>
      <arglist>(int size, double *&amp;dual)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartDual</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a055404cb632cbfe17eb1c1a66810a8f8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartDual</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a2a37a10a53b29173ae6ee7ceaf332fa8</anchor>
      <arglist>(int size, const double *dual)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartDual</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>ac833e8d978b488ce43692a516e2e002e</anchor>
      <arglist>(const CoinWarmStartDual &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStartDual &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a3a1661180d65cb1c1db49f76b7986b50</anchor>
      <arglist>(const CoinWarmStartDual &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a3d134339db03a1ff43dd284c98f194ef</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartDual</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>a75304c239a67e49d73324d26743fad7a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>generateDiff</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>aedf8dc7db1c5071b534c29848bc2b876</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyDiff</name>
      <anchorfile>classCoinWarmStartDual.html</anchorfile>
      <anchor>ad134c9ecc9050547c936420c14f8959d</anchor>
      <arglist>(const CoinWarmStartDiff *const cwsdDiff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartDualDiff</name>
    <filename>classCoinWarmStartDualDiff.html</filename>
    <base virtualness="virtual">CoinWarmStartDiff</base>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>a81f2f688d7b01220ac75f55dff5666e2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDualDiff &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>aec8ad64179694e130f06f6290ddf92fb</anchor>
      <arglist>(const CoinWarmStartDualDiff &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartDualDiff</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>ac30f01a19032758b4fe0235a9f8b15c1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartDualDiff</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>a9a003bf35577b85d639a864117f43596</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartDualDiff</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>a9131951e5de5de961dc097afef69bfdf</anchor>
      <arglist>(const CoinWarmStartDualDiff &amp;rhs)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend CoinWarmStartDiff *</type>
      <name>CoinWarmStartDual::generateDiff</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>a9c88c00bd8c813d67d1f705130a05cec</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinWarmStartDual::applyDiff</name>
      <anchorfile>classCoinWarmStartDualDiff.html</anchorfile>
      <anchor>aae64790f893436e9c837c3b9ddc03504</anchor>
      <arglist>(const CoinWarmStartDiff *const diff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartPrimalDual</name>
    <filename>classCoinWarmStartPrimalDual.html</filename>
    <base virtualness="virtual">CoinWarmStart</base>
    <member kind="function">
      <type>int</type>
      <name>dualSize</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a02cc1534fce325454c10f1c3b5d3f7bf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>dual</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>ae6cac1e10e5406018359557e9d5d9848</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>primalSize</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>acdff3ec6b7aefee691d82bc5f44a72cc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const double *</type>
      <name>primal</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>ac5352f84bf7e8be4699857f6894c1adb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assign</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a0372868eb4b224897229636baebbff66</anchor>
      <arglist>(int primalSize, int dualSize, double *&amp;primal, double *&amp;dual)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartPrimalDual</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>ad556d3774922d941201884e0bf83ac72</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartPrimalDual</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>aba0bfd85de51123f137e759b92f45f84</anchor>
      <arglist>(int primalSize, int dualSize, const double *primal, const double *dual)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartPrimalDual</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a55d3206ca1afeaa2c94649b691dc2431</anchor>
      <arglist>(const CoinWarmStartPrimalDual &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStartPrimalDual &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a5296d8ef3c8479f065a667b53cfa0c4a</anchor>
      <arglist>(const CoinWarmStartPrimalDual &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a3c51a93e282ca8b46a9db1be2df4ac8d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a809f78b059d18f7829f055870cf38a9e</anchor>
      <arglist>(CoinWarmStartPrimalDual &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>af813fa903851c9a4ad8f5157015ca975</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartPrimalDual</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>ac8540254ebf399325f4924841ea288aa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>generateDiff</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>acdb81be9c1b801c45af014def14011d9</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyDiff</name>
      <anchorfile>classCoinWarmStartPrimalDual.html</anchorfile>
      <anchor>a28c83465e8aa14bcfa190a7566129887</anchor>
      <arglist>(const CoinWarmStartDiff *const cwsdDiff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartPrimalDualDiff</name>
    <filename>classCoinWarmStartPrimalDualDiff.html</filename>
    <base virtualness="virtual">CoinWarmStartDiff</base>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>aabfde25435e7cfa10fad45a1aa8149ec</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartPrimalDualDiff</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>ae43998c3c78df438ecad3f7c383a7990</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartPrimalDualDiff</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>a95b41500f400d8612fed5bd7bb10730c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>CoinWarmStartPrimalDualDiff</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>a4134df0931f2018997099f8f1af92214</anchor>
      <arglist>(const CoinWarmStartPrimalDualDiff &amp;rhs)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>adcccdc7c0f19016890b380577a68d75e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>a6ed79847c3794c23efa13dcf08490536</anchor>
      <arglist>(CoinWarmStartPrimalDualDiff &amp;rhs)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend CoinWarmStartDiff *</type>
      <name>CoinWarmStartPrimalDual::generateDiff</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>a86f53bab05bbff809e275f66cbf5a3c6</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinWarmStartPrimalDual::applyDiff</name>
      <anchorfile>classCoinWarmStartPrimalDualDiff.html</anchorfile>
      <anchor>a82295ef03ec610266dc7d1efcc41e650</anchor>
      <arglist>(const CoinWarmStartDiff *const diff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartVector</name>
    <filename>classCoinWarmStartVector.html</filename>
    <templarg>T</templarg>
    <base virtualness="virtual">CoinWarmStart</base>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>ac10aa63693bd1fcce133b3a571eb1439</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const T *</type>
      <name>values</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a2dbe342e0e1dc5adaf8d25b736a30685</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignVector</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a047b7d10c3fe165070ad07d441aaf692</anchor>
      <arglist>(int size, T *&amp;vec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVector</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>ae5ef0bf41bf1c023b47002f2c3be7312</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVector</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a1254e2a414d930f1a81e5f702c1c3406</anchor>
      <arglist>(int size, const T *vec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVector</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>ad9d50c20148bcd49125b269cc996d94d</anchor>
      <arglist>(const CoinWarmStartVector &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStartVector &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a4fadab47c620d4e8b8058414a052b79d</anchor>
      <arglist>(const CoinWarmStartVector &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a29cf7dce4f627560cbfc5b287e6dddc4</anchor>
      <arglist>(CoinWarmStartVector &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a478b7d9514e0becd471916428e6da684</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartVector</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a2c5a6010744c05cc1f74cbad90b5c34c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a540754506c6b0244f60a0666f9780150</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>generateDiff</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a34eba44193a922793e01973fad84377f</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyDiff</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a6fb9d1d85fb89e1702906cd02a07940b</anchor>
      <arglist>(const CoinWarmStartDiff *const cwsdDiff)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfDestructor</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>a97bea3e43e2556efde30de6e7d3ee1fa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>gutsOfCopy</name>
      <anchorfile>classCoinWarmStartVector.html</anchorfile>
      <anchor>af13b1d1da2a44be6331ba12bbcd5841d</anchor>
      <arglist>(const CoinWarmStartVector&lt; T &gt; &amp;rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartVectorDiff</name>
    <filename>classCoinWarmStartVectorDiff.html</filename>
    <templarg>T</templarg>
    <base virtualness="virtual">CoinWarmStartDiff</base>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>ab387f60efe7ad585986712785ec7ed67</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartVectorDiff &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a07b1ae19b1a5f2a47030b711bf3a6935</anchor>
      <arglist>(const CoinWarmStartVectorDiff&lt; T &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartVectorDiff</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a4e5f5048046cf1c1f0493c3b9fd5a75b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>aa6d5688ef8725225abd380679b114519</anchor>
      <arglist>(CoinWarmStartVectorDiff &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorDiff</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a38dd7e0cdac7fa560274313b270891c3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorDiff</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a05fc83820f7057d7de2a5a9f8655ecea</anchor>
      <arglist>(const CoinWarmStartVectorDiff&lt; T &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorDiff</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a7a2732bfd1672041637eceb42fa21090</anchor>
      <arglist>(int sze, const unsigned int *const diffNdxs, const T *const diffVals)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a1dda9bd7b27b302d8506e881436ca2d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend CoinWarmStartDiff *</type>
      <name>CoinWarmStartVector</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a349457aaa9fea9c7ec2cc8ea51d70f0a</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinWarmStartVector</name>
      <anchorfile>classCoinWarmStartVectorDiff.html</anchorfile>
      <anchor>a592e6ede002b84d1b510f73eeb484fd9</anchor>
      <arglist>(const CoinWarmStartDiff *const diff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartVectorPair</name>
    <filename>classCoinWarmStartVectorPair.html</filename>
    <templarg>T</templarg>
    <templarg>U</templarg>
    <base virtualness="virtual">CoinWarmStart</base>
    <member kind="function">
      <type>int</type>
      <name>size0</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a0c1339d621fa7645b676775f7954eb2a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size1</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>ac4f8f7a91a40ceac227fb25cb627ce02</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const T *</type>
      <name>values0</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a0f3db3fd296d616b508cc77c9e7e39c3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const U *</type>
      <name>values1</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>ac1ead77c7a34a2144fdd9a7253625928</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignVector0</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>af33d6211b0d8ef888b126efeb38768cf</anchor>
      <arglist>(int size, T *&amp;vec)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>assignVector1</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a43eeab72eb043eecaa271872d5b4cd3b</anchor>
      <arglist>(int size, U *&amp;vec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorPair</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a99ea8ea7ba8120914a2eaea74763c3ee</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorPair</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>aebedbb555e9ac50086c268cab6ae2a72</anchor>
      <arglist>(int s0, const T *v0, int s1, const U *v1)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorPair</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>aaed2475aa9fccce0a78ccef2e7d11572</anchor>
      <arglist>(const CoinWarmStartVectorPair&lt; T, U &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>CoinWarmStartVectorPair &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a3f91f7362dbec14e46ae8bb444a16c93</anchor>
      <arglist>(const CoinWarmStartVectorPair&lt; T, U &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a0a50047617b46b42cc6b9c4e5ee224a0</anchor>
      <arglist>(CoinWarmStartVectorPair&lt; T, U &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStart *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a8250afb4b54d3a9ea09c4e3af032035b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartVectorPair</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>ae81257838c6a6e738fb258e31c84b70d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a211134e97ebd7ce97829118e2c3c81af</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>generateDiff</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a3c5e23f13933ddefb6230ba041fb7054</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>applyDiff</name>
      <anchorfile>classCoinWarmStartVectorPair.html</anchorfile>
      <anchor>a14f6dca3c926e160c893f761c2587954</anchor>
      <arglist>(const CoinWarmStartDiff *const cwsdDiff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinWarmStartVectorPairDiff</name>
    <filename>classCoinWarmStartVectorPairDiff.html</filename>
    <templarg>T</templarg>
    <templarg>U</templarg>
    <base virtualness="virtual">CoinWarmStartDiff</base>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorPairDiff</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>af61b1e7ca7b628487c72afaac40d7ac0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CoinWarmStartVectorPairDiff</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>aa9ad73fa9fa3b455845151436e739a17</anchor>
      <arglist>(const CoinWarmStartVectorPairDiff&lt; T, U &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CoinWarmStartVectorPairDiff</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>a2187919fd3e93e76a1c30327b1aa8195</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartVectorPairDiff &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>ab8d89f3b3c007da06bbef619fa33efc0</anchor>
      <arglist>(const CoinWarmStartVectorPairDiff&lt; T, U &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual CoinWarmStartDiff *</type>
      <name>clone</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>afab34c8b81111e0f549fe5f833724ec2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>swap</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>afe361cae71df11c6ae47424e80c15036</anchor>
      <arglist>(CoinWarmStartVectorPairDiff&lt; T, U &gt; &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>ab3fedcbab9fea145a4c1d91fba0f6e58</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend CoinWarmStartDiff *</type>
      <name>CoinWarmStartVectorPair</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>a850b85ea0c76611ce71445eaf64a8318</anchor>
      <arglist>(const CoinWarmStart *const oldCWS) const </arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>CoinWarmStartVectorPair</name>
      <anchorfile>classCoinWarmStartVectorPairDiff.html</anchorfile>
      <anchor>a68f06863042ab2bf85c5124df4c324cb</anchor>
      <arglist>(const CoinWarmStartDiff *const diff)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CoinYacc</name>
    <filename>classCoinYacc.html</filename>
    <member kind="function">
      <type></type>
      <name>CoinYacc</name>
      <anchorfile>classCoinYacc.html</anchorfile>
      <anchor>af78227ee3ef8436933d4d31dd89804ab</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CoinYacc</name>
      <anchorfile>classCoinYacc.html</anchorfile>
      <anchor>afae7d411feb5af815ade82abbd050eb0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>symrec *</type>
      <name>symtable</name>
      <anchorfile>classCoinYacc.html</anchorfile>
      <anchor>a287d1588bd142abceeab4cb656a7319b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>symbuf</name>
      <anchorfile>classCoinYacc.html</anchorfile>
      <anchor>a58d6cabbe62117d4c972ad0c8996d74c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>length</name>
      <anchorfile>classCoinYacc.html</anchorfile>
      <anchor>a1cef6eea9e301a55452ed84c5c383b8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>unsetValue</name>
      <anchorfile>classCoinYacc.html</anchorfile>
      <anchor>af152994408d7a5a72b47620f18c215c3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>do_tighten_action</name>
    <filename>classdo__tighten__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classdo__tighten__action.html</anchorfile>
      <anchor>acce2a5dcbf86872f823327c3ef4dc2e6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classdo__tighten__action.html</anchorfile>
      <anchor>a9a816b0932b2c7657592e9ac8abee341</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~do_tighten_action</name>
      <anchorfile>classdo__tighten__action.html</anchorfile>
      <anchor>ab3805fb1d8164299fd28fe81c571961c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdo__tighten__action.html</anchorfile>
      <anchor>aec788733332be94e3ab00eb9298800e4</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>doubleton_action</name>
    <filename>classdoubleton__action.html</filename>
    <base>CoinPresolveAction</base>
    <class kind="struct">doubleton_action::action</class>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classdoubleton__action.html</anchorfile>
      <anchor>a8822ceeec5c8d450ca9cbc2bed6b32e0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classdoubleton__action.html</anchorfile>
      <anchor>a4fd4a77befc773b5894ed4f42eded046</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~doubleton_action</name>
      <anchorfile>classdoubleton__action.html</anchorfile>
      <anchor>aa3cf864b6ed954afce0f82989a3fa0f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdoubleton__action.html</anchorfile>
      <anchor>a3b58817cf014842f1113bf2d44c67f40</anchor>
      <arglist>(CoinPresolveMatrix *, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="variable">
      <type>const int</type>
      <name>nactions_</name>
      <anchorfile>classdoubleton__action.html</anchorfile>
      <anchor>ad84d9cae72461469d3acc30ecc2c1f7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const action *const</type>
      <name>actions_</name>
      <anchorfile>classdoubleton__action.html</anchorfile>
      <anchor>abc467d39f36ad18e3548f7e241293ebe</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>doubleton_action::action</name>
    <filename>structdoubleton__action_1_1action.html</filename>
    <member kind="variable">
      <type>double</type>
      <name>clox</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>a749064cc4b20ff207b43698febb833f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>cupx</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>a89280ee88c202561c11d289b92631cb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>costx</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>ad6e47ae9c6f2cf6e0d64bdc3a2a72dda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>costy</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>ad135c2da5f2dd80482cbf625cc1bd45d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>rlo</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>a6ab937755433c0e342caa493985edd0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>coeffx</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>aaf930d9ca9d9c7ff0111842c8303fedc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>coeffy</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>ae12c72b85bb5c26e6dcda0271b3ae73f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>colel</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>ab25e0ea3309eb0083802ce6ecc400199</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>icolx</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>a3eb17140d2a0bca1e1b6278fd35ac360</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>icoly</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>aa2e6ac1398c5c606d23593e44e271cf3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>row</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>a28e0740d74201b5b5b2ca5cd91e94206</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncolx</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>ac8b6d782d043f39ae9209ab39f259d73</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncoly</name>
      <anchorfile>structdoubleton__action_1_1action.html</anchorfile>
      <anchor>abc105591d51e0ccbec7bdacb74debaa7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>drop_empty_cols_action</name>
    <filename>classdrop__empty__cols__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classdrop__empty__cols__action.html</anchorfile>
      <anchor>a4b353603e9f0cb788533bbc9c6283c4b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classdrop__empty__cols__action.html</anchorfile>
      <anchor>af67a4d8d50319ede6c71e6787938e54f</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~drop_empty_cols_action</name>
      <anchorfile>classdrop__empty__cols__action.html</anchorfile>
      <anchor>af476f26388def9e653b949291805662d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdrop__empty__cols__action.html</anchorfile>
      <anchor>a9c1abd100a499dcb96db633bdae457d9</anchor>
      <arglist>(CoinPresolveMatrix *, const int *ecols, int necols, const CoinPresolveAction *)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdrop__empty__cols__action.html</anchorfile>
      <anchor>a61590ba6eaa9d789c6cdfbbdc913bf2e</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>drop_empty_rows_action</name>
    <filename>classdrop__empty__rows__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classdrop__empty__rows__action.html</anchorfile>
      <anchor>a532876d0173b2c867eb2f6799b67fe67</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classdrop__empty__rows__action.html</anchorfile>
      <anchor>ad7db919861939b555088ed0393afb217</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~drop_empty_rows_action</name>
      <anchorfile>classdrop__empty__rows__action.html</anchorfile>
      <anchor>a494afaaf301456080c36964b96a1e18f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdrop__empty__rows__action.html</anchorfile>
      <anchor>ad3794690928107bd0739b5c4eb85e6d4</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>drop_zero_coefficients_action</name>
    <filename>classdrop__zero__coefficients__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classdrop__zero__coefficients__action.html</anchorfile>
      <anchor>a77398cd56f948750fe6213d2e14518a5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classdrop__zero__coefficients__action.html</anchorfile>
      <anchor>ae90208166b32e759beb9245ca5d1fac1</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~drop_zero_coefficients_action</name>
      <anchorfile>classdrop__zero__coefficients__action.html</anchorfile>
      <anchor>a22e92be73a65ff6659dcffce26e2aa69</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdrop__zero__coefficients__action.html</anchorfile>
      <anchor>a904d5fbbcfcb28e0511f4ec9542571ff</anchor>
      <arglist>(CoinPresolveMatrix *prob, int *checkcols, int ncheckcols, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>dropped_zero</name>
    <filename>structdropped__zero.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>row</name>
      <anchorfile>structdropped__zero.html</anchorfile>
      <anchor>ab9c4181b667c8b962b8d71aec39d0865</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>col</name>
      <anchorfile>structdropped__zero.html</anchorfile>
      <anchor>acdec790e0e7364235b6dc66284835a54</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>dupcol_action</name>
    <filename>classdupcol__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classdupcol__action.html</anchorfile>
      <anchor>a72aa1aae87260fc0908116775302f3bb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classdupcol__action.html</anchorfile>
      <anchor>ab9f852bc6f230af8813496e2cc7d4c4f</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~dupcol_action</name>
      <anchorfile>classdupcol__action.html</anchorfile>
      <anchor>afa9b9fb4bd246f5e7bcc35f67cc89433</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classdupcol__action.html</anchorfile>
      <anchor>ac5703bd7d81e358b5c920a5f023ede9e</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>duprow3_action</name>
    <filename>classduprow3__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classduprow3__action.html</anchorfile>
      <anchor>a9e5f38c53b6e8c53614dd55626292e0a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classduprow3__action.html</anchorfile>
      <anchor>a959c0b8cad54ee32e7780735d59ae87e</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classduprow3__action.html</anchorfile>
      <anchor>a73c11fc57c1191575261914b3a0c6b92</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>duprow_action</name>
    <filename>classduprow__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classduprow__action.html</anchorfile>
      <anchor>aa132254972522e156ca10d9c1b02960d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classduprow__action.html</anchorfile>
      <anchor>aac01dcb7ea5190792172707de476b96a</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classduprow__action.html</anchorfile>
      <anchor>a84a2f59b0fa3b2fdc44ae7f36083d40d</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>EKKHlink</name>
    <filename>structEKKHlink.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>suc</name>
      <anchorfile>structEKKHlink.html</anchorfile>
      <anchor>a74bb6f042a4569eb782ec7f87b3f8890</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>pre</name>
      <anchorfile>structEKKHlink.html</anchorfile>
      <anchor>a2c4ecaaf957a95806ca33c11f440c85b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>FactorPointers</name>
    <filename>classFactorPointers.html</filename>
    <member kind="function">
      <type></type>
      <name>FactorPointers</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a7e087dca1d0f52e4bbb7db889f79b7b7</anchor>
      <arglist>(int numRows, int numCols, int *UrowLengths_, int *UcolLengths_)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~FactorPointers</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a6fe5f472fc8e63066b50a8ca776ffe68</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>rowMax</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a1f779bc65934313a9cfc8a05440040c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>firstRowKnonzeros</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a234fca7f081452e60416111582ea6cd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>prevRow</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a933943093b03fa505bc36aa1058057b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>nextRow</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a119e7a554eb166e7ca5ba53d47f654f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>firstColKnonzeros</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a0a06bd05209344d552d99e6458a5bbcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>prevColumn</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>ac3710b37763cdd6a98f46a27f7f18b85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>nextColumn</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a71b43d2599b15111b48039c8ba299bd0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>newCols</name>
      <anchorfile>classFactorPointers.html</anchorfile>
      <anchor>a1b68fc6e402ee2067dcb2469e1734d21</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>forcing_constraint_action</name>
    <filename>classforcing__constraint__action.html</filename>
    <base>CoinPresolveAction</base>
    <class kind="struct">forcing_constraint_action::action</class>
    <member kind="function">
      <type></type>
      <name>forcing_constraint_action</name>
      <anchorfile>classforcing__constraint__action.html</anchorfile>
      <anchor>a911ccf266ede0ef9d1f0a0dddb509b92</anchor>
      <arglist>(int nactions, const action *actions, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classforcing__constraint__action.html</anchorfile>
      <anchor>af201cfb2ba6205a11c43922ca8efa27f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classforcing__constraint__action.html</anchorfile>
      <anchor>a158d6b4bd886e96a458d2bb356a61bbf</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~forcing_constraint_action</name>
      <anchorfile>classforcing__constraint__action.html</anchorfile>
      <anchor>a742c40c4dd7a645a46464670505a102b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classforcing__constraint__action.html</anchorfile>
      <anchor>ab0e795367a9baec6af70641e1ee084d9</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>forcing_constraint_action::action</name>
    <filename>structforcing__constraint__action_1_1action.html</filename>
    <member kind="variable">
      <type>const int *</type>
      <name>rowcols</name>
      <anchorfile>structforcing__constraint__action_1_1action.html</anchorfile>
      <anchor>add03bf325638f4fb18c443e6dce3c926</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const double *</type>
      <name>bounds</name>
      <anchorfile>structforcing__constraint__action_1_1action.html</anchorfile>
      <anchor>a12c215fdb6a470b9ffd328eff7c162e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>row</name>
      <anchorfile>structforcing__constraint__action_1_1action.html</anchorfile>
      <anchor>ad963d0026bcc450a44264892321ac9c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nlo</name>
      <anchorfile>structforcing__constraint__action_1_1action.html</anchorfile>
      <anchor>a3c08071fb3085c36e1827fa594af81e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nup</name>
      <anchorfile>structforcing__constraint__action_1_1action.html</anchorfile>
      <anchor>a30a05ed45ea7f5856e1d7df6d0b3ffc4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>gubrow_action</name>
    <filename>classgubrow__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classgubrow__action.html</anchorfile>
      <anchor>a2c495eeaec205c194476c3b1e1c1b450</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classgubrow__action.html</anchorfile>
      <anchor>a3b056cac20b779e2ba20f786303c30b0</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~gubrow_action</name>
      <anchorfile>classgubrow__action.html</anchorfile>
      <anchor>a62ccef13f6988180848229fa0bbf6668</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classgubrow__action.html</anchorfile>
      <anchor>a6c0ecf81a210553cf5f1c05f0caf0868</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>implied_free_action</name>
    <filename>classimplied__free__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classimplied__free__action.html</anchorfile>
      <anchor>a5b6f3e7863512f13d6b2daf044f0338a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classimplied__free__action.html</anchorfile>
      <anchor>a412b7fda979a81f1e6f0b0648a445d58</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~implied_free_action</name>
      <anchorfile>classimplied__free__action.html</anchorfile>
      <anchor>a61cc8c4f5bd735d3e44d3c8170f07d4b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classimplied__free__action.html</anchorfile>
      <anchor>abd98983d585b06fefc2ddf849bc8050e</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next, int &amp;fillLevel)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>isolated_constraint_action</name>
    <filename>classisolated__constraint__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classisolated__constraint__action.html</anchorfile>
      <anchor>a9e33a9404237295fbf028edc8efe817c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classisolated__constraint__action.html</anchorfile>
      <anchor>a7746f11b1f3cb91378a3c5bf0a97f72b</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~isolated_constraint_action</name>
      <anchorfile>classisolated__constraint__action.html</anchorfile>
      <anchor>a170394ade12ee7f0717be8588f1d9608</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classisolated__constraint__action.html</anchorfile>
      <anchor>ab3e9bbf099db2cddd728715104ea4a36</anchor>
      <arglist>(CoinPresolveMatrix *prob, int row, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>make_fixed_action</name>
    <filename>classmake__fixed__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classmake__fixed__action.html</anchorfile>
      <anchor>a428b07d1a5245da65703aafa7f5d89eb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classmake__fixed__action.html</anchorfile>
      <anchor>a4b4763c7748da261d23959cb92de4b8b</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~make_fixed_action</name>
      <anchorfile>classmake__fixed__action.html</anchorfile>
      <anchor>a617f52acc16292b470b7c1cb42f8af1b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classmake__fixed__action.html</anchorfile>
      <anchor>a3c0dc3c28404754cc6f3be5ada2baf91</anchor>
      <arglist>(CoinPresolveMatrix *prob, int *fcols, int nfcols, bool fix_to_lower, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="function">
      <type>const CoinPresolveAction *</type>
      <name>make_fixed</name>
      <anchorfile>classmake__fixed__action.html</anchorfile>
      <anchor>ac6f1f904c819f18dc80e4490b3d6b766</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>transferCosts</name>
      <anchorfile>classmake__fixed__action.html</anchorfile>
      <anchor>a398b99f631df8ef3ae3281a861bdb957</anchor>
      <arglist>(CoinPresolveMatrix *prob)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>presolvehlink</name>
    <filename>classpresolvehlink.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>pre</name>
      <anchorfile>classpresolvehlink.html</anchorfile>
      <anchor>ae8da9718768d0a4d84dc377b528da122</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>suc</name>
      <anchorfile>classpresolvehlink.html</anchorfile>
      <anchor>aeb3bfa52f3fdbab21556bf1030eb8657</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PRESOLVE_REMOVE_LINK</name>
      <anchorfile>classpresolvehlink.html</anchorfile>
      <anchor>a684859187a4ec7fc4ac037b371b06eeb</anchor>
      <arglist>(presolvehlink *link, int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PRESOLVE_INSERT_LINK</name>
      <anchorfile>classpresolvehlink.html</anchorfile>
      <anchor>a679608d3f2fb803fe2818855ad82d466</anchor>
      <arglist>(presolvehlink *link, int i, int j)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PRESOLVE_MOVE_LINK</name>
      <anchorfile>classpresolvehlink.html</anchorfile>
      <anchor>a3878effe2f08d1b62cd7b7a3d41ad2af</anchor>
      <arglist>(presolvehlink *link, int i, int j)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>remove_dual_action</name>
    <filename>classremove__dual__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type></type>
      <name>~remove_dual_action</name>
      <anchorfile>classremove__dual__action.html</anchorfile>
      <anchor>ac832fe048277bcf436bbdc6d496bd8d8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classremove__dual__action.html</anchorfile>
      <anchor>ac4e2f8f83166c481e2764e8d3621d870</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classremove__dual__action.html</anchorfile>
      <anchor>abe3b6c8060fd44f635c626396c14b07b</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classremove__dual__action.html</anchorfile>
      <anchor>a98647101136cbc1d6363b7fa7f0b9258</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>remove_fixed_action</name>
    <filename>classremove__fixed__action.html</filename>
    <base>CoinPresolveAction</base>
    <class kind="struct">remove_fixed_action::action</class>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>a90af2968078197f706201e2bb61ada9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>ad34689395e9bd18f7b795f26e607aa7a</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~remove_fixed_action</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>aceb3b2ba1bf6319364691d7e135f54c4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const remove_fixed_action *</type>
      <name>presolve</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>adbdfa63eaf6a3364a62047affe78e106</anchor>
      <arglist>(CoinPresolveMatrix *prob, int *fcols, int nfcols, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>colrows_</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>a2791731ceb875db9ad270656adde87c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>colels_</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>af6668d20c236cfeb70dd0db53fe032b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nactions_</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>af2e8942479bf06c1bf5dc0832d98158b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>action *</type>
      <name>actions_</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>ab419ceccd70ebbbaa31b674d1c5842bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>const CoinPresolveAction *</type>
      <name>remove_fixed</name>
      <anchorfile>classremove__fixed__action.html</anchorfile>
      <anchor>a90d4335644aafd2d5f4bb56f10d65f29</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>remove_fixed_action::action</name>
    <filename>structremove__fixed__action_1_1action.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>col</name>
      <anchorfile>structremove__fixed__action_1_1action.html</anchorfile>
      <anchor>a71400284e0c03af8856ac9651851b969</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>start</name>
      <anchorfile>structremove__fixed__action_1_1action.html</anchorfile>
      <anchor>ae45f55a1c6be7c84ca0e82acc3f332be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>sol</name>
      <anchorfile>structremove__fixed__action_1_1action.html</anchorfile>
      <anchor>a5c210ccabed3741c3e90414e0ee9ebbc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>slack_doubleton_action</name>
    <filename>classslack__doubleton__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classslack__doubleton__action.html</anchorfile>
      <anchor>a13420f970a37fec5a146af189b56a3db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classslack__doubleton__action.html</anchorfile>
      <anchor>aeaf8623924b9717943041aa8a38dc83d</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~slack_doubleton_action</name>
      <anchorfile>classslack__doubleton__action.html</anchorfile>
      <anchor>aea8e23757d734166e4376471e7f608f5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classslack__doubleton__action.html</anchorfile>
      <anchor>a5d6b88ca30155e937f714152d64a6baf</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next, bool &amp;notFinished)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>slack_singleton_action</name>
    <filename>classslack__singleton__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classslack__singleton__action.html</anchorfile>
      <anchor>ae9f29a8d4e085e004c27ed31ac37e756</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classslack__singleton__action.html</anchorfile>
      <anchor>ae1a7c198258e596127658c6ae47e067a</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~slack_singleton_action</name>
      <anchorfile>classslack__singleton__action.html</anchorfile>
      <anchor>a72fbf138434a5875c1b582169cadce04</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classslack__singleton__action.html</anchorfile>
      <anchor>a196ce23e2bf38042a00d8d7ebc6cdb82</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next, double *rowObjective)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>subst_constraint_action</name>
    <filename>classsubst__constraint__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classsubst__constraint__action.html</anchorfile>
      <anchor>a4fed47311ccd46536232671a3502b7bb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classsubst__constraint__action.html</anchorfile>
      <anchor>ac75e991f75081fca54241278eb1b65b3</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~subst_constraint_action</name>
      <anchorfile>classsubst__constraint__action.html</anchorfile>
      <anchor>ac25dde81a9b2a8dc0618cb1a82952cec</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classsubst__constraint__action.html</anchorfile>
      <anchor>ab0d77ef2491d13fde335588606e662b7</anchor>
      <arglist>(CoinPresolveMatrix *prob, const int *implied_free, const int *which, int numberFree, const CoinPresolveAction *next, int fill_level)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolveX</name>
      <anchorfile>classsubst__constraint__action.html</anchorfile>
      <anchor>a9cf84a60287a0ddca3d9a9abbf277dc9</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next, int fillLevel)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>symrec</name>
    <filename>structsymrec.html</filename>
    <member kind="variable">
      <type>char *</type>
      <name>name</name>
      <anchorfile>structsymrec.html</anchorfile>
      <anchor>a13b3ddedd56ed8a393dfd4b266bb81e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>type</name>
      <anchorfile>structsymrec.html</anchorfile>
      <anchor>a3ed0bae32ad0e16423a49153484094f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>union symrec::@0</type>
      <name>value</name>
      <anchorfile>structsymrec.html</anchorfile>
      <anchor>aabe0b508928c6472fcbdf5071c33b61a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct symrec *</type>
      <name>next</name>
      <anchorfile>structsymrec.html</anchorfile>
      <anchor>a799aa9b5b3ee76aa0634b0ad96f80ea1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>tripleton_action</name>
    <filename>classtripleton__action.html</filename>
    <base>CoinPresolveAction</base>
    <class kind="struct">tripleton_action::action</class>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classtripleton__action.html</anchorfile>
      <anchor>adabb758aa5ca42af374897bb83be340a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classtripleton__action.html</anchorfile>
      <anchor>abbcc6c7e8bbe5ac17f4dd3dd9c2dad06</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~tripleton_action</name>
      <anchorfile>classtripleton__action.html</anchorfile>
      <anchor>ad55f87ba58c99b5c1ba3bbe4c41e7ae0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classtripleton__action.html</anchorfile>
      <anchor>a901d547e58946049bafd4e68ec921653</anchor>
      <arglist>(CoinPresolveMatrix *, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="variable">
      <type>const int</type>
      <name>nactions_</name>
      <anchorfile>classtripleton__action.html</anchorfile>
      <anchor>a5aa8bb7178b347e4c3878d8ff1bde3bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const action *const</type>
      <name>actions_</name>
      <anchorfile>classtripleton__action.html</anchorfile>
      <anchor>ae89b2ea6002f7597b0e8402828a46e51</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>tripleton_action::action</name>
    <filename>structtripleton__action_1_1action.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>icolx</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a836a0dfa5a1d5e2d0b9c49d41414238e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>icolz</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>ab0342158dd1b51c1aa8b80bf1e115f45</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>row</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>acde11d46675e95a4424200c377e2fb2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>icoly</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a28744a038ef6b1efa3d4fbd4118ffc03</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>cloy</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>ac9fe95bd8ed26d5b0456387de6b2ce27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>cupy</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a18f2034ad0fa66070bb3cc514fc708e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>costy</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>adc28cf31056a3070cb0656a363c56edf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>clox</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>ad7c00005d0696c0cb8fd1dec79562d6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>cupx</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a402b1ef834c2dc5bd339f183e2ce2a34</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>costx</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a750beadb03a164c3ae3812cac4365dbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>rlo</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a613ac6f167ce5135b5a883328c8b4133</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>rup</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>adc709650930b20035907799563606c6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>coeffx</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a456789bf4326e6de389b8e57206ef08f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>coeffy</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a4b4d9ec0a3501cd37bebfa7a7dbd4361</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>coeffz</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>adf94a88378c06f38b6519a43cfc02364</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double *</type>
      <name>colel</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a895ac5336255a089f7af8fd42365caf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncolx</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a4d011a2a1dd15f733d49d738aa1c201d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncoly</name>
      <anchorfile>structtripleton__action_1_1action.html</anchorfile>
      <anchor>a4a68ef1848ddba28c8871ebecc5f33e5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>twoxtwo_action</name>
    <filename>classtwoxtwo__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classtwoxtwo__action.html</anchorfile>
      <anchor>ab88e43aa5a90afd57b3d02c47aafd5ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classtwoxtwo__action.html</anchorfile>
      <anchor>a77866303b2caa4056d1223164dd37aa7</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~twoxtwo_action</name>
      <anchorfile>classtwoxtwo__action.html</anchorfile>
      <anchor>a50e88e4c4c93905f07659abcd14e0d1d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classtwoxtwo__action.html</anchorfile>
      <anchor>ae27f12915a7522788c16b25bf6cec33b</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>useless_constraint_action</name>
    <filename>classuseless__constraint__action.html</filename>
    <base>CoinPresolveAction</base>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classuseless__constraint__action.html</anchorfile>
      <anchor>a1e4ab35bc3066a6ddb895ea5a52347e2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>postsolve</name>
      <anchorfile>classuseless__constraint__action.html</anchorfile>
      <anchor>a1ada69bf17110603addd35397d5e6f1a</anchor>
      <arglist>(CoinPostsolveMatrix *prob) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~useless_constraint_action</name>
      <anchorfile>classuseless__constraint__action.html</anchorfile>
      <anchor>a714cb17b7ee96ffeebf91730102aeb9a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const CoinPresolveAction *</type>
      <name>presolve</name>
      <anchorfile>classuseless__constraint__action.html</anchorfile>
      <anchor>a9da07ecf6a7eea181c5654e9b4bd22fd</anchor>
      <arglist>(CoinPresolveMatrix *prob, const int *useless_rows, int nuseless_rows, const CoinPresolveAction *next)</arglist>
    </member>
    <member kind="function">
      <type>const CoinPresolveAction *</type>
      <name>testRedundant</name>
      <anchorfile>classuseless__constraint__action.html</anchorfile>
      <anchor>aad1b37ae994255e104d0ec94a0d9baa8</anchor>
      <arglist>(CoinPresolveMatrix *prob, const CoinPresolveAction *next)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>Coin</name>
    <filename>namespaceCoin.html</filename>
    <class kind="class">Coin::ReferencedObject</class>
    <class kind="class">Coin::SmartPtr</class>
    <member kind="function">
      <type>bool</type>
      <name>ComparePointers</name>
      <anchorfile>namespaceCoin.html</anchorfile>
      <anchor>a0dcec666fa32b8155aa349c90c8403ad</anchor>
      <arglist>(const U1 *lhs, const U2 *rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Coin::ReferencedObject</name>
    <filename>classCoin_1_1ReferencedObject.html</filename>
    <member kind="function">
      <type></type>
      <name>ReferencedObject</name>
      <anchorfile>classCoin_1_1ReferencedObject.html</anchorfile>
      <anchor>a33f8fa7df67d7f03d1e381fc0f3cdf29</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ReferencedObject</name>
      <anchorfile>classCoin_1_1ReferencedObject.html</anchorfile>
      <anchor>a5e4ce3e01905b3cf1d14978edaed8a6e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>ReferenceCount</name>
      <anchorfile>classCoin_1_1ReferencedObject.html</anchorfile>
      <anchor>a1f547d3096e09e254eb2b1526f6aab32</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddRef</name>
      <anchorfile>classCoin_1_1ReferencedObject.html</anchorfile>
      <anchor>afff3730615f57fc4ac3384bb926c8c33</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>ReleaseRef</name>
      <anchorfile>classCoin_1_1ReferencedObject.html</anchorfile>
      <anchor>abbdf98719bc4f3de55a9e95b6fcd9f0f</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Coin::SmartPtr</name>
    <filename>classCoin_1_1SmartPtr.html</filename>
    <templarg>T</templarg>
    <member kind="function">
      <type>T *</type>
      <name>GetRawPtr</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>abfa998032bf837a8d73e426eb812c666</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>IsValid</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a933bd5dd6594fee8e4a09b171aa3d65f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>IsNull</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a7b98ed9f84f193a9fca08231248d8257</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SmartPtr</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>aa8830dc0c8542c5f3dcaca9946c5ec8d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SmartPtr</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>ab5d03014ee3c49dca6220f28bb942299</anchor>
      <arglist>(const SmartPtr&lt; T &gt; &amp;copy)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SmartPtr</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>aa3443dfddcc2c2bc19417017dc30f603</anchor>
      <arglist>(T *ptr)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~SmartPtr</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a033f6fc82a8440ca7940a1982e26faef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>operator-&gt;</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>aea99642086b104d60be06e331eaecab9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>T &amp;</type>
      <name>operator*</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>af05fd0caf36898d59575f7791ce4de05</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SmartPtr&lt; T &gt; &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>ad0fff64cafc7ef016d132179bcf16dd0</anchor>
      <arglist>(T *rhs)</arglist>
    </member>
    <member kind="function">
      <type>SmartPtr&lt; T &gt; &amp;</type>
      <name>operator=</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a858cd5bc123c2a4bfdd379e987048f0c</anchor>
      <arglist>(const SmartPtr&lt; T &gt; &amp;rhs)</arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator==</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a5c0a37eb0bcf3b054d0a32e1270bc705</anchor>
      <arglist>(const SmartPtr&lt; U1 &gt; &amp;lhs, const SmartPtr&lt; U2 &gt; &amp;rhs)</arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator==</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a6ae6c0fa7e1d082d4ac70eb13ef1a42d</anchor>
      <arglist>(const SmartPtr&lt; U1 &gt; &amp;lhs, U2 *raw_rhs)</arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator==</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a606d88811c3808ee456098c476f9a351</anchor>
      <arglist>(U1 *lhs, const SmartPtr&lt; U2 &gt; &amp;raw_rhs)</arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator!=</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a5830da9a8edcea70c214a3cfd0d5be93</anchor>
      <arglist>(const SmartPtr&lt; U1 &gt; &amp;lhs, const SmartPtr&lt; U2 &gt; &amp;rhs)</arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator!=</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a184d462875e8cd414a1782b0b84189e3</anchor>
      <arglist>(const SmartPtr&lt; U1 &gt; &amp;lhs, U2 *raw_rhs)</arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator!=</name>
      <anchorfile>classCoin_1_1SmartPtr.html</anchorfile>
      <anchor>a30fa32ae58fc32b425fc1c107621f909</anchor>
      <arglist>(U1 *lhs, const SmartPtr&lt; U2 &gt; &amp;raw_rhs)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>CoinParamUtils</name>
    <filename>namespaceCoinParamUtils.html</filename>
    <member kind="function">
      <type>void</type>
      <name>setInputSrc</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a437ee486e6f58cad5ea13f3cb3532e3a</anchor>
      <arglist>(FILE *src)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isCommandLine</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a0a2e247f2d799edefef0eed6cea7b087</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInteractive</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a93925af7f6c75fe717625c8b37385a4b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getStringField</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a0370af16338883f2f1289c1c0cee4fa6</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getIntField</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>ab1cc490cbb2657785903be303c0a2bf0</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getDoubleField</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>aac3f4f30307501ae40fab327010550bb</anchor>
      <arglist>(int argc, const char *argv[], int *valid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>matchParam</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a13ff379969e4df15a0d427cd48be3d25</anchor>
      <arglist>(const CoinParamVec &amp;paramVec, std::string name, int &amp;matchNdx, int &amp;shortCnt)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getCommand</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a105f78e2e32385a41ec2f6390944ec1e</anchor>
      <arglist>(int argc, const char *argv[], const std::string prompt, std::string *pfx=0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>lookupParam</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>ac67d48449be5c2455c4ba4f463c3ebae</anchor>
      <arglist>(std::string name, CoinParamVec &amp;paramVec, int *matchCnt=0, int *shortCnt=0, int *queryCnt=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printIt</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>adf3cb1860afdffcd34d691fea63c2ec6</anchor>
      <arglist>(const char *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shortOrHelpOne</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a6e7f4833f924c1ac5dbbf9be51c96e11</anchor>
      <arglist>(CoinParamVec &amp;paramVec, int matchNdx, std::string name, int numQuery)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shortOrHelpMany</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a817130d7012b494c823fc6e9689eb205</anchor>
      <arglist>(CoinParamVec &amp;paramVec, std::string name, int numQuery)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printGenericHelp</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>a77c2516ed11f7154b5db5d0a2dd78a63</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printHelp</name>
      <anchorfile>namespaceCoinParamUtils.html</anchorfile>
      <anchor>aced5b09e341f2c76bf00207eb05c631f</anchor>
      <arglist>(CoinParamVec &amp;paramVec, int firstParam, int lastParam, std::string prefix, bool shortHelp, bool longHelp, bool hidden)</arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src</name>
    <path>/home/tsokalo/workspace/tmaTool/coin-Clp/CoinUtils/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>Coin_C_defines.h</file>
    <file>CoinAlloc.hpp</file>
    <file>CoinBuild.hpp</file>
    <file>CoinDenseFactorization.hpp</file>
    <file>CoinDenseVector.hpp</file>
    <file>CoinDistance.hpp</file>
    <file>CoinError.hpp</file>
    <file>CoinFactorization.hpp</file>
    <file>CoinFileIO.hpp</file>
    <file>CoinFinite.hpp</file>
    <file>CoinFloatEqual.hpp</file>
    <file>CoinHelperFunctions.hpp</file>
    <file>CoinIndexedVector.hpp</file>
    <file>CoinLpIO.hpp</file>
    <file>CoinMessage.hpp</file>
    <file>CoinMessageHandler.hpp</file>
    <file>CoinModel.hpp</file>
    <file>CoinModelUseful.hpp</file>
    <file>CoinMpsIO.hpp</file>
    <file>CoinOslC.h</file>
    <file>CoinOslFactorization.hpp</file>
    <file>CoinPackedMatrix.hpp</file>
    <file>CoinPackedVector.hpp</file>
    <file>CoinPackedVectorBase.hpp</file>
    <file>CoinParam.hpp</file>
    <file>CoinPragma.hpp</file>
    <file>CoinPresolveDoubleton.hpp</file>
    <file>CoinPresolveDual.hpp</file>
    <file>CoinPresolveDupcol.hpp</file>
    <file>CoinPresolveEmpty.hpp</file>
    <file>CoinPresolveFixed.hpp</file>
    <file>CoinPresolveForcing.hpp</file>
    <file>CoinPresolveImpliedFree.hpp</file>
    <file>CoinPresolveIsolated.hpp</file>
    <file>CoinPresolveMatrix.hpp</file>
    <file>CoinPresolveMonitor.hpp</file>
    <file>CoinPresolvePsdebug.hpp</file>
    <file>CoinPresolveSingleton.hpp</file>
    <file>CoinPresolveSubst.hpp</file>
    <file>CoinPresolveTighten.hpp</file>
    <file>CoinPresolveTripleton.hpp</file>
    <file>CoinPresolveUseless.hpp</file>
    <file>CoinPresolveZeros.hpp</file>
    <file>CoinRational.hpp</file>
    <file>CoinSearchTree.hpp</file>
    <file>CoinShallowPackedVector.hpp</file>
    <file>CoinSignal.hpp</file>
    <file>CoinSimpFactorization.hpp</file>
    <file>CoinSmartPtr.hpp</file>
    <file>CoinSnapshot.hpp</file>
    <file>CoinSort.hpp</file>
    <file>CoinStructuredModel.hpp</file>
    <file>CoinTime.hpp</file>
    <file>CoinTypes.hpp</file>
    <file>CoinUtility.hpp</file>
    <file>CoinUtilsConfig.h</file>
    <file>CoinWarmStart.hpp</file>
    <file>CoinWarmStartBasis.hpp</file>
    <file>CoinWarmStartDual.hpp</file>
    <file>CoinWarmStartPrimalDual.hpp</file>
    <file>CoinWarmStartVector.hpp</file>
    <file>config.h</file>
    <file>config_coinutils.h</file>
    <file>config_coinutils_default.h</file>
    <file>config_default.h</file>
  </compound>
</tagfile>
