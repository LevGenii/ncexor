################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../network/comm-net.cpp \
../network/comm-node.cpp 

OBJS += \
./network/comm-net.o \
./network/comm-node.o 

CPP_DEPS += \
./network/comm-net.d \
./network/comm-node.d 


# Each subdirectory must supply rules for building sources it contributes
network/%.o: ../network/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/tsokalo/workspace/SimpleNetworkSimulator/network" -I"/home/tsokalo/workspace/SimpleNetworkSimulator" -I"/home/tsokalo/workspace/SimpleNetworkSimulator/routing-rules" -I"/home/tsokalo/workspace/SimpleNetworkSimulator/utils" -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


