### How do I get set up? ###
(for Ubuntu)

1. Clone this with git
2. Try "./ waf configure" inside the ncexor folder. Follow the instructions to make it working:

a) G++: Install the version 4.8.5 of g++. Then run the following command to switch your g++ Compiler version.
If you want to switch back later, modify the version number at the end and run the command again

```
#!bash
ls -la /usr/bin/ | grep -oP "[\S]*(g\+\+)(-[a-z]+)*[\s]" | xargs bash -c 'for link in ${@:1}; do sudo ln -s -f "/usr/bin/${link}-${0}" "/usr/bin/${link}"; done' 4.8

```

b) BOOST: Install "libboost-all-dev" from the official repositories. Then, modify the path to your boost installation in the wscript in the root of the ncexor folder. 

c) COIN-CLP: Run "./configure" and "make install" from the coin-Clp folder. Then, modify the path to your coin-clp installation in the wscript in the root of the ncexor folder.

d) Clone the repository [kodo-rlnc] from https://github.com/steinwurf/kodo-rlnc. You need to apply for the academic license (http://steinwurf.com/license.html). Then, inside the kodo-rlnc folder run "./waf config", then "./waf build" and finally "./waf install --install_static_libs --install_path="./kodo_build".