/*
 * test.cpp
 *
 *  Created on: 03.10.2016
 *      Author: tsokalo
 */
#include "filter-arithmetics.h"

namespace ncr
{
void TestFilterArithmetics() {

	LossMap lp1;
	lp1.add(true);
	lp1.add(false);
	lp1.add(false);
	lp1.add(true);
	lp1.add(true);
	lp1.add(false);
	lp1.add(true);
	lp1.add(false);
	lp1.add(false);
	lp1.add(false);
	lp1.add(true);
	lp1.add(true);

	LossMap lp2;
	lp2.add(true);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(false);
	lp2.add(true);
	lp2.add(true);
	lp2.add(false);

	FilterArithmetics fa;
	fa.add(lp1);
	fa.add(lp2);

	std::cout << "lp1:\t" << lp1 << std::cout << "lp2:\t" << lp2 << std::cout << "AND:\t" << fa.do_and() << std::cout << "OR:\t" << fa.do_or() << std::cout
			<< "XOR:\t" << fa.do_xor() << std::cout;

}

}
